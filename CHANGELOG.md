# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en-1.0.0/)

## [4.1.8046] - 2022-01-11
* Stored vb code as legacy;
* Updated vb code in preparation of transition to C#:
  * Removed parameterized properties.
  * renamed projects to match the OL namespaces;
  * renamed the forms project to Chooser.

## [4.0.8045] - 2022-01-10
* Uses the new ISR VB Framework

## [4.0.7097] - 2019-06-07
* Splits off forms to the Forms library
* Uses a Single I/O control from the Forms library and
displays using the console form.
* WinForms: New  library,

## [3.1.6667] - 2018-04-03
* 2018 release

## [3.0.6166] - 2016-11-18
* Updated for VS 2015 and to Open Layers 7.8.1.

## [2.3.5346] - 2014-08-21
* Updated to Open Layers 7.5.0.

## [2.3.4904] - 2013-06-05
* Upgrade to open layers 7.3. Handles board disconnect.

## [2.3.4869] - 2013-05-01
* Fixes how the existence of a device is determined.

## [2.3.4805] - 2013-02-26
* Scope: Changes status bar to status strip.

## [2.3.4781] - 2013-02-02
* Update to Open Layers 7.1.

## [2.3.4773] - 2013-01-25  
* Single I/O: Derived from the Tester program.
* Display: Fixes buffer allocation in strip chart mode.
* Display: Initialize update and calculation counts to non zero values. Adds items
to default values. Does not configure when restoring default values.

## [2.3.4772] - 2013-01-24  
* Display: Fixes decimation for strip charting-- works in
scope; not yet in analog input display. Replaces private property value
with controls as necessary.

## [2.3.4765] - 2013-01-17 
* Display: Adds the hidden attribute to all properties that are
not browsable.

## [2.3.4714] - 2012-11-27
* Display: Created from Open Layers library.

## [2.3.4710] - 2012-11-23
* Updated to .NET 4.0.

## [2.2.4232] - 2011-08-08
* Standardize code elements and documentation.

## [2.2.4213] - 2011-07-15
* Simplifies the assembly information.

## [2.2.3482] - 2009-07-14
* Updated to conform to current standard.

## [2.1.2961] - 2008-02-09
* Updated to .NET 3.5.

## [2.0.2614] - 2007-02-27
* Updated to Visual Studio 2005 edition.

## [1.0.2220] - 2006-01-29
* First release based on isr.Drivers.OL.Easy and using
the new DT.Open Layers library.

\(C\) 2005 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - 20[date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
[8070] - 2022-02-04 - Merged branch vb2cs onto main.
```

[4.1.8046]: https://www.bitbucket.org/davidhary/vs.openlayers
