﻿Imports isr.IO.OL.Testers.ExceptionExtensions

''' <summary> Form for viewing the spectrum panels. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/6/2019 </para></remarks>
Public Class SingleInputOutputForm
    Inherits isr.Core.Forma.ConsoleForm

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    Private Property ModelViewBase As isr.Core.Forma.ModelViewBase = Nothing
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.ModelViewBase IsNot Nothing Then Me.ModelViewBase.Dispose() : Me.ModelViewBase = Nothing
        MyBase.Dispose(disposing)
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding single I/O view"
            Me.ModelViewBase = New isr.IO.OL.WinControls.SingleInputOutputView
            Me.AddPropertyNotifyControl($"Single I/O", Me.ModelViewBase, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.Application.Logger)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await selection task enabled. </summary>
    ''' <value> The await selection task enabled. </value>
    Protected Property AwaitSelectionTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.ModelViewBase.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.ModelViewBase IsNot Nothing Then Me.ModelViewBase.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#Region " SINGLETON "

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As SingleInputOutputForm

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SingleInputOutputForm
        SyncLock SingleInputOutputForm.syncLocker
            If Not SingleInputOutputForm.Instantiated Then
                SingleInputOutputForm.instance = New SingleInputOutputForm
            End If
            Return SingleInputOutputForm.instance
        End SyncLock
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Forms.SingleInputOutputForm IsNot Nothing AndAlso Not My.Forms.SingleInputOutputForm.IsDisposed
        End Get
    End Property

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
