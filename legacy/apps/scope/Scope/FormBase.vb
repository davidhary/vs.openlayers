Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Permissions

''' <summary> Form with drop shadow. </summary>
''' <remarks> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 08/13/2007" by="Nicholas Seward" revision="1.0.2781.x"> http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. 
''' David, 08/13/2007, 1.0.2781.x">      Convert from C#. </para></remarks>
Partial Public Class FormBase
    Inherits Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

End Class

