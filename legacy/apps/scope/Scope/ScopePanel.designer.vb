<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
 Partial Class ScopePanel

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _AbortButton As System.Windows.Forms.Button
    Private WithEvents _EnablePlottingCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _StopButton As System.Windows.Forms.Button
    Private WithEvents _StartButton As System.Windows.Forms.Button
    Private WithEvents _ConfigButton As System.Windows.Forms.Button
    Private WithEvents _FrequencyTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChannelBufferSizeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _BuffersCountTextBox As System.Windows.Forms.TextBox
    Private WithEvents _FrequencyTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelBufferSizeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _BuffersCountTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _InitializeButton As System.Windows.Forms.Button
    Private WithEvents _YMax2Label As System.Windows.Forms.Label
    Private WithEvents _YMin2Label As System.Windows.Forms.Label
    Private WithEvents _AutoScaleButton As System.Windows.Forms.Button
    Private WithEvents _ColorGridsButton As System.Windows.Forms.Button
    Private WithEvents _SeparatorPanel2 As System.Windows.Forms.Panel
    Private WithEvents _SeparatorPanel1 As System.Windows.Forms.Panel
    Private WithEvents _ColorAxesButton As System.Windows.Forms.Button
    Private WithEvents _ColorDataButton As System.Windows.Forms.Button
    Private WithEvents _DeleteSignalButton As System.Windows.Forms.Button
    Private WithEvents _SignalListComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _YMaxTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _YMinTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _YMax1Label As System.Windows.Forms.Label
    Private WithEvents _YMin1Label As System.Windows.Forms.Label
    Private WithEvents _XMaxTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _XMinTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _XMaxLabel As System.Windows.Forms.Label
    Private WithEvents _XMinLabel As System.Windows.Forms.Label
    Private WithEvents _YAxisLabel As System.Windows.Forms.Label
    Private WithEvents _XAxisLabel As System.Windows.Forms.Label
    Private WithEvents _BandModeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _MultipleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _SingleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _AddSignalButton As System.Windows.Forms.Button
    Private WithEvents _ColorDialog As System.Windows.Forms.ColorDialog
    Private WithEvents _DeviceNameChooser1 As isr.IO.OL.WinForms.DeviceNameChooser
    Private WithEvents _DeviceNameLabel As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ScopePanel))
        Me._ColorDialog = New System.Windows.Forms.ColorDialog()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._DeleteSignalButton = New System.Windows.Forms.Button()
        Me._AddSignalButton = New System.Windows.Forms.Button()
        Me._InitializeButton = New System.Windows.Forms.Button()
        Me._FrequencyTextBox = New System.Windows.Forms.TextBox()
        Me._ChannelBufferSizeTextBox = New System.Windows.Forms.TextBox()
        Me._FiniteBufferingCheckBox = New System.Windows.Forms.CheckBox()
        Me._SmartBufferingCheckBox = New System.Windows.Forms.CheckBox()
        Me._SampleSizeTextBoxLabel = New System.Windows.Forms.Label()
        Me._SampleSizeTextBox = New System.Windows.Forms.TextBox()
        Me._SignalMemorySizeTextBox = New System.Windows.Forms.TextBox()
        Me._SamplingRateTextBox = New System.Windows.Forms.TextBox()
        Me._SignalBufferSizeTextBox = New System.Windows.Forms.TextBox()
        Me._DisplayRateTextBox = New System.Windows.Forms.TextBox()
        Me._AbortButton = New System.Windows.Forms.Button()
        Me._EnablePlottingCheckBox = New System.Windows.Forms.CheckBox()
        Me._StopButton = New System.Windows.Forms.Button()
        Me._StartButton = New System.Windows.Forms.Button()
        Me._ConfigButton = New System.Windows.Forms.Button()
        Me._BuffersCountTextBox = New System.Windows.Forms.TextBox()
        Me._FrequencyTextBoxLabel = New System.Windows.Forms.Label()
        Me._ChannelBufferSizeTextBoxLabel = New System.Windows.Forms.Label()
        Me._BuffersCountTextBoxLabel = New System.Windows.Forms.Label()
        Me._DeviceNameLabel = New System.Windows.Forms.Label()
        Me._YMax2Label = New System.Windows.Forms.Label()
        Me._YMin2Label = New System.Windows.Forms.Label()
        Me._AutoScaleButton = New System.Windows.Forms.Button()
        Me._ColorGridsButton = New System.Windows.Forms.Button()
        Me._SeparatorPanel2 = New System.Windows.Forms.Panel()
        Me._SeparatorPanel1 = New System.Windows.Forms.Panel()
        Me._ColorAxesButton = New System.Windows.Forms.Button()
        Me._ColorDataButton = New System.Windows.Forms.Button()
        Me._SignalListComboBox = New System.Windows.Forms.ComboBox()
        Me._YMaxTrackBar = New System.Windows.Forms.TrackBar()
        Me._YMinTrackBar = New System.Windows.Forms.TrackBar()
        Me._YMax1Label = New System.Windows.Forms.Label()
        Me._YMin1Label = New System.Windows.Forms.Label()
        Me._XMaxTrackBar = New System.Windows.Forms.TrackBar()
        Me._XMinTrackBar = New System.Windows.Forms.TrackBar()
        Me._XMaxLabel = New System.Windows.Forms.Label()
        Me._XMinLabel = New System.Windows.Forms.Label()
        Me._YAxisLabel = New System.Windows.Forms.Label()
        Me._XAxisLabel = New System.Windows.Forms.Label()
        Me._BandModeGroupBox = New System.Windows.Forms.GroupBox()
        Me._MultipleRadioButton = New System.Windows.Forms.RadioButton()
        Me._SingleRadioButton = New System.Windows.Forms.RadioButton()
        Me._DeviceNameChooser1 = New isr.IO.OL.WinForms.DeviceNameChooser()
        Me._Chart = New OpenLayers.Controls.Display()
        Me._StripChartCheckBox = New System.Windows.Forms.CheckBox()
        Me._SignalBufferSizeTextBoxLabel = New System.Windows.Forms.Label()
        Me._RestoreDefaultsButton = New System.Windows.Forms.Button()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._BuffersToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._SignalMemorySizeTextBoxLabel = New System.Windows.Forms.Label()
        Me._SamplingRateTextBoxLabel = New System.Windows.Forms.Label()
        Me._DisplayRateTextBoxLabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        CType(Me._YMaxTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._YMinTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._XMaxTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._XMinTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._BandModeGroupBox.SuspendLayout()
        CType(Me._Chart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_DeleteSignalButton
        '
        Me._DeleteSignalButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._DeleteSignalButton.Location = New System.Drawing.Point(802, 232)
        Me._DeleteSignalButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._DeleteSignalButton.Name = "_DeleteSignalButton"
        Me._DeleteSignalButton.Size = New System.Drawing.Size(95, 32)
        Me._DeleteSignalButton.TabIndex = 65
        Me._DeleteSignalButton.Text = "Delete Channel"
        Me._ToolTip.SetToolTip(Me._DeleteSignalButton, "Delete last channel")
        '
        '_AddSignalButton
        '
        Me._AddSignalButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._AddSignalButton.Location = New System.Drawing.Point(802, 272)
        Me._AddSignalButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._AddSignalButton.Name = "_AddSignalButton"
        Me._AddSignalButton.Size = New System.Drawing.Size(95, 32)
        Me._AddSignalButton.TabIndex = 64
        Me._AddSignalButton.Text = "Add Channel"
        Me._ToolTip.SetToolTip(Me._AddSignalButton, "Add input channel")
        '
        '_InitializeButton
        '
        Me._InitializeButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._InitializeButton.Enabled = False
        Me._InitializeButton.Location = New System.Drawing.Point(28, 421)
        Me._InitializeButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._InitializeButton.Name = "_InitializeButton"
        Me._InitializeButton.Size = New System.Drawing.Size(145, 24)
        Me._InitializeButton.TabIndex = 88
        Me._InitializeButton.Text = "Initialize Board"
        Me._ToolTip.SetToolTip(Me._InitializeButton, "Initializes the selected device.")
        '
        '_FrequencyTextBox
        '
        Me._FrequencyTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FrequencyTextBox.Enabled = False
        Me._FrequencyTextBox.Location = New System.Drawing.Point(325, 361)
        Me._FrequencyTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._FrequencyTextBox.Name = "_FrequencyTextBox"
        Me._FrequencyTextBox.Size = New System.Drawing.Size(61, 25)
        Me._FrequencyTextBox.TabIndex = 96
        Me._FrequencyTextBox.Text = "5000"
        Me._ToolTip.SetToolTip(Me._FrequencyTextBox, "Board sampling rate")
        '
        '_ChannelBufferSizeTextBox
        '
        Me._ChannelBufferSizeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._ChannelBufferSizeTextBox.Enabled = False
        Me._ChannelBufferSizeTextBox.Location = New System.Drawing.Point(325, 389)
        Me._ChannelBufferSizeTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChannelBufferSizeTextBox.Name = "_ChannelBufferSizeTextBox"
        Me._ChannelBufferSizeTextBox.Size = New System.Drawing.Size(47, 25)
        Me._ChannelBufferSizeTextBox.TabIndex = 94
        Me._ChannelBufferSizeTextBox.Text = "1000"
        Me._ToolTip.SetToolTip(Me._ChannelBufferSizeTextBox, "Size of actual buffer allocated per channel.")
        '
        '_FiniteBufferingCheckBox
        '
        Me._FiniteBufferingCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._FiniteBufferingCheckBox.AutoSize = True
        Me._FiniteBufferingCheckBox.Location = New System.Drawing.Point(567, 391)
        Me._FiniteBufferingCheckBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._FiniteBufferingCheckBox.Name = "_FiniteBufferingCheckBox"
        Me._FiniteBufferingCheckBox.Size = New System.Drawing.Size(113, 21)
        Me._FiniteBufferingCheckBox.TabIndex = 100
        Me._FiniteBufferingCheckBox.Text = "Finite Buffering"
        Me._ToolTip.SetToolTip(Me._FiniteBufferingCheckBox, "Samples the specified number of buffers and stops.")
        '
        '_SmartBufferingCheckBox
        '
        Me._SmartBufferingCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SmartBufferingCheckBox.AutoSize = True
        Me._SmartBufferingCheckBox.Enabled = False
        Me._SmartBufferingCheckBox.Location = New System.Drawing.Point(567, 447)
        Me._SmartBufferingCheckBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SmartBufferingCheckBox.Name = "_SmartBufferingCheckBox"
        Me._SmartBufferingCheckBox.Size = New System.Drawing.Size(117, 21)
        Me._SmartBufferingCheckBox.TabIndex = 100
        Me._SmartBufferingCheckBox.Text = "Smart Buffering"
        Me._ToolTip.SetToolTip(Me._SmartBufferingCheckBox, "Assigns 256 points for each buffer and adjusts the sampling rate accordingly.")
        '
        '_SampleSizeTextBoxLabel
        '
        Me._SampleSizeTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SampleSizeTextBoxLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._SampleSizeTextBoxLabel.Location = New System.Drawing.Point(386, 395)
        Me._SampleSizeTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SampleSizeTextBoxLabel.Name = "_SampleSizeTextBoxLabel"
        Me._SampleSizeTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._SampleSizeTextBoxLabel.TabIndex = 91
        Me._SampleSizeTextBoxLabel.Text = "Sample Size:"
        Me._SampleSizeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ToolTip.SetToolTip(Me._SampleSizeTextBoxLabel, "The number of samples used per channel buffer.")
        '
        '_SampleSizeTextBox
        '
        Me._SampleSizeTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SampleSizeTextBox.Location = New System.Drawing.Point(503, 389)
        Me._SampleSizeTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SampleSizeTextBox.Name = "_SampleSizeTextBox"
        Me._SampleSizeTextBox.Size = New System.Drawing.Size(47, 25)
        Me._SampleSizeTextBox.TabIndex = 93
        Me._SampleSizeTextBox.Text = "1000"
        Me._ToolTip.SetToolTip(Me._SampleSizeTextBox, "Number of samples per channel buffer")
        '
        '_SignalMemorySizeTextBox
        '
        Me._SignalMemorySizeTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SignalMemorySizeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalMemorySizeTextBox.Enabled = False
        Me._SignalMemorySizeTextBox.Location = New System.Drawing.Point(503, 445)
        Me._SignalMemorySizeTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SignalMemorySizeTextBox.Name = "_SignalMemorySizeTextBox"
        Me._SignalMemorySizeTextBox.Size = New System.Drawing.Size(47, 25)
        Me._SignalMemorySizeTextBox.TabIndex = 94
        Me._SignalMemorySizeTextBox.Text = "10000"
        Me._ToolTip.SetToolTip(Me._SignalMemorySizeTextBox, "Number of samples stored in the strip chart voltage buffer.")
        '
        '_SamplingRateTextBox
        '
        Me._SamplingRateTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SamplingRateTextBox.Location = New System.Drawing.Point(503, 361)
        Me._SamplingRateTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SamplingRateTextBox.Name = "_SamplingRateTextBox"
        Me._SamplingRateTextBox.Size = New System.Drawing.Size(47, 25)
        Me._SamplingRateTextBox.TabIndex = 96
        Me._SamplingRateTextBox.Text = "5000"
        Me._ToolTip.SetToolTip(Me._SamplingRateTextBox, "Actual desired sampling rate")
        '
        '_SignalBufferSizeTextBox
        '
        Me._SignalBufferSizeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalBufferSizeTextBox.Enabled = False
        Me._SignalBufferSizeTextBox.Location = New System.Drawing.Point(325, 445)
        Me._SignalBufferSizeTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SignalBufferSizeTextBox.Name = "_SignalBufferSizeTextBox"
        Me._SignalBufferSizeTextBox.Size = New System.Drawing.Size(47, 25)
        Me._SignalBufferSizeTextBox.TabIndex = 110
        Me._SignalBufferSizeTextBox.Text = "1000"
        Me._ToolTip.SetToolTip(Me._SignalBufferSizeTextBox, "Number of points per display")
        '
        '_DisplayRateTextBox
        '
        Me._DisplayRateTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._DisplayRateTextBox.Location = New System.Drawing.Point(325, 423)
        Me._DisplayRateTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._DisplayRateTextBox.Name = "_DisplayRateTextBox"
        Me._DisplayRateTextBox.Size = New System.Drawing.Size(47, 25)
        Me._DisplayRateTextBox.TabIndex = 94
        Me._DisplayRateTextBox.Text = "24.0"
        Me._ToolTip.SetToolTip(Me._DisplayRateTextBox, "Refresh rate for display.")
        '
        '_AbortButton
        '
        Me._AbortButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._AbortButton.Location = New System.Drawing.Point(802, 428)
        Me._AbortButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(95, 32)
        Me._AbortButton.TabIndex = 102
        Me._AbortButton.Text = "Abort"
        '
        '_EnablePlottingCheckBox
        '
        Me._EnablePlottingCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._EnablePlottingCheckBox.AutoSize = True
        Me._EnablePlottingCheckBox.Location = New System.Drawing.Point(567, 363)
        Me._EnablePlottingCheckBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._EnablePlottingCheckBox.Name = "_EnablePlottingCheckBox"
        Me._EnablePlottingCheckBox.Size = New System.Drawing.Size(114, 21)
        Me._EnablePlottingCheckBox.TabIndex = 100
        Me._EnablePlottingCheckBox.Text = "Enable Plotting"
        '
        '_StopButton
        '
        Me._StopButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._StopButton.Location = New System.Drawing.Point(802, 388)
        Me._StopButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._StopButton.Name = "_StopButton"
        Me._StopButton.Size = New System.Drawing.Size(95, 32)
        Me._StopButton.TabIndex = 98
        Me._StopButton.Text = "Stop"
        '
        '_StartButton
        '
        Me._StartButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._StartButton.Location = New System.Drawing.Point(802, 348)
        Me._StartButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._StartButton.Name = "_StartButton"
        Me._StartButton.Size = New System.Drawing.Size(95, 32)
        Me._StartButton.TabIndex = 97
        Me._StartButton.Text = "Start"
        '
        '_ConfigButton
        '
        Me._ConfigButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._ConfigButton.Location = New System.Drawing.Point(694, 376)
        Me._ConfigButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ConfigButton.Name = "_ConfigButton"
        Me._ConfigButton.Size = New System.Drawing.Size(92, 32)
        Me._ConfigButton.TabIndex = 63
        Me._ConfigButton.Text = "Configure"
        '
        '_BuffersCountTextBox
        '
        Me._BuffersCountTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._BuffersCountTextBox.Location = New System.Drawing.Point(503, 423)
        Me._BuffersCountTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._BuffersCountTextBox.Name = "_BuffersCountTextBox"
        Me._BuffersCountTextBox.Size = New System.Drawing.Size(47, 25)
        Me._BuffersCountTextBox.TabIndex = 93
        Me._BuffersCountTextBox.Text = "5"
        '
        '_FrequencyTextBoxLabel
        '
        Me._FrequencyTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._FrequencyTextBoxLabel.Location = New System.Drawing.Point(208, 365)
        Me._FrequencyTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._FrequencyTextBoxLabel.Name = "_FrequencyTextBoxLabel"
        Me._FrequencyTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._FrequencyTextBoxLabel.TabIndex = 95
        Me._FrequencyTextBoxLabel.Text = "Clock Frequency:"
        Me._FrequencyTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ChannelBufferSizeTextBoxLabel
        '
        Me._ChannelBufferSizeTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._ChannelBufferSizeTextBoxLabel.Location = New System.Drawing.Point(208, 393)
        Me._ChannelBufferSizeTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._ChannelBufferSizeTextBoxLabel.Name = "_ChannelBufferSizeTextBoxLabel"
        Me._ChannelBufferSizeTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._ChannelBufferSizeTextBoxLabel.TabIndex = 92
        Me._ChannelBufferSizeTextBoxLabel.Text = "Channel Buffer Size:"
        Me._ChannelBufferSizeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_BuffersCountTextBoxLabel
        '
        Me._BuffersCountTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._BuffersCountTextBoxLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._BuffersCountTextBoxLabel.Location = New System.Drawing.Point(386, 425)
        Me._BuffersCountTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._BuffersCountTextBoxLabel.Name = "_BuffersCountTextBoxLabel"
        Me._BuffersCountTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._BuffersCountTextBoxLabel.TabIndex = 91
        Me._BuffersCountTextBoxLabel.Text = "Number of Buffers:"
        Me._BuffersCountTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_DeviceNameLabel
        '
        Me._DeviceNameLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._DeviceNameLabel.Location = New System.Drawing.Point(29, 376)
        Me._DeviceNameLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._DeviceNameLabel.Name = "_DeviceNameLabel"
        Me._DeviceNameLabel.Size = New System.Drawing.Size(110, 16)
        Me._DeviceNameLabel.TabIndex = 87
        Me._DeviceNameLabel.Text = "Device Name:"
        Me._DeviceNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_YMax2Label
        '
        Me._YMax2Label.Cursor = System.Windows.Forms.Cursors.Default
        Me._YMax2Label.Location = New System.Drawing.Point(516, 561)
        Me._YMax2Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._YMax2Label.Name = "_YMax2Label"
        Me._YMax2Label.Size = New System.Drawing.Size(65, 13)
        Me._YMax2Label.TabIndex = 85
        Me._YMax2Label.Text = "(0...20)"
        Me._YMax2Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_YMin2Label
        '
        Me._YMin2Label.Location = New System.Drawing.Point(232, 561)
        Me._YMin2Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._YMin2Label.Name = "_YMin2Label"
        Me._YMin2Label.Size = New System.Drawing.Size(65, 13)
        Me._YMin2Label.TabIndex = 84
        Me._YMin2Label.Text = "(-20...0)"
        Me._YMin2Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_AutoScaleButton
        '
        Me._AutoScaleButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._AutoScaleButton.Location = New System.Drawing.Point(802, 184)
        Me._AutoScaleButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._AutoScaleButton.Name = "_AutoScaleButton"
        Me._AutoScaleButton.Size = New System.Drawing.Size(95, 23)
        Me._AutoScaleButton.TabIndex = 74
        Me._AutoScaleButton.Text = "Auto Scale"
        '
        '_ColorGridsButton
        '
        Me._ColorGridsButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ColorGridsButton.Location = New System.Drawing.Point(802, 152)
        Me._ColorGridsButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ColorGridsButton.Name = "_ColorGridsButton"
        Me._ColorGridsButton.Size = New System.Drawing.Size(95, 23)
        Me._ColorGridsButton.TabIndex = 73
        Me._ColorGridsButton.Text = "Grids color"
        '
        '_SeparatorPanel2
        '
        Me._SeparatorPanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SeparatorPanel2.BackColor = System.Drawing.SystemColors.Control
        Me._SeparatorPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SeparatorPanel2.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SeparatorPanel2.Location = New System.Drawing.Point(14, 472)
        Me._SeparatorPanel2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SeparatorPanel2.Name = "_SeparatorPanel2"
        Me._SeparatorPanel2.Size = New System.Drawing.Size(892, 1)
        Me._SeparatorPanel2.TabIndex = 83
        '
        '_SeparatorPanel1
        '
        Me._SeparatorPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SeparatorPanel1.BackColor = System.Drawing.SystemColors.Control
        Me._SeparatorPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SeparatorPanel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SeparatorPanel1.Location = New System.Drawing.Point(14, 532)
        Me._SeparatorPanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SeparatorPanel1.Name = "_SeparatorPanel1"
        Me._SeparatorPanel1.Size = New System.Drawing.Size(892, 1)
        Me._SeparatorPanel1.TabIndex = 82
        '
        '_ColorAxesButton
        '
        Me._ColorAxesButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ColorAxesButton.Location = New System.Drawing.Point(802, 120)
        Me._ColorAxesButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ColorAxesButton.Name = "_ColorAxesButton"
        Me._ColorAxesButton.Size = New System.Drawing.Size(95, 23)
        Me._ColorAxesButton.TabIndex = 72
        Me._ColorAxesButton.Text = "Axes color"
        '
        '_ColorDataButton
        '
        Me._ColorDataButton.Location = New System.Drawing.Point(802, 550)
        Me._ColorDataButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ColorDataButton.Name = "_ColorDataButton"
        Me._ColorDataButton.Size = New System.Drawing.Size(86, 23)
        Me._ColorDataButton.TabIndex = 71
        Me._ColorDataButton.Text = "Signal color"
        '
        '_SignalListComboBox
        '
        Me._SignalListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SignalListComboBox.Location = New System.Drawing.Point(72, 551)
        Me._SignalListComboBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SignalListComboBox.Name = "_SignalListComboBox"
        Me._SignalListComboBox.Size = New System.Drawing.Size(114, 25)
        Me._SignalListComboBox.TabIndex = 68
        '
        '_YMaxTrackBar
        '
        Me._YMaxTrackBar.LargeChange = 2
        Me._YMaxTrackBar.Location = New System.Drawing.Point(581, 539)
        Me._YMaxTrackBar.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._YMaxTrackBar.Maximum = 20
        Me._YMaxTrackBar.Name = "_YMaxTrackBar"
        Me._YMaxTrackBar.Size = New System.Drawing.Size(192, 45)
        Me._YMaxTrackBar.SmallChange = 10
        Me._YMaxTrackBar.TabIndex = 70
        Me._YMaxTrackBar.Value = 20
        '
        '_YMinTrackBar
        '
        Me._YMinTrackBar.LargeChange = 2
        Me._YMinTrackBar.Location = New System.Drawing.Point(293, 539)
        Me._YMinTrackBar.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._YMinTrackBar.Maximum = 0
        Me._YMinTrackBar.Minimum = -20
        Me._YMinTrackBar.Name = "_YMinTrackBar"
        Me._YMinTrackBar.Size = New System.Drawing.Size(192, 45)
        Me._YMinTrackBar.SmallChange = 10
        Me._YMinTrackBar.TabIndex = 69
        '
        '_YMax1Label
        '
        Me._YMax1Label.Location = New System.Drawing.Point(516, 545)
        Me._YMax1Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._YMax1Label.Name = "_YMax1Label"
        Me._YMax1Label.Size = New System.Drawing.Size(65, 13)
        Me._YMax1Label.TabIndex = 81
        Me._YMax1Label.Text = "max"
        Me._YMax1Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_YMin1Label
        '
        Me._YMin1Label.Location = New System.Drawing.Point(232, 545)
        Me._YMin1Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._YMin1Label.Name = "_YMin1Label"
        Me._YMin1Label.Size = New System.Drawing.Size(65, 13)
        Me._YMin1Label.TabIndex = 80
        Me._YMin1Label.Text = "min"
        Me._YMin1Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_XMaxTrackBar
        '
        Me._XMaxTrackBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMaxTrackBar.LargeChange = 100
        Me._XMaxTrackBar.Location = New System.Drawing.Point(581, 480)
        Me._XMaxTrackBar.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._XMaxTrackBar.Maximum = 2500
        Me._XMaxTrackBar.Minimum = 10
        Me._XMaxTrackBar.Name = "_XMaxTrackBar"
        Me._XMaxTrackBar.Size = New System.Drawing.Size(192, 45)
        Me._XMaxTrackBar.SmallChange = 10
        Me._XMaxTrackBar.TabIndex = 67
        Me._XMaxTrackBar.TickFrequency = 100
        Me._XMaxTrackBar.Value = 1000
        '
        '_XMinTrackBar
        '
        Me._XMinTrackBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMinTrackBar.LargeChange = 100
        Me._XMinTrackBar.Location = New System.Drawing.Point(293, 480)
        Me._XMinTrackBar.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._XMinTrackBar.Maximum = 990
        Me._XMinTrackBar.Name = "_XMinTrackBar"
        Me._XMinTrackBar.Size = New System.Drawing.Size(192, 45)
        Me._XMinTrackBar.SmallChange = 10
        Me._XMinTrackBar.TabIndex = 66
        Me._XMinTrackBar.TickFrequency = 100
        '
        '_XMaxLabel
        '
        Me._XMaxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMaxLabel.Location = New System.Drawing.Point(485, 491)
        Me._XMaxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._XMaxLabel.Name = "_XMaxLabel"
        Me._XMaxLabel.Size = New System.Drawing.Size(96, 23)
        Me._XMaxLabel.TabIndex = 79
        Me._XMaxLabel.Text = "max (0...2500)"
        Me._XMaxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_XMinLabel
        '
        Me._XMinLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMinLabel.Location = New System.Drawing.Point(201, 491)
        Me._XMinLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._XMinLabel.Name = "_XMinLabel"
        Me._XMinLabel.Size = New System.Drawing.Size(96, 23)
        Me._XMinLabel.TabIndex = 78
        Me._XMinLabel.Text = "min (0...1000)"
        Me._XMinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_YAxisLabel
        '
        Me._YAxisLabel.Location = New System.Drawing.Point(24, 555)
        Me._YAxisLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._YAxisLabel.Name = "_YAxisLabel"
        Me._YAxisLabel.Size = New System.Drawing.Size(48, 13)
        Me._YAxisLabel.TabIndex = 77
        Me._YAxisLabel.Text = "Y-Axis:"
        Me._YAxisLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_XAxisLabel
        '
        Me._XAxisLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XAxisLabel.Location = New System.Drawing.Point(36, 496)
        Me._XAxisLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._XAxisLabel.Name = "_XAxisLabel"
        Me._XAxisLabel.Size = New System.Drawing.Size(48, 13)
        Me._XAxisLabel.TabIndex = 76
        Me._XAxisLabel.Text = "X-Axis:"
        '
        '_BandModeGroupBox
        '
        Me._BandModeGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._BandModeGroupBox.Controls.Add(Me._MultipleRadioButton)
        Me._BandModeGroupBox.Controls.Add(Me._SingleRadioButton)
        Me._BandModeGroupBox.Location = New System.Drawing.Point(792, 8)
        Me._BandModeGroupBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._BandModeGroupBox.Name = "_BandModeGroupBox"
        Me._BandModeGroupBox.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._BandModeGroupBox.Size = New System.Drawing.Size(106, 100)
        Me._BandModeGroupBox.TabIndex = 75
        Me._BandModeGroupBox.TabStop = False
        Me._BandModeGroupBox.Text = "Band mode"
        '
        '_MultipleRadioButton
        '
        Me._MultipleRadioButton.Location = New System.Drawing.Point(19, 56)
        Me._MultipleRadioButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._MultipleRadioButton.Name = "_MultipleRadioButton"
        Me._MultipleRadioButton.Size = New System.Drawing.Size(77, 24)
        Me._MultipleRadioButton.TabIndex = 1
        Me._MultipleRadioButton.Text = "Multiple"
        '
        '_SingleRadioButton
        '
        Me._SingleRadioButton.Checked = True
        Me._SingleRadioButton.Location = New System.Drawing.Point(19, 24)
        Me._SingleRadioButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SingleRadioButton.Name = "_SingleRadioButton"
        Me._SingleRadioButton.Size = New System.Drawing.Size(67, 24)
        Me._SingleRadioButton.TabIndex = 0
        Me._SingleRadioButton.TabStop = True
        Me._SingleRadioButton.Text = "Single"
        '
        '_DeviceNameChooser1
        '
        Me._DeviceNameChooser1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DeviceNameChooser1.Location = New System.Drawing.Point(29, 391)
        Me._DeviceNameChooser1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._DeviceNameChooser1.Name = "_DeviceNameChooser1"
        Me._DeviceNameChooser1.Size = New System.Drawing.Size(144, 25)
        Me._DeviceNameChooser1.TabIndex = 104
        '
        '_Chart
        '
        Me._Chart.AutoScale = False
        Me._Chart.AxesColor = System.Drawing.Color.Blue
        Me._Chart.BackGradientAngle = 0!
        Me._Chart.BackGradientColor = System.Drawing.SystemColors.Control
        Me._Chart.BackgroundStyle = OpenLayers.Chart.Layers.BackgroundStyle.Color
        Me._Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand
        Me._Chart.Footer = String.Empty
        Me._Chart.FooterFont = New System.Drawing.Font("Segoe UI", 8.0!)
        Me._Chart.GridColor = System.Drawing.Color.Blue
        Me._Chart.Location = New System.Drawing.Point(12, 8)
        Me._Chart.Name = "_Chart"
        Me._Chart.SignalBufferLength = CType(0, Long)
        Me._Chart.Size = New System.Drawing.Size(773, 349)
        Me._Chart.TabIndex = 105
        Me._Chart.Title = "Chart"
        Me._Chart.TitleFont = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me._Chart.XDataCurrentRangeMax = 1000.0R
        Me._Chart.XDataCurrentRangeMin = 0R
        Me._Chart.XDataName = "Time"
        Me._Chart.XDataRangeMax = 1000.0R
        Me._Chart.XDataRangeMin = 0R
        Me._Chart.XDataUnit = "sec"
        '
        '_StripChartCheckBox
        '
        Me._StripChartCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._StripChartCheckBox.AutoSize = True
        Me._StripChartCheckBox.Location = New System.Drawing.Point(567, 421)
        Me._StripChartCheckBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._StripChartCheckBox.Name = "_StripChartCheckBox"
        Me._StripChartCheckBox.Size = New System.Drawing.Size(128, 21)
        Me._StripChartCheckBox.TabIndex = 106
        Me._StripChartCheckBox.Text = "Strip Chart Mode"
        '
        '_SignalBufferSizeTextBoxLabel
        '
        Me._SignalBufferSizeTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SignalBufferSizeTextBoxLabel.Location = New System.Drawing.Point(208, 449)
        Me._SignalBufferSizeTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SignalBufferSizeTextBoxLabel.Name = "_SignalBufferSizeTextBoxLabel"
        Me._SignalBufferSizeTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._SignalBufferSizeTextBoxLabel.TabIndex = 92
        Me._SignalBufferSizeTextBoxLabel.Text = "Signal Buffer Size:"
        Me._SignalBufferSizeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_RestoreDefaultsButton
        '
        Me._RestoreDefaultsButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._RestoreDefaultsButton.Location = New System.Drawing.Point(694, 417)
        Me._RestoreDefaultsButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._RestoreDefaultsButton.Name = "_RestoreDefaultsButton"
        Me._RestoreDefaultsButton.Size = New System.Drawing.Size(92, 48)
        Me._RestoreDefaultsButton.TabIndex = 107
        Me._RestoreDefaultsButton.Text = "Restore Defaults"
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusToolStripStatusLabel, Me._BuffersToolStripStatusLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 582)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Size = New System.Drawing.Size(922, 24)
        Me._StatusStrip.TabIndex = 108
        Me._StatusStrip.Text = "_StatusStrip1"
        '
        '_StatusToolStripStatusLabel
        '
        Me._StatusToolStripStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._StatusToolStripStatusLabel.Name = "_StatusToolStripStatusLabel"
        Me._StatusToolStripStatusLabel.Size = New System.Drawing.Size(890, 19)
        Me._StatusToolStripStatusLabel.Spring = True
        Me._StatusToolStripStatusLabel.Text = "Status: None"
        Me._StatusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        '_BuffersToolStripStatusLabel
        '
        Me._BuffersToolStripStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._BuffersToolStripStatusLabel.Name = "_BuffersToolStripStatusLabel"
        Me._BuffersToolStripStatusLabel.Size = New System.Drawing.Size(17, 19)
        Me._BuffersToolStripStatusLabel.Text = "0"
        Me._BuffersToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._BuffersToolStripStatusLabel.ToolTipText = "Buffers completed"
        '
        '_SignalMemorySizeTextBoxLabel
        '
        Me._SignalMemorySizeTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SignalMemorySizeTextBoxLabel.Location = New System.Drawing.Point(386, 449)
        Me._SignalMemorySizeTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SignalMemorySizeTextBoxLabel.Name = "_SignalMemorySizeTextBoxLabel"
        Me._SignalMemorySizeTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._SignalMemorySizeTextBoxLabel.TabIndex = 92
        Me._SignalMemorySizeTextBoxLabel.Text = "Signal Memory Size:"
        Me._SignalMemorySizeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SamplingRateTextBoxLabel
        '
        Me._SamplingRateTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SamplingRateTextBoxLabel.Location = New System.Drawing.Point(386, 365)
        Me._SamplingRateTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SamplingRateTextBoxLabel.Name = "_SamplingRateTextBoxLabel"
        Me._SamplingRateTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._SamplingRateTextBoxLabel.TabIndex = 95
        Me._SamplingRateTextBoxLabel.Text = "Sampling Rate:"
        Me._SamplingRateTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_DisplayRateTextBoxLabel
        '
        Me._DisplayRateTextBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._DisplayRateTextBoxLabel.Location = New System.Drawing.Point(208, 425)
        Me._DisplayRateTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._DisplayRateTextBoxLabel.Name = "_DisplayRateTextBoxLabel"
        Me._DisplayRateTextBoxLabel.Size = New System.Drawing.Size(116, 16)
        Me._DisplayRateTextBoxLabel.TabIndex = 92
        Me._DisplayRateTextBoxLabel.Text = "Display Rate [1/sec]: "
        Me._DisplayRateTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label1.Location = New System.Drawing.Point(386, 421)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 16)
        Me.Label1.TabIndex = 91
        Me.Label1.Text = "Number of Buffers:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(208, 421)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(116, 16)
        Me.Label2.TabIndex = 92
        Me.Label2.Text = "Display Rate [1/sec]: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(503, 417)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(47, 25)
        Me.TextBox1.TabIndex = 93
        Me.TextBox1.Text = "5"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox2.Location = New System.Drawing.Point(325, 417)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(47, 25)
        Me.TextBox2.TabIndex = 94
        Me.TextBox2.Text = "24.0"
        '
        'CheckBox1
        '
        Me.CheckBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(567, 419)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(128, 21)
        Me.CheckBox1.TabIndex = 106
        Me.CheckBox1.Text = "Strip Chart Mode"
        '
        'ScopePanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(922, 606)
        Me.Controls.Add(Me._SignalBufferSizeTextBox)
        Me.Controls.Add(Me._StatusStrip)
        Me.Controls.Add(Me._RestoreDefaultsButton)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me._StripChartCheckBox)
        Me.Controls.Add(Me._Chart)
        Me.Controls.Add(Me._DeviceNameChooser1)
        Me.Controls.Add(Me._AbortButton)
        Me.Controls.Add(Me._SmartBufferingCheckBox)
        Me.Controls.Add(Me._FiniteBufferingCheckBox)
        Me.Controls.Add(Me._EnablePlottingCheckBox)
        Me.Controls.Add(Me._StopButton)
        Me.Controls.Add(Me._StartButton)
        Me.Controls.Add(Me._ConfigButton)
        Me.Controls.Add(Me._SamplingRateTextBox)
        Me.Controls.Add(Me._FrequencyTextBox)
        Me.Controls.Add(Me._SignalMemorySizeTextBox)
        Me.Controls.Add(Me._SampleSizeTextBox)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me._DisplayRateTextBox)
        Me.Controls.Add(Me._ChannelBufferSizeTextBox)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me._BuffersCountTextBox)
        Me.Controls.Add(Me._SamplingRateTextBoxLabel)
        Me.Controls.Add(Me._SignalBufferSizeTextBoxLabel)
        Me.Controls.Add(Me._SignalMemorySizeTextBoxLabel)
        Me.Controls.Add(Me._FrequencyTextBoxLabel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me._DisplayRateTextBoxLabel)
        Me.Controls.Add(Me._SampleSizeTextBoxLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me._ChannelBufferSizeTextBoxLabel)
        Me.Controls.Add(Me._BuffersCountTextBoxLabel)
        Me.Controls.Add(Me._InitializeButton)
        Me.Controls.Add(Me._DeviceNameLabel)
        Me.Controls.Add(Me._YMax2Label)
        Me.Controls.Add(Me._YMin2Label)
        Me.Controls.Add(Me._AutoScaleButton)
        Me.Controls.Add(Me._ColorGridsButton)
        Me.Controls.Add(Me._SeparatorPanel2)
        Me.Controls.Add(Me._SeparatorPanel1)
        Me.Controls.Add(Me._ColorAxesButton)
        Me.Controls.Add(Me._ColorDataButton)
        Me.Controls.Add(Me._DeleteSignalButton)
        Me.Controls.Add(Me._SignalListComboBox)
        Me.Controls.Add(Me._YMaxTrackBar)
        Me.Controls.Add(Me._YMinTrackBar)
        Me.Controls.Add(Me._YMax1Label)
        Me.Controls.Add(Me._YMin1Label)
        Me.Controls.Add(Me._XMaxTrackBar)
        Me.Controls.Add(Me._XMinTrackBar)
        Me.Controls.Add(Me._XMaxLabel)
        Me.Controls.Add(Me._XMinLabel)
        Me.Controls.Add(Me._YAxisLabel)
        Me.Controls.Add(Me._XAxisLabel)
        Me.Controls.Add(Me._BandModeGroupBox)
        Me.Controls.Add(Me._AddSignalButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ScopePanel"
        Me.Text = "Scope Panel"
        CType(Me._YMaxTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._YMinTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._XMaxTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._XMinTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me._BandModeGroupBox.ResumeLayout(False)
        CType(Me._Chart, System.ComponentModel.ISupportInitialize).EndInit()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _Chart As OpenLayers.Controls.Display
    Private WithEvents _StripChartCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SignalBufferSizeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _RestoreDefaultsButton As System.Windows.Forms.Button
    Private WithEvents _FiniteBufferingCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SmartBufferingCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _StatusToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _BuffersToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _SampleSizeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SignalMemorySizeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SamplingRateTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SampleSizeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalMemorySizeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SamplingRateTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalBufferSizeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _DisplayRateTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _DisplayRateTextBox As System.Windows.Forms.TextBox
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents TextBox1 As System.Windows.Forms.TextBox
    Private WithEvents TextBox2 As System.Windows.Forms.TextBox
    Private WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
