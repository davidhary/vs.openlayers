Copyright 2006 Integrated Scientific Resources, Inc. All rights
reserved.

Licensed under [The MIT License](http://opensource.org/licenses/MIT).

**Installation Information.** This program installs the Single I/O
tester program and device drivers. As necessary, the program installs
the .Net Framework, which requires an Internet connection. You can ran
the installer using the -layout command line option, which downloads all
the required files so that they can be installed locally.

**Required Hardware.** This software uses the DT9816A data acquisition
module from [Data Translation](http://www.datatranslation.com).
