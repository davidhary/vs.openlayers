''' <summary> Adds functionality to the Data Translation Open Layers
''' <see cref="OpenLayers.Base.AnalogInputSubsystem">analog input subsystem</see>. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 8/7/2013 </para></remarks>
Public Class AnalogInputSubsystem
    Inherits Global.OpenLayers.Base.AnalogInputSubsystem

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <param name="device">        A reference to
    ''' <see cref="OpenLayers.Base.Device">an open layers device</see>. </param>
    ''' <param name="elementNumber"> Specifies the subsystem logical element number. </param>
    Public Sub New(ByVal device As OpenLayers.Base.Device, ByVal elementNumber As Integer)
        MyBase.New(device, elementNumber)
        Me._lastSingleReadingsChannel = -1
        Me._lastSingleVoltagesChannel = -1
        Me._lastSingleVoltages = New Collections.Generic.Dictionary(Of Integer, Double)
        Me._lastSingleReadings = New Collections.Generic.Dictionary(Of Integer, Integer)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.disposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.HandlesBufferDoneEvents = False
                    MyBase.BufferQueue?.FreeAllQueuedBuffers()
                    If Me._Buffers.Any Then
                        For Each buffer As Buffer In Me._Buffers
                            buffer.Dispose()
                        Next
                        Me._Buffers = Nothing
                    End If
                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " CONFIGURATION "

    ''' <summary> Lists the available Analog Input physical channel numbers. </summary>
    ''' <param name="format"> Specifies a format string for listing the channels. The format string
    ''' should specify one element for receiving the number, e.g., '{0}'. </param>
    ''' <returns> An array of <see cref="T:System.Sting"/> .</returns>
    Public Function AvailableChannels(ByVal format As String) As String()
        Dim channels(MyBase.NumberOfChannels - 1) As String
        For i As Integer = 0 To MyBase.NumberOfChannels - 1
            Dim channelInfo As OpenLayers.Base.SupportedChannelInfo = MyBase.SupportedChannels()(i)
            channels(i) = String.Format(Globalization.CultureInfo.CurrentCulture, format, channelInfo.PhysicalChannelNumber)
        Next i
        Return channels
    End Function

    ''' <summary> Lists the available Analog Input Ranges for the board based. </summary>
    ''' <param name="format"> Specifies a format string for listing the range. The format string should
    ''' specify two elements for receiving the range, e.g., '{0/1}'. </param>
    ''' <returns> An array of <see cref="T:System.Sting"/> .</returns>
    Public Function AvailableBoardRanges(ByVal format As String) As String()
        Dim ranges(MyBase.NumberOfSupportedGains - 1) As String
        For i As Integer = 0 To MyBase.SupportedVoltageRanges.Length - 1
            Dim supportedRange As OpenLayers.Base.Range = MyBase.SupportedVoltageRanges(i)
            ranges(i) = String.Format(Globalization.CultureInfo.CurrentCulture, format, supportedRange.Low, supportedRange.High)
        Next i
        Return ranges
    End Function

    ''' <summary> Lists the available Analog Input Ranges based on the current range and list of gains. </summary>
    ''' <param name="format"> Specifies a format string for listing the range. The format string should
    ''' specify two elements for receiving the range, e.g., '{0/1}'. </param>
    ''' <returns> An array of <see cref="T:System.Sting"/> .</returns>
    Public Function AvailableRanges(ByVal format As String) As String()
        Dim ranges(MyBase.NumberOfSupportedGains - 1) As String
        For i As Integer = 0 To MyBase.NumberOfSupportedGains - 1
            Dim gain As Double = MyBase.SupportedGains()(i)
            ranges(i) = String.Format(Globalization.CultureInfo.CurrentCulture, format,
                                      MyBase.VoltageRange.Low / gain, MyBase.VoltageRange.High / gain)
        Next i
        Return ranges
    End Function

    ''' <summary> <c>True</c> if has bipolar range. </summary>
    ''' <value> <c>True</c> if bipolar range; otherwise, <c>False</c>. </value>
    Public ReadOnly Property BipolarRange() As Boolean
        Get
            Return MyBase.VoltageRange.Low < 0
        End Get
    End Property

    ''' <summary> Get the index for the gain list item that best fits the voltage range to best fit the
    ''' specified voltage. </summary>
    ''' <param name="voltage"> Specifies the maximum voltage expected at the input. </param>
    ''' <returns> The index into the list of
    ''' <see cref="OpenLayers.Base.AnalogInputSubsystem.SupportedVoltageRanges">ranges</see>
    ''' for selecting the range or gain.  Returns -1 if the voltage is too high for the maximum
    ''' range. </returns>
    Public Function GainIndex(ByVal voltage As Double) As Integer
        If MyBase.SupportsProgrammableGain Then
            Dim i As Integer = 0
            ' Look for the highest gain
            Do While i < MyBase.NumberOfSupportedGains AndAlso
                voltage <= MyBase.VoltageRange.High / MyBase.SupportedGains()(i)
                i += 1
            Loop
            Return i - 1
        Else
            Return 0
        End If
    End Function

    ''' <summary> Gets the number of channels allocated for sampling. </summary>
    ''' <value> The channels count. </value>
    Public ReadOnly Property ChannelsCount() As Integer
        Get
            Return MyBase.ChannelList.Count
        End Get
    End Property

    ''' <summary> Returns true if the subsystem has channels defined. </summary>
    ''' <value> <c>True</c> if this instance has channels; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasChannels() As Boolean
        Get
            Return MyBase.ChannelList.Count > 0
        End Get
    End Property

    ''' <summary> true to handles buffer done events. </summary>
    Private _HandlesBufferDoneEvents As Boolean

    ''' <summary> Gets or sets or Sets the condition as True have the sub system process buffer done
    ''' events. </summary>
    ''' <value> <c>True</c> if [handles buffer done events]; otherwise, <c>False</c>. </value>
    Public Property HandlesBufferDoneEvents() As Boolean
        Get
            Return Me._handlesBufferDoneEvents
        End Get
        Set(ByVal value As Boolean)
            If Me._handlesBufferDoneEvents <> Value Then
                If Value Then
                    AddHandler Me.BufferDoneEvent, AddressOf Me.HandleBufferDoneEvent
                Else
                    RemoveHandler Me.BufferDoneEvent, AddressOf Me.HandleBufferDoneEvent
                End If
                Me._HandlesBufferDoneEvents = Value
            End If
        End Set
    End Property

    ''' <summary> Returns true of the subsystem supports differential inputs. </summary>
    ''' <value> <c>True</c> if [supports differential input]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property SupportsDifferentialInput() As Boolean
        Get
            Return MyBase.MaxDifferentialChannels > 0
        End Get
    End Property

    ''' <summary> Returns true of the subsystem supports single ended inputs. </summary>
    ''' <value> <c>True</c> if [supports single ended input]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property SupportsSingleEndedInput() As Boolean
        Get
            Return MyBase.MaxSingleEndedChannels > 0
        End Get
    End Property

    ''' <summary> Returns true of the subsystem supports different gain settings for channels on the
    ''' channel list.  The DT9800 series boards support multiple gains but all gains on the channel
    ''' list must be identical. </summary>
    ''' <value> <c>True</c> if [supports individual gains]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property SupportsIndividualGains() As Boolean
        Get
            Return MyBase.SupportedGains().Length > 1 AndAlso
                   MyBase.Device.DeviceFamily() <> DeviceFamily.DT9800
        End Get
    End Property

    ''' <summary> Returns the analog input voltage resolution. </summary>
    ''' <value> The voltage resolution. </value>
    Public ReadOnly Property VoltageResolution() As Double
        Get
            Return Me.VoltageResolution(Me.SelectedChannel)
        End Get
    End Property

    ''' <summary> Returns the analog input voltage resolution. </summary>
    ''' <value> The voltage resolution. </value>
    Public ReadOnly Property VoltageResolution(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        Get
            If channel Is Nothing Then Throw New ArgumentNullException(NameOf(channel))
            Return (Me.MaxVoltage(channel) - Me.MinVoltage(channel)) / MyBase.Resolution
        End Get
    End Property

#End Region

#Region " CONFIG "

    ''' <summary> Configures a single point analog input sub-system.  The input is configured for
    ''' differential input unless the sub system allows only single-ended. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <param name="gain">                  Specifies the channel gain. </param>
    Public Sub Configure(ByVal physicalChannelNumber As Integer, ByVal gain As Double)

        Dim channel As OpenLayers.Base.ChannelListEntry
        If True Then
            MyBase.ChannelList.Clear()
            channel = Me.ChannelList.Add(physicalChannelNumber)
            channel.Gain = gain
        Else
            ' replace with this code
            If Not MyBase.ChannelList Is Nothing AndAlso MyBase.ChannelList.Count > 1 Then
                MyBase.ChannelList.Clear()
                channel = Me.ChannelList.Add(physicalChannelNumber)
                channel.Gain = gain
                MyBase.ChannelList.Add(physicalChannelNumber)
                'Else
                '  channel = MyBase.ChannelList(???)
            End If
        End If
        MyBase.DataFlow = OpenLayers.Base.DataFlow.SingleValue
        MyBase.Config()

    End Sub

    ''' <summary> Configures continuous analog input sub-system for contiguous channels starting at the
    ''' specified channel. </summary>
    ''' <param name="bufferCount">                The buffer count. </param>
    ''' <param name="sampleCount">                The sample count. </param>
    ''' <param name="firstPhysicalChannelNumber"> Specifies the physical channel number of the first
    ''' channel. </param>
    ''' <param name="channelCount">               The channel count. </param>
    ''' <param name="samplingRate">               The sampling rate. </param>
    ''' <param name="maxVoltage">                 The max voltage. </param>
    Public Sub Configure(ByVal bufferCount As Integer, ByVal sampleCount As Integer,
                         ByVal firstPhysicalChannelNumber As Integer, ByVal channelCount As Integer,
                         ByVal samplingRate As Double, ByVal maxVoltage As Double)

        ' set the channel type
        If MyBase.ChannelType = OpenLayers.Base.ChannelType.SingleEnded Then
            MyBase.ChannelType = If(MyBase.SupportsSingleEnded, OpenLayers.Base.ChannelType.SingleEnded, OpenLayers.Base.ChannelType.Differential)
        Else
            MyBase.ChannelType = If(MyBase.SupportsDifferential, OpenLayers.Base.ChannelType.Differential, OpenLayers.Base.ChannelType.SingleEnded)
        End If

        ' Set the board range
        If MyBase.NumberOfRanges > 0 Then
            If Me.BipolarRange() Then
                MyBase.VoltageRange = MyBase.SupportedVoltageRanges(0)
                'MyBase.VoltageRange.High = MyBase.SupportedVoltageRanges(0).High
                'MyBase.VoltageRange.Low = MyBase.SupportedVoltageRanges(0).Low
            ElseIf MyBase.NumberOfRanges > 1 Then
                MyBase.VoltageRange = MyBase.SupportedVoltageRanges(1)
                '        MyBase.VoltageRange.High = MyBase.SupportedVoltageRanges(1).High
                '        MyBase.VoltageRange.Low = MyBase.SupportedVoltageRanges(1).Low
            End If
        End If

        ' set up for continuous operation
        MyBase.DataFlow = OpenLayers.Base.DataFlow.Continuous

        ' DateTimeOffset.Now set the rest of the parameters

        ' check if the board supports triggered scan
        If MyBase.SupportsTriggeredScan Then

            ' if we can re-trigger the scan

            ' turn on the re-trigger mode
            Dim ts As OpenLayers.Base.TriggeredScan = MyBase.TriggeredScan
            ts.Enabled = True

            ' set the number of scans to 1
            ts.MultiScanCount = 1

            ' settings for internal trigger

            ' set the scan mode to internal
            ts.RetriggerSource = OpenLayers.Base.TriggerType.Software

            ' set the re-trigger frequency to the
            ' required sampling rage
            ts.RetriggerFrequency = samplingRate

            ' set the time between channels to 100 times the sampling rate
            MyBase.Clock.Frequency = 100 * samplingRate

        Else

            ' if we cannot re-trigger the scan, turn off the
            ' re-trigger mode
            Dim ts As OpenLayers.Base.TriggeredScan = MyBase.TriggeredScan
            ts.Enabled = False

            ' Is A/D subsystem a Simultaneous Sample & Hold ??
            If MyBase.SupportsSimultaneousSampleHold Then

                ' set the clock frequency
                MyBase.Clock.Frequency = samplingRate

            Else

                ' set the clock frequency times the channel count
                MyBase.Clock.Frequency = samplingRate * channelCount

            End If

        End If

        ' set up channel list
        Me.AddChannels(firstPhysicalChannelNumber, firstPhysicalChannelNumber + channelCount - 1, Me.BestGain(maxVoltage))

        ' allocate buffers.
        Me.AllocateBuffers(bufferCount, channelCount * sampleCount)

        ' apply the configuration.
        MyBase.Config()

    End Sub

    ''' <summary>
    ''' Configures triggered sampling. The input is configured for differential input unless the sub system allows only single-ended.
    ''' </summary>
    ''' <param name="channelCount"> The channel count. </param>
    ''' <param name="samplingRate"> The sampling rate. </param>
    Public Sub ConfigureContinuousSampling(ByVal channelCount As Integer, ByVal samplingRate As Double)

        ' set the channel type
        If MyBase.ChannelType = OpenLayers.Base.ChannelType.SingleEnded Then
            MyBase.ChannelType = If(MyBase.SupportsSingleEnded, OpenLayers.Base.ChannelType.SingleEnded, OpenLayers.Base.ChannelType.Differential)
        Else
            MyBase.ChannelType = If(MyBase.SupportsDifferential, OpenLayers.Base.ChannelType.Differential, OpenLayers.Base.ChannelType.SingleEnded)
        End If

        ' Set the board range
        If MyBase.NumberOfRanges > 0 Then
            If Me.BipolarRange() Then
                MyBase.VoltageRange = MyBase.SupportedVoltageRanges(0)
                'MyBase.VoltageRange.High = MyBase.SupportedVoltageRanges(0).High
                'MyBase.VoltageRange.Low = MyBase.SupportedVoltageRanges(0).Low
            ElseIf MyBase.NumberOfRanges > 1 Then
                MyBase.VoltageRange = MyBase.SupportedVoltageRanges(1)
                '        MyBase.VoltageRange.High = MyBase.SupportedVoltageRanges(1).High
                '        MyBase.VoltageRange.Low = MyBase.SupportedVoltageRanges(1).Low
            End If
        End If

        ' set up for continuous operation
        MyBase.DataFlow = OpenLayers.Base.DataFlow.Continuous

        ' DateTimeOffset.Now set the rest of the parameters

        ' check if the board supports triggered scan
        If MyBase.SupportsTriggeredScan Then

            ' if we can re-trigger the scan

            ' turn on the re-trigger mode
            Dim ts As OpenLayers.Base.TriggeredScan = MyBase.TriggeredScan
            ts.Enabled = True

            ' set the number of scans to 1
            ts.MultiScanCount = 1

            ' settings for internal trigger

            ' set the scan mode to internal
            ts.RetriggerSource = OpenLayers.Base.TriggerType.Software

            ' set the re-trigger frequency to the
            ' required sampling rage
            ts.RetriggerFrequency = samplingRate

            ' set the time between channels to 100 times the sampling rate
            MyBase.Clock.Frequency = 100 * samplingRate

        Else

            ' if we cannot re-trigger the scan, turn off the
            ' re-trigger mode
            Dim ts As OpenLayers.Base.TriggeredScan = MyBase.TriggeredScan
            ts.Enabled = False

            ' Is A/D subsystem a Simultaneous Sample & Hold ??
            If MyBase.SupportsSimultaneousSampleHold Then

                ' set the clock frequency
                MyBase.Clock.Frequency = samplingRate

            Else

                ' set the clock frequency times the channel count
                MyBase.Clock.Frequency = samplingRate * channelCount

            End If

        End If

    End Sub

#End Region

#Region " CONTINUOUS OPERATION "

    ''' <summary> Stops a continuous operation on the subsystem immediately without waiting for the
    ''' current buffer to be filled. </summary>
    Public Overloads Overrides Sub Abort()
        MyBase.Abort()
    End Sub

    ''' <summary> Stops a continuous operation on the subsystem immediately without waiting for the
    ''' current buffer to be filled. </summary>
    ''' <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
    ''' <param name="timeout"> The timeout waiting for operations to stop. </param>
    Public Overloads Sub Abort(ByVal timeout As TimeSpan)

        If States.Stopping = Me.State OrElse States.Aborting = Me.State Then

            If States.Stopping = Me.State Then
                Me.Abort()
            End If

            ' wait for the device to stop 
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do While sw.Elapsed <= timeout AndAlso (States.Stopping = Me.State OrElse States.Aborting = Me.State)
                Threading.Thread.Sleep(100)
                My.MyLibrary.DoEvents()
            Loop
            If sw.Elapsed > timeout AndAlso (States.Stopping = Me.State OrElse States.Aborting = Me.State) Then
                Throw New TimeoutException($"Abort timeout--system still at the '{Me.State}' state after abort.")
            End If

        ElseIf Me.ContinuousOperationActive Then

            Me.Abort()

            ' wait for the device to stop 
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do While sw.Elapsed <= timeout AndAlso Me.ContinuousOperationActive
                Threading.Thread.Sleep(100)
                My.MyLibrary.DoEvents()
            Loop
            If sw.Elapsed > timeout AndAlso Me.ContinuousOperationActive Then
                Throw New TimeoutException($"Abort timeout--system still at the '{Me.State}' state after abort")
            End If

        End If

    End Sub

    ''' <summary> Returns true if the analog input is in continuous operation running state. </summary>
    ''' <value> The continuous operation running. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContinuousOperationRunning() As Boolean
        Get
            Return Global.OpenLayers.Base.SubsystemBase.States.Running = Me.State
        End Get
    End Property

    ''' <summary> Returns true if the analog input is in continuous operation. </summary>
    ''' <value> The continuous operation active. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContinuousOperationActive() As Boolean
        Get
            Return Global.OpenLayers.Base.SubsystemBase.States.Running = Me.State OrElse
                   Global.OpenLayers.Base.SubsystemBase.States.IoComplete = Me.State OrElse
                   Global.OpenLayers.Base.SubsystemBase.States.PreStarted = Me.State
        End Get
    End Property

    ''' <summary> Starts the subsystem operations. </summary>
    Public Overrides Sub [Start]()

        ' queue allocated buffers
        Me.QueueAllBuffers()

        ' get started.
        MyBase.Start()

    End Sub

    ''' <summary> Stops a continuous operation on the subsystem after the current buffer is filled. </summary>
    ''' <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs--continuous
    ''' operation still
    ''' <see cref="ContinuousOperationActive">active</see> after stop. </exception>
    ''' <param name="timeout"> The timeout waiting for operations to stop. </param>
    Public Overloads Sub [Stop](ByVal timeout As TimeSpan)

        If States.Stopping = Me.State OrElse States.Aborting = Me.State Then

            ' wait for the device to stop 
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do While sw.Elapsed <= timeout AndAlso (States.Stopping = Me.State OrElse States.Aborting = Me.State)
                My.MyLibrary.Delay(100)
                My.MyLibrary.DoEvents()
            Loop
            If sw.Elapsed > timeout AndAlso (States.Stopping = Me.State OrElse States.Aborting = Me.State) Then
                Throw New TimeoutException($"Stop timeout--system still at the '{Me.State}' state after stop.")
            End If

        ElseIf Me.ContinuousOperationActive Then

            MyBase.Stop()

            ' wait for the device to stop 
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do While sw.Elapsed <= timeout AndAlso Me.ContinuousOperationActive
                My.MyLibrary.Delay(100)
                My.MyLibrary.DoEvents()
            Loop
            If sw.Elapsed > timeout AndAlso Me.ContinuousOperationActive Then
                Throw New TimeoutException($"Stop timeout--system still at the '{Me.State}' state after stop.")
            End If

        End If

    End Sub

#End Region

#Region " DATA "

    ''' <summary> Get the maximum gain that would still fit in the specified voltage.. </summary>
    ''' <param name="voltage"> Specifies the maximum voltage expected at the input. </param>
    ''' <returns> System.Double. </returns>
    Public Function BestGain(ByVal voltage As Double) As Double
        If MyBase.SupportsProgrammableGain Then
            Dim i As Integer = Me.GainIndex(voltage)
            Return If(i < 0, MyBase.SupportedGains()(0), MyBase.SupportedGains()(i))
        Else
            Return MyBase.SupportedGains()(0)
        End If
    End Function

    ''' <summary> Clears the means and standard deviations. </summary>
    Public Sub ClearMeanAndSigma()
        ReDim Me._Means(MyBase.ChannelList.Count - 1)
        ReDim Me._StandardDeviations(MyBase.ChannelList.Count - 1)
    End Sub

#Disable Warning IDE0044 ' Add read only modifier
    Private _Means() As Double
#Enable Warning IDE0044
    ''' <summary> Returns the means array. </summary>
    ''' <returns> An array of <see cref="T:System.Double"/> .</returns>
    Public Function Means() As Double()
        Return Me._Means
    End Function

#Disable Warning IDE0044 ' Add read only modifier
    Private _StandardDeviations() As Double
#Enable Warning IDE0044
    ''' <summary> Returns the standard deviation of the channel item number. </summary>
    ''' <returns> An array of <see cref="T:System.Double"/> .</returns>
    Public Function StandardDeviations() As Double()
        Return Me._StandardDeviations
    End Function

    ''' <summary> true to calculates mean sigma. </summary>
    Private _CalculatesMeanSigma As Boolean

    ''' <summary> Gets or sets or Sets the condition as True have the buffer processed to calculate
    ''' mean and standard deviation. </summary>
    ''' <value> <c>True</c> if [calculates mean sigma]; otherwise, <c>False</c>. </value>
    Public Property CalculatesMeanSigma() As Boolean
        Get
            Return Me._CalculatesMeanSigma
        End Get
        Set(ByVal value As Boolean)
            Me._CalculatesMeanSigma = Value
            If Value Then
                Me.RetrievesVoltages = True
            End If
        End Set
    End Property

    ''' <summary> The last single voltages channel. </summary>
    Private _LastSingleVoltagesChannel As Integer
    ''' <summary> The last single voltages. </summary>
    Private ReadOnly _LastSingleVoltages As Collections.Generic.Dictionary(Of Integer, Double)

    ''' <summary> Returns the last voltage read using <see cref="SingleVoltage" />. </summary>
    ''' <value> The last single voltage. </value>
    Public Property LastSingleVoltage() As Double
        Get
            Return Me._LastSingleVoltages(Me._LastSingleVoltagesChannel)
        End Get
        Private Set(value As Double)
            If Me._LastSingleVoltages.ContainsKey(Me._LastSingleVoltagesChannel) Then
                Me._LastSingleVoltages.Remove(Me._LastSingleVoltagesChannel)
            End If
            Me._LastSingleVoltages.Add(Me._LastSingleVoltagesChannel, value)
        End Set
    End Property

    ''' <summary> The last single readings channel. </summary>
    Private _LastSingleReadingsChannel As Integer
    ''' <summary> The last single readings. </summary>
    Private ReadOnly _LastSingleReadings As Collections.Generic.Dictionary(Of Integer, Integer)

    ''' <summary> Returns the last input reading read using <see cref="SingleVoltage" />. </summary>
    ''' <value> The last single reading. </value>
    Public Property LastSingleReading() As Integer
        Get
            Return Me._LastSingleReadings(Me._LastSingleReadingsChannel)
        End Get
        Private Set(value As Integer)
            If Me._LastSingleReadings.ContainsKey(Me._LastSingleReadingsChannel) Then
                Me._LastSingleReadings.Remove(Me._LastSingleReadingsChannel)
            End If
            Me._LastSingleVoltages.Add(Me._LastSingleReadingsChannel, value)
        End Set
    End Property

    ''' <summary> Returns the subsystem maximum voltage. </summary>
    ''' <returns> System.Double. </returns>
    Public Function MaxVoltage() As Double
        Return Me.MaxVoltage(Me.SelectedChannel)
    End Function

    ''' <summary> Returns the subsystem maximum voltage. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> Specifies a reference to a
    ''' <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <returns> System.Double. </returns>
    Public Function MaxVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return If(channel.Gain = 0, 0, MyBase.VoltageRange.High / channel.Gain)
    End Function

    ''' <summary> Returns the subsystem minimum voltage. </summary>
    ''' <returns> System.Double. </returns>
    Public Function MinVoltage() As Double
        Return Me.MinVoltage(Me.SelectedChannel)
    End Function

    ''' <summary> Returns the subsystem minimum voltage. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> Specifies a reference to a
    ''' <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <returns> System.Double. </returns>
    Public Function MinVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        If channel Is Nothing Then Throw New ArgumentNullException(NameOf(channel))
        Return If(channel.Gain = 0, 0, MyBase.VoltageRange.Low / channel.Gain)
    End Function

    ''' <summary> Gets or Sets the condition as True have the system retrieve readings as long unwrapped values. </summary>
    ''' <value> <c>True</c> if retrieves unwrapped readings; otherwise, <c>False</c>. </value>
    Public Property RetrievesUnwrappedReadings() As Boolean

    ''' <summary> Gets or Sets the condition as True have the system retrieve readings. </summary>
    ''' <value> <c>True</c> if [retrieves readings]; otherwise, <c>False</c>. </value>
    Public Property RetrievesReadings() As Boolean

    ''' <summary> Gets or Sets the condition as True have the system retrieve Voltages. </summary>
    ''' <value> <c>True</c> if [retrieves voltages]; otherwise, <c>False</c>. </value>
    Public Property RetrievesVoltages() As Boolean

    ''' <summary> Returns a single reading from the selected channel. </summary>
    ''' <value> The single reading. </value>
    Public ReadOnly Property SingleReading() As Integer
        Get
            Return Me.SingleReading(Me.SelectedChannel)
        End Get
    End Property

    ''' <summary> Returns a single reading from the specified channel. </summary>
    ''' <value> The single reading. </value>
    Public ReadOnly Property SingleReading(ByVal channel As OpenLayers.Base.ChannelListEntry) As Integer
        Get
            If channel Is Nothing Then Throw New ArgumentNullException(NameOf(channel))
            Return Me.SingleReading(channel.PhysicalChannelNumber, channel.Gain)
        End Get
    End Property

    ''' <summary> Returns a single value from the sub system. </summary>
    ''' <value> The single reading. </value>
    Public ReadOnly Property SingleReading(ByVal physicalChannelNumber As Integer, ByVal gain As Double) As Integer
        Get
            Me._LastSingleReadingsChannel = physicalChannelNumber
            Dim value As Integer = MyBase.GetSingleValueAsRaw(physicalChannelNumber, gain)
            Me.LastSingleReading = value
            Return value
        End Get
    End Property

    ''' <summary> Returns a single voltage from the selected channel. </summary>
    ''' <value> The single voltage. </value>
    Public ReadOnly Property SingleVoltage() As Double
        Get
            Return Me.SingleVoltage(Me.SelectedChannel)
        End Get
    End Property

    ''' <summary> Returns a single voltage from the specified channel. </summary>
    ''' <value> The single voltage. </value>
    Public ReadOnly Property SingleVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        Get
            If channel Is Nothing Then
                Throw New ArgumentNullException(NameOf(channel))
            End If
            Me._LastSingleVoltagesChannel = channel.PhysicalChannelNumber
            Dim value As Double = MyBase.GetSingleValueAsVolts(channel.PhysicalChannelNumber, channel.Gain)
            Me.LastSingleVoltage = value
            Return value
        End Get
    End Property

    ''' <summary> Returns a single voltage from the specified channel. </summary>
    ''' <value> The single voltage. </value>
    Public ReadOnly Property SingleVoltage(ByVal physicalChannelNumber As Integer, ByVal gain As Double) As Double
        Get
            Me._LastSingleVoltagesChannel = physicalChannelNumber
            Dim value As Double = MyBase.GetSingleValueAsVolts(physicalChannelNumber, gain)
            Me.LastSingleVoltage = value
            Return value
        End Get
    End Property

#End Region

#Region " DATA EPOCH  "

    ''' <summary> Validates the data epoch. </summary>
    ''' <param name="dataArray">    Array of data. </param>
    ''' <param name="currentIndex"> Specifies the last data point collected into the array. </param>
    ''' <param name="pointCount">   Specifies the number of data points to return. </param>
    ''' <param name="currentCount"> Specifies the total number of valid points in the array. </param>
    ''' <param name="details">      [in,out] The details. </param>
    ''' <returns> <c>True</c> if valid; otherwise, <c>False</c>. </returns>
    Public Shared Function TryValidateDataEpoch(ByVal dataArray() As Double, ByVal currentIndex As Integer,
                                                ByVal pointCount As Integer, ByVal currentCount As Long, ByRef details As String) As Boolean

        If dataArray Is Nothing OrElse dataArray.Length = 0 Then
            details = "Data array is nothing or empty"
            Return False
        ElseIf pointCount < 1 OrElse pointCount > dataArray.Length Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                    "Point count {0} must be positive up to {1}", pointCount, dataArray.Length)
            Return False
        ElseIf currentIndex < dataArray.GetLowerBound(0) OrElse currentIndex > dataArray.GetUpperBound(0) Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                    "Current index {0} must be between {1} and {2}", currentIndex,
                                    dataArray.GetLowerBound(0), dataArray.GetUpperBound(0))
            Return False
        ElseIf currentIndex >= currentCount Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                    "Current index {0} must be between lower than the current count of valid values {1}", currentIndex, currentCount)
            Return False
        End If
        Return True

    End Function

    ''' <summary> Validates the data epoch. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="dataArray">    Array of data. </param>
    ''' <param name="currentIndex"> Specifies the last data point collected into the array. </param>
    ''' <param name="pointCount">   Specifies the number of data points to return. </param>
    ''' <param name="currentCount"> Specifies the total number of valid points in the array. </param>
    Public Shared Sub ValidateDataEpoch(ByVal dataArray() As Double, ByVal currentIndex As Integer,
                                     ByVal pointCount As Integer, ByVal currentCount As Long)

        If dataArray Is Nothing OrElse dataArray.Length = 0 Then

            Throw New ArgumentNullException(NameOf(dataArray))

        ElseIf pointCount < 1 OrElse pointCount > dataArray.Length Then

            Throw New ArgumentOutOfRangeException(NameOf(pointCount), pointCount,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                "Must be positive up to {0}", dataArray.Length))

        ElseIf currentIndex < dataArray.GetLowerBound(0) OrElse currentIndex > dataArray.GetUpperBound(0) Then

            Throw New ArgumentOutOfRangeException(NameOf(currentIndex), currentIndex,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                "Must be between {0} and {1}", dataArray.GetLowerBound(0), dataArray.GetUpperBound(0)))

        ElseIf currentIndex >= currentCount Then

            Throw New ArgumentOutOfRangeException(NameOf(currentIndex), currentIndex,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                "Must be between lower than the current count of valid values {0}", currentCount))
        End If

    End Sub

    ''' <summary> Return a voltages array from the specified voltages. </summary>
    ''' <remarks> Use <see cref="ValidateDataEpoch">validate</see> or
    ''' <see cref="TryValidateDataEpoch">try validate</see> to validate the argument. Validation is
    ''' excluded to improve speed. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dataArray">    Array of data. </param>
    ''' <param name="currentIndex"> Specifies the last data point collected into the array. </param>
    ''' <param name="pointCount">   Specifies the number of data points to return. </param>
    ''' <param name="currentCount"> Specifies the total number of valid points in the array. </param>
    ''' <returns> Voltages array from the specified voltages. </returns>
    ''' ### <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    ''' outside the required range. </exception>
    Public Shared Function DataEpoch(ByVal dataArray() As Double, ByVal currentIndex As Integer,
                                     ByVal pointCount As Integer, ByVal currentCount As Long) As Double()

        If dataArray Is Nothing Then
            Throw New ArgumentNullException(NameOf(dataArray))
        End If

        Dim epoch(pointCount - 1) As Double

        If currentIndex < 0 OrElse currentCount <= 0 Then

            ' if we have no data, return the zero-valued epoch
            Return epoch

        End If

        Dim i As Integer = currentIndex

        Dim k As Integer = pointCount
        If k > currentCount Then
            k = CInt(currentCount)
        End If

        ' fill data from the end of the data set to the end of the signal.
        Dim j As Integer = pointCount
        Do
            j -= 1
            epoch(j) = dataArray(i)
            i -= 1
            If i < 0 Then
                i = dataArray.Length - 1
            End If
            k -= 1
        Loop While k > 0
        Return epoch
    End Function

    ''' <summary> Returns the average. </summary>
    ''' <param name="dataArray"> . </param>
    ''' <returns> The average value. </returns>
    Public Shared Function Average(ByVal dataArray() As Double) As Double
        If dataArray Is Nothing OrElse dataArray.Length = 0 Then
            Return 0
        End If
        Dim sum As Double
        For Each value As Double In dataArray
            sum += value
        Next
        Return sum / dataArray.Length
    End Function

    ''' <summary> Returns the standard deviation. </summary>
    ''' <param name="dataArray"> . </param>
    ''' <param name="mean">      . </param>
    ''' <returns> The standard deviation. </returns>
    Public Shared Function StandardDeviation(ByVal dataArray() As Double, ByVal mean As Double) As Double
        If dataArray Is Nothing OrElse dataArray.Length = 0 Then
            Return 0
        ElseIf dataArray.Length = 1 Then
            Dim value As Double = dataArray(0) - mean
            Return value * value
        End If
        Dim sum As Double
        For Each value As Double In dataArray
            value -= mean
            sum += value * value
        Next
        Return sum / (dataArray.Length - 1)
    End Function

#End Region

#Region " BUFFERS "

    ''' <summary> The buffers. </summary>
    Private _Buffers() As Buffer

    ''' <summary> Select buffer. </summary>
    ''' <param name="bufferIndex"> Zero-based index of the buffer. </param>
    ''' <returns> An OpenLayers.Base.OlBuffer. </returns>
    Public Function SelectBuffer(ByVal bufferIndex As Integer) As OpenLayers.Base.OlBuffer
        Return Me._Buffers(bufferIndex)
    End Function

    ''' <summary> Fill buffers. </summary>
    ''' <param name="sampleValues"> The sample values. </param>
    <CLSCompliant(False)>
    Public Sub FillBuffers(ByVal sampleValues As UInteger()())
        Buffer.FillBuffers(sampleValues, Me._Buffers)
    End Sub

    ''' <summary> Allocates a set of buffers for data collection. </summary>
    ''' <param name="bufferCount">  The buffer count. </param>
    ''' <param name="bufferLength"> Length of the buffer. </param>
    Public Sub AllocateBuffers(ByVal bufferCount As Integer, ByVal bufferLength As Integer)
        ' save the buffer length
        Me._BufferLength = bufferLength

        ' allocate the buffers array.  This is needed to buffer can be queued when restarting.
        Me._Buffers = New Buffer(bufferCount - 1) {}

        ' allocate queue for data collection
        Dim i As Integer
        Do While i < bufferCount
            Me._Buffers(i) = New isr.IO.OL.Buffer(Me._BufferLength, Me)
            i += 1
        Loop
    End Sub

    ''' <summary> Gets the number of buffers that were allocated. </summary>
    ''' <value> The buffers count. </value>
    Public ReadOnly Property BuffersCount() As Integer
        Get
            Return If(Me._Buffers Is Nothing, 0, Me._Buffers.Length)
        End Get
    End Property

    Private _BufferLength As Integer
    ''' <summary> Gets buffer size that was allocated. </summary>
    ''' <value> The length of the buffer. </value>
    Public ReadOnly Property BufferLength() As Integer
        Get
            Return Me._BufferLength
        End Get
    End Property

    ''' <summary> Queue all allocated buffers. </summary>
    ''' <returns> An OpenLayers.Base.BufferQueue. </returns>
    Public Function QueueAllBuffers() As OpenLayers.Base.BufferQueue
        MyBase.BufferQueue.FreeAllQueuedBuffers()
        Return Me.QueueAllFreeBuffers()
    End Function

    ''' <summary> Queue all allocated buffers which states are Idle or Completed. </summary>
    Public Function QueueAllFreeBuffers() As OpenLayers.Base.BufferQueue
        For Each buffer As OpenLayers.Base.OlBuffer In Me._Buffers
            If buffer.State = OpenLayers.Base.OlBuffer.BufferState.Idle OrElse
                buffer.State = OpenLayers.Base.OlBuffer.BufferState.Completed Then
                MyBase.BufferQueue.QueueBuffer(buffer)
            End If
        Next
        Return MyBase.BufferQueue
    End Function

    ''' <summary> The voltages. </summary>
    Private _Voltages()() As Double

    ''' <summary> Returns a read only reference to the voltage array. </summary>
    ''' <returns> An array of <see cref="T:System.Double"/> .</returns>
    Public Function Voltages() As Double()()
        Return Me._Voltages
    End Function

    ''' <summary> The readings. </summary>
    Private _Readings()() As Integer

    ''' <summary> Returns a read only reference to the readings array. </summary>
    ''' <returns> An array of <see cref="T:System.UInteger"/> .</returns>
    Public Function Readings() As Integer()()
        Return Me._Readings
    End Function

#End Region

#Region " CHANNELS "

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> The channel number. </param>
    ''' <param name="gain">          The gain. </param>
    ''' <param name="name">          The name. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function AddChannel(ByVal channelNumber As Integer, ByVal gain As Double, ByVal name As String) As OpenLayers.Base.ChannelListEntry

        ' add the new channel to the channel list.
        Me._SelectedChannelLogicalNumber = MyBase.ChannelList.Count
        Me._SelectedChannel = MyBase.ChannelList.Add(channelNumber)

        ' set the channel name or leave the default name.
        If Not String.IsNullOrWhiteSpace(name) AndAlso Me.SelectedChannelInfo.Name <> name Then
            Me.SelectedChannelInfo.Name = name
        End If

        ' set channel gain.
        If MyBase.SupportsProgrammableGain Then
            Dim gainIndex As Integer = Me.SupportedGainIndex(gain)
            If gainIndex >= 0 Then
                If Me.SupportsIndividualGains Then
                    Me._SelectedChannel.Gain = gain
                Else
                    For Each channel As OpenLayers.Base.ChannelListEntry In MyBase.ChannelList
                        channel.Gain = gain
                    Next
                End If
            End If
        End If

        Return Me._SelectedChannel

    End Function

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> The channel number. </param>
    ''' <param name="gainIndex">     Index of the gain. </param>
    ''' <param name="name">          The name. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function AddChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer, ByVal name As String) As OpenLayers.Base.ChannelListEntry

        ' add the new channel to the channel list.
        Me._SelectedChannelLogicalNumber = MyBase.ChannelList.Count
        Me._SelectedChannel = MyBase.ChannelList.Add(channelNumber)

        ' set the channel name or leave the default name.
        If Not String.IsNullOrWhiteSpace(name) AndAlso Me.SelectedChannelInfo.Name <> name Then
            Me.SelectedChannelInfo.Name = name
        End If

        ' set channel gain.
        If MyBase.SupportsProgrammableGain Then
            If Me.SupportsIndividualGains Then
                Me._SelectedChannel.Gain = MyBase.SupportedGains()(gainIndex)
            Else
                For Each channel As OpenLayers.Base.ChannelListEntry In MyBase.ChannelList
                    channel.Gain = MyBase.SupportedGains()(gainIndex)
                Next
            End If
        End If

        Return Me._SelectedChannel

    End Function

    ''' <summary> The gain to find. </summary>
    Private _GainToFind As Double

    ''' <summary> A predicate to call for finding the specified gain in the supported gain list. </summary>
    ''' <param name="gain"> The gain. </param>
    ''' <returns> <c>True</c> if gain was found, <c>False</c> otherwise. </returns>
    Private Function FindGain(ByVal gain As Double) As Boolean
        Dim limen As Double = 0.0001
        Return Math.Abs(gain - Me._GainToFind) < limen
    End Function

    ''' <summary> Returns the index to the
    ''' <see cref="SupportedGains">supported gains</see> for this <paramref name="gain" />. </summary>
    ''' <param name="gain"> The gain. </param>
    ''' <returns> System.Int32. </returns>
    Public Function SupportedGainIndex(ByVal gain As Double) As Integer
        Me._GainToFind = gain
        Return Array.FindIndex(MyBase.SupportedGains, New Predicate(Of Double)(AddressOf Me.FindGain))
    End Function

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> The channel number. </param>
    ''' <param name="gainIndex">     Index of the gain. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function AddChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer) As OpenLayers.Base.ChannelListEntry

        ' add the new channel to the channel list.
        Me._SelectedChannelLogicalNumber = MyBase.ChannelList.Count
        Me._SelectedChannel = MyBase.ChannelList.Add(channelNumber)

        ' set channel gain.
        If MyBase.SupportsProgrammableGain Then
            If Me.SupportsIndividualGains Then
                Me._SelectedChannel.Gain = MyBase.SupportedGains()(gainIndex)
            Else
                For Each channel As OpenLayers.Base.ChannelListEntry In MyBase.ChannelList
                    channel.Gain = MyBase.SupportedGains()(gainIndex)
                Next
            End If
        End If

        Return Me._SelectedChannel

    End Function

    ''' <summary> Add a set of channels. </summary>
    ''' <param name="firstPhysicalChannel"> The first physical channel. </param>
    ''' <param name="lastPhysicalChannel">  The last physical channel. </param>
    ''' <returns> OpenLayers.Base.ChannelList. </returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber)

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    ''' <summary> Add a set of channels. </summary>
    ''' <param name="firstPhysicalChannel"> The first physical channel. </param>
    ''' <param name="lastPhysicalChannel">  The last physical channel. </param>
    ''' <param name="gain">                 The gain. </param>
    ''' <returns> OpenLayers.Base.ChannelList. </returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer,
                                ByVal gain As Double) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber).Gain = gain

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    ''' <summary> Returns count of Analog Input Channels. </summary>
    ''' <value> The available channels count. </value>
    Public ReadOnly Property AvailableChannelsCount() As Integer
        Get
            Return If(MyBase.ChannelType = OpenLayers.Base.ChannelType.Differential, MyBase.MaxDifferentialChannels, MyBase.MaxSingleEndedChannels)
        End Get
    End Property

    ''' <summary> Gets or sets the selected channel gain. </summary>
    ''' <value> The selected channel gain. </value>
    Public Property SelectedChannelGain() As Double
        Get
            If Me._SelectedChannel Is Nothing Then
                ' return zero if no channel selected - no exception allowed in Get methods.
                Return 0
            Else
                Return Me._SelectedChannel.Gain
            End If
        End Get
        Set(ByVal value As Double)
            If Me._SelectedChannel Is Nothing Then
                Throw New System.InvalidOperationException("No channel selected.")
            Else
                Me._SelectedChannel.Gain = value
            End If
        End Set
    End Property

    ''' <summary> The selected channel logical number. </summary>
    Private _SelectedChannelLogicalNumber As Integer

    ''' <summary> Gets the logical number (index) of the selected channel. </summary>
    ''' <value> The selected channel logical number. </value>
    Public ReadOnly Property SelectedChannelLogicalNumber() As Integer
        Get
            Return Me._SelectedChannelLogicalNumber
        End Get
    End Property

    ''' <summary> Returns the channel info for the selected channel.  If no channel was selected this
    ''' returns the info for physical channel 0. </summary>
    ''' <value> The selected channel info. </value>
    Public ReadOnly Property SelectedChannelInfo() As OpenLayers.Base.SupportedChannelInfo
        Get
            Return If(Me._SelectedChannel IsNot Nothing,
                MyBase.SupportedChannels.GetChannelInfo(Me._SelectedChannel.PhysicalChannelNumber),
                MyBase.SupportedChannels.GetChannelInfo(0))
        End Get
    End Property

    ''' <summary> The selected channel. </summary>
    Private _SelectedChannel As OpenLayers.Base.ChannelListEntry

    ''' <summary> Gets reference to the selected channel for this subsystem. </summary>
    ''' <value> The selected channel. </value>
    Public ReadOnly Property SelectedChannel() As OpenLayers.Base.ChannelListEntry
        Get
            Return Me._SelectedChannel
        End Get
    End Property

    ''' <summary> Gets the selected channel physical number. </summary>
    ''' <value> The selected channel physical number. </value>
    Public ReadOnly Property SelectedChannelPhysicalNumber() As Integer
        Get
            Return Me._SelectedChannel.PhysicalChannelNumber
        End Get
    End Property

    ''' <summary> Selects a channel. </summary>
    ''' <param name="logicalChannelNumber"> The logical channel number (channel index in the
    ''' <see cref="ChannelList">channel list</see>.) </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function SelectLogicalChannel(ByVal logicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        Me._SelectedChannel = MyBase.ChannelList.SelectLogicalChannel(logicalChannelNumber)
        Return Me._SelectedChannel
    End Function

    ''' <summary> Selects a channel from the subsystem channel list by its channel name. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channelName"> Specifies the physical channel number. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function SelectNamedChannel(ByVal channelName As String) As OpenLayers.Base.ChannelListEntry
        If String.IsNullOrWhiteSpace(channelName) Then
            Throw New ArgumentNullException(NameOf(channelName), "Channel name must not be empty.")
        Else
            Me._SelectedChannel = MyBase.ChannelList.SelectNamedChannel(channelName)
            Return Me._SelectedChannel
        End If
    End Function

    ''' <summary> Queries if a given named channel exists. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channelName"> Specifies the physical channel number. </param>
    ''' <returns> <c>True</c> if it succeeds, false if it fails. </returns>
    Public Function NamedChannelExists(ByVal channelName As String) As Boolean
        If String.IsNullOrWhiteSpace(channelName) Then
            Throw New ArgumentNullException(NameOf(channelName), "Channel name must not be empty.")
        Else
            Return MyBase.ChannelList.Contains(channelName)
        End If
    End Function

    ''' <summary> Selects a channel from the subsystem channel list by its physical channel number. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function SelectPhysicalChannel(ByVal physicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        If Me.PhysicalChannelExists(physicalChannelNumber) Then
            Me._SelectedChannelLogicalNumber = MyBase.ChannelList.LogicalChannelNumber(physicalChannelNumber)
            Me._SelectedChannel = MyBase.ChannelList.Item(Me._SelectedChannelLogicalNumber)
        End If
        Return Me._SelectedChannel
    End Function

    ''' <summary> Queries if a given physical channel exists. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns> <c>True</c> if it succeeds, false if it fails. </returns>
    Public Function PhysicalChannelExists(ByVal physicalChannelNumber As Integer) As Boolean

        Return MyBase.ChannelList.Contains(physicalChannelNumber)

    End Function

    ''' <summary> Updates a channel in the channel list. </summary>
    ''' <param name="channelNumber"> The channel number. </param>
    ''' <param name="gainIndex">     Index of the gain. </param>
    ''' <param name="name">          The name. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function UpdateChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer,
                                  ByVal name As String) As OpenLayers.Base.ChannelListEntry

        ' select the channel
        Me.SelectPhysicalChannel(channelNumber)

        ' set the channel name or leave the default name.
        If Not String.IsNullOrWhiteSpace(name) AndAlso Me.SelectedChannelInfo.Name <> name Then
            Me.SelectedChannelInfo.Name = name
        End If

        ' set channel gain.
        If MyBase.SupportsProgrammableGain Then
            If Me.SupportsIndividualGains Then
                Me._SelectedChannel.Gain = MyBase.SupportedGains()(gainIndex)
            Else
                For Each channel As OpenLayers.Base.ChannelListEntry In MyBase.ChannelList
                    channel.Gain = MyBase.SupportedGains()(gainIndex)
                Next
            End If
        End If

        Return Me._SelectedChannel

    End Function

#End Region

#Region " EVENT HANDLERS: BUFFER DONE "

    ''' <summary> Handles buffer done events. </summary>
    ''' <param name="sender"> Specifies reference to the
    ''' <see cref="OpenLayers.Base.SubsystemBase">subsystem</see> </param>
    ''' <param name="e">      Specifies the
    ''' <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>. </param>
    Private Sub HandleBufferDoneEvent(ByVal sender As Object, ByVal e As OpenLayers.Base.BufferDoneEventArgs)
        Me.OnBufferDone(e)
    End Sub

    ''' <summary> Handles buffer done events. </summary>
    ''' <param name="e"> Specifies the
    ''' <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>. </param>
    Protected Overridable Sub OnBufferDone(ByVal e As OpenLayers.Base.BufferDoneEventArgs)

        If e Is Nothing Then Return
        Dim activeBuffer As isr.IO.OL.Buffer = CType(e.OlBuffer, isr.IO.OL.Buffer)

        ' check if a buffer is ready to be processed.
        If activeBuffer.ValidSamples > 0 Then

            If Me.RetrievesReadings Then
                ' retrieve the readings
                Me._readings = activeBuffer.Readings(e.Subsystem.ChannelList)
            End If

            If Me.RetrievesVoltages Then

                Me._Voltages = activeBuffer.Voltages(e.Subsystem.ChannelList)

                Dim voltage As Double
                If Me._calculatesMeanSigma Then
                    For channelNumber As Integer = 0 To e.Subsystem.ChannelList.Count - 1
                        Dim sum As Double = 0
                        Dim ssq As Double = 0
                        Dim sampleCount As Integer = Me._voltages.GetUpperBound(channelNumber) + 1
                        For sampleNumber As Integer = 0 To sampleCount - 1
                            voltage = Me._voltages(channelNumber)(sampleNumber)
                            sum += voltage
                            ssq += voltage * voltage
                        Next
                        Dim average As Double = sum / sampleCount
                        Me._means(channelNumber) = average
                        Me._standardDeviations(channelNumber) = Math.Sqrt((ssq - average * average) / sampleCount)
                    Next
                End If

                ' MyBase.OnBufferDone(e)

            End If

        Else

            If Me.RetrievesReadings Then

                If Not Me._readings Is Nothing Then
                    ' clear the readings
                    Array.Clear(Me._readings, 0, Me._readings.Length)
                End If

            End If

            If Me.RetrievesVoltages Then

                If Not Me._voltages Is Nothing Then
                    Array.Clear(Me._voltages, 0, Me._voltages.Length)
                End If

                If Not Me._means Is Nothing Then
                    Array.Clear(Me.Means, 0, Me.Means.Length)
                End If

                If Not Me._standardDeviations Is Nothing Then
                    Array.Clear(Me._standardDeviations, 0, Me._standardDeviations.Length)
                End If

            End If

        End If

        ' recycle the buffer
        e.Subsystem.BufferQueue.QueueBuffer(activeBuffer)

    End Sub

#End Region

End Class

