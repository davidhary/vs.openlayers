Imports System.Collections.Generic
''' <summary>
''' Provides added functionality to the Data Translation Open Layers
''' <see cref="OpenLayers.Base.OlBuffer">buffer</see>.</summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Class Buffer
    Inherits Global.OpenLayers.Base.OlBuffer

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Public Sub New(ByVal sampleCount As Integer, ByVal subsystem As OpenLayers.Base.AnalogSubsystem)

        MyBase.New(sampleCount, subsystem)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.disposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Returns an Integer array of arrays. </summary>
    ''' <param name="channelCount">      Number of channels. </param>
    ''' <param name="samplesPerChannel"> The samples per channel. </param>
    ''' <returns> . </returns>
    Public Shared Function AllocateInteger(ByVal channelCount As Integer, ByVal samplesPerChannel As Integer) As Integer()()
        ' allocate array for readings data.
        Dim readings()() As Integer = New Integer(channelCount - 1)() {}
        For i As Integer = 0 To channelCount - 1
            readings(i) = New Integer(samplesPerChannel - 1) {}
        Next i
        Return readings
    End Function

    ''' <summary> Returns an empty Double array of arrays. </summary>
    ''' <param name="channelCount">      . </param>
    ''' <param name="samplesPerChannel"> . </param>
    ''' <returns> . </returns>
    Public Shared Function AllocateDouble(ByVal channelCount As Integer, ByVal samplesPerChannel As Integer) As Double()()
        ' allocate array for readings data.
        Dim voltages()() As Double = New Double(channelCount - 1)() {}
        For i As Integer = 0 To channelCount - 1
            voltages(i) = New Double(samplesPerChannel - 1) {}
        Next i
        Return voltages
    End Function

    ''' <summary>Retrieves double-precision data from the buffer to an array of arrays.</summary>
    ''' <param name="buffer">Specifies the buffer.</param>
    ''' <param name="channelList">Specifies the channel list.</param>
    ''' <returns>Number of valid samples.</returns>
    Public Shared Function Voltages(ByVal buffer As OpenLayers.Base.OlBuffer,
                                    ByVal channelList As OpenLayers.Base.ChannelList) As Double()()

        If buffer Is Nothing Then Throw New ArgumentNullException(NameOf(buffer))
        If channelList Is Nothing Then Throw New ArgumentNullException(NameOf(channelList))

        Dim data(channelList.Count - 1)() As Double
        For i As Integer = 0 To channelList.Count - 1
            data(i) = buffer.GetDataAsVolts(channelList.Item(i))
        Next
        'For Each channel As OpenLayers.Base.ChannelListEntry In channelList
        ' LogicalChannelNumber not defined data(channel.LogicalChannelNumber) = buffer.GetDataAsVolts(channel)
        ' Next

        Return data

    End Function

    ''' <summary> Fill buffers. </summary>
    ''' <param name="readings"> The readings. </param>
    ''' <param name="buffers">  The buffers. </param>
    <CLSCompliant(False)>
    Public Shared Sub FillBuffers(ByVal readings As UInteger()(), ByVal buffers() As Global.OpenLayers.Base.OlBuffer)
        Dim samples(readings.GetUpperBound(0)) As UInteger
        Dim buffer As OpenLayers.Base.OlBuffer = Nothing
        Dim channelCount As Integer = readings.GetUpperBound(0) + 1
        Dim samplesPerChannel As Integer = CInt(buffer.BufferSizeInSamples / channelCount)
        Dim startingIndex As Integer = 0
        For iBuffer As Integer = 1 To buffers.Count
            buffer = buffers(iBuffer - 1)
            ' now we must place the data in the buffer.
            For channelIndex As Integer = 0 To readings.GetUpperBound(0)
                For sampleIndex As Integer = startingIndex To startingIndex + samplesPerChannel - 1
                    samples(sampleIndex) = readings(channelIndex)(sampleIndex)
                Next sampleIndex
            Next channelIndex
            startingIndex += samplesPerChannel
            buffer.PutDataAsRaw(samples)
        Next iBuffer
    End Sub

    ''' <summary> Allocates a set of buffers for data collection. </summary>
    ''' <param name="bufferCount">  The buffer count. </param>
    ''' <param name="bufferLength"> Length of the buffer. </param>
    Public Shared Function AllocateBuffers(ByVal bufferCount As Integer, ByVal bufferLength As Integer,
                                           ByVal subsystem As OpenLayers.Base.AnalogSubsystem) As IEnumerable(Of Buffer)
        ' allocate the buffers array.  This is needed to buffer can be queued when restarting.
        Dim buffers As New List(Of Buffer)
        Do While buffers.Count < bufferCount
            buffers.Add(New Buffer(bufferLength, subsystem))
        Loop
        Return buffers
    End Function

#End Region

#Region " METHODS "

    ''' <summary>Retrieves voltages from the buffer to an array of arrays.</summary>
    ''' <param name="channelList">Specifies the channel list.</param>
    ''' <returns>Number of valid samples.</returns>
    Public Function Voltages(ByVal channelList As OpenLayers.Base.ChannelList) As Double()()
        If channelList Is Nothing Then Throw New ArgumentNullException(NameOf(channelList))
        Dim data(channelList.Count - 1)() As Double
        For i As Integer = 0 To channelList.Count - 1
            data(i) = MyBase.GetDataAsVolts(channelList.Item(i))
        Next
        Return data

    End Function

    ''' <summary>Retrieves readings from the buffer to an array of arrays.</summary>
    ''' <param name="channelList">Specifies the channel list.</param>
    ''' <returns>Number of valid samples.</returns>
    <CLSCompliant(False)>
    Public Function RawReadings(ByVal channelList As OpenLayers.Base.ChannelList) As UInteger()()
        If channelList Is Nothing Then Throw New ArgumentNullException(NameOf(channelList))
        Dim data(channelList.Count - 1)() As UInt32
        For i As Integer = 0 To channelList.Count - 1
            data(i) = MyBase.GetDataAsRawUInt32(channelList.Item(i))
        Next
        Return data
    End Function

    ''' <summary>
    ''' Retrieves readings from the buffer to an array of arrays. If any unsigned integer value is
    ''' bigger than the maximum integer value, it is simply wrapped around to the corresponding
    ''' negative integer value. 
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channelList"> Specifies the channel list. </param>
    ''' <returns> Number of valid samples. </returns>
    Public Function Readings(ByVal channelList As OpenLayers.Base.ChannelList) As Integer()()
        If channelList Is Nothing Then Throw New ArgumentNullException(NameOf(channelList))
        Dim data(channelList.Count - 1)() As Integer
        For i As Integer = 0 To channelList.Count - 1
            ' https://www.c-sharpcorner.com/uploadfile/b942f9/how-to-convert-unsigned-integer-arrays-to-signed-arrays-and-vice-versa/
            data(i) = CType(CType(MyBase.GetDataAsRawUInt32(channelList.Item(i)), Object), Integer())
        Next
        Return data
    End Function

    ''' <summary> Unwrapped readings. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channelList"> Specifies the channel list. </param>
    ''' <returns> An Integer()() </returns>
    Public Function UnwrappedReadings(ByVal channelList As OpenLayers.Base.ChannelList) As Long()()
        If channelList Is Nothing Then Throw New ArgumentNullException(NameOf(channelList))
        Dim data(channelList.Count - 1)() As Long
        For i As Integer = 0 To channelList.Count - 1
            data(i) = CType(CType(MyBase.GetDataAsRawUInt32(channelList.Item(i)), Object), Long())
        Next
        Return data
    End Function

#End Region

End Class
