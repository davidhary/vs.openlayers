﻿''' <summary>
''' reads device info.
''' </summary>
''' <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Class DeviceInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DeviceInfo" /> class.
    ''' </summary>
    ''' <param name="deviceName">Name of the device.</param>
    Public Sub New(ByVal deviceName As String)

        MyBase.new()
        Me._DeviceName = deviceName
        Me._SerialNumber = -1
        Me._ProductId = -1
        Me._FetchBoardInfo()

    End Sub

#End Region

#Region " DEVICE INFORMATION "

    ''' <summary>
    ''' Gets or sets the name of the device.
    ''' </summary>
    ''' <value>The name of the device.</value>
    Public Property DeviceName As String

    Private _SerialNumber As Integer
    ''' <summary>Returns the board serial number.
    ''' Contains the year (1 or 2 digits), week (1 or 2 digits), test station (1 digit), 
    ''' and sequence number (3 digits) of the device when it was tested in Manufacturing. 
    ''' For example, if BoardId contains the value 5469419, this device was tested in 2005, 
    ''' week 46, on test station 9, and is unit number 419. 
    ''' </summary>
    Public ReadOnly Property SerialNumber() As Integer
        Get
            If Me._SerialNumber < 0 Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._SerialNumber
        End Get
    End Property

    Private _DeviceId As Integer
    ''' <summary>
    ''' Returns the version number of the product. 
    ''' If only one version of the product exists, this number is 1. 
    ''' If two versions of the product exist, this number could be 1 or 2. 
    ''' </summary>
    Public ReadOnly Property DeviceId() As Integer
        Get
            If Me._DeviceId < 0 Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._DeviceId
        End Get
    End Property

    Private _DriverVersion As String
    ''' <summary>
    ''' Gets the board driver version.
    ''' </summary>
    ''' <value>The driver version.</value>
    Public ReadOnly Property DriverVersion() As String
        Get
            If String.IsNullOrWhiteSpace(Me._DriverVersion) Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._DriverVersion
        End Get
    End Property

    Private _InstanceNumber As Int32
    ''' <summary>
    ''' Gets the instance number of the board.
    ''' </summary>
    ''' <value>The instance number.</value>
    ''' <remarks>
    ''' Instance number not provided with the .NET SDK. 
    ''' This number is set to 1.
    ''' </remarks>
    Public ReadOnly Property InstanceNumber() As Int32
        Get
            If Me._InstanceNumber <= 0 Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._InstanceNumber
        End Get
    End Property

    Private _ProductId As Integer
    ''' <summary>
    ''' Gets the product identification number, such as DT9832.
    ''' </summary>
    ''' <value>The board product id.</value>
    Public ReadOnly Property ProductId() As Integer
        Get
            If Me._ProductId < 0 Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._ProductId
        End Get
    End Property

    Private _SoftwareVersion As String
    ''' <summary>Gets the board software SDK version.</summary>
    Public ReadOnly Property SoftwareVersion() As String
        Get
            If String.IsNullOrWhiteSpace(Me._SoftwareVersion) Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._SoftwareVersion
        End Get
    End Property

    Private _VendorId As Integer
    ''' <summary>
    ''' Gets the identification number of the vendor.
    ''' For most devices, this is 0x087 hexadecimal
    ''' </summary>
    ''' <value>The vendor id.</value>
    Public ReadOnly Property VendorId() As Integer
        Get
            If Me._VendorId < 0 Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._VendorId
        End Get
    End Property

    Private _DriverName As String = String.Empty
    ''' <summary>
    ''' Gets the open layers driver name.
    ''' e.g., DT9816
    ''' </summary>
    ''' <value>The name of the driver.</value>
    Public ReadOnly Property DriverName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._driverName) Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._driverName
        End Get
    End Property

    Private _ModuleName As String = String.Empty
    ''' <summary>
    ''' Gets the Module name.
    ''' e.g., DT9816-A
    ''' </summary>
    ''' <value>The name of the module.</value>
    Public ReadOnly Property ModuleName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._moduleName) Then
                If Me.FetchBoardInfo() Then
                End If
            End If
            Return Me._moduleName
        End Get
    End Property

#End Region

#Region " DEVICE MANAGER "

    ''' <summary>
    ''' Fetches the board information from the device.
    ''' </summary>
    ''' <returns><c>True</c> if success, <c>False</c> otherwise</returns>
    Private Function _FetchBoardInfo() As Boolean

        If String.IsNullOrWhiteSpace(Me._DeviceName) Then
            Return False
        End If

        If Not DeviceInfo.HardwareExists Then
            Return False
        End If

        If Not Me.Exists Then
            Return False
        End If

        ' get the device.
        Using device As New OpenLayers.Base.Device(Me.DeviceName)
            Me._DriverVersion = device.DriverVersion ' "V6.7.3.0"
            My.MyLibrary.DoEvents()

            ' This method is not provided in the new Open Layers
            ' Me._boardSoftwareVersion = isr.Drivers.OL.Helper.SoftwareVersion()
            Me._SoftwareVersion = OpenLayers.Base.DeviceMgr.Get.DeviceDriverVersion(Me.DeviceName)
            Me._DriverName = device.DriverName ' "DT9816"
            Me._ModuleName = device.BoardModelName ' "DT9816-A"

            ' update the board hardware information
            Dim hardwareInfo As OpenLayers.Base.HardwareInfo = device.GetHardwareInfo
            Me._InstanceNumber = 1
            Me._SerialNumber = hardwareInfo.BoardId ' 22639
            Me._ProductId = hardwareInfo.ProductId ' 38934
            Me._DeviceId = hardwareInfo.DeviceId ' 1
            Me._VendorId = hardwareInfo.VendorId ' 2151
        End Using

        Return Me.SerialNumber > 0

    End Function

    ''' <summary>
    ''' Fetches the board information from the device.
    ''' </summary>
    ''' <returns><c>True</c> if success, <c>False</c> otherwise</returns>
    Public Function FetchBoardInfo() As Boolean
        Return Me._FetchBoardInfo
    End Function

    ''' <summary>
    ''' Gets a value indicating whether hardware exists.
    ''' </summary>
    ''' <value><c>True</c> if hardware exists; otherwise, <c>False</c>.</value>
    Public Shared ReadOnly Property HardwareExists() As Boolean
        Get
            Dim affirmative As Boolean = OpenLayers.Base.DeviceMgr.Get.HardwareAvailable
            My.MyLibrary.DoEvents()
            Return affirmative
        End Get
    End Property

    ''' <summary>
    ''' Determines whether the device exists.
    ''' </summary>
    ''' <returns><c>True</c> if this device exists; otherwise, <c>False</c>.</returns>
    Public Function Exists() As Boolean
        Dim affirmative As Boolean = OpenLayers.Base.DeviceMgr.Get.Exists(Me.DeviceName)
        My.MyLibrary.DoEvents()
        Return affirmative
    End Function

#End Region

End Class
