﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.OpenLayers

        Public Const AssemblyTitle As String = "Open Layers Library"
        Public Const AssemblyDescription As String = "Open Layers Library"
        Public Const AssemblyProduct As String = "Open.Layers.Library"

    End Class

End Namespace

