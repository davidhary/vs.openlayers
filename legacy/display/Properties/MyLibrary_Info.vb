﻿Namespace My

    Partial Public NotInheritable Class MyLibrary

        Private Sub New()
            MyBase.New
        End Sub

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = isr.IO.OL.My.ProjectTraceEventId.OpenLayersDisplay

        Public Const AssemblyTitle As String = "Open Layers Display Library"
        Public Const AssemblyDescription As String = "Open Layers Display Library"
        Public Const AssemblyProduct As String = "Open.Layers.Display.Library"

    End Class

End Namespace

