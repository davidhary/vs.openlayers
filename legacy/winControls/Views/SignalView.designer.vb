<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class SignalView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ColorDialog = New System.Windows.Forms.ColorDialog()
        Me._Display = New OpenLayers.Controls.Display()
        Me._RefreshTimeLabel = New System.Windows.Forms.Label()
        Me._RampCheckBox = New System.Windows.Forms.CheckBox()
        Me._PrintButton = New System.Windows.Forms.Button()
        Me._TimeLabel = New System.Windows.Forms.Label()
        Me._YMax2Label = New System.Windows.Forms.Label()
        Me._YMin2Label = New System.Windows.Forms.Label()
        Me._AutoScaleButton = New System.Windows.Forms.Button()
        Me._ColorGridsButton = New System.Windows.Forms.Button()
        Me._Seperator2 = New System.Windows.Forms.Panel()
        Me._Seperator1 = New System.Windows.Forms.Panel()
        Me._ColorAxesButton = New System.Windows.Forms.Button()
        Me._ColorDataButton = New System.Windows.Forms.Button()
        Me._SineWaveCheckBox = New System.Windows.Forms.CheckBox()
        Me._SquareWaveCheckBox = New System.Windows.Forms.CheckBox()
        Me._XMinTrackBar = New System.Windows.Forms.TrackBar()
        Me._XMaxLabel = New System.Windows.Forms.Label()
        Me._XMinLabel = New System.Windows.Forms.Label()
        Me._YAxisLabel = New System.Windows.Forms.Label()
        Me._MultipleRadioButton = New System.Windows.Forms.RadioButton()
        Me._SingleRadioButton = New System.Windows.Forms.RadioButton()
        Me._XAxisLabel = New System.Windows.Forms.Label()
        Me._BandModeGroupBox = New System.Windows.Forms.GroupBox()
        Me._YMinTrackBar = New System.Windows.Forms.TrackBar()
        Me._YMax1Label = New System.Windows.Forms.Label()
        Me._YMin1Label = New System.Windows.Forms.Label()
        Me._SignalListComboBox = New System.Windows.Forms.ComboBox()
        Me._YMaxTrackBar = New System.Windows.Forms.TrackBar()
        Me._XMaxTrackBar = New System.Windows.Forms.TrackBar()
        Me._CreateDataTimer = New System.Windows.Forms.Timer(Me.components)
        CType(Me._Display, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._XMinTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._BandModeGroupBox.SuspendLayout()
        CType(Me._YMinTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._YMaxTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._XMaxTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Display
        '
        Me._Display.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._Display.AutoScale = False
        Me._Display.AxesColor = System.Drawing.Color.Blue
        Me._Display.BackGradientAngle = 90.0!
        Me._Display.BackGradientColor = System.Drawing.Color.Chartreuse
        Me._Display.BackgroundStyle = OpenLayers.Chart.Layers.BackgroundStyle.Gradient
        Me._Display.BandMode = OpenLayers.Controls.BandMode.SingleBand
        Me._Display.Footer = String.Empty
        Me._Display.FooterFont = New System.Drawing.Font(Me.Font.FontFamily, 8.0!)
        Me._Display.GridColor = System.Drawing.Color.Blue
        Me._Display.Location = New System.Drawing.Point(141, 13)
        Me._Display.Name = "_Display"
        Me._Display.SignalBufferLength = CType(0, Long)
        Me._Display.Size = New System.Drawing.Size(604, 362)
        Me._Display.TabIndex = 76
        Me._Display.Title = "Chart"
        Me._Display.TitleFont = New System.Drawing.Font(Me.Font.FontFamily, 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me._Display.XDataCurrentRangeMax = 1000.0R
        Me._Display.XDataCurrentRangeMin = 0.0R
        Me._Display.XDataName = "Time"
        Me._Display.XDataRangeMax = 1000.0R
        Me._Display.XDataRangeMin = 0.0R
        Me._Display.XDataUnit = "sec"
        '
        '_RefreshTimeLabel
        '
        Me._RefreshTimeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._RefreshTimeLabel.AutoSize = True
        Me._RefreshTimeLabel.Location = New System.Drawing.Point(514, 378)
        Me._RefreshTimeLabel.Name = "_RefreshTimeLabel"
        Me._RefreshTimeLabel.Size = New System.Drawing.Size(132, 17)
        Me._RefreshTimeLabel.TabIndex = 75
        Me._RefreshTimeLabel.Text = "Signals Refresh Time:"
        Me._RefreshTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_RampCheckBox
        '
        Me._RampCheckBox.AutoSize = True
        Me._RampCheckBox.Location = New System.Drawing.Point(16, 348)
        Me._RampCheckBox.Name = "_RampCheckBox"
        Me._RampCheckBox.Size = New System.Drawing.Size(97, 21)
        Me._RampCheckBox.TabIndex = 74
        Me._RampCheckBox.Text = "Ramp Wave"
        '
        '_PrintButton
        '
        Me._PrintButton.Location = New System.Drawing.Point(24, 261)
        Me._PrintButton.Name = "_PrintButton"
        Me._PrintButton.Size = New System.Drawing.Size(72, 32)
        Me._PrintButton.TabIndex = 73
        Me._PrintButton.Text = "Print"
        '
        '_TimeLabel
        '
        Me._TimeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._TimeLabel.Location = New System.Drawing.Point(652, 378)
        Me._TimeLabel.Name = "_TimeLabel"
        Me._TimeLabel.Size = New System.Drawing.Size(93, 16)
        Me._TimeLabel.TabIndex = 72
        '
        '_YMax2Label
        '
        Me._YMax2Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YMax2Label.AutoSize = True
        Me._YMax2Label.Cursor = System.Windows.Forms.Cursors.Default
        Me._YMax2Label.Location = New System.Drawing.Point(434, 501)
        Me._YMax2Label.Name = "_YMax2Label"
        Me._YMax2Label.Size = New System.Drawing.Size(60, 17)
        Me._YMax2Label.TabIndex = 71
        Me._YMax2Label.Text = "(0...1500)"
        '
        '_YMin2Label
        '
        Me._YMin2Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YMin2Label.AutoSize = True
        Me._YMin2Label.Location = New System.Drawing.Point(153, 501)
        Me._YMin2Label.Name = "_YMin2Label"
        Me._YMin2Label.Size = New System.Drawing.Size(79, 17)
        Me._YMin2Label.TabIndex = 70
        Me._YMin2Label.Text = "(-500...1000)"
        '
        '_AutoScaleButton
        '
        Me._AutoScaleButton.Location = New System.Drawing.Point(24, 213)
        Me._AutoScaleButton.Name = "_AutoScaleButton"
        Me._AutoScaleButton.Size = New System.Drawing.Size(72, 32)
        Me._AutoScaleButton.TabIndex = 60
        Me._AutoScaleButton.Text = "Auto Scale"
        '
        '_ColorGridsButton
        '
        Me._ColorGridsButton.Location = New System.Drawing.Point(24, 165)
        Me._ColorGridsButton.Name = "_ColorGridsButton"
        Me._ColorGridsButton.Size = New System.Drawing.Size(72, 32)
        Me._ColorGridsButton.TabIndex = 59
        Me._ColorGridsButton.Text = "Grids color"
        '
        '_Seperator2
        '
        Me._Seperator2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._Seperator2.BackColor = System.Drawing.SystemColors.Control
        Me._Seperator2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._Seperator2.ForeColor = System.Drawing.SystemColors.ControlText
        Me._Seperator2.Location = New System.Drawing.Point(0, 411)
        Me._Seperator2.Name = "_Seperator2"
        Me._Seperator2.Size = New System.Drawing.Size(758, 1)
        Me._Seperator2.TabIndex = 69
        '
        '_Seperator1
        '
        Me._Seperator1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._Seperator1.BackColor = System.Drawing.SystemColors.Control
        Me._Seperator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._Seperator1.ForeColor = System.Drawing.SystemColors.ControlText
        Me._Seperator1.Location = New System.Drawing.Point(11, 469)
        Me._Seperator1.Name = "_Seperator1"
        Me._Seperator1.Size = New System.Drawing.Size(758, 1)
        Me._Seperator1.TabIndex = 68
        '
        '_ColorAxesButton
        '
        Me._ColorAxesButton.Location = New System.Drawing.Point(24, 117)
        Me._ColorAxesButton.Name = "_ColorAxesButton"
        Me._ColorAxesButton.Size = New System.Drawing.Size(72, 32)
        Me._ColorAxesButton.TabIndex = 58
        Me._ColorAxesButton.Text = "Axes color"
        '
        '_ColorDataButton
        '
        Me._ColorDataButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._ColorDataButton.Location = New System.Drawing.Point(673, 485)
        Me._ColorDataButton.Name = "_ColorDataButton"
        Me._ColorDataButton.Size = New System.Drawing.Size(72, 31)
        Me._ColorDataButton.TabIndex = 57
        Me._ColorDataButton.Text = "Signal color"
        '
        '_SineWaveCheckBox
        '
        Me._SineWaveCheckBox.AutoSize = True
        Me._SineWaveCheckBox.Location = New System.Drawing.Point(16, 315)
        Me._SineWaveCheckBox.Name = "_SineWaveCheckBox"
        Me._SineWaveCheckBox.Size = New System.Drawing.Size(87, 21)
        Me._SineWaveCheckBox.TabIndex = 50
        Me._SineWaveCheckBox.Text = "Sine Wave"
        '
        '_SquareWaveCheckBox
        '
        Me._SquareWaveCheckBox.AutoSize = True
        Me._SquareWaveCheckBox.Location = New System.Drawing.Point(16, 381)
        Me._SquareWaveCheckBox.Name = "_SquareWaveCheckBox"
        Me._SquareWaveCheckBox.Size = New System.Drawing.Size(104, 21)
        Me._SquareWaveCheckBox.TabIndex = 51
        Me._SquareWaveCheckBox.Text = "Square Wave"
        '
        '_XMinTrackBar
        '
        Me._XMinTrackBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMinTrackBar.LargeChange = 100
        Me._XMinTrackBar.Location = New System.Drawing.Point(236, 416)
        Me._XMinTrackBar.Maximum = 990
        Me._XMinTrackBar.Name = "_XMinTrackBar"
        Me._XMinTrackBar.Size = New System.Drawing.Size(160, 45)
        Me._XMinTrackBar.SmallChange = 10
        Me._XMinTrackBar.TabIndex = 52
        Me._XMinTrackBar.TickFrequency = 100
        '
        '_XMaxLabel
        '
        Me._XMaxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMaxLabel.AutoSize = True
        Me._XMaxLabel.Location = New System.Drawing.Point(406, 430)
        Me._XMaxLabel.Name = "_XMaxLabel"
        Me._XMaxLabel.Size = New System.Drawing.Size(88, 17)
        Me._XMaxLabel.TabIndex = 65
        Me._XMaxLabel.Text = "max (0...1000)"
        '
        '_XMinLabel
        '
        Me._XMinLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMinLabel.AutoSize = True
        Me._XMinLabel.Location = New System.Drawing.Point(147, 430)
        Me._XMinLabel.Name = "_XMinLabel"
        Me._XMinLabel.Size = New System.Drawing.Size(85, 17)
        Me._XMinLabel.TabIndex = 64
        Me._XMinLabel.Text = "min (0...1000)"
        '
        '_YAxisLabel
        '
        Me._YAxisLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YAxisLabel.AutoSize = True
        Me._YAxisLabel.Location = New System.Drawing.Point(8, 492)
        Me._YAxisLabel.Name = "_YAxisLabel"
        Me._YAxisLabel.Size = New System.Drawing.Size(46, 17)
        Me._YAxisLabel.TabIndex = 63
        Me._YAxisLabel.Text = "Y-Axis:"
        '
        '_MultipleRadioButton
        '
        Me._MultipleRadioButton.AutoSize = True
        Me._MultipleRadioButton.Location = New System.Drawing.Point(16, 56)
        Me._MultipleRadioButton.Name = "_MultipleRadioButton"
        Me._MultipleRadioButton.Size = New System.Drawing.Size(73, 21)
        Me._MultipleRadioButton.TabIndex = 1
        Me._MultipleRadioButton.Text = "Multiple"
        '
        '_SingleRadioButton
        '
        Me._SingleRadioButton.AutoSize = True
        Me._SingleRadioButton.Checked = True
        Me._SingleRadioButton.Location = New System.Drawing.Point(16, 24)
        Me._SingleRadioButton.Name = "_SingleRadioButton"
        Me._SingleRadioButton.Size = New System.Drawing.Size(61, 21)
        Me._SingleRadioButton.TabIndex = 0
        Me._SingleRadioButton.TabStop = True
        Me._SingleRadioButton.Text = "Single"
        '
        '_XAxisLabel
        '
        Me._XAxisLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XAxisLabel.AutoSize = True
        Me._XAxisLabel.Location = New System.Drawing.Point(8, 430)
        Me._XAxisLabel.Name = "_XAxisLabel"
        Me._XAxisLabel.Size = New System.Drawing.Size(47, 17)
        Me._XAxisLabel.TabIndex = 62
        Me._XAxisLabel.Text = "X-Axis:"
        '
        '_BandModeGroupBox
        '
        Me._BandModeGroupBox.Controls.Add(Me._MultipleRadioButton)
        Me._BandModeGroupBox.Controls.Add(Me._SingleRadioButton)
        Me._BandModeGroupBox.Location = New System.Drawing.Point(16, 5)
        Me._BandModeGroupBox.Name = "_BandModeGroupBox"
        Me._BandModeGroupBox.Size = New System.Drawing.Size(104, 92)
        Me._BandModeGroupBox.TabIndex = 61
        Me._BandModeGroupBox.TabStop = False
        Me._BandModeGroupBox.Text = "Band mode"
        '
        '_YMinTrackBar
        '
        Me._YMinTrackBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YMinTrackBar.LargeChange = 100
        Me._YMinTrackBar.Location = New System.Drawing.Point(236, 477)
        Me._YMinTrackBar.Maximum = 1490
        Me._YMinTrackBar.Minimum = -500
        Me._YMinTrackBar.Name = "_YMinTrackBar"
        Me._YMinTrackBar.Size = New System.Drawing.Size(160, 45)
        Me._YMinTrackBar.SmallChange = 10
        Me._YMinTrackBar.TabIndex = 55
        Me._YMinTrackBar.TickFrequency = 100
        '
        '_YMax1Label
        '
        Me._YMax1Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YMax1Label.AutoSize = True
        Me._YMax1Label.Location = New System.Drawing.Point(462, 478)
        Me._YMax1Label.Name = "_YMax1Label"
        Me._YMax1Label.Size = New System.Drawing.Size(32, 17)
        Me._YMax1Label.TabIndex = 67
        Me._YMax1Label.Text = "max"
        '
        '_YMin1Label
        '
        Me._YMin1Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YMin1Label.AutoSize = True
        Me._YMin1Label.Location = New System.Drawing.Point(203, 478)
        Me._YMin1Label.Name = "_YMin1Label"
        Me._YMin1Label.Size = New System.Drawing.Size(29, 17)
        Me._YMin1Label.TabIndex = 66
        Me._YMin1Label.Text = "min"
        '
        '_SignalListComboBox
        '
        Me._SignalListComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SignalListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SignalListComboBox.Location = New System.Drawing.Point(61, 488)
        Me._SignalListComboBox.Name = "_SignalListComboBox"
        Me._SignalListComboBox.Size = New System.Drawing.Size(80, 25)
        Me._SignalListComboBox.TabIndex = 54
        '
        '_YMaxTrackBar
        '
        Me._YMaxTrackBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._YMaxTrackBar.LargeChange = 100
        Me._YMaxTrackBar.Location = New System.Drawing.Point(500, 477)
        Me._YMaxTrackBar.Maximum = 1500
        Me._YMaxTrackBar.Minimum = 10
        Me._YMaxTrackBar.Name = "_YMaxTrackBar"
        Me._YMaxTrackBar.Size = New System.Drawing.Size(160, 45)
        Me._YMaxTrackBar.SmallChange = 10
        Me._YMaxTrackBar.TabIndex = 56
        Me._YMaxTrackBar.TickFrequency = 100
        Me._YMaxTrackBar.Value = 1000
        '
        '_XMaxTrackBar
        '
        Me._XMaxTrackBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._XMaxTrackBar.LargeChange = 100
        Me._XMaxTrackBar.Location = New System.Drawing.Point(500, 416)
        Me._XMaxTrackBar.Maximum = 1000
        Me._XMaxTrackBar.Minimum = 10
        Me._XMaxTrackBar.Name = "_XMaxTrackBar"
        Me._XMaxTrackBar.Size = New System.Drawing.Size(160, 45)
        Me._XMaxTrackBar.SmallChange = 10
        Me._XMaxTrackBar.TabIndex = 53
        Me._XMaxTrackBar.TickFrequency = 100
        Me._XMaxTrackBar.Value = 1000
        '
        '_CreateDataTimer
        '
        Me._CreateDataTimer.Interval = 1000
        '
        'SignalDisplay
        '
        Me.ClientSize = New System.Drawing.Size(758, 531)
        Me.Controls.Add(Me._Display)
        Me.Controls.Add(Me._RefreshTimeLabel)
        Me.Controls.Add(Me._RampCheckBox)
        Me.Controls.Add(Me._PrintButton)
        Me.Controls.Add(Me._TimeLabel)
        Me.Controls.Add(Me._YMax2Label)
        Me.Controls.Add(Me._YMin2Label)
        Me.Controls.Add(Me._AutoScaleButton)
        Me.Controls.Add(Me._ColorGridsButton)
        Me.Controls.Add(Me._Seperator2)
        Me.Controls.Add(Me._Seperator1)
        Me.Controls.Add(Me._ColorAxesButton)
        Me.Controls.Add(Me._ColorDataButton)
        Me.Controls.Add(Me._SineWaveCheckBox)
        Me.Controls.Add(Me._SquareWaveCheckBox)
        Me.Controls.Add(Me._XMinTrackBar)
        Me.Controls.Add(Me._XMaxLabel)
        Me.Controls.Add(Me._XMinLabel)
        Me.Controls.Add(Me._YAxisLabel)
        Me.Controls.Add(Me._XAxisLabel)
        Me.Controls.Add(Me._BandModeGroupBox)
        Me.Controls.Add(Me._YMinTrackBar)
        Me.Controls.Add(Me._YMax1Label)
        Me.Controls.Add(Me._YMin1Label)
        Me.Controls.Add(Me._SignalListComboBox)
        Me.Controls.Add(Me._YMaxTrackBar)
        Me.Controls.Add(Me._XMaxTrackBar)
        Me.Name = "SignalDisplay"
        Me.Text = "Signal Display"
        CType(Me._Display, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._XMinTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me._BandModeGroupBox.ResumeLayout(False)
        Me._BandModeGroupBox.PerformLayout()
        CType(Me._YMinTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._YMaxTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._XMaxTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ColorDialog As System.Windows.Forms.ColorDialog
    Private WithEvents _Display As OpenLayers.Controls.Display
    Private WithEvents _RefreshTimeLabel As System.Windows.Forms.Label
    Private WithEvents _RampCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PrintButton As System.Windows.Forms.Button
    Private WithEvents _TimeLabel As System.Windows.Forms.Label
    Private WithEvents _YMax2Label As System.Windows.Forms.Label
    Private WithEvents _YMin2Label As System.Windows.Forms.Label
    Private WithEvents _AutoScaleButton As System.Windows.Forms.Button
    Private WithEvents _ColorGridsButton As System.Windows.Forms.Button
    Private WithEvents _Seperator2 As System.Windows.Forms.Panel
    Private WithEvents _Seperator1 As System.Windows.Forms.Panel
    Private WithEvents _ColorAxesButton As System.Windows.Forms.Button
    Private WithEvents _ColorDataButton As System.Windows.Forms.Button
    Private WithEvents _SineWaveCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SquareWaveCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _XMinTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _XMaxLabel As System.Windows.Forms.Label
    Private WithEvents _XMinLabel As System.Windows.Forms.Label
    Private WithEvents _YAxisLabel As System.Windows.Forms.Label
    Private WithEvents _MultipleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _SingleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _XAxisLabel As System.Windows.Forms.Label
    Private WithEvents _BandModeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _YMinTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _YMax1Label As System.Windows.Forms.Label
    Private WithEvents _YMin1Label As System.Windows.Forms.Label
    Private WithEvents _SignalListComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _YMaxTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _XMaxTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents _CreateDataTimer As System.Windows.Forms.Timer
End Class
