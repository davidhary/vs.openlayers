<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SingleInputOutputView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SingleInputOutputView))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._InputFourVoltageTextBox = New System.Windows.Forms.TextBox()
        Me._InputThreeRangeCombo = New System.Windows.Forms.ComboBox()
        Me._InputFourRangeCombo = New System.Windows.Forms.ComboBox()
        Me._InputTwoRangeCombo = New System.Windows.Forms.ComboBox()
        Me._InputOneRangeCombo = New System.Windows.Forms.ComboBox()
        Me._InputFourChannelCombo = New System.Windows.Forms.ComboBox()
        Me._InputThreeChannelCombo = New System.Windows.Forms.ComboBox()
        Me._InputTwoChannelCombo = New System.Windows.Forms.ComboBox()
        Me._InputOneChannelCombo = New System.Windows.Forms.ComboBox()
        Me._InputOneVoltageTextBox = New System.Windows.Forms.TextBox()
        Me._InputTwoVoltageTextBox = New System.Windows.Forms.TextBox()
        Me._InputThreeVoltageTextBox = New System.Windows.Forms.TextBox()
        Me._AutoUpdateCheckBox = New System.Windows.Forms.CheckBox()
        Me._OpenDeviceCheckBox = New System.Windows.Forms.CheckBox()
        Me._UpdateIOButton = New System.Windows.Forms.Button()
        Me._AnalogOutputZeroVoltageNumeric = New System.Windows.Forms.NumericUpDown()
        Me._RangeLabel = New System.Windows.Forms.Label()
        Me._AnalogInputGroupBox = New System.Windows.Forms.GroupBox()
        Me._AnalogInputLabelA = New System.Windows.Forms.Label()
        Me._ChannelLabel = New System.Windows.Forms.Label()
        Me._UpdateIOTimer = New System.Windows.Forms.Timer(Me.components)
        Me._DigitalIoGroupBox = New System.Windows.Forms.GroupBox()
        Me._DigitalOutputNumericUpDown = New isr.Core.Controls.NumericUpDown()
        Me._DigitalInputNumericUpDown = New isr.Core.Controls.NumericUpDown()
        Me._DigitalInputCheckBox = New System.Windows.Forms.CheckBox()
        Me._DigitalOutputCheckBox = New System.Windows.Forms.CheckBox()
        Me._AnalogOutputGroupBox = New System.Windows.Forms.GroupBox()
        Me._AnalogOutputOneVoltageNumeric = New System.Windows.Forms.NumericUpDown()
        Me._AnalogOutputZeroLabel = New System.Windows.Forms.Label()
        Me._AnalogOutputVoltageLabel = New System.Windows.Forms.Label()
        Me._AnalogOutputOneLabel = New System.Windows.Forms.Label()
        Me._AnalogOutputChannelLabel = New System.Windows.Forms.Label()
        Me._CounterGroupBox = New System.Windows.Forms.GroupBox()
        Me._CountTextBox = New System.Windows.Forms.TextBox()
        Me._CountCaptionLabel = New System.Windows.Forms.Label()
        Me._ResetCountCheckBox = New System.Windows.Forms.CheckBox()
        Me._BoardMessagesComboBoxLabel = New System.Windows.Forms.Label()
        Me._ApplicationMessagesComboBoxLabel = New System.Windows.Forms.Label()
        Me._BoardMessagesComboBox = New System.Windows.Forms.ComboBox()
        Me._ApplicationMessagesComboBox = New System.Windows.Forms.ComboBox()
        CType(Me._AnalogOutputZeroVoltageNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._AnalogInputGroupBox.SuspendLayout()
        Me._DigitalIoGroupBox.SuspendLayout()
        CType(Me._DigitalOutputNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DigitalInputNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._AnalogOutputGroupBox.SuspendLayout()
        CType(Me._AnalogOutputOneVoltageNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._CounterGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_InputFourVoltageTextBox
        '
        Me._InputFourVoltageTextBox.AcceptsReturn = True
        Me._InputFourVoltageTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._InputFourVoltageTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._InputFourVoltageTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputFourVoltageTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputFourVoltageTextBox.Location = New System.Drawing.Point(164, 131)
        Me._InputFourVoltageTextBox.MaxLength = 0
        Me._InputFourVoltageTextBox.Name = "_InputFourVoltageTextBox"
        Me._InputFourVoltageTextBox.ReadOnly = True
        Me._InputFourVoltageTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputFourVoltageTextBox.Size = New System.Drawing.Size(65, 25)
        Me._InputFourVoltageTextBox.TabIndex = 14
        Me._ToolTip.SetToolTip(Me._InputFourVoltageTextBox, "Input voltage")
        '
        '_InputThreeRangeCombo
        '
        Me._InputThreeRangeCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputThreeRangeCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputThreeRangeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputThreeRangeCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputThreeRangeCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputThreeRangeCombo.Items.AddRange(New Object() {"1", "2", "4", "5", "8", "10", "16", "20"})
        Me._InputThreeRangeCombo.Location = New System.Drawing.Point(94, 100)
        Me._InputThreeRangeCombo.Name = "_InputThreeRangeCombo"
        Me._InputThreeRangeCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputThreeRangeCombo.Size = New System.Drawing.Size(55, 25)
        Me._InputThreeRangeCombo.TabIndex = 10
        Me._ToolTip.SetToolTip(Me._InputThreeRangeCombo, "Select input range")
        '
        '_InputFourRangeCombo
        '
        Me._InputFourRangeCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputFourRangeCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputFourRangeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputFourRangeCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputFourRangeCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputFourRangeCombo.Items.AddRange(New Object() {"1", "2", "4", "5", "8", "10", "16", "20"})
        Me._InputFourRangeCombo.Location = New System.Drawing.Point(94, 129)
        Me._InputFourRangeCombo.Name = "_InputFourRangeCombo"
        Me._InputFourRangeCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputFourRangeCombo.Size = New System.Drawing.Size(55, 25)
        Me._InputFourRangeCombo.TabIndex = 13
        Me._ToolTip.SetToolTip(Me._InputFourRangeCombo, "Select input range")
        '
        '_InputTwoRangeCombo
        '
        Me._InputTwoRangeCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputTwoRangeCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputTwoRangeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputTwoRangeCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputTwoRangeCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputTwoRangeCombo.Items.AddRange(New Object() {"1", "2", "4", "5", "8", "10", "16", "20"})
        Me._InputTwoRangeCombo.Location = New System.Drawing.Point(94, 71)
        Me._InputTwoRangeCombo.Name = "_InputTwoRangeCombo"
        Me._InputTwoRangeCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputTwoRangeCombo.Size = New System.Drawing.Size(55, 25)
        Me._InputTwoRangeCombo.TabIndex = 7
        Me._ToolTip.SetToolTip(Me._InputTwoRangeCombo, "Select input range")
        '
        '_InputOneRangeCombo
        '
        Me._InputOneRangeCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputOneRangeCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputOneRangeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputOneRangeCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputOneRangeCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputOneRangeCombo.Items.AddRange(New Object() {"±20.00V", "±10.0.V", "  ±5.00V", "  ±4.00V", "  ±2.50V", "  ±2.00V", "  ±1.25V", "  ±1.00V"})
        Me._InputOneRangeCombo.Location = New System.Drawing.Point(94, 42)
        Me._InputOneRangeCombo.Name = "_InputOneRangeCombo"
        Me._InputOneRangeCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputOneRangeCombo.Size = New System.Drawing.Size(55, 25)
        Me._InputOneRangeCombo.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._InputOneRangeCombo, "Select input range")
        '
        '_InputFourChannelCombo
        '
        Me._InputFourChannelCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputFourChannelCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputFourChannelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputFourChannelCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputFourChannelCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputFourChannelCombo.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me._InputFourChannelCombo.Location = New System.Drawing.Point(20, 129)
        Me._InputFourChannelCombo.Name = "_InputFourChannelCombo"
        Me._InputFourChannelCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputFourChannelCombo.Size = New System.Drawing.Size(58, 25)
        Me._InputFourChannelCombo.TabIndex = 12
        Me._ToolTip.SetToolTip(Me._InputFourChannelCombo, "Select input channel number")
        '
        '_InputThreeChannelCombo
        '
        Me._InputThreeChannelCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputThreeChannelCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputThreeChannelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputThreeChannelCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputThreeChannelCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputThreeChannelCombo.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me._InputThreeChannelCombo.Location = New System.Drawing.Point(20, 100)
        Me._InputThreeChannelCombo.Name = "_InputThreeChannelCombo"
        Me._InputThreeChannelCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputThreeChannelCombo.Size = New System.Drawing.Size(58, 25)
        Me._InputThreeChannelCombo.TabIndex = 9
        Me._ToolTip.SetToolTip(Me._InputThreeChannelCombo, "Select input channel number")
        '
        '_InputTwoChannelCombo
        '
        Me._InputTwoChannelCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputTwoChannelCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputTwoChannelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputTwoChannelCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputTwoChannelCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputTwoChannelCombo.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me._InputTwoChannelCombo.Location = New System.Drawing.Point(20, 71)
        Me._InputTwoChannelCombo.Name = "_InputTwoChannelCombo"
        Me._InputTwoChannelCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputTwoChannelCombo.Size = New System.Drawing.Size(58, 25)
        Me._InputTwoChannelCombo.TabIndex = 6
        Me._ToolTip.SetToolTip(Me._InputTwoChannelCombo, "Select input channel number")
        '
        '_InputOneChannelCombo
        '
        Me._InputOneChannelCombo.BackColor = System.Drawing.SystemColors.Window
        Me._InputOneChannelCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputOneChannelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputOneChannelCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputOneChannelCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputOneChannelCombo.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me._InputOneChannelCombo.Location = New System.Drawing.Point(20, 42)
        Me._InputOneChannelCombo.Name = "_InputOneChannelCombo"
        Me._InputOneChannelCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputOneChannelCombo.Size = New System.Drawing.Size(58, 25)
        Me._InputOneChannelCombo.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._InputOneChannelCombo, "Select input channel number")
        '
        '_InputOneVoltageTextBox
        '
        Me._InputOneVoltageTextBox.AcceptsReturn = True
        Me._InputOneVoltageTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._InputOneVoltageTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._InputOneVoltageTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputOneVoltageTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputOneVoltageTextBox.Location = New System.Drawing.Point(164, 44)
        Me._InputOneVoltageTextBox.MaxLength = 0
        Me._InputOneVoltageTextBox.Name = "_InputOneVoltageTextBox"
        Me._InputOneVoltageTextBox.ReadOnly = True
        Me._InputOneVoltageTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputOneVoltageTextBox.Size = New System.Drawing.Size(65, 25)
        Me._InputOneVoltageTextBox.TabIndex = 5
        Me._ToolTip.SetToolTip(Me._InputOneVoltageTextBox, "Input voltage")
        '
        '_InputTwoVoltageTextBox
        '
        Me._InputTwoVoltageTextBox.AcceptsReturn = True
        Me._InputTwoVoltageTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._InputTwoVoltageTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._InputTwoVoltageTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputTwoVoltageTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputTwoVoltageTextBox.Location = New System.Drawing.Point(164, 73)
        Me._InputTwoVoltageTextBox.MaxLength = 0
        Me._InputTwoVoltageTextBox.Name = "_InputTwoVoltageTextBox"
        Me._InputTwoVoltageTextBox.ReadOnly = True
        Me._InputTwoVoltageTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputTwoVoltageTextBox.Size = New System.Drawing.Size(65, 25)
        Me._InputTwoVoltageTextBox.TabIndex = 8
        Me._ToolTip.SetToolTip(Me._InputTwoVoltageTextBox, "Input voltage")
        '
        '_InputThreeVoltageTextBox
        '
        Me._InputThreeVoltageTextBox.AcceptsReturn = True
        Me._InputThreeVoltageTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._InputThreeVoltageTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._InputThreeVoltageTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputThreeVoltageTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputThreeVoltageTextBox.Location = New System.Drawing.Point(164, 102)
        Me._InputThreeVoltageTextBox.MaxLength = 0
        Me._InputThreeVoltageTextBox.Name = "_InputThreeVoltageTextBox"
        Me._InputThreeVoltageTextBox.ReadOnly = True
        Me._InputThreeVoltageTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputThreeVoltageTextBox.Size = New System.Drawing.Size(65, 25)
        Me._InputThreeVoltageTextBox.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._InputThreeVoltageTextBox, "Input voltage")
        '
        '_AutoUpdateCheckBox
        '
        Me._AutoUpdateCheckBox.AutoSize = True
        Me._AutoUpdateCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._AutoUpdateCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._AutoUpdateCheckBox.Enabled = False
        Me._AutoUpdateCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._AutoUpdateCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AutoUpdateCheckBox.Location = New System.Drawing.Point(26, 18)
        Me._AutoUpdateCheckBox.Name = "_AutoUpdateCheckBox"
        Me._AutoUpdateCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AutoUpdateCheckBox.Size = New System.Drawing.Size(105, 21)
        Me._AutoUpdateCheckBox.TabIndex = 2
        Me._AutoUpdateCheckBox.Text = "Auto Update"
        Me._ToolTip.SetToolTip(Me._AutoUpdateCheckBox, "Check to update all inputs and outputs on a timer event")
        Me._AutoUpdateCheckBox.UseVisualStyleBackColor = False
        '
        '_OpenDeviceCheckBox
        '
        Me._OpenDeviceCheckBox.Appearance = System.Windows.Forms.Appearance.Button
        Me._OpenDeviceCheckBox.Enabled = False
        Me._OpenDeviceCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._OpenDeviceCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._OpenDeviceCheckBox.Location = New System.Drawing.Point(436, 12)
        Me._OpenDeviceCheckBox.Name = "_OpenDeviceCheckBox"
        Me._OpenDeviceCheckBox.Size = New System.Drawing.Size(120, 32)
        Me._OpenDeviceCheckBox.TabIndex = 0
        Me._OpenDeviceCheckBox.Text = "&Open Device"
        Me._OpenDeviceCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ToolTip.SetToolTip(Me._OpenDeviceCheckBox, "Click to open the device")
        '
        '_UpdateIOButton
        '
        Me._UpdateIOButton.Enabled = False
        Me._UpdateIOButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._UpdateIOButton.Location = New System.Drawing.Point(137, 12)
        Me._UpdateIOButton.Name = "_UpdateIOButton"
        Me._UpdateIOButton.Size = New System.Drawing.Size(120, 32)
        Me._UpdateIOButton.TabIndex = 1
        Me._UpdateIOButton.Text = "&Update I/O"
        Me._ToolTip.SetToolTip(Me._UpdateIOButton, "Updates the I/O")
        Me._UpdateIOButton.UseVisualStyleBackColor = True
        '
        '_AnalogOutputZeroVoltageNumeric
        '
        Me._AnalogOutputZeroVoltageNumeric.DecimalPlaces = 4
        Me._AnalogOutputZeroVoltageNumeric.Enabled = False
        Me._AnalogOutputZeroVoltageNumeric.Location = New System.Drawing.Point(46, 44)
        Me._AnalogOutputZeroVoltageNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._AnalogOutputZeroVoltageNumeric.Minimum = New Decimal(New Integer() {10, 0, 0, -2147483648})
        Me._AnalogOutputZeroVoltageNumeric.Name = "_AnalogOutputZeroVoltageNumeric"
        Me._AnalogOutputZeroVoltageNumeric.Size = New System.Drawing.Size(69, 25)
        Me._AnalogOutputZeroVoltageNumeric.TabIndex = 5
        Me._ToolTip.SetToolTip(Me._AnalogOutputZeroVoltageNumeric, "Output voltage")
        '
        '_RangeLabel
        '
        Me._RangeLabel.AutoSize = True
        Me._RangeLabel.BackColor = System.Drawing.SystemColors.Control
        Me._RangeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._RangeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RangeLabel.Location = New System.Drawing.Point(89, 23)
        Me._RangeLabel.Name = "_RangeLabel"
        Me._RangeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RangeLabel.Size = New System.Drawing.Size(65, 17)
        Me._RangeLabel.TabIndex = 1
        Me._RangeLabel.Text = "Range [V]"
        '
        '_AnalogInputGroupBox
        '
        Me._AnalogInputGroupBox.Controls.Add(Me._InputFourVoltageTextBox)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputThreeRangeCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._AnalogInputLabelA)
        Me._AnalogInputGroupBox.Controls.Add(Me._RangeLabel)
        Me._AnalogInputGroupBox.Controls.Add(Me._ChannelLabel)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputFourRangeCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputTwoRangeCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputOneRangeCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputFourChannelCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputThreeChannelCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputTwoChannelCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputOneChannelCombo)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputOneVoltageTextBox)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputTwoVoltageTextBox)
        Me._AnalogInputGroupBox.Controls.Add(Me._InputThreeVoltageTextBox)
        Me._AnalogInputGroupBox.Enabled = False
        Me._AnalogInputGroupBox.Location = New System.Drawing.Point(312, 54)
        Me._AnalogInputGroupBox.Name = "_AnalogInputGroupBox"
        Me._AnalogInputGroupBox.Size = New System.Drawing.Size(249, 170)
        Me._AnalogInputGroupBox.TabIndex = 5
        Me._AnalogInputGroupBox.TabStop = False
        Me._AnalogInputGroupBox.Text = "Analog Inputs"
        '
        '_AnalogInputLabelA
        '
        Me._AnalogInputLabelA.AutoSize = True
        Me._AnalogInputLabelA.BackColor = System.Drawing.SystemColors.Control
        Me._AnalogInputLabelA.Cursor = System.Windows.Forms.Cursors.Default
        Me._AnalogInputLabelA.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AnalogInputLabelA.Location = New System.Drawing.Point(170, 25)
        Me._AnalogInputLabelA.Name = "_AnalogInputLabelA"
        Me._AnalogInputLabelA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AnalogInputLabelA.Size = New System.Drawing.Size(52, 17)
        Me._AnalogInputLabelA.TabIndex = 2
        Me._AnalogInputLabelA.Text = "Voltage"
        '
        '_ChannelLabel
        '
        Me._ChannelLabel.AutoSize = True
        Me._ChannelLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ChannelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ChannelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ChannelLabel.Location = New System.Drawing.Point(22, 23)
        Me._ChannelLabel.Name = "_ChannelLabel"
        Me._ChannelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelLabel.Size = New System.Drawing.Size(54, 17)
        Me._ChannelLabel.TabIndex = 0
        Me._ChannelLabel.Text = "Channel"
        '
        '_UpdateIOTimer
        '
        Me._UpdateIOTimer.Interval = 1000
        '
        '_DigitalIoGroupBox
        '
        Me._DigitalIoGroupBox.Controls.Add(Me._DigitalOutputNumericUpDown)
        Me._DigitalIoGroupBox.Controls.Add(Me._DigitalInputNumericUpDown)
        Me._DigitalIoGroupBox.Controls.Add(Me._DigitalInputCheckBox)
        Me._DigitalIoGroupBox.Controls.Add(Me._DigitalOutputCheckBox)
        Me._DigitalIoGroupBox.Enabled = False
        Me._DigitalIoGroupBox.Location = New System.Drawing.Point(12, 54)
        Me._DigitalIoGroupBox.Name = "_DigitalIoGroupBox"
        Me._DigitalIoGroupBox.Size = New System.Drawing.Size(142, 111)
        Me._DigitalIoGroupBox.TabIndex = 3
        Me._DigitalIoGroupBox.TabStop = False
        Me._DigitalIoGroupBox.Text = "Digital I/O"
        '
        '_DigitalOutputNumericUpDown
        '
        Me._DigitalOutputNumericUpDown.Hexadecimal = True
        Me._DigitalOutputNumericUpDown.Location = New System.Drawing.Point(80, 73)
        Me._DigitalOutputNumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me._DigitalOutputNumericUpDown.Name = "_DigitalOutputNumericUpDown"
        Me._DigitalOutputNumericUpDown.NullValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._DigitalOutputNumericUpDown.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._DigitalOutputNumericUpDown.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._DigitalOutputNumericUpDown.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._DigitalOutputNumericUpDown.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._DigitalOutputNumericUpDown.Size = New System.Drawing.Size(48, 25)
        Me._DigitalOutputNumericUpDown.TabIndex = 3
        Me._DigitalOutputNumericUpDown.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_DigitalInputNumericUpDown
        '
        Me._DigitalInputNumericUpDown.BackColor = System.Drawing.SystemColors.Control
        Me._DigitalInputNumericUpDown.CausesValidation = False
        Me._DigitalInputNumericUpDown.ForeColor = System.Drawing.SystemColors.WindowText
        Me._DigitalInputNumericUpDown.Hexadecimal = True
        Me._DigitalInputNumericUpDown.Location = New System.Drawing.Point(80, 44)
        Me._DigitalInputNumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me._DigitalInputNumericUpDown.Name = "_DigitalInputNumericUpDown"
        Me._DigitalInputNumericUpDown.NullValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._DigitalInputNumericUpDown.ReadOnly = True
        Me._DigitalInputNumericUpDown.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._DigitalInputNumericUpDown.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._DigitalInputNumericUpDown.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._DigitalInputNumericUpDown.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._DigitalInputNumericUpDown.Size = New System.Drawing.Size(48, 25)
        Me._DigitalInputNumericUpDown.TabIndex = 1
        Me._DigitalInputNumericUpDown.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_DigitalInputCheckBox
        '
        Me._DigitalInputCheckBox.AutoSize = True
        Me._DigitalInputCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._DigitalInputCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._DigitalInputCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._DigitalInputCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._DigitalInputCheckBox.Location = New System.Drawing.Point(8, 46)
        Me._DigitalInputCheckBox.Name = "_DigitalInputCheckBox"
        Me._DigitalInputCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DigitalInputCheckBox.Size = New System.Drawing.Size(60, 21)
        Me._DigitalInputCheckBox.TabIndex = 0
        Me._DigitalInputCheckBox.Text = "Input"
        Me._DigitalInputCheckBox.UseVisualStyleBackColor = False
        '
        '_DigitalOutputCheckBox
        '
        Me._DigitalOutputCheckBox.AutoSize = True
        Me._DigitalOutputCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._DigitalOutputCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._DigitalOutputCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._DigitalOutputCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._DigitalOutputCheckBox.Location = New System.Drawing.Point(7, 75)
        Me._DigitalOutputCheckBox.Name = "_DigitalOutputCheckBox"
        Me._DigitalOutputCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DigitalOutputCheckBox.Size = New System.Drawing.Size(71, 21)
        Me._DigitalOutputCheckBox.TabIndex = 2
        Me._DigitalOutputCheckBox.Text = "Output"
        Me._DigitalOutputCheckBox.UseVisualStyleBackColor = False
        '
        '_AnalogOutputGroupBox
        '
        Me._AnalogOutputGroupBox.Controls.Add(Me._AnalogOutputOneVoltageNumeric)
        Me._AnalogOutputGroupBox.Controls.Add(Me._AnalogOutputZeroVoltageNumeric)
        Me._AnalogOutputGroupBox.Controls.Add(Me._AnalogOutputZeroLabel)
        Me._AnalogOutputGroupBox.Controls.Add(Me._AnalogOutputVoltageLabel)
        Me._AnalogOutputGroupBox.Controls.Add(Me._AnalogOutputOneLabel)
        Me._AnalogOutputGroupBox.Controls.Add(Me._AnalogOutputChannelLabel)
        Me._AnalogOutputGroupBox.Enabled = False
        Me._AnalogOutputGroupBox.Location = New System.Drawing.Point(172, 54)
        Me._AnalogOutputGroupBox.Name = "_AnalogOutputGroupBox"
        Me._AnalogOutputGroupBox.Size = New System.Drawing.Size(129, 111)
        Me._AnalogOutputGroupBox.TabIndex = 4
        Me._AnalogOutputGroupBox.TabStop = False
        Me._AnalogOutputGroupBox.Text = "Analog Outputs"
        '
        '_AnalogOutputOneVoltageNumeric
        '
        Me._AnalogOutputOneVoltageNumeric.DecimalPlaces = 4
        Me._AnalogOutputOneVoltageNumeric.Enabled = False
        Me._AnalogOutputOneVoltageNumeric.Location = New System.Drawing.Point(46, 71)
        Me._AnalogOutputOneVoltageNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._AnalogOutputOneVoltageNumeric.Minimum = New Decimal(New Integer() {10, 0, 0, -2147483648})
        Me._AnalogOutputOneVoltageNumeric.Name = "_AnalogOutputOneVoltageNumeric"
        Me._AnalogOutputOneVoltageNumeric.Size = New System.Drawing.Size(69, 25)
        Me._AnalogOutputOneVoltageNumeric.TabIndex = 5
        '
        '_AnalogOutputZeroLabel
        '
        Me._AnalogOutputZeroLabel.AutoSize = True
        Me._AnalogOutputZeroLabel.BackColor = System.Drawing.SystemColors.Control
        Me._AnalogOutputZeroLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._AnalogOutputZeroLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AnalogOutputZeroLabel.Location = New System.Drawing.Point(26, 48)
        Me._AnalogOutputZeroLabel.Name = "_AnalogOutputZeroLabel"
        Me._AnalogOutputZeroLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AnalogOutputZeroLabel.Size = New System.Drawing.Size(18, 17)
        Me._AnalogOutputZeroLabel.TabIndex = 2
        Me._AnalogOutputZeroLabel.Text = "0:"
        Me._AnalogOutputZeroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_AnalogOutputVoltageLabel
        '
        Me._AnalogOutputVoltageLabel.AutoSize = True
        Me._AnalogOutputVoltageLabel.BackColor = System.Drawing.SystemColors.Control
        Me._AnalogOutputVoltageLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._AnalogOutputVoltageLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AnalogOutputVoltageLabel.Location = New System.Drawing.Point(58, 25)
        Me._AnalogOutputVoltageLabel.Name = "_AnalogOutputVoltageLabel"
        Me._AnalogOutputVoltageLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AnalogOutputVoltageLabel.Size = New System.Drawing.Size(52, 17)
        Me._AnalogOutputVoltageLabel.TabIndex = 1
        Me._AnalogOutputVoltageLabel.Text = "Voltage"
        '
        '_AnalogOutputOneLabel
        '
        Me._AnalogOutputOneLabel.AutoSize = True
        Me._AnalogOutputOneLabel.BackColor = System.Drawing.SystemColors.Control
        Me._AnalogOutputOneLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._AnalogOutputOneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AnalogOutputOneLabel.Location = New System.Drawing.Point(26, 75)
        Me._AnalogOutputOneLabel.Name = "_AnalogOutputOneLabel"
        Me._AnalogOutputOneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AnalogOutputOneLabel.Size = New System.Drawing.Size(18, 17)
        Me._AnalogOutputOneLabel.TabIndex = 4
        Me._AnalogOutputOneLabel.Text = "1:"
        Me._AnalogOutputOneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_AnalogOutputChannelLabel
        '
        Me._AnalogOutputChannelLabel.AutoSize = True
        Me._AnalogOutputChannelLabel.BackColor = System.Drawing.SystemColors.Control
        Me._AnalogOutputChannelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._AnalogOutputChannelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AnalogOutputChannelLabel.Location = New System.Drawing.Point(4, 25)
        Me._AnalogOutputChannelLabel.Name = "_AnalogOutputChannelLabel"
        Me._AnalogOutputChannelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AnalogOutputChannelLabel.Size = New System.Drawing.Size(54, 17)
        Me._AnalogOutputChannelLabel.TabIndex = 0
        Me._AnalogOutputChannelLabel.Text = "Channel"
        '
        '_CounterGroupBox
        '
        Me._CounterGroupBox.Controls.Add(Me._CountTextBox)
        Me._CounterGroupBox.Controls.Add(Me._CountCaptionLabel)
        Me._CounterGroupBox.Controls.Add(Me._ResetCountCheckBox)
        Me._CounterGroupBox.Enabled = False
        Me._CounterGroupBox.Location = New System.Drawing.Point(12, 172)
        Me._CounterGroupBox.Name = "_CounterGroupBox"
        Me._CounterGroupBox.Size = New System.Drawing.Size(289, 53)
        Me._CounterGroupBox.TabIndex = 6
        Me._CounterGroupBox.TabStop = False
        Me._CounterGroupBox.Text = "Counter"
        '
        '_CountTextBox
        '
        Me._CountTextBox.AcceptsReturn = True
        Me._CountTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._CountTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._CountTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CountTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._CountTextBox.Location = New System.Drawing.Point(56, 18)
        Me._CountTextBox.MaxLength = 0
        Me._CountTextBox.Name = "_CountTextBox"
        Me._CountTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CountTextBox.Size = New System.Drawing.Size(80, 25)
        Me._CountTextBox.TabIndex = 1
        '
        '_CountCaptionLabel
        '
        Me._CountCaptionLabel.AutoSize = True
        Me._CountCaptionLabel.BackColor = System.Drawing.SystemColors.Control
        Me._CountCaptionLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CountCaptionLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CountCaptionLabel.Location = New System.Drawing.Point(5, 23)
        Me._CountCaptionLabel.Name = "_CountCaptionLabel"
        Me._CountCaptionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CountCaptionLabel.Size = New System.Drawing.Size(49, 17)
        Me._CountCaptionLabel.TabIndex = 0
        Me._CountCaptionLabel.Text = "Count: "
        Me._CountCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ResetCountCheckBox
        '
        Me._ResetCountCheckBox.AutoSize = True
        Me._ResetCountCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._ResetCountCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ResetCountCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ResetCountCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ResetCountCheckBox.Location = New System.Drawing.Point(164, 19)
        Me._ResetCountCheckBox.Name = "_ResetCountCheckBox"
        Me._ResetCountCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ResetCountCheckBox.Size = New System.Drawing.Size(113, 21)
        Me._ResetCountCheckBox.TabIndex = 2
        Me._ResetCountCheckBox.Text = "Reset Counter"
        Me._ResetCountCheckBox.UseVisualStyleBackColor = False
        '
        '_BoardMessagesComboBoxLabel
        '
        Me._BoardMessagesComboBoxLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._BoardMessagesComboBoxLabel.AutoSize = True
        Me._BoardMessagesComboBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._BoardMessagesComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._BoardMessagesComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._BoardMessagesComboBoxLabel.Location = New System.Drawing.Point(9, 228)
        Me._BoardMessagesComboBoxLabel.Name = "_BoardMessagesComboBoxLabel"
        Me._BoardMessagesComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._BoardMessagesComboBoxLabel.Size = New System.Drawing.Size(106, 17)
        Me._BoardMessagesComboBoxLabel.TabIndex = 7
        Me._BoardMessagesComboBoxLabel.Text = "Board Messages"
        '
        '_ApplicationMessagesComboBoxLabel
        '
        Me._ApplicationMessagesComboBoxLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplicationMessagesComboBoxLabel.AutoSize = True
        Me._ApplicationMessagesComboBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ApplicationMessagesComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ApplicationMessagesComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ApplicationMessagesComboBoxLabel.Location = New System.Drawing.Point(9, 279)
        Me._ApplicationMessagesComboBoxLabel.Name = "_ApplicationMessagesComboBoxLabel"
        Me._ApplicationMessagesComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ApplicationMessagesComboBoxLabel.Size = New System.Drawing.Size(130, 17)
        Me._ApplicationMessagesComboBoxLabel.TabIndex = 9
        Me._ApplicationMessagesComboBoxLabel.Text = "Application Message"
        '
        '_BoardMessagesComboBox
        '
        Me._BoardMessagesComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._BoardMessagesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._BoardMessagesComboBox.FormattingEnabled = True
        Me._BoardMessagesComboBox.Location = New System.Drawing.Point(9, 247)
        Me._BoardMessagesComboBox.Name = "_BoardMessagesComboBox"
        Me._BoardMessagesComboBox.Size = New System.Drawing.Size(552, 25)
        Me._BoardMessagesComboBox.TabIndex = 8
        '
        '_ApplicationMessagesComboBox
        '
        Me._ApplicationMessagesComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplicationMessagesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ApplicationMessagesComboBox.FormattingEnabled = True
        Me._ApplicationMessagesComboBox.Location = New System.Drawing.Point(9, 298)
        Me._ApplicationMessagesComboBox.Name = "_ApplicationMessagesComboBox"
        Me._ApplicationMessagesComboBox.Size = New System.Drawing.Size(552, 25)
        Me._ApplicationMessagesComboBox.TabIndex = 10
        '
        'SingleIO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(571, 335)
        Me.Controls.Add(Me._ApplicationMessagesComboBox)
        Me.Controls.Add(Me._BoardMessagesComboBox)
        Me.Controls.Add(Me._AnalogInputGroupBox)
        Me.Controls.Add(Me._DigitalIoGroupBox)
        Me.Controls.Add(Me._AnalogOutputGroupBox)
        Me.Controls.Add(Me._CounterGroupBox)
        Me.Controls.Add(Me._BoardMessagesComboBoxLabel)
        Me.Controls.Add(Me._AutoUpdateCheckBox)
        Me.Controls.Add(Me._OpenDeviceCheckBox)
        Me.Controls.Add(Me._UpdateIOButton)
        Me.Controls.Add(Me._ApplicationMessagesComboBoxLabel)
        Me.Name = "SingleInputOutputView"
        Me.Text = "Single Input Output"
        CType(Me._AnalogOutputZeroVoltageNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._AnalogInputGroupBox.ResumeLayout(False)
        Me._AnalogInputGroupBox.PerformLayout()
        Me._DigitalIoGroupBox.ResumeLayout(False)
        Me._DigitalIoGroupBox.PerformLayout()
        CType(Me._DigitalOutputNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DigitalInputNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me._AnalogOutputGroupBox.ResumeLayout(False)
        Me._AnalogOutputGroupBox.PerformLayout()
        CType(Me._AnalogOutputOneVoltageNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._CounterGroupBox.ResumeLayout(False)
        Me._CounterGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _InputFourVoltageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _InputThreeRangeCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputFourRangeCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputTwoRangeCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputOneRangeCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputFourChannelCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputThreeChannelCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputTwoChannelCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputOneChannelCombo As System.Windows.Forms.ComboBox
    Private WithEvents _InputOneVoltageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _InputTwoVoltageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _InputThreeVoltageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _AutoUpdateCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _OpenDeviceCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _UpdateIOButton As System.Windows.Forms.Button
    Private WithEvents _RangeLabel As System.Windows.Forms.Label
    Private WithEvents _AnalogInputGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _AnalogInputLabelA As System.Windows.Forms.Label
    Private WithEvents _ChannelLabel As System.Windows.Forms.Label
    Private WithEvents _UpdateIOTimer As System.Windows.Forms.Timer
    Private WithEvents _DigitalIoGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DigitalOutputNumericUpDown As isr.Core.Controls.NumericUpDown
    Private WithEvents _DigitalInputNumericUpDown As isr.Core.Controls.NumericUpDown
    Private WithEvents _DigitalInputCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _DigitalOutputCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _AnalogOutputGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _AnalogOutputZeroLabel As System.Windows.Forms.Label
    Private WithEvents _AnalogOutputOneLabel As System.Windows.Forms.Label
    Private WithEvents _CounterGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _CountTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CountCaptionLabel As System.Windows.Forms.Label
    Private WithEvents _ResetCountCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _BoardMessagesComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ApplicationMessagesComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _BoardMessagesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ApplicationMessagesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _AnalogOutputVoltageLabel As System.Windows.Forms.Label
    Private WithEvents _AnalogOutputChannelLabel As System.Windows.Forms.Label
    Private WithEvents _AnalogOutputOneVoltageNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _AnalogOutputZeroVoltageNumeric As System.Windows.Forms.NumericUpDown
End Class
