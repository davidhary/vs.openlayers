Imports System.Collections.Generic
Imports isr.Core
Imports isr.IO.OL.WinControls.ExceptionExtensions
Imports isr.Core.Forma
''' <summary> Displays collected data in a scope format. </summary>
''' <remarks> This is the scope tester.  To start this module, call the Show or ShowDialog method
''' from the default instance.  <para>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 1/21/2014 </para></remarks>
Public Class AnalogInputDisplayPanel
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me._AnalogInputDisplay?.RemoveListeners()
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary> Message describing the status. </summary>
    ''' <remarks> Use this property to get the status message generated by the object. </remarks>
    ''' <value> A <see cref="System.String">String</see>. </value>
    Public ReadOnly Property StatusMessage() As String = String.Empty


#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Event handler. Called by  for update tab caption events. </summary>
    ''' <param name="tabPage"> The tab page. </param>
    ''' <param name="value">   The value. </param>
    Private Sub UpdateTabCaptionThis(ByVal tabPage As TabPage, ByVal value As String)
        If tabPage.InvokeRequired Then
            tabPage.Invoke(New Action(Of TabPage, String)(AddressOf Me.UpdateTabCaptionThis), New Object() {tabPage, value})
        Else
            tabPage.Text = value
        End If
    End Sub

    ''' <summary> Event handler. Called by _AnalogInputDisplay for acquisition started events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Trace message event information. </param>
    Private Sub AnalogInputDisplay_AcquisitionStarted(sender As Object, e As TraceMessageEventArgs) Handles _AnalogInputDisplay.AcquisitionStarted
        Me.Talker.Publish(e.TraceMessage)
    End Sub

    ''' <summary> Event handler. Called by _AnalogInputDisplay for boards located events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AnalogInputDisplay_BoardsLocated(sender As Object, e As System.EventArgs) Handles _AnalogInputDisplay.BoardsLocated
    End Sub

    ''' <summary> Event handler. Called by _AnalogInputDisplay for buffer done events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AnalogInputDisplay_BufferDone(sender As Object, e As System.EventArgs) Handles _AnalogInputDisplay.BufferDone
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies the talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Overloads Sub AddPrivateListeners()
        Me._AnalogInputDisplay.AddListeners(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Overrides Sub AddListeners(listeners As IList(Of IMessageListener))
        Me._AnalogInputDisplay.AddListeners(listeners)
        MyBase.AddListeners(listeners)
    End Sub

    ''' <summary> Adds a listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub AddListener(ByVal listener As IMessageListener)
        Me._AnalogInputDisplay.AddListener(listener)
        MyBase.AddListener(listener)
    End Sub

    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        ' this should apply only to the listeners associated with this form
        ' Me._AnalogInputDisplay.ApplyListenerTraceLevel(listenerType, value)
        ' MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    Public Overrides Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker)
        ' this should apply only to the listeners associated with this form
        'MyBase.ApplyListenerTraceLevel(talker)
        'Me._AnalogInputDisplay.ApplyListenerTraceLevel(talker)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        Me._AnalogInputDisplay.ApplyTalkerTraceLevel(listenerType, value)
        MyBase.ApplyTalkerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker)
        MyBase.ApplyTalkerTraceLevels(talker)
        Me._AnalogInputDisplay.ApplyTalkerTraceLevels(talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
