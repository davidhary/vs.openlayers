''' <summary>
''' Selects a device name.
''' </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
<Description("Open Layers Device Name Chooser Control"), DefaultEvent("DeviceSelected"), ToolboxBitmap("DeviceNameChooser.bmp")>
Public Class DeviceNameChooser
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me.DevicesLocatedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.DevicesLocatedEvent.GetInvocationList
                        RemoveHandler Me.DevicesLocated, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.DeviceSelectedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.DeviceSelectedEvent.GetInvocationList
                        RemoveHandler Me.DeviceSelected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                Me.components?.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " METHODS, PROPERTIES, EVENTS "

    ''' <summary>
    ''' Returns the selected device name.
    ''' </summary>
    ''' <value>The name of the device.</value>
    Public ReadOnly Property DeviceName() As String
        Get
            Return Me._NamesComboBox.Text
        End Get
    End Property

    ''' <summary>
    ''' Updates the device name list and raises the device located event if devices where found.
    ''' </summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Public Sub RefreshDeviceNamesList()
        If OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            Me._NamesComboBox.DataSource = OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()
            Me.OnDevicesLocated(New EventArgs)
        Else
            Me._NamesComboBox.DataSource = Array.Empty(Of String)()
        End If
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Notifies of the location of device.
    ''' </summary>
    Public Event DevicesLocated As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises the device located event.
    ''' </summary>
    ''' <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    Private Sub OnDevicesLocated(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DevicesLocatedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Notifies of the selected of a device.
    ''' </summary>
    Public Event DeviceSelected As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises the Device Selected event.
    ''' </summary>
    ''' <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    Protected Sub OnDeviceNameSelected(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DeviceSelectedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the availableDeviceNamesComboBox control.
    ''' Selects a device.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub AvailableDeviceNamesComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _NamesComboBox.SelectedIndexChanged

        If Me._NamesComboBox.SelectedItem IsNot Nothing Then
            Me._ToolTip.SetToolTip(Me._NamesComboBox, Me._NamesComboBox.SelectedItem.ToString)
            Me.OnDeviceNameSelected(New EventArgs)
        End If

    End Sub

    ''' <summary>
    ''' Handles the Click event of the findButton control.
    ''' Looks for devices.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub FindButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FindButton.Click
        Me.RefreshDeviceNamesList()
    End Sub


#End Region

End Class
