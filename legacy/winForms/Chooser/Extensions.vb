﻿Imports System.Runtime.CompilerServices
''' <summary>
''' Extends the Data Translation Open Layers <see cref="OpenLayers.Base.SubsystemBase">subsystem base</see>,
''' <see cref="OpenLayers.Base.Device">Device</see>,
''' <see cref="OpenLayers.Base.ChannelList">Channel List</see>, and 
''' <see cref="OpenLayers.Signals.MemorySignalList">Signal List</see> functionality.
''' </summary>
''' <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Module Extensions

#Region " DEVICE MANAGER "

    ''' <summary> Selects and returns an open layers device.  If the system has multiple board, this
    ''' would allow the operator to select a board from a list. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager"> The device manager. </param>
    ''' <returns> OpenLayers.Base.Device or nothing if no hardware or no such device. </returns>
    <Extension()>
    Public Function SelectDevice(ByVal manager As OpenLayers.Base.DeviceMgr) As OpenLayers.Base.Device

        If manager Is Nothing Then
            Throw New ArgumentNullException(NameOf(manager))
        End If
        Return If(manager.HardwareAvailable, manager.SelectDevice(DeviceChooser.SelectDeviceName()), Nothing)

    End Function

#End Region

End Module

