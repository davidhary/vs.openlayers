using System;
using System.Diagnostics;

using isr.Core;

namespace isr.IO.OL.Display
{
    public partial class AnalogInputDisplay : ITraceMessageListener
    {

        /// <summary> Gets the is disposed. </summary>
        /// <value> The is disposed. </value>
        protected new bool IsDisposed => base.IsDisposed;

        /// <summary> Writes. </summary>
        /// <param name="message"> The message to add. </param>
        public void Write( string message )
        {
            _ = this.TraceEvent( new TraceMessage( TraceEventType.Information, My.MyLibrary.TraceEventId, message ) );
        }

        /// <summary> Writes a line. </summary>
        /// <param name="message"> The message to add. </param>
        public void WriteLine( string message )
        {
            _ = this.TraceEvent( new TraceMessage( TraceEventType.Information, My.MyLibrary.TraceEventId, message ) );
        }

        /// <summary> Registers this event. </summary>
        /// <param name="level"> The level. </param>
        public virtual void Register( TraceEventType level )
        {
        }

        /// <summary> Gets the trace level. </summary>
        /// <value> The trace level. </value>
        public virtual TraceEventType TraceLevel { get; set; }

        /// <summary> Checks if the log should trace the event type. </summary>
        /// <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
        /// <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
        public bool ShouldTrace( TraceEventType value )
        {
            return value <= this.TraceLevel;
        }

        /// <summary> Gets the sentinel indicating this as thread safe. </summary>
        /// <value> True if thread safe. </value>
        public virtual bool IsThreadSafe => true;

        /// <summary> Gets or sets a unique identifier. </summary>
        /// <value> The identifier of the unique. </value>
        public Guid UniqueId { get; } = Guid.NewGuid();

        /// <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
        /// <param name="other"> The i trace message listener to compare to this object. </param>
        /// <returns>
        /// <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
        /// </returns>
        public bool Equals( IMessageListener other )
        {
            return other is object && Equals( other.UniqueId, this.UniqueId );
        }

        private void ApplyTraceLevelThis( TraceEventType value )
        {
            this.TraceLevel = value;
        }

        /// <summary> Applies the trace level. </summary>
        /// <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        public void ApplyTraceLevel( TraceEventType value )
        {
            this.ApplyTraceLevelThis( value );
        }

        /// <summary> Gets the type of the listener. </summary>
        /// <value> The type of the listener. </value>
        public ListenerType ListenerType => ListenerType.Display;

        /// <summary> Trace event overriding the trace level. </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> A String. </returns>
        public string TraceEventOverride( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                _ = this.TraceEvent( value );
                OL.My.MyLibrary.DoEvents();
                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listener overriding the trace level. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEventOverride( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEventOverride( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEvent( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                if ( this.ShouldTrace( value.EventType ) )
                {
                    this._ChartToolStripStatusLabel.ToolTipText = value.Details;
                    this._ChartToolStripStatusLabel.Text = value.ExtractSynopsis();
                }

                OL.My.MyLibrary.DoEvents();
                return value.Details;
            }
        }
    }
}
