
namespace isr.IO.OL.Display.My
{
    /// <summary>   my library. This class cannot be inherited. </summary>
    /// <remarks>   David, 2022-01-11. </remarks>
    public sealed partial class MyLibrary
    {
        private MyLibrary() : base()
        {
        }

        /// <summary> Identifier for the trace event. </summary>
        public const int TraceEventId = ( int ) OL.My.ProjectTraceEventId.OpenLayersDisplay;

        /// <summary>   (Immutable) the assembly title. </summary>
        public const string AssemblyTitle = "Open Layers Display Library";

        /// <summary>   (Immutable) information describing the assembly. </summary>
        public const string AssemblyDescription = "Open Layers Display Library";

        /// <summary>   (Immutable) the assembly product. </summary>
        public const string AssemblyProduct = "Open.Layers.Display.Library";
    }
}
