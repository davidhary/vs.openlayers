using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.IO.OL.DeviceChooser
{
    /// <summary>
    /// Displays a form for selecting a device.
    /// </summary>
    /// <remarks>Launch this form by calling its Show or ShowDialog method from its default
    ///   instance.(c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public partial class DeviceChooser : Core.Forma.FormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Prevents a default instance of the <see cref="DeviceChooser" /> class from being created.
        /// </summary>
        private DeviceChooser() : base()
        {
            this.InitializingComponents = true;
            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static DeviceChooser _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't 
        /// created until the first time it's retrieved.
        /// </remarks>
        public static DeviceChooser Get()
        {
            if ( _Instance is not object || _Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    _Instance = new DeviceChooser();
            }

            return _Instance;
        }

        #endregion

        #region " SHARD "

        /// <summary>
        /// Selects and returns an open layers device name.  If the board name is not
        /// found, allow the user to select from the list of existing boards..
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        /// <returns>System.String.</returns>
        public static string SelectDeviceName( string deviceName )
        {
            if ( OpenLayers.Base.DeviceMgr.Get().HardwareAvailable() )
            {
                var boards = OpenLayers.Base.DeviceMgr.Get().GetDeviceNames();
                if ( OpenLayers.Base.DeviceMgr.Get().GetDeviceNames().Length == 1 )
                {
                    return OpenLayers.Base.DeviceMgr.Get().GetDeviceNames()[0];
                }
                else
                {
                    int index = Array.IndexOf( boards, deviceName );
                    return index >= 0 ? boards[index] : Get().ShowDialog() == DialogResult.OK ? Get().DeviceName : string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Selects and returns an open layers device name.  If the system has multiple
        /// boards, this would allow the operator to select a board from a list.
        /// </summary>
        /// <returns>System.String.</returns>
        public static string SelectDeviceName()
        {
            return OpenLayers.Base.DeviceMgr.Get().HardwareAvailable() ? OpenLayers.Base.DeviceMgr.Get().GetDeviceNames().Length == 1 ? OpenLayers.Base.DeviceMgr.Get().GetDeviceNames()[0] : Get().ShowDialog() == DialogResult.OK ? Get().DeviceName : string.Empty : string.Empty;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Returns the selected device name.
        /// </summary>
        /// <value>The name of the device.</value>
        public string DeviceName => this._DeviceNameChooser.DeviceName;

        #endregion

        #region " FORM AND CONTROL EVENT HANDLERS "

        /// <summary>
        /// Accepts changes and exits this form.
        /// </summary>
        private void OkButton_Click( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Cancels changes and exits this form.
        /// </summary>
        private void CancelButton1_Click( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Handles the Load event of the form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // center the form
                this.CenterToScreen();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Handles the Shown event of the form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void Form_Shown( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // Initialize and set the user interface
                this._DeviceNameChooser.RefreshDeviceNamesList();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary>
        /// Handles the DeviceSelected event of the DeviceNameChooser1 control.
        /// Enables acceptance exist.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void DeviceNameChooser_DeviceSelected( object sender, EventArgs e )
        {
            this._AcceptButton.Enabled = this._DeviceNameChooser.DeviceName.Length > 0;
        }

        #endregion

    }
}
