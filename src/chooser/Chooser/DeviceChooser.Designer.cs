using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.IO.OL.DeviceChooser
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class DeviceChooser
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _DeviceNameChooser = new DeviceNameChooser();
            _DeviceNameChooser.DeviceSelected += new EventHandler<EventArgs>(DeviceNameChooser_DeviceSelected);
            _CancelButton = new Button();
            _AcceptButton = new Button();
            _DeviceNameChooserLabel = new Label();
            SuspendLayout();
            // 
            // _DeviceNameChooser
            // 
            _DeviceNameChooser.Location = new Point(12, 22);
            _DeviceNameChooser.Name = "_DeviceNameChooser";
            _DeviceNameChooser.Size = new Size(166, 29);
            _DeviceNameChooser.TabIndex = 28;
            // 
            // _CancelButton
            // 
            _CancelButton.DialogResult = DialogResult.Cancel;
            _CancelButton.FlatStyle = FlatStyle.System;
            _CancelButton.Font = new Font(Font, FontStyle.Bold);
            _CancelButton.Location = new Point(14, 52);
            _CancelButton.Name = "_CancelButton";
            _CancelButton.Size = new Size(75, 23);
            _CancelButton.TabIndex = 27;
            _CancelButton.Text = "&Cancel";
            _CancelButton.Click += new EventHandler( CancelButton1_Click );
            // 
            // _AcceptButton
            // 
            _AcceptButton.Enabled = false;
            _AcceptButton.FlatStyle = FlatStyle.System;
            _AcceptButton.Font = new Font(Font, FontStyle.Bold);
            _AcceptButton.Location = new Point(102, 52);
            _AcceptButton.Name = "_AcceptButton";
            _AcceptButton.Size = new Size(75, 23);
            _AcceptButton.TabIndex = 26;
            _AcceptButton.Text = "&OK";
            _AcceptButton.Click += new EventHandler( OkButton_Click );
            // 
            // _DeviceNameChooserLabel
            // 
            _DeviceNameChooserLabel.AutoSize = true;
            _DeviceNameChooserLabel.BackColor = SystemColors.Control;
            _DeviceNameChooserLabel.Cursor = Cursors.Default;
            _DeviceNameChooserLabel.FlatStyle = FlatStyle.System;
            _DeviceNameChooserLabel.ForeColor = SystemColors.WindowText;
            _DeviceNameChooserLabel.Location = new Point(19, 3);
            _DeviceNameChooserLabel.Name = "_DeviceNameChooserLabel";
            _DeviceNameChooserLabel.RightToLeft = RightToLeft.No;
            _DeviceNameChooserLabel.Size = new Size(152, 17);
            _DeviceNameChooserLabel.TabIndex = 25;
            _DeviceNameChooserLabel.Text = "Select a Device by Name";
            _DeviceNameChooserLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // DeviceChooser
            // 
            AcceptButton = _AcceptButton;
            CancelButton = _CancelButton;
            ClientSize = new Size(192, 81);
            Controls.Add(_DeviceNameChooser);
            Controls.Add(_CancelButton);
            Controls.Add(_AcceptButton);
            Controls.Add(_DeviceNameChooserLabel);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Name = "DeviceChooser";
            Text = "Select a board";
            Load += new EventHandler(Form_Load);
            Shown += new EventHandler(Form_Shown);
            ResumeLayout(false);
            PerformLayout();
        }

        private DeviceNameChooser _DeviceNameChooser;
        private Button _CancelButton;
        private Button _AcceptButton;
        private Label _DeviceNameChooserLabel;
    }
}
