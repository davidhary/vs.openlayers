﻿
namespace isr.IO.OL.DeviceChooser.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        /// <summary> Identifier for the trace event. </summary>
        public const int TraceEventId = ( int ) OL.My.ProjectTraceEventId.OpenLayersForms;
        public const string AssemblyTitle = "Open Layers Forms Library";
        public const string AssemblyDescription = "Open Layers Forms Library";
        public const string AssemblyProduct = "Open.Layers.Forms.Library";
    }
}