using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO.OL.Scope.Demo
{
    [DesignerGenerated()]
    public partial class ScopePanel
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        private ToolTip _ToolTip;
        private Button _AbortButton;
        private CheckBox _EnablePlottingCheckBox;
        private Button _StopButton;
        private Button _StartButton;
        private Button _ConfigButton;
        private TextBox _FrequencyTextBox;
        private TextBox _ChannelBufferSizeTextBox;
        private TextBox _BuffersCountTextBox;
        private Label _FrequencyTextBoxLabel;
        private Label _ChannelBufferSizeTextBoxLabel;
        private Label _BuffersCountTextBoxLabel;
        private Button _InitializeButton;
        private Label _YMax2Label;
        private Label _YMin2Label;
        private Button _AutoScaleButton;
        private Button _ColorGridsButton;
        private Panel _SeparatorPanel2;
        private Panel _SeparatorPanel1;
        private Button _ColorAxesButton;
        private Button _ColorDataButton;
        private Button _DeleteSignalButton;
        private ComboBox _SignalListComboBox;
        private TrackBar _YMaxTrackBar;
        private TrackBar _YMinTrackBar;
        private Label _YMax1Label;
        private Label _YMin1Label;
        private TrackBar _XMaxTrackBar;
        private TrackBar _XMinTrackBar;
        private Label _XMaxLabel;
        private Label _XMinLabel;
        private Label _YAxisLabel;
        private Label _XAxisLabel;
        private GroupBox _BandModeGroupBox;
        private RadioButton _MultipleRadioButton;
        private RadioButton _SingleRadioButton;
        private Button _AddSignalButton;
        private ColorDialog _ColorDialog;
        private DeviceChooser.DeviceNameChooser _DeviceNameChooser1;
        private Label _DeviceNameLabel;

        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ScopePanel));
            _ColorDialog = new ColorDialog();
            _ToolTip = new ToolTip(components);
            _DeleteSignalButton = new Button();
            _AddSignalButton = new Button();
            _InitializeButton = new Button();
            _FrequencyTextBox = new TextBox();
            _ChannelBufferSizeTextBox = new TextBox();
            _FiniteBufferingCheckBox = new CheckBox();
            _SmartBufferingCheckBox = new CheckBox();
            _SampleSizeTextBoxLabel = new Label();
            _SampleSizeTextBox = new TextBox();
            _SignalMemorySizeTextBox = new TextBox();
            _SamplingRateTextBox = new TextBox();
            _SignalBufferSizeTextBox = new TextBox();
            _DisplayRateTextBox = new TextBox();
            _AbortButton = new Button();
            _EnablePlottingCheckBox = new CheckBox();
            _StopButton = new Button();
            _StartButton = new Button();
            _BuffersCountTextBox = new TextBox();
            _ConfigButton = new Button();
            _FrequencyTextBoxLabel = new Label();
            _ChannelBufferSizeTextBoxLabel = new Label();
            _BuffersCountTextBoxLabel = new Label();
            _DeviceNameLabel = new Label();
            _YMax2Label = new Label();
            _YMin2Label = new Label();
            _AutoScaleButton = new Button();
            _ColorGridsButton = new Button();
            _SeparatorPanel2 = new Panel();
            _SeparatorPanel1 = new Panel();
            _ColorAxesButton = new Button();
            _ColorDataButton = new Button();
            _SignalListComboBox = new ComboBox();
            _YMaxTrackBar = new TrackBar();
            _YMinTrackBar = new TrackBar();
            _YMax1Label = new Label();
            _YMin1Label = new Label();
            _XMaxTrackBar = new TrackBar();
            _XMinTrackBar = new TrackBar();
            _XMaxLabel = new Label();
            _XMinLabel = new Label();
            _YAxisLabel = new Label();
            _XAxisLabel = new Label();
            _BandModeGroupBox = new GroupBox();
            _MultipleRadioButton = new RadioButton();
            _SingleRadioButton = new RadioButton();
            _DeviceNameChooser1 = new DeviceChooser.DeviceNameChooser();
            _Chart = new OpenLayers.Controls.Display();
            _StripChartCheckBox = new CheckBox();
            _SignalBufferSizeTextBoxLabel = new Label();
            _RestoreDefaultsButton = new Button();
            _StatusStrip = new StatusStrip();
            _StatusToolStripStatusLabel = new ToolStripStatusLabel();
            _BuffersToolStripStatusLabel = new ToolStripStatusLabel();
            _SignalMemorySizeTextBoxLabel = new Label();
            _SamplingRateTextBoxLabel = new Label();
            _DisplayRateTextBoxLabel = new Label();
            Label1 = new Label();
            Label2 = new Label();
            TextBox1 = new TextBox();
            TextBox2 = new TextBox();
            CheckBox1 = new CheckBox();
            CheckBox1.CheckedChanged += new EventHandler(StripChartCheckBox_CheckedChanged);
            ((System.ComponentModel.ISupportInitialize)_YMaxTrackBar).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_YMinTrackBar).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_XMaxTrackBar).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_XMinTrackBar).BeginInit();
            _BandModeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_Chart).BeginInit();
            _StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _DeleteSignalButton
            // 
            _DeleteSignalButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _DeleteSignalButton.Location = new Point(802, 232);
            _DeleteSignalButton.Margin = new Padding(4, 3, 4, 3);
            _DeleteSignalButton.Name = "_DeleteSignalButton";
            _DeleteSignalButton.Size = new Size(95, 32);
            _DeleteSignalButton.TabIndex = 65;
            _DeleteSignalButton.Text = "Delete Channel";
            _ToolTip.SetToolTip(_DeleteSignalButton, "Delete last channel");
            _DeleteSignalButton.Click += new EventHandler( DeleteSignalButton_Click );
            // 
            // _AddSignalButton
            // 
            _AddSignalButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _AddSignalButton.Location = new Point(802, 272);
            _AddSignalButton.Margin = new Padding(4, 3, 4, 3);
            _AddSignalButton.Name = "_AddSignalButton";
            _AddSignalButton.Size = new Size(95, 32);
            _AddSignalButton.TabIndex = 64;
            _AddSignalButton.Text = "Add Channel";
            _ToolTip.SetToolTip(_AddSignalButton, "Add input channel");
            _AddSignalButton.Click += new EventHandler( AddSignalButton_Click );
            // 
            // _InitializeButton
            // 
            _InitializeButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _InitializeButton.Enabled = false;
            _InitializeButton.Location = new Point(28, 421);
            _InitializeButton.Margin = new Padding(4, 3, 4, 3);
            _InitializeButton.Name = "_InitializeButton";
            _InitializeButton.Size = new Size(145, 24);
            _InitializeButton.TabIndex = 88;
            _InitializeButton.Text = "Initialize Board";
            _ToolTip.SetToolTip(_InitializeButton, "Initializes the selected device.");
            _InitializeButton.Click += new EventHandler( InitializeButton_Click );
            // 
            // _FrequencyTextBox
            // 
            _FrequencyTextBox.BackColor = SystemColors.Window;
            _FrequencyTextBox.Enabled = false;
            _FrequencyTextBox.Location = new Point(325, 361);
            _FrequencyTextBox.Margin = new Padding(4, 3, 4, 3);
            _FrequencyTextBox.Name = "_FrequencyTextBox";
            _FrequencyTextBox.Size = new Size(61, 25);
            _FrequencyTextBox.TabIndex = 96;
            _FrequencyTextBox.Text = "5000";
            _ToolTip.SetToolTip(_FrequencyTextBox, "Board sampling rate");
            // 
            // _ChannelBufferSizeTextBox
            // 
            _ChannelBufferSizeTextBox.BackColor = SystemColors.Window;
            _ChannelBufferSizeTextBox.Enabled = false;
            _ChannelBufferSizeTextBox.Location = new Point(325, 389);
            _ChannelBufferSizeTextBox.Margin = new Padding(4, 3, 4, 3);
            _ChannelBufferSizeTextBox.Name = "_ChannelBufferSizeTextBox";
            _ChannelBufferSizeTextBox.Size = new Size(47, 25);
            _ChannelBufferSizeTextBox.TabIndex = 94;
            _ChannelBufferSizeTextBox.Text = "1000";
            _ToolTip.SetToolTip(_ChannelBufferSizeTextBox, "Size of actual buffer allocated per channel.");
            // 
            // _FiniteBufferingCheckBox
            // 
            _FiniteBufferingCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _FiniteBufferingCheckBox.AutoSize = true;
            _FiniteBufferingCheckBox.Location = new Point(567, 391);
            _FiniteBufferingCheckBox.Margin = new Padding(4, 3, 4, 3);
            _FiniteBufferingCheckBox.Name = "_FiniteBufferingCheckBox";
            _FiniteBufferingCheckBox.Size = new Size(113, 21);
            _FiniteBufferingCheckBox.TabIndex = 100;
            _FiniteBufferingCheckBox.Text = "Finite Buffering";
            _ToolTip.SetToolTip(_FiniteBufferingCheckBox, "Samples the specified number of buffers and stops.");
            // 
            // _SmartBufferingCheckBox
            // 
            _SmartBufferingCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SmartBufferingCheckBox.AutoSize = true;
            _SmartBufferingCheckBox.Enabled = false;
            _SmartBufferingCheckBox.Location = new Point(567, 447);
            _SmartBufferingCheckBox.Margin = new Padding(4, 3, 4, 3);
            _SmartBufferingCheckBox.Name = "_SmartBufferingCheckBox";
            _SmartBufferingCheckBox.Size = new Size(117, 21);
            _SmartBufferingCheckBox.TabIndex = 100;
            _SmartBufferingCheckBox.Text = "Smart Buffering";
            _ToolTip.SetToolTip(_SmartBufferingCheckBox, "Assigns 256 points for each buffer and adjusts the sampling rate accordingly.");
            // 
            // _SampleSizeTextBoxLabel
            // 
            _SampleSizeTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SampleSizeTextBoxLabel.ImageAlign = ContentAlignment.MiddleRight;
            _SampleSizeTextBoxLabel.Location = new Point(386, 395);
            _SampleSizeTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _SampleSizeTextBoxLabel.Name = "_SampleSizeTextBoxLabel";
            _SampleSizeTextBoxLabel.Size = new Size(116, 16);
            _SampleSizeTextBoxLabel.TabIndex = 91;
            _SampleSizeTextBoxLabel.Text = "Sample Size:";
            _SampleSizeTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            _ToolTip.SetToolTip(_SampleSizeTextBoxLabel, "The number of samples used per channel buffer.");
            // 
            // _SampleSizeTextBox
            // 
            _SampleSizeTextBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SampleSizeTextBox.Location = new Point(503, 389);
            _SampleSizeTextBox.Margin = new Padding(4, 3, 4, 3);
            _SampleSizeTextBox.Name = "_SampleSizeTextBox";
            _SampleSizeTextBox.Size = new Size(47, 25);
            _SampleSizeTextBox.TabIndex = 93;
            _SampleSizeTextBox.Text = "1000";
            _ToolTip.SetToolTip(_SampleSizeTextBox, "Number of samples per channel buffer");
            // 
            // _SignalMemorySizeTextBox
            // 
            _SignalMemorySizeTextBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SignalMemorySizeTextBox.BackColor = SystemColors.Window;
            _SignalMemorySizeTextBox.Enabled = false;
            _SignalMemorySizeTextBox.Location = new Point(503, 445);
            _SignalMemorySizeTextBox.Margin = new Padding(4, 3, 4, 3);
            _SignalMemorySizeTextBox.Name = "_SignalMemorySizeTextBox";
            _SignalMemorySizeTextBox.Size = new Size(47, 25);
            _SignalMemorySizeTextBox.TabIndex = 94;
            _SignalMemorySizeTextBox.Text = "10000";
            _ToolTip.SetToolTip(_SignalMemorySizeTextBox, "Number of samples stored in the strip chart voltage buffer.");
            // 
            // _SamplingRateTextBox
            // 
            _SamplingRateTextBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SamplingRateTextBox.Location = new Point(503, 361);
            _SamplingRateTextBox.Margin = new Padding(4, 3, 4, 3);
            _SamplingRateTextBox.Name = "_SamplingRateTextBox";
            _SamplingRateTextBox.Size = new Size(47, 25);
            _SamplingRateTextBox.TabIndex = 96;
            _SamplingRateTextBox.Text = "5000";
            _ToolTip.SetToolTip(_SamplingRateTextBox, "Actual desired sampling rate");
            // 
            // _SignalBufferSizeTextBox
            // 
            _SignalBufferSizeTextBox.BackColor = SystemColors.Window;
            _SignalBufferSizeTextBox.Enabled = false;
            _SignalBufferSizeTextBox.Location = new Point(325, 445);
            _SignalBufferSizeTextBox.Margin = new Padding(4, 3, 4, 3);
            _SignalBufferSizeTextBox.Name = "_SignalBufferSizeTextBox";
            _SignalBufferSizeTextBox.Size = new Size(47, 25);
            _SignalBufferSizeTextBox.TabIndex = 110;
            _SignalBufferSizeTextBox.Text = "1000";
            _ToolTip.SetToolTip(_SignalBufferSizeTextBox, "Number of points per display");
            // 
            // _DisplayRateTextBox
            // 
            _DisplayRateTextBox.BackColor = SystemColors.Window;
            _DisplayRateTextBox.Location = new Point(325, 423);
            _DisplayRateTextBox.Margin = new Padding(4, 3, 4, 3);
            _DisplayRateTextBox.Name = "_DisplayRateTextBox";
            _DisplayRateTextBox.Size = new Size(47, 25);
            _DisplayRateTextBox.TabIndex = 94;
            _DisplayRateTextBox.Text = "24.0";
            _ToolTip.SetToolTip(_DisplayRateTextBox, "Refresh rate for display.");
            // 
            // _AbortButton
            // 
            _AbortButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _AbortButton.Location = new Point(802, 428);
            _AbortButton.Margin = new Padding(4, 3, 4, 3);
            _AbortButton.Name = "_AbortButton";
            _AbortButton.Size = new Size(95, 32);
            _AbortButton.TabIndex = 102;
            _AbortButton.Text = "Abort";
            _AbortButton.Click += new EventHandler( AbortButton_Click );
            // 
            // _EnablePlottingCheckBox
            // 
            _EnablePlottingCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _EnablePlottingCheckBox.AutoSize = true;
            _EnablePlottingCheckBox.Location = new Point(567, 363);
            _EnablePlottingCheckBox.Margin = new Padding(4, 3, 4, 3);
            _EnablePlottingCheckBox.Name = "_EnablePlottingCheckBox";
            _EnablePlottingCheckBox.Size = new Size(114, 21);
            _EnablePlottingCheckBox.TabIndex = 100;
            _EnablePlottingCheckBox.Text = "Enable Plotting";
            // 
            // _StopButton
            // 
            _StopButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _StopButton.Location = new Point(802, 388);
            _StopButton.Margin = new Padding(4, 3, 4, 3);
            _StopButton.Name = "_StopButton";
            _StopButton.Size = new Size(95, 32);
            _StopButton.TabIndex = 98;
            _StopButton.Text = "Stop";
            _StopButton.Click += new EventHandler( StopButton_Click );
            // 
            // _StartButton
            // 
            _StartButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _StartButton.Location = new Point(802, 348);
            _StartButton.Margin = new Padding(4, 3, 4, 3);
            _StartButton.Name = "_StartButton";
            _StartButton.Size = new Size(95, 32);
            _StartButton.TabIndex = 97;
            _StartButton.Text = "Start";
            _StartButton.Click += new EventHandler( StartButton_Click );
            // 
            // _ConfigButton
            // 
            _ConfigButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _ConfigButton.Location = new Point(694, 376);
            _ConfigButton.Margin = new Padding(4, 3, 4, 3);
            _ConfigButton.Name = "_ConfigButton";
            _ConfigButton.Size = new Size(92, 32);
            _ConfigButton.TabIndex = 63;
            _ConfigButton.Text = "Configure";
            _ConfigButton.Click += new EventHandler( ConfigButton_Click );
            // 
            // _BuffersCountTextBox
            // 
            _BuffersCountTextBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _BuffersCountTextBox.Location = new Point(503, 423);
            _BuffersCountTextBox.Margin = new Padding(4, 3, 4, 3);
            _BuffersCountTextBox.Name = "_BuffersCountTextBox";
            _BuffersCountTextBox.Size = new Size(47, 25);
            _BuffersCountTextBox.TabIndex = 93;
            _BuffersCountTextBox.Text = "5";
            // 
            // _FrequencyTextBoxLabel
            // 
            _FrequencyTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _FrequencyTextBoxLabel.Location = new Point(208, 365);
            _FrequencyTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _FrequencyTextBoxLabel.Name = "_FrequencyTextBoxLabel";
            _FrequencyTextBoxLabel.Size = new Size(116, 16);
            _FrequencyTextBoxLabel.TabIndex = 95;
            _FrequencyTextBoxLabel.Text = "Clock Frequency:";
            _FrequencyTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _ChannelBufferSizeTextBoxLabel
            // 
            _ChannelBufferSizeTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _ChannelBufferSizeTextBoxLabel.Location = new Point(208, 393);
            _ChannelBufferSizeTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _ChannelBufferSizeTextBoxLabel.Name = "_ChannelBufferSizeTextBoxLabel";
            _ChannelBufferSizeTextBoxLabel.Size = new Size(116, 16);
            _ChannelBufferSizeTextBoxLabel.TabIndex = 92;
            _ChannelBufferSizeTextBoxLabel.Text = "Channel Buffer Size:";
            _ChannelBufferSizeTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _BuffersCountTextBoxLabel
            // 
            _BuffersCountTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _BuffersCountTextBoxLabel.ImageAlign = ContentAlignment.MiddleRight;
            _BuffersCountTextBoxLabel.Location = new Point(386, 425);
            _BuffersCountTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _BuffersCountTextBoxLabel.Name = "_BuffersCountTextBoxLabel";
            _BuffersCountTextBoxLabel.Size = new Size(116, 16);
            _BuffersCountTextBoxLabel.TabIndex = 91;
            _BuffersCountTextBoxLabel.Text = "Number of Buffers:";
            _BuffersCountTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _DeviceNameLabel
            // 
            _DeviceNameLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _DeviceNameLabel.Location = new Point(29, 376);
            _DeviceNameLabel.Margin = new Padding(4, 0, 4, 0);
            _DeviceNameLabel.Name = "_DeviceNameLabel";
            _DeviceNameLabel.Size = new Size(110, 16);
            _DeviceNameLabel.TabIndex = 87;
            _DeviceNameLabel.Text = "Device Name:";
            _DeviceNameLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _YMax2Label
            // 
            _YMax2Label.Cursor = Cursors.Default;
            _YMax2Label.Location = new Point(516, 561);
            _YMax2Label.Margin = new Padding(4, 0, 4, 0);
            _YMax2Label.Name = "_YMax2Label";
            _YMax2Label.Size = new Size(65, 13);
            _YMax2Label.TabIndex = 85;
            _YMax2Label.Text = "(0...20)";
            _YMax2Label.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _YMin2Label
            // 
            _YMin2Label.Location = new Point(232, 561);
            _YMin2Label.Margin = new Padding(4, 0, 4, 0);
            _YMin2Label.Name = "_YMin2Label";
            _YMin2Label.Size = new Size(65, 13);
            _YMin2Label.TabIndex = 84;
            _YMin2Label.Text = "(-20...0)";
            _YMin2Label.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _AutoScaleButton
            // 
            _AutoScaleButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _AutoScaleButton.Location = new Point(802, 184);
            _AutoScaleButton.Margin = new Padding(4, 3, 4, 3);
            _AutoScaleButton.Name = "_AutoScaleButton";
            _AutoScaleButton.Size = new Size(95, 23);
            _AutoScaleButton.TabIndex = 74;
            _AutoScaleButton.Text = "Auto Scale";
            _AutoScaleButton.Click += new EventHandler( AutoScaleButton_Click );
            // 
            // _ColorGridsButton
            // 
            _ColorGridsButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ColorGridsButton.Location = new Point(802, 152);
            _ColorGridsButton.Margin = new Padding(4, 3, 4, 3);
            _ColorGridsButton.Name = "_ColorGridsButton";
            _ColorGridsButton.Size = new Size(95, 23);
            _ColorGridsButton.TabIndex = 73;
            _ColorGridsButton.Text = "Grids color";
            _ColorGridsButton.Click += new EventHandler( ColorGridsButton_Click );
            // 
            // _SeparatorPanel2
            // 
            _SeparatorPanel2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _SeparatorPanel2.BackColor = SystemColors.Control;
            _SeparatorPanel2.BorderStyle = BorderStyle.FixedSingle;
            _SeparatorPanel2.ForeColor = SystemColors.ControlText;
            _SeparatorPanel2.Location = new Point(14, 472);
            _SeparatorPanel2.Margin = new Padding(4, 3, 4, 3);
            _SeparatorPanel2.Name = "_SeparatorPanel2";
            _SeparatorPanel2.Size = new Size(892, 1);
            _SeparatorPanel2.TabIndex = 83;
            // 
            // _SeparatorPanel1
            // 
            _SeparatorPanel1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _SeparatorPanel1.BackColor = SystemColors.Control;
            _SeparatorPanel1.BorderStyle = BorderStyle.FixedSingle;
            _SeparatorPanel1.ForeColor = SystemColors.ControlText;
            _SeparatorPanel1.Location = new Point(14, 532);
            _SeparatorPanel1.Margin = new Padding(4, 3, 4, 3);
            _SeparatorPanel1.Name = "_SeparatorPanel1";
            _SeparatorPanel1.Size = new Size(892, 1);
            _SeparatorPanel1.TabIndex = 82;
            // 
            // _ColorAxesButton
            // 
            _ColorAxesButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ColorAxesButton.Location = new Point(802, 120);
            _ColorAxesButton.Margin = new Padding(4, 3, 4, 3);
            _ColorAxesButton.Name = "_ColorAxesButton";
            _ColorAxesButton.Size = new Size(95, 23);
            _ColorAxesButton.TabIndex = 72;
            _ColorAxesButton.Text = "Axes color";
            _ColorAxesButton.Click += new EventHandler( ColorAxesButton_Click );
            // 
            // _ColorDataButton
            // 
            _ColorDataButton.Location = new Point(802, 550);
            _ColorDataButton.Margin = new Padding(4, 3, 4, 3);
            _ColorDataButton.Name = "_ColorDataButton";
            _ColorDataButton.Size = new Size(86, 23);
            _ColorDataButton.TabIndex = 71;
            _ColorDataButton.Text = "Signal color";
            _ColorDataButton.Click += new EventHandler( ColorDataButton_Click );
            // 
            // _SignalListComboBox
            // 
            _SignalListComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _SignalListComboBox.Location = new Point(72, 551);
            _SignalListComboBox.Margin = new Padding(4, 3, 4, 3);
            _SignalListComboBox.Name = "_SignalListComboBox";
            _SignalListComboBox.Size = new Size(114, 25);
            _SignalListComboBox.TabIndex = 68;
            _SignalListComboBox.SelectedValueChanged += new EventHandler( SignalListComboBox_SelectedValueChanged );
            // 
            // _YMaxTrackBar
            // 
            _YMaxTrackBar.LargeChange = 2;
            _YMaxTrackBar.Location = new Point(581, 539);
            _YMaxTrackBar.Margin = new Padding(4, 3, 4, 3);
            _YMaxTrackBar.Maximum = 20;
            _YMaxTrackBar.Name = "_YMaxTrackBar";
            _YMaxTrackBar.Size = new Size(192, 45);
            _YMaxTrackBar.SmallChange = 10;
            _YMaxTrackBar.TabIndex = 70;
            _YMaxTrackBar.Value = 20;
            _YMaxTrackBar.Scroll += new EventHandler( YMaxTrackBar_Scroll );
            // 
            // _YMinTrackBar
            // 
            _YMinTrackBar.LargeChange = 2;
            _YMinTrackBar.Location = new Point(293, 539);
            _YMinTrackBar.Margin = new Padding(4, 3, 4, 3);
            _YMinTrackBar.Maximum = 0;
            _YMinTrackBar.Minimum = -20;
            _YMinTrackBar.Name = "_YMinTrackBar";
            _YMinTrackBar.Size = new Size(192, 45);
            _YMinTrackBar.SmallChange = 10;
            _YMinTrackBar.TabIndex = 69;
            _YMinTrackBar.Scroll += new EventHandler( YMinTrackBar_Scroll );
            // 
            // _YMax1Label
            // 
            _YMax1Label.Location = new Point(516, 545);
            _YMax1Label.Margin = new Padding(4, 0, 4, 0);
            _YMax1Label.Name = "_YMax1Label";
            _YMax1Label.Size = new Size(65, 13);
            _YMax1Label.TabIndex = 81;
            _YMax1Label.Text = "max";
            _YMax1Label.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _YMin1Label
            // 
            _YMin1Label.Location = new Point(232, 545);
            _YMin1Label.Margin = new Padding(4, 0, 4, 0);
            _YMin1Label.Name = "_YMin1Label";
            _YMin1Label.Size = new Size(65, 13);
            _YMin1Label.TabIndex = 80;
            _YMin1Label.Text = "min";
            _YMin1Label.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _XMaxTrackBar
            // 
            _XMaxTrackBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMaxTrackBar.LargeChange = 100;
            _XMaxTrackBar.Location = new Point(581, 480);
            _XMaxTrackBar.Margin = new Padding(4, 3, 4, 3);
            _XMaxTrackBar.Maximum = 2500;
            _XMaxTrackBar.Minimum = 10;
            _XMaxTrackBar.Name = "_XMaxTrackBar";
            _XMaxTrackBar.Size = new Size(192, 45);
            _XMaxTrackBar.SmallChange = 10;
            _XMaxTrackBar.TabIndex = 67;
            _XMaxTrackBar.TickFrequency = 100;
            _XMaxTrackBar.Value = 1000;
            _XMaxTrackBar.Scroll += new EventHandler( XMaxTrackBar_Scroll );
            // 
            // _XMinTrackBar
            // 
            _XMinTrackBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMinTrackBar.LargeChange = 100;
            _XMinTrackBar.Location = new Point(293, 480);
            _XMinTrackBar.Margin = new Padding(4, 3, 4, 3);
            _XMinTrackBar.Maximum = 990;
            _XMinTrackBar.Name = "_XMinTrackBar";
            _XMinTrackBar.Size = new Size(192, 45);
            _XMinTrackBar.SmallChange = 10;
            _XMinTrackBar.TabIndex = 66;
            _XMinTrackBar.TickFrequency = 100;
            _XMinTrackBar.Scroll += new EventHandler( XMinTrackBar_Scroll );
            // 
            // _XMaxLabel
            // 
            _XMaxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMaxLabel.Location = new Point(485, 491);
            _XMaxLabel.Margin = new Padding(4, 0, 4, 0);
            _XMaxLabel.Name = "_XMaxLabel";
            _XMaxLabel.Size = new Size(96, 23);
            _XMaxLabel.TabIndex = 79;
            _XMaxLabel.Text = "max (0...2500)";
            _XMaxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _XMinLabel
            // 
            _XMinLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMinLabel.Location = new Point(201, 491);
            _XMinLabel.Margin = new Padding(4, 0, 4, 0);
            _XMinLabel.Name = "_XMinLabel";
            _XMinLabel.Size = new Size(96, 23);
            _XMinLabel.TabIndex = 78;
            _XMinLabel.Text = "min (0...1000)";
            _XMinLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _YAxisLabel
            // 
            _YAxisLabel.Location = new Point(24, 555);
            _YAxisLabel.Margin = new Padding(4, 0, 4, 0);
            _YAxisLabel.Name = "_YAxisLabel";
            _YAxisLabel.Size = new Size(48, 13);
            _YAxisLabel.TabIndex = 77;
            _YAxisLabel.Text = "Y-Axis:";
            _YAxisLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _XAxisLabel
            // 
            _XAxisLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XAxisLabel.Location = new Point(36, 496);
            _XAxisLabel.Margin = new Padding(4, 0, 4, 0);
            _XAxisLabel.Name = "_XAxisLabel";
            _XAxisLabel.Size = new Size(48, 13);
            _XAxisLabel.TabIndex = 76;
            _XAxisLabel.Text = "X-Axis:";
            // 
            // _BandModeGroupBox
            // 
            _BandModeGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _BandModeGroupBox.Controls.Add(_MultipleRadioButton);
            _BandModeGroupBox.Controls.Add(_SingleRadioButton);
            _BandModeGroupBox.Location = new Point(792, 8);
            _BandModeGroupBox.Margin = new Padding(4, 3, 4, 3);
            _BandModeGroupBox.Name = "_BandModeGroupBox";
            _BandModeGroupBox.Padding = new Padding(4, 3, 4, 3);
            _BandModeGroupBox.Size = new Size(106, 100);
            _BandModeGroupBox.TabIndex = 75;
            _BandModeGroupBox.TabStop = false;
            _BandModeGroupBox.Text = "Band mode";
            // 
            // _MultipleRadioButton
            // 
            _MultipleRadioButton.Location = new Point(19, 56);
            _MultipleRadioButton.Margin = new Padding(4, 3, 4, 3);
            _MultipleRadioButton.Name = "_MultipleRadioButton";
            _MultipleRadioButton.Size = new Size(77, 24);
            _MultipleRadioButton.TabIndex = 1;
            _MultipleRadioButton.Text = "Multiple";
            // 
            // _SingleRadioButton
            // 
            _SingleRadioButton.Checked = true;
            _SingleRadioButton.Location = new Point(19, 24);
            _SingleRadioButton.Margin = new Padding(4, 3, 4, 3);
            _SingleRadioButton.Name = "_SingleRadioButton";
            _SingleRadioButton.Size = new Size(67, 24);
            _SingleRadioButton.TabIndex = 0;
            _SingleRadioButton.TabStop = true;
            _SingleRadioButton.Text = "Single";
            _SingleRadioButton.CheckedChanged += new EventHandler( SingleRadioButton_CheckedChanged );
            // 
            // _DeviceNameChooser1
            // 
            _DeviceNameChooser1.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _DeviceNameChooser1.Location = new Point(29, 391);
            _DeviceNameChooser1.Margin = new Padding(4, 3, 4, 3);
            _DeviceNameChooser1.Name = "_DeviceNameChooser1";
            _DeviceNameChooser1.Size = new Size(144, 25);
            _DeviceNameChooser1.TabIndex = 104;
            _DeviceNameChooser1.DeviceSelected += new EventHandler<EventArgs>( DeviceNameChooser1_DeviceSelected );
            // 
            // _Chart
            // 
            _Chart.AutoScale = false;
            _Chart.AxesColor = Color.Blue;
            _Chart.BackGradientAngle = 0f;
            _Chart.BackGradientColor = SystemColors.Control;
            _Chart.BackgroundStyle = OpenLayers.Chart.Layers.BackgroundStyle.Color;
            _Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand;
            _Chart.Footer = string.Empty;
            _Chart.FooterFont = new Font("Segoe UI", 8.0f);
            _Chart.GridColor = Color.Blue;
            _Chart.Location = new Point(12, 8);
            _Chart.Name = "_Chart";
            _Chart.SignalBufferLength = Conversions.ToLong(0);
            _Chart.Size = new Size(773, 349);
            _Chart.TabIndex = 105;
            _Chart.Title = "Chart";
            _Chart.TitleFont = new Font("Segoe UI", 9.75f, FontStyle.Bold | FontStyle.Underline);
            _Chart.XDataCurrentRangeMax = 1000.0d;
            _Chart.XDataCurrentRangeMin = 0d;
            _Chart.XDataName = "Time";
            _Chart.XDataRangeMax = 1000.0d;
            _Chart.XDataRangeMin = 0d;
            _Chart.XDataUnit = "sec";
            // 
            // _StripChartCheckBox
            // 
            _StripChartCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _StripChartCheckBox.AutoSize = true;
            _StripChartCheckBox.Location = new Point(567, 421);
            _StripChartCheckBox.Margin = new Padding(4, 3, 4, 3);
            _StripChartCheckBox.Name = "_StripChartCheckBox";
            _StripChartCheckBox.Size = new Size(128, 21);
            _StripChartCheckBox.TabIndex = 106;
            _StripChartCheckBox.Text = "Strip Chart Mode";
            _StripChartCheckBox.CheckedChanged += new EventHandler( StripChartCheckBox_CheckedChanged );
            // 
            // _SignalBufferSizeTextBoxLabel
            // 
            _SignalBufferSizeTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SignalBufferSizeTextBoxLabel.Location = new Point(208, 449);
            _SignalBufferSizeTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _SignalBufferSizeTextBoxLabel.Name = "_SignalBufferSizeTextBoxLabel";
            _SignalBufferSizeTextBoxLabel.Size = new Size(116, 16);
            _SignalBufferSizeTextBoxLabel.TabIndex = 92;
            _SignalBufferSizeTextBoxLabel.Text = "Signal Buffer Size:";
            _SignalBufferSizeTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _RestoreDefaultsButton
            // 
            _RestoreDefaultsButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _RestoreDefaultsButton.Location = new Point(694, 417);
            _RestoreDefaultsButton.Margin = new Padding(4, 3, 4, 3);
            _RestoreDefaultsButton.Name = "_RestoreDefaultsButton";
            _RestoreDefaultsButton.Size = new Size(92, 48);
            _RestoreDefaultsButton.TabIndex = 107;
            _RestoreDefaultsButton.Text = "Restore Defaults";
            _RestoreDefaultsButton.Click += new EventHandler( RestoreDefaultsButton_Click );
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _StatusToolStripStatusLabel, _BuffersToolStripStatusLabel });
            _StatusStrip.Location = new Point(0, 582);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Size = new Size(922, 24);
            _StatusStrip.TabIndex = 108;
            _StatusStrip.Text = "_StatusStrip1";
            // 
            // _StatusToolStripStatusLabel
            // 
            _StatusToolStripStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.Left | ToolStripStatusLabelBorderSides.Top | ToolStripStatusLabelBorderSides.Right | ToolStripStatusLabelBorderSides.Bottom;

            _StatusToolStripStatusLabel.Name = "_StatusToolStripStatusLabel";
            _StatusToolStripStatusLabel.Size = new Size(890, 19);
            _StatusToolStripStatusLabel.Spring = true;
            _StatusToolStripStatusLabel.Text = "Status: None";
            _StatusToolStripStatusLabel.TextAlign = ContentAlignment.TopLeft;
            // 
            // _BuffersToolStripStatusLabel
            // 
            _BuffersToolStripStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.Left | ToolStripStatusLabelBorderSides.Top | ToolStripStatusLabelBorderSides.Right | ToolStripStatusLabelBorderSides.Bottom;

            _BuffersToolStripStatusLabel.Name = "_BuffersToolStripStatusLabel";
            _BuffersToolStripStatusLabel.Size = new Size(17, 19);
            _BuffersToolStripStatusLabel.Text = "0";
            _BuffersToolStripStatusLabel.TextAlign = ContentAlignment.TopCenter;
            _BuffersToolStripStatusLabel.ToolTipText = "Buffers completed";
            // 
            // _SignalMemorySizeTextBoxLabel
            // 
            _SignalMemorySizeTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SignalMemorySizeTextBoxLabel.Location = new Point(386, 449);
            _SignalMemorySizeTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _SignalMemorySizeTextBoxLabel.Name = "_SignalMemorySizeTextBoxLabel";
            _SignalMemorySizeTextBoxLabel.Size = new Size(116, 16);
            _SignalMemorySizeTextBoxLabel.TabIndex = 92;
            _SignalMemorySizeTextBoxLabel.Text = "Signal Memory Size:";
            _SignalMemorySizeTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _SamplingRateTextBoxLabel
            // 
            _SamplingRateTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SamplingRateTextBoxLabel.Location = new Point(386, 365);
            _SamplingRateTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _SamplingRateTextBoxLabel.Name = "_SamplingRateTextBoxLabel";
            _SamplingRateTextBoxLabel.Size = new Size(116, 16);
            _SamplingRateTextBoxLabel.TabIndex = 95;
            _SamplingRateTextBoxLabel.Text = "Sampling Rate:";
            _SamplingRateTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _DisplayRateTextBoxLabel
            // 
            _DisplayRateTextBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _DisplayRateTextBoxLabel.Location = new Point(208, 425);
            _DisplayRateTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _DisplayRateTextBoxLabel.Name = "_DisplayRateTextBoxLabel";
            _DisplayRateTextBoxLabel.Size = new Size(116, 16);
            _DisplayRateTextBoxLabel.TabIndex = 92;
            _DisplayRateTextBoxLabel.Text = "Display Rate [1/sec]: ";
            _DisplayRateTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // Label1
            // 
            Label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            Label1.ImageAlign = ContentAlignment.MiddleRight;
            Label1.Location = new Point(386, 421);
            Label1.Margin = new Padding(4, 0, 4, 0);
            Label1.Name = "Label1";
            Label1.Size = new Size(116, 16);
            Label1.TabIndex = 91;
            Label1.Text = "Number of Buffers:";
            Label1.TextAlign = ContentAlignment.MiddleRight;
            // 
            // Label2
            // 
            Label2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            Label2.Location = new Point(208, 421);
            Label2.Margin = new Padding(4, 0, 4, 0);
            Label2.Name = "Label2";
            Label2.Size = new Size(116, 16);
            Label2.TabIndex = 92;
            Label2.Text = "Display Rate [1/sec]: ";
            Label2.TextAlign = ContentAlignment.MiddleRight;
            // 
            // TextBox1
            // 
            TextBox1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            TextBox1.Location = new Point(503, 417);
            TextBox1.Margin = new Padding(4, 3, 4, 3);
            TextBox1.Name = "TextBox1";
            TextBox1.Size = new Size(47, 25);
            TextBox1.TabIndex = 93;
            TextBox1.Text = "5";
            // 
            // TextBox2
            // 
            TextBox2.BackColor = SystemColors.Window;
            TextBox2.Location = new Point(325, 417);
            TextBox2.Margin = new Padding(4, 3, 4, 3);
            TextBox2.Name = "TextBox2";
            TextBox2.Size = new Size(47, 25);
            TextBox2.TabIndex = 94;
            TextBox2.Text = "24.0";
            // 
            // CheckBox1
            // 
            CheckBox1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            CheckBox1.AutoSize = true;
            CheckBox1.Location = new Point(567, 419);
            CheckBox1.Margin = new Padding(4, 3, 4, 3);
            CheckBox1.Name = "CheckBox1";
            CheckBox1.Size = new Size(128, 21);
            CheckBox1.TabIndex = 106;
            CheckBox1.Text = "Strip Chart Mode";
            // 
            // ScopePanel
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            ClientSize = new Size(922, 606);
            Controls.Add(_SignalBufferSizeTextBox);
            Controls.Add(_StatusStrip);
            Controls.Add(_RestoreDefaultsButton);
            Controls.Add(CheckBox1);
            Controls.Add(_StripChartCheckBox);
            Controls.Add(_Chart);
            Controls.Add(_DeviceNameChooser1);
            Controls.Add(_AbortButton);
            Controls.Add(_SmartBufferingCheckBox);
            Controls.Add(_FiniteBufferingCheckBox);
            Controls.Add(_EnablePlottingCheckBox);
            Controls.Add(_StopButton);
            Controls.Add(_StartButton);
            Controls.Add(_ConfigButton);
            Controls.Add(_SamplingRateTextBox);
            Controls.Add(_FrequencyTextBox);
            Controls.Add(_SignalMemorySizeTextBox);
            Controls.Add(_SampleSizeTextBox);
            Controls.Add(TextBox2);
            Controls.Add(_DisplayRateTextBox);
            Controls.Add(_ChannelBufferSizeTextBox);
            Controls.Add(TextBox1);
            Controls.Add(_BuffersCountTextBox);
            Controls.Add(_SamplingRateTextBoxLabel);
            Controls.Add(_SignalBufferSizeTextBoxLabel);
            Controls.Add(_SignalMemorySizeTextBoxLabel);
            Controls.Add(_FrequencyTextBoxLabel);
            Controls.Add(Label2);
            Controls.Add(_DisplayRateTextBoxLabel);
            Controls.Add(_SampleSizeTextBoxLabel);
            Controls.Add(Label1);
            Controls.Add(_ChannelBufferSizeTextBoxLabel);
            Controls.Add(_BuffersCountTextBoxLabel);
            Controls.Add(_InitializeButton);
            Controls.Add(_DeviceNameLabel);
            Controls.Add(_YMax2Label);
            Controls.Add(_YMin2Label);
            Controls.Add(_AutoScaleButton);
            Controls.Add(_ColorGridsButton);
            Controls.Add(_SeparatorPanel2);
            Controls.Add(_SeparatorPanel1);
            Controls.Add(_ColorAxesButton);
            Controls.Add(_ColorDataButton);
            Controls.Add(_DeleteSignalButton);
            Controls.Add(_SignalListComboBox);
            Controls.Add(_YMaxTrackBar);
            Controls.Add(_YMinTrackBar);
            Controls.Add(_YMax1Label);
            Controls.Add(_YMin1Label);
            Controls.Add(_XMaxTrackBar);
            Controls.Add(_XMinTrackBar);
            Controls.Add(_XMaxLabel);
            Controls.Add(_XMinLabel);
            Controls.Add(_YAxisLabel);
            Controls.Add(_XAxisLabel);
            Controls.Add(_BandModeGroupBox);
            Controls.Add(_AddSignalButton);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "ScopePanel";
            Text = "Scope Panel";
            ((System.ComponentModel.ISupportInitialize)_YMaxTrackBar).EndInit();
            ((System.ComponentModel.ISupportInitialize)_YMinTrackBar).EndInit();
            ((System.ComponentModel.ISupportInitialize)_XMaxTrackBar).EndInit();
            ((System.ComponentModel.ISupportInitialize)_XMinTrackBar).EndInit();
            _BandModeGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_Chart).EndInit();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            Closing += new System.ComponentModel.CancelEventHandler(Form_Closing);
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
            PerformLayout();
        }

        private OpenLayers.Controls.Display _Chart;
        private CheckBox _StripChartCheckBox;
        private Label _SignalBufferSizeTextBoxLabel;
        private Button _RestoreDefaultsButton;
        private CheckBox _FiniteBufferingCheckBox;
        private CheckBox _SmartBufferingCheckBox;
        private StatusStrip _StatusStrip;
        private ToolStripStatusLabel _StatusToolStripStatusLabel;
        private ToolStripStatusLabel _BuffersToolStripStatusLabel;
        private Label _SampleSizeTextBoxLabel;
        private Label _SignalMemorySizeTextBoxLabel;
        private Label _SamplingRateTextBoxLabel;
        private TextBox _SampleSizeTextBox;
        private TextBox _SignalMemorySizeTextBox;
        private TextBox _SamplingRateTextBox;
        private TextBox _SignalBufferSizeTextBox;
        private Label _DisplayRateTextBoxLabel;
        private TextBox _DisplayRateTextBox;
        private Label Label1;
        private Label Label2;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private CheckBox CheckBox1;
    }
}
