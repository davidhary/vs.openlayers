﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.IO.OL.Scope.Demo.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public ScopePanel m_ScopePanel;

            public ScopePanel ScopePanel
            {
                [DebuggerHidden]
                get
                {
                    m_ScopePanel = Create__Instance__(m_ScopePanel);
                    return m_ScopePanel;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_ScopePanel))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_ScopePanel);
                }
            }
        }
    }
}