using System;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;

using Microsoft.VisualBasic;

namespace isr.IO.OL.Scope.Demo.My
{

    /// <summary>   my application. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
    [System.CodeDom.Compiler.GeneratedCode("MyTemplate", "11.0.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal partial class MyApplication : Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase
    {
        [STAThread()]
        [DebuggerHidden()]
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
        internal static void Main( string[] Args )
        {
            try
            {
                Application.SetCompatibleTextRenderingDefault( UseCompatibleTextRendering );
            }
            finally
            {
            }

            MyProject.Application.Run( Args );
        }
    }

    /// <summary>   my computer. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [System.CodeDom.Compiler.GeneratedCode("MyTemplate", "11.0.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal partial class MyComputer : Microsoft.VisualBasic.Devices.Computer
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [DebuggerHidden()]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public MyComputer() : base()
        {
        }
    }

        /// <summary>   my project. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
    [HideModuleName()]
    [System.CodeDom.Compiler.GeneratedCode("MyTemplate", "11.0.0.0")]
    internal static partial class MyProject
    {

        /// <summary>   The computer object provider. </summary>
        private readonly static ThreadSafeObjectProvider<MyComputer> _ComputerObjectProvider = new ThreadSafeObjectProvider<MyComputer>();

        /// <summary>   Gets the computer. </summary>
        /// <value> The computer. </value>
        [System.ComponentModel.Design.HelpKeyword("My.Computer")]
        internal static MyComputer Computer
        {
            [DebuggerHidden()]
            get
            {
                return _ComputerObjectProvider.GetInstance;
            }
        }

        /// <summary>   The application object provider. </summary>
        private readonly static ThreadSafeObjectProvider<MyApplication> _AppObjectProvider = new ThreadSafeObjectProvider<MyApplication>();

        /// <summary>   Gets the application. </summary>
        /// <value> The application. </value>
        [System.ComponentModel.Design.HelpKeyword("My.Application")]
        internal static MyApplication Application
        {
            [DebuggerHidden()]
            get
            {
                return _AppObjectProvider.GetInstance;
            }
        }

        /// <summary>   The user object provider. </summary>
        private readonly static ThreadSafeObjectProvider<Microsoft.VisualBasic.ApplicationServices.User> _UserObjectProvider = new ThreadSafeObjectProvider<Microsoft.VisualBasic.ApplicationServices.User>();

        /// <summary>   Gets the user. </summary>
        /// <value> The user. </value>
        [System.ComponentModel.Design.HelpKeyword("My.User")]
        internal static Microsoft.VisualBasic.ApplicationServices.User User
        {
            [DebuggerHidden()]
            get
            {
                return _UserObjectProvider.GetInstance;
            }
        }

        /// <summary>   my web services object provider. </summary>
        private readonly static ThreadSafeObjectProvider<MyWebServices> _MyWebServicesObjectProvider = new ThreadSafeObjectProvider<MyWebServices>();

        /// <summary>   Gets the web services. </summary>
        /// <value> The web services. </value>
        [System.ComponentModel.Design.HelpKeyword("My.WebServices")]
        internal static MyWebServices WebServices
        {
            [DebuggerHidden()]
            get
            {
                return _MyWebServicesObjectProvider.GetInstance;
            }
        }

        /// <summary>   my web services. This class cannot be inherited. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        [MyGroupCollection("System.Web.Services.Protocols.SoapHttpClientProtocol", "Create__Instance__", "Dispose__Instance__", "")]
        internal sealed class MyWebServices
        {
            /// <summary>   Determines whether the specified object is equal to the current object. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <param name="o">    The object to compare with the current object. </param>
            /// <returns>
            /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
            /// <see langword="false" />.
            /// </returns>
            [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
            [DebuggerHidden()]
            public override bool Equals(object o)
            {
                return base.Equals(o);
            }

            /// <summary>   Serves as the default hash function. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <returns>   A hash code for the current object. </returns>
            [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
            [DebuggerHidden()]
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            /// <summary>   Gets the <see cref="T:System.Type" /> of the current instance. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <returns>   The exact runtime type of the current instance. </returns>
            [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
            [DebuggerHidden()]
            internal new Type GetType()
            {
                return typeof(MyWebServices);
            }

            /// <summary>   Returns a string that represents the current object. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <returns>   A string that represents the current object. </returns>
            [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
            [DebuggerHidden()]
            public override string ToString()
            {
                return base.ToString();
            }

            /// <summary>   Creates an instance. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <typeparam name="T">    Generic type parameter. </typeparam>
            /// <param name="instance"> The instance. </param>
            /// <returns>   The new instance. </returns>
            [DebuggerHidden()]
            private static T Create__Instance__<T>(T instance) where T : new()
            {
                if (instance is not object)
                {
                    return new T();
                }
                else
                {
                    return instance;
                }
            }

            /// <summary>   Dispose instance. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <typeparam name="T">    Generic type parameter. </typeparam>
            /// <param name="instance"> [in,out] The instance. </param>
            [DebuggerHidden()]
            private void Dispose__Instance__<T>(ref T instance)
            {
                instance = default;
            }

            /// <summary>   Default constructor. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            [DebuggerHidden()]
            [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
            public MyWebServices() : base()
            {
            }
        }

        /// <summary>   A thread safe object provider. This class cannot be inherited. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Runtime.InteropServices.ComVisible(false)]
        internal sealed class ThreadSafeObjectProvider<T> where T : new()
        {
            /// <summary>   The context. </summary>
            private readonly Microsoft.VisualBasic.MyServices.Internal.ContextValue<T> _Context = new Microsoft.VisualBasic.MyServices.Internal.ContextValue<T>();
            /// <summary>   Gets the get instance. </summary>
            /// <value> The get instance. </value>
            internal T GetInstance
            {
                [DebuggerHidden()]
                get
                {
                    var Value = _Context.Value;
                    if (Value is not object)
                    {
                        Value = new T();
                        _Context.Value = Value;
                    }

                    return Value;
                }
            }

            /// <summary>   Default constructor. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            [DebuggerHidden()]
            [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
            public ThreadSafeObjectProvider() : base()
            {
            }

        }

        [System.ComponentModel.Design.HelpKeyword( "My.Forms" )]
        internal static MyForms Forms
        {
            [DebuggerHidden()]
            get {
                return m_MyFormsObjectProvider.GetInstance;
            }
        }

        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        [MyGroupCollection( "System.Windows.Forms.Form", "Create__Instance__", "Dispose__Instance__", "My.MyProject.Forms" )]
        internal sealed partial class MyForms
        {
            [DebuggerHidden()]
            private static T Create__Instance__<T>( T Instance ) where T : Form, new()
            {
                if ( Instance is null || Instance.IsDisposed )
                {
                    if ( m_FormBeingCreated is object )
                    {
                        if ( m_FormBeingCreated.ContainsKey( typeof( T ) ) == true )
                        {
                            throw new InvalidOperationException( Microsoft.VisualBasic.CompilerServices.Utils.GetResourceString( "WinForms_RecursiveFormCreate" ) );
                        }
                    }
                    else
                    {
                        m_FormBeingCreated = new Hashtable();
                    }

                    m_FormBeingCreated.Add( typeof( T ), null );
                    try
                    {
                        return new T();
                    }
                    catch ( System.Reflection.TargetInvocationException ex ) when ( ex.InnerException is object )
                    {
                        string BetterMessage = Microsoft.VisualBasic.CompilerServices.Utils.GetResourceString( "WinForms_SeeInnerException", ex.InnerException.Message );
                        throw new InvalidOperationException( BetterMessage, ex.InnerException );
                    }
                    finally
                    {
                        m_FormBeingCreated.Remove( typeof( T ) );
                    }
                }
                else
                {
                    return Instance;
                }
            }

            [DebuggerHidden()]
            private void Dispose__Instance__<T>( ref T instance ) where T : Form
            {
                instance.Dispose();
                instance = null;
            }

            [DebuggerHidden()]
            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            public MyForms() : base()
            {
            }

            [ThreadStatic()]
            private static Hashtable m_FormBeingCreated;

            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            public override bool Equals( object o )
            {
                return base.Equals( o );
            }

            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            internal new Type GetType()
            {
                return typeof( MyForms );
            }

            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            public override string ToString()
            {
                return base.ToString();
            }
        }

        private static ThreadSafeObjectProvider<MyForms> m_MyFormsObjectProvider = new ThreadSafeObjectProvider<MyForms>();

    }
}
