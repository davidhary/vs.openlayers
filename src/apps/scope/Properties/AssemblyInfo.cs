﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Review the values of the assembly attributes

[assembly: AssemblyTitle( "Open Layers Library Tester" )]
[assembly: AssemblyDescription( "Tester for the Open Layers Library" )]
[assembly: AssemblyProduct( "IO.Open.Layers.Library.Scope.Tester" )]
[assembly: CLSCompliant( false )]

// Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
[assembly: ComVisible( false )]
