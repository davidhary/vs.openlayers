using System;
using System.Diagnostics;
using System.Windows.Forms;
using isr.IO.OL.SingleIO.ExceptionExtensions;

namespace isr.IO.OL.SingleIO
{

    /// <summary> Form for viewing the spectrum panels. </summary>
/// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 5/6/2019 </para></remarks>
    public class SingleInputOutputForm : Core.Forma.ConsoleForm
    {

        /// <summary> Default constructor. </summary>
        public SingleInputOutputForm() : base()
        {
        }

        private Core.Forma.ModelViewBase ModelViewBase { get; set; } = null;

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2022-01-11. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose(bool disposing)
        {
            if ( this.ModelViewBase is object)
            {
                this.ModelViewBase.Dispose();
                this.ModelViewBase = null;
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks>   David, 2022-01-11. </remarks>
        /// <param name="e">    A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains
        ///                     the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing(e);
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks>   David, 2022-01-11. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected override void OnLoad(EventArgs e)
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding single I/O view";
                this.ModelViewBase = new WinViews.SingleInputOutputView();
                this.AddPropertyNotifyControl($"Single I/O", this.ModelViewBase, false, false);
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener(My.MyProject.Application.Logger);
            }
            catch (Exception ex)
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad(e);
            }
        }

        /// <summary> Gets or sets the await selection task enabled. </summary>
    /// <value> The await selection task enabled. </value>
        protected bool AwaitSelectionTaskEnabled { get; set; }

        /// <summary>
    /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    /// </summary>
    /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected override void OnShown(EventArgs e)
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ModelViewBase.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown(e);
            }
            catch (Exception ex)
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.ModelViewBase is object)
                    this.ModelViewBase.Cursor = Cursors.Default;
            }
        }

        #region " SINGLETON "

        /// <summary>
    /// The locking object to enforce thread safety when creating the singleton instance.
    /// </summary>
        private static readonly object SyncLocker = new();

        /// <summary>
    /// The shared instance.
    /// </summary>
        private static SingleInputOutputForm _Instance;

        /// <summary> Instantiates the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
        public static SingleInputOutputForm Get()
        {
            lock (SyncLocker)
            {
                if (!Instantiated)
                {
                    _Instance = new SingleInputOutputForm();
                }

                return _Instance;
            }
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> The instantiated. </value>
        internal static bool Instantiated => My.MyProject.Forms.m_SingleInputOutputForm is object && !My.MyProject.Forms.SingleInputOutputForm.IsDisposed;

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyProject.Application.Identify( this.Talker );
        }

        /// <summary>
    /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    /// </summary>
    /// <param name="eventType"> Type of the event. </param>
    /// <param name="activity">  The activity. </param>
    /// <returns> A String. </returns>
        protected override string Publish(TraceEventType eventType, string activity)
        {
            return this.Publish(new Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity));
        }

        /// <summary> Publish exception. </summary>
    /// <param name="activity"> The activity. </param>
    /// <param name="ex">       The ex. </param>
    /// <returns> A String. </returns>
        protected override string PublishException(string activity, Exception ex)
        {
            return this.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}");
        }

        #endregion

    }
}
