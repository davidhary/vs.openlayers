using System;
using System.Diagnostics;
using System.Linq;

namespace isr.IO.OL.SingleIO
{
    /// <summary> A sealed class the parses the command line and provides the command line values. </summary>
/// <remarks> (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>  
/// David, 02/02/2011, x.x.4050.x. </para></remarks>
    public sealed class CommandLineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private CommandLineInfo() : base()
        {
        }

        #endregion

        #region " PARSER "

        /// <summary> Gets the no-operation command line option. </summary>
        /// <value> The no operation option. </value>
        public static string NoOperationOption => "/nop";

        /// <summary> Gets the Device-Enabled option. </summary>
        /// <value> The Device enabled option. </value>
        public static string DevicesEnabledOption => "/d:";

        /// <summary> Validate the command line. </summary>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        /// not handled. </exception>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ValidateCommandLine(System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs)
        {
            if (commandLineArgs is null || !commandLineArgs.Any())
            {
                CommandLine = string.Empty;
            }
            else
            {
                CommandLine = string.Join(",", commandLineArgs);
                foreach (string argument in commandLineArgs)
                {
                    if (false)
                    {
                    }
                    else if (argument.StartsWith(DevicesEnabledOption, StringComparison.OrdinalIgnoreCase))
                    {
                    }
                    else
                    {
                        Nop = argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase )
                            ? true
                            : throw new ArgumentException( $"Unknown command line argument '{argument}' was detected. Should be Ignored.", nameof( commandLineArgs ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
    /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine(System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs)
        {
            if (commandLineArgs is null || !commandLineArgs.Any())
            {
                CommandLine = string.Empty;
            }
            else
            {
                CommandLine = string.Join(",", commandLineArgs);
                foreach (string argument in commandLineArgs)
                {
                    if (false)
                    {
                    }
                    else if (argument.StartsWith(DevicesEnabledOption, StringComparison.OrdinalIgnoreCase))
                    {
                        string value = argument.Substring(DevicesEnabledOption.Length);
                        DevicesEnabled = !value.StartsWith("n", StringComparison.OrdinalIgnoreCase);
                    }
                    else if (argument.StartsWith(NoOperationOption, StringComparison.OrdinalIgnoreCase))
                    {
                        Nop = true;
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
    /// <param name="commandLineArgs"> The command line arguments. </param>
    /// <returns> True if success or false if Exception occurred. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        public static bool TryParseCommandLine(System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs)
        {
            bool result = true;
            string activity = string.Empty;
            if (commandLineArgs is null || !commandLineArgs.Any())
            {
                CommandLine = string.Empty;
            }
            else
            {
                try
                {
                    CommandLine = string.Join(",", commandLineArgs);
                    activity = $"Parsing the commandLine {CommandLine}";
                    // Data.ParseCommandLine(commandLineArgs)
                    ParseCommandLine(commandLineArgs);
                }
                catch (ArgumentException ex)
                {
                    My.MyProject.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId, $"Failed {activity};  Unknown argument ignored");
                }
                catch (Exception ex)
                {
                    My.MyProject.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId, $"Failed {activity}");
                    result = false;
                }
            }

            return result;
        }

        #endregion

        #region " COMMAND LINE ELEMENTS "

        /// <summary> Gets the command line. </summary>
    /// <value> The command line. </value>
        public static string CommandLine { get; private set; }

        /// <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
    /// <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
        public static bool? DevicesEnabled { get; set; }

        /// <summary> Gets or sets the no operation option. </summary>
    /// <value> The nop. </value>
        public static bool Nop { get; set; }

        #endregion

    }
}
