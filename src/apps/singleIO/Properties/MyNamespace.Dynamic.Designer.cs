﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.IO.OL.SingleIO.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public SingleInputOutputForm m_SingleInputOutputForm;

            public SingleInputOutputForm SingleInputOutputForm
            {
                [DebuggerHidden]
                get
                {
                    m_SingleInputOutputForm = Create__Instance__(m_SingleInputOutputForm);
                    return m_SingleInputOutputForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_SingleInputOutputForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_SingleInputOutputForm);
                }
            }
        }
    }
}