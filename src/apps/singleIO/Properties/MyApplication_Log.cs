﻿using System;
using System.Diagnostics;
using isr.Core;
using isr.IO.OL.SingleIO.ExceptionExtensions;

namespace isr.IO.OL.SingleIO.My
{
    internal partial class MyApplication
    {

        /// <summary> Logs unpublished exception. </summary>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public void LogUnpublishedException(string activity, Exception exception)
        {
            _ = this.LogUnpublishedMessage( new TraceMessage( TraceEventType.Error, TraceEventId, $"Exception {activity};. {exception.ToFullBlownString()}" ) );
        }

        /// <summary> Applies the given value. </summary>
        /// <param name="value"> The value. </param>
        public void Apply(Logger value)
        {
            this._Logger = value;
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <param name="value"> The value. </param>
        public void ApplyTraceLogLevel(TraceEventType value)
        {
            this.TraceLevel = value;
            this.Logger.ApplyTraceLevel(value);
        }
    }
}