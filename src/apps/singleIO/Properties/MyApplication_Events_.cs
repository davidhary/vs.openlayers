using System;
using System.Diagnostics;
using System.Windows.Forms;
// isr.Core.Services.DLL
using isr.Core;

namespace isr.IO.OL.SingleIO.My
{
    internal partial class MyApplication : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks> Do not make this method Overridable (virtual) because a derived class should not be
        /// able to override this method. </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary> Gets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                          <c>False</c> to release only unmanaged resources when called from the
        ///                          runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.Destroy();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " APPLICATION EXTENSIONS "

        private MyAssemblyInfo _MyApplicationInfo;

        /// <summary> Gets an object that provides information about the application's assembly. </summary>
        /// <value> The assembly information object. </value>
        public new MyAssemblyInfo Info
        {
            get
            {
                if ( this._MyApplicationInfo is null)
                {
                    this._MyApplicationInfo = new MyAssemblyInfo(base.Info);
                }

                return this._MyApplicationInfo;
            }
        }

        private static string _CurrentProcessName;
        /// <summary> Gets the current process name. </summary>
        public static string CurrentProcessName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_CurrentProcessName))
                {
                    _CurrentProcessName = Process.GetCurrentProcess().ProcessName;
                }

                return _CurrentProcessName;
            }
        }

        /// <summary> Gets the number of current process threads. </summary>
        /// <value> The number of current process threads. </value>
        public static int CurrentProcessThreadCount => Process.GetCurrentProcess().Threads.Count;

        /// <summary> Gets a value indicating whether the application is running under the IDE in design
        /// mode. </summary>
        /// <value> <c>True</c> if the application is running under the IDE in design mode; otherwise,
        /// <c>False</c>. </value>
        public static bool InDesignMode => Debugger.IsAttached;

        #endregion

        #region " SLASH TRACE EVENT "

        /// <summary> Traces the event and displays on the splash screen if exists. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="format">    The details. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        private void SplashTraceEvent(TraceEventType eventType, string format, params object[] args)
        {
            this.SplashTraceEvent(eventType, TraceEventId, string.Format(System.Globalization.CultureInfo.CurrentCulture, format, args));
        }

        /// <summary> Traces the event and displays on the splash screen if exists. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="details">   The details. </param>
        private void SplashTraceEvent(TraceEventType eventType, string details)
        {
            this.SplashTraceEvent(eventType, TraceEventId, details);
        }

        /// <summary> Traces the event and displays on the splash screen if exists. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="details">   The details. </param>
        private void SplashTraceEvent(TraceEventType eventType, int id, string details)
        {
            _ = this.Logger.WriteLogEntry( eventType, id, details );
        }

        #endregion

        #region " APPLICATION EVENTS "

        /// <summary> Occurs when the network connection is connected or disconnected. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Network available event information. </param>
        private void HandleNetworkAvailabilityChanged(object sender, Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs e)
        {
        }

        /// <summary> Handles the Shutdown event of the MyApplication control. Saves user settings for all
        /// related libraries. </summary>
        /// <remarks> This event is not raised if the application terminates abnormally. Application log is
        /// set at verbose level to log shut down operations. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void HandleShutdown(object sender, EventArgs e)
        {
            try
            {
                this.ProcessShutDown();
            }
            catch
            {
            }
            finally
            {
                this.Logger.TraceSource.Flush();
            }

            this.Dispose();
            // do some garbage collection
            GC.Collect();
        }

        /// <summary> Occurs when the application starts, before the startup form is created. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Startup event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void HandleStartup(object sender, Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e)
        {
            if (e is null)
                return;

            // Turn on the screen hourglass
            Cursor.Current = Cursors.AppStarting;
            Application.DoEvents();
            try
            {
                Cursor.Current = Cursors.AppStarting;
                Trace.CorrelationManager.StartLogicalOperation(System.Reflection.MethodBase.GetCurrentMethod().Name);
                this.ProcessStartup(e);
                if (e.Cancel)
                {

                    // Show the exception message box with three custom buttons.
                    Cursor.Current = Cursors.Default;
                    if (MyDialogResult.Ok == MyMessageBox.ShowDialogIgnoreExit("Failed parsing command line.", "Failed Starting Program", MyMessageBoxIcon.Stop))
                    {
                        this.SplashTraceEvent(TraceEventType.Error, TraceEventId, "Application aborted by the user because of failure to parse the command line.");
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Cancel = false;
                    }

                    Cursor.Current = Cursors.AppStarting;
                }

                if (!e.Cancel)
                {
                    e.Cancel = !this.TryinitializeKnownState();
                    if (e.Cancel)
                    {
                        _ = MyMessageBox.ShowDialogExit( $"Failed initializing application state. Check the program log at '{this.Logger?.FullLogFileName}' for additional information.", "Failed Starting Program", MyMessageBoxIcon.Stop );
                    }
                }

                if (e.Cancel)
                {
                    this.SplashTraceEvent(TraceEventType.Error, TraceEventId, "Application failed to start up.");
                    this.Logger.TraceSource.Flush();

                    // exit with an error code
                    Environment.Exit(-1);
                    Application.Exit();
                }
                else
                {
                    this.SplashTraceEvent(TraceEventType.Verbose, TraceEventId, "Loading application window...");
                }
            }
            catch (Exception ex)
            {
                this.SplashTraceEvent(TraceEventType.Error, TraceEventId, "Exception occurred starting application.");
                Cursor.Current = Cursors.Default;
                this.Logger.WriteExceptionDetails(ex, TraceEventId);
                ex.Data.Add("@isr", "Exception occurred starting this application");
                if (MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore(ex))
                {
                    // exit with an error code
                    Environment.Exit(-1);
                    Application.Exit();
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }
        }

        /// <summary> Occurs when launching a single-instance application and the application is already
        /// active. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Startup next instance event information. </param>
        private void HandleStartupNextInstance(object sender, Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs e)
        {
            this.SplashTraceEvent(TraceEventType.Information, TraceEventId, "Application next instant starting.");
        }

        /// <summary> When overridden in a derived class, allows for code to run when an unhandled
        /// exception occurs in the application. </summary>
        /// <param name="e"> <see cref="T:Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs" />. </param>
        /// <returns> A <see cref="T:System.Boolean" /> that indicates whether the
        /// <see cref="E:Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase.UnhandledException" />
        /// event was raised. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected override bool OnUnhandledException(Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs e)
        {
            bool returnedValue = true;
            if (e is null)
            {
                Debug.Assert(!Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing.");
                return base.OnUnhandledException(e);
            }

            try
            {
                this.Logger.DefaultFileLogWriter.Flush();
            }
            catch (Exception ex)
            {
                Debug.Assert(!Debugger.IsAttached, "Exception occurred flushing the log", "Exception occurred flushing the log: {0}", ex);
            }

            try
            {
                e.Exception.Data.Add("@isr", "Unhandled Exception Occurred.");
                this.Logger.WriteExceptionDetails(e.Exception, TraceEventId);
                if (MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore(e.Exception))
                {
                    // exit with an error code
                    Environment.Exit(-1);
                    Application.Exit();
                }
            }
            catch
            {
                if (System.Windows.Forms.MessageBox.Show(e.Exception.ToString(), "Unhandled Exception occurred.", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly) == DialogResult.Abort)
                {
                }
            }
            finally
            {
            }

            return returnedValue;
        }

        /// <summary> Occurs when the application initializes. Replaces the default trace listener with the
        /// modified listener. </summary>
        /// <param name="commandLineArgs"> A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection" /> 
        ///                                of String, containing the command-line arguments as strings for the current
        /// application. </param>
        /// <returns> A <see cref="T:System.Boolean" /> indicating if application startup should continue. </returns>
        protected override bool OnInitialize(System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs)
        {
            this.CreateLogger();
            return base.OnInitialize(commandLineArgs);
        }

        #endregion

    }
}
