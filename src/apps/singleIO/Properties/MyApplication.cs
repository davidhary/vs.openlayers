﻿
namespace isr.IO.OL.SingleIO.My
{
    internal partial class MyApplication
    {

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = (int)OL.My.ProjectTraceEventId.OpenLayersSingleIO;
        public const string AssemblyTitle = "Open Layers Library Single I/O Tester";
        public const string AssemblyDescription = "Single I/O Tester for the Open Layers Library";
        public const string AssemblyProduct = "IO.Open.Layers.SingleIO.Tester";
    }
}