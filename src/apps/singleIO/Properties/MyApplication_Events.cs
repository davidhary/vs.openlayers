using System;
using System.Diagnostics;
using System.Windows.Forms;
using isr.IO.OL.SingleIO.ExceptionExtensions;

namespace isr.IO.OL.SingleIO.My
{
    internal partial class MyApplication
    {

        /// <summary> Builds the default caption. </summary>
        /// <returns> The caption. </returns>
        internal string BuildDefaultCaption()
        {
            var suffix = new System.Text.StringBuilder();
            _ = suffix.Append( " " );
            return Core.ApplicationInfo.BuildApplicationDescriptionCaption(suffix.ToString());
        }

        /// <summary> Destroys objects for this project. </summary>
        internal void Destroy()
        {
            this.SplashScreen = null;
        }

        /// <summary> Instantiates the application to its known state. </summary>
        /// <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private bool TryinitializeKnownState()
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;

                // show status
                if (InDesignMode)
                {
                    this.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Design Mode.");
                }
                else
                {
                    this.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Runtime Mode.");
                }

                return true;
            }
            catch (Exception ex)
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
                this.SplashTraceEvent(TraceEventType.Error, "Exception occurred initializing application known state;. {0}", ex.ToFullBlownString());
                try
                {
                    this.Destroy();
                }
                finally
                {
                }

                return false;
            }
            finally
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Processes the shut down. </summary>
        private void ProcessShutDown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            if (MyProject.Application.SaveMySettingsOnExit)
            {
                // Save library settings here
            }
        }

        /// <summary> Processes the startup. Sets the event arguments
        /// <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs">cancel</see>
        /// value if failed. </summary>
        /// <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        /// instance containing the event data. </param>
        private void ProcessStartup(Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e)
        {
            if (!e.Cancel)
            {
                this.SplashTraceEvent(TraceEventType.Verbose, "Using splash panel.");
                this.SplashTraceEvent(TraceEventType.Verbose, "Parsing command line");
                e.Cancel = !CommandLineInfo.TryParseCommandLine(e.CommandLine);
            }
        }
    }
}
