﻿using System;
// C:\My\LIBRARIES\VS\Share\Extensions\TimeSpanExtensions_.vb
using System.Windows.Threading;

namespace isr.IO.OL.My
{
    public sealed partial class MyLibrary
    {

        /// <summary> Lets Windows process all the messages currently in the message queue. </summary>
        public static void DoEvents()
        {
            Core.DispatcherExtensions.DispatcherExtensionMethods.DoEvents( Dispatcher.CurrentDispatcher );
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. T
        /// </summary>
        /// <param name="delayMilliseconds"> The delay in milliseconds. </param>
        public static void Delay( double delayMilliseconds )
        {
            Delay( Core.TimeSpanExtensions.TimeSpanExtensionMethods.FromMilliseconds( delayMilliseconds ) );
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. sions.DoEvents(Dispatcher)"/> to release messages currently in the message queue.
        /// </summary>
        /// <param name="delayTime"> The delay time. </param>
        public static void Delay( TimeSpan delayTime )
        {
            Core.TimeSpanExtensions.TimeSpanExtensionMethods.StartDelayTask( delayTime ).Wait();
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// <paramref name="resolution"/> times the delay time. 
        /// </summary>       
        /// <param name="delayTime">  The delay time. </param>
        /// <param name="resolution"> The resolution. </param>
        public static void Delay( TimeSpan delayTime, double resolution )
        {
            Core.TimeSpanExtensions.TimeSpanExtensionMethods.StartDelayTask( delayTime, resolution ).Wait();
        }

        /// <summary> Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority. </summary>
        /// <param name="act"> The act. </param>
        public static void Render( Action act )
        {
            Core.DispatcherExtensions.DispatcherExtensionMethods.Render( Dispatcher.CurrentDispatcher, act );
        }
    }
}