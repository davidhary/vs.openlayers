﻿using System.ComponentModel;

namespace isr.IO.OL.My
{

    /// <summary> Values that represent project trace event identifiers. </summary>
    public enum ProjectTraceEventId
    {
        [Description( "None" )]
        None = 0,
        [Description( "Open Layers" )]
        OpenLayers = Core.ProjectTraceEventId.OpenLayers,
        [Description( "Open Layers Forms" )]
        OpenLayersForms = OpenLayers + 0x1,
        [Description( "Open Layers Display" )]
        OpenLayersDisplay = OpenLayers + 0x2,
        [Description( "Open Layers Single IO" )]
        OpenLayersSingleIO = OpenLayers + 0x3,
        [Description( "Open Layers Scope" )]
        OpenLayersScope = OpenLayers + 0x4,
        [Description( "Open Layers Tester" )]
        OpenLayersTester = OpenLayers + 0x5
    }
}