﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.IO.OL.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.IO.OL.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.IO.OL.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
