
namespace isr.IO.OL
{
    /// <summary> Adds functionality to the Data Translation Open Layers
    /// <see cref="OpenLayers.Base.DigitalInputSubsystem">digital Output subsystem</see>. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 8/7/2013 </para></remarks>
    public class DigitalOutputSubsystem : OpenLayers.Base.DigitalOutputSubsystem
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <param name="device">        A reference to
        /// <see cref="OpenLayers.Base.Device">an open layers device</see>. </param>
        /// <param name="elementNumber"> Specifies the subsystem logical element number. </param>
        public DigitalOutputSubsystem( OpenLayers.Base.Device device, int elementNumber ) : base( device, elementNumber )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
        /// the method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed. </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.disposed )
                {
                    if ( disposing )
                    {

                        // remove handlers
                        this.HandlesBufferDoneEvents = false;
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SUBSYSTEM "

        /// <summary> Configures digital output sub-system. </summary>
        /// <param name="dataFLow"> The data f low. </param>
        public void Configure( OpenLayers.Base.DataFlow dataFLow )
        {
            this.DataFlow = dataFLow;
            base.Config();
        }

        /// <summary> Gets or sets a single value on the sub system. </summary>
        /// <value> The single reading. </value>
        public int SingleReading
        {
            get => this.LastSingleReading;

            set {
                this.LastSingleReading = value;
                this.SetSingleValue( value );
            }
        }

        #endregion

        #region " CHANNELS "

        /// <summary> Add a set of channels. </summary>
        /// <param name="firstPhysicalChannel"> . </param>
        /// <param name="lastPhysicalChannel">  . </param>
        /// <returns> A list of. </returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                _ = this.ChannelList.Add( phyicalChannelNumber );
            return this.ChannelList;
        }

        /// <summary> Add a set of channels. </summary>
        /// <param name="firstPhysicalChannel"> . </param>
        /// <param name="lastPhysicalChannel">  . </param>
        /// <param name="gain">                 . </param>
        /// <returns> A list of. </returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel, double gain )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                this.ChannelList.Add( phyicalChannelNumber ).Gain = gain;
            return this.ChannelList;
        }

        /// <summary> Gets reference to the selected channel for this subsystem. </summary>
        /// <value> The selected channel. </value>
        public OpenLayers.Base.ChannelListEntry SelectedChannel { get; private set; }

        /// <summary> Gets the logical number (index) of the selected channel. </summary>
        /// <value> The selected channel logical number. </value>
        public int SelectedChannelLogicalNumber { get; private set; }

        /// <summary> Selects a channel. </summary>
        /// <param name="logicalChannelNumber"> The logical channel number (the channel index in the
        /// <see cref="OpenLayers.Base.ChannelList">channel list</see>.) </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry SelectLogicalChannel( int logicalChannelNumber )
        {
            this.SelectedChannel = this.ChannelList.SelectLogicalChannel( logicalChannelNumber );
            return this.SelectedChannel;
        }

        /// <summary> Selects a channel from the subsystem channel list by its physical channel number. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns> A list of. </returns>
        public OpenLayers.Base.ChannelListEntry SelectPhysicalChannel( int physicalChannelNumber )
        {
            if ( this.PhysicalChannelExists( physicalChannelNumber ) )
            {
                this.SelectedChannelLogicalNumber = this.ChannelList.LogicalChannelNumber( physicalChannelNumber );
                this.SelectedChannel = this.ChannelList[this.SelectedChannelLogicalNumber];
            }

            return this.SelectedChannel;
        }

        /// <summary> Returns true if the specified channel exists. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns> A list of. </returns>
        public bool PhysicalChannelExists( int physicalChannelNumber )
        {
            return this.ChannelList.Contains( physicalChannelNumber );
        }

        #endregion

        #region " METHODS "

        /// <summary> Aborts the subsystem operations. </summary>
        public override void Abort()
        {
            base.Abort();
            if ( this.BufferQueue is object )
            {
                this.BufferQueue.FreeAllQueuedBuffers();
            }
        }

        /// <summary> Configures the digital output for single channel output. </summary>
        public void ConfigureSingleOutput()
        {
            this.SelectedChannel = this.ChannelList.Count == 0 ? this.ChannelList.Add( 0 ) : this.ChannelList[0];
            this.SelectedChannel.Gain = 1d;
        }

        /// <summary> Starts the subsystem operations. </summary>
        public override void Start()
        {
            base.Start();
        }

        /// <summary> Stops the subsystem operations. </summary>
        public override void Stop()
        {
            base.Stop();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> true to handles buffer done events. </summary>
        private bool _HandlesBufferDoneEvents;

        /// <summary> Gets or sets or Sets the condition as True have the sub system process buffer done
        /// events. </summary>
        /// <value> The handles buffer done events. </value>
        public bool HandlesBufferDoneEvents
        {
            get => this._HandlesBufferDoneEvents;

            set {
                if ( this._HandlesBufferDoneEvents != value )
                {
                    if ( value )
                    {
                        BufferDoneEvent += this.BufferDoneHandler;
                    }
                    else
                    {
                        BufferDoneEvent -= this.BufferDoneHandler;
                    }

                    this._HandlesBufferDoneEvents = value;
                }
            }
        }

        /// <summary> Returns true if the subsystem has channels defined. </summary>
        /// <value> The has channels. </value>
        public bool HasChannels => this.ChannelList.Count > 0;

        /// <summary> Returns the last output reading set using <see cref="SingleReading"/>. </summary>
        /// <value> The last single reading. </value>
        public int LastSingleReading { get; private set; }

        /// <summary> Gets or sets the length of each buffer allocated. </summary>
        /// <value> The length of the buffer. </value>
        public int BufferLength { get; set; }

        /// <summary> true to retrieves readings. </summary>
        private bool _RetrievesReadings;

        /// <summary> Gets or sets or Sets the condition as True have the system retrieve readings. </summary>
        /// <value> The retrieves readings. </value>
        public bool RetrievesReadings
        {
            get => this._RetrievesReadings;

            set {
                if ( value )
                {
                    this._HandlesBufferDoneEvents = true;
                }

                this._RetrievesReadings = value;
            }
        }

        /// <summary> Gets the status message. </summary>
        /// <remarks> Use this property to get the status message generated by the object. </remarks>
        /// <value> A <see cref="System.String">String</see>. </value>
        public string StatusMessage { get; private set; } = string.Empty;

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Handles buffer done events. </summary>
        /// <param name="sender"> Specifies reference to the
        /// <see cref="OpenLayers.Base.SubsystemBase">subsystem</see> </param>
        /// <param name="e">      Specifies the
        /// <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>. </param>
        private void BufferDoneHandler( object sender, OpenLayers.Base.BufferDoneEventArgs e )
        {
        }

        /// <summary> Handle buffer done events. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnBufferDone( OpenLayers.Base.BufferDoneEventArgs e )
        {
        }

        #endregion

    }
}
