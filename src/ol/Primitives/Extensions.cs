using System;
using System.Linq;

namespace isr.IO.OL
{
    /// <summary>
    /// Extends the Data Translation Open Layers <see cref="OpenLayers.Base.SubsystemBase">subsystem base</see>,
    /// <see cref="OpenLayers.Base.Device">Device</see>,
    /// <see cref="OpenLayers.Base.ChannelList">Channel List</see>, and 
    /// <see cref="OpenLayers.Signals.MemorySignalList">Signal List</see> functionality.
    /// </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public static class Extensions
    {

        #region " CHANNEL LIST "

        /// <summary> Returns the logical channel number (the channel index in the
        /// <see cref="OpenLayers.Base.ChannelList">channel list</see>)
        /// of the specified channel name. </summary>
        /// <param name="channelList"> The channel list. </param>
        /// <param name="channelName"> Name of the channel. </param>
        /// <returns> Non-negative channel number if found; otherwise -1. </returns>
        public static int LogicalChannelNumber( this OpenLayers.Base.ChannelList channelList, string channelName )
        {
            int number = -1;
            if ( !string.IsNullOrWhiteSpace( channelName ) && channelList is object )
            {
                foreach ( OpenLayers.Base.ChannelListEntry entry in channelList )
                {
                    number += 1;
                    if ( channelName.Equals( entry.Name, StringComparison.OrdinalIgnoreCase ) )
                    {
                        break;
                    }
                }
            }

            return number;
        }

        /// <summary> Returns the logical channel number (the channel index in the
        /// <see cref="OpenLayers.Base.ChannelList">channel list</see>)
        /// of the specified channel number. </summary>
        /// <param name="channelList">           The channel list. </param>
        /// <param name="physicalChannelNumber"> The physical channel number. </param>
        /// <returns> Non-negative channel number if found; otherwise -1. </returns>
        public static int LogicalChannelNumber( this OpenLayers.Base.ChannelList channelList, int physicalChannelNumber )
        {
            int number = -1;
            if ( channelList is object )
            {
                foreach ( OpenLayers.Base.ChannelListEntry entry in channelList )
                {
                    number += 1;
                    if ( entry.PhysicalChannelNumber == physicalChannelNumber )
                    {
                        break;
                    }
                }
            }

            return number;
        }

        /// <summary> Returns the logical channel number (the channel index in the
        /// <see cref="OpenLayers.Base.ChannelList">channel list</see>)
        /// of the specified channel number. </summary>
        /// <param name="channelList"> The channel list. </param>
        /// <param name="channel">     The channel. </param>
        /// <returns> Non-negative channel number if found; otherwise -1. </returns>
        public static int LogicalChannelNumber( this OpenLayers.Base.ChannelList channelList, OpenLayers.Base.ChannelListEntry channel )
        {
            return channelList is not object || channel is not object ? -1 : channelList.LogicalChannelNumber( channel.PhysicalChannelNumber );
        }

        /// <summary> Checks if a named channel exists. </summary>
        /// <param name="channelList"> The channel list. </param>
        /// <param name="channelName"> Name of the channel. </param>
        /// <returns> <c>True</c> if exists, <c>False</c> otherwise. </returns>
        public static bool Contains( this OpenLayers.Base.ChannelList channelList, string channelName )
        {
            bool result = false;
            if ( channelList is object && !string.IsNullOrWhiteSpace( channelName ) )
            {
                foreach ( OpenLayers.Base.ChannelListEntry entry in channelList )
                {
                    result = channelName.Equals( entry.Name, StringComparison.OrdinalIgnoreCase );
                    if ( result )
                        break;
                }
            }

            return result;
        }

        /// <summary> Checks if a physical channel exists. </summary>
        /// <param name="channelList">           The channel list. </param>
        /// <param name="physicalChannelNumber"> The physical channel number. </param>
        /// <returns> <c>True</c> if exists, <c>False</c> otherwise. </returns>
        public static bool Contains( this OpenLayers.Base.ChannelList channelList, int physicalChannelNumber )
        {
            bool result = false;
            if ( channelList is object )
            {
                foreach ( OpenLayers.Base.ChannelListEntry entry in channelList )
                {
                    result = entry.PhysicalChannelNumber == physicalChannelNumber;
                    if ( result )
                        break;
                }
            }

            return result;
        }

        /// <summary> Selects a channel from the channel list. </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="channelList">          The channel list. </param>
        /// <param name="logicalChannelNumber"> The logical channel number (the channel index in the
        /// <see cref="OpenLayers.Base.ChannelList">channel list</see>.) </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public static OpenLayers.Base.ChannelListEntry SelectLogicalChannel( this OpenLayers.Base.ChannelList channelList, int logicalChannelNumber )
        {
            return channelList is not object
                ? throw new ArgumentNullException( nameof( channelList ), "Channel list is not defined" )
                : channelList.Count == 0
                    ? throw new ArgumentNullException( nameof( channelList ), "Channel list is empty" )
                    : channelList.Count < logicalChannelNumber + 1
                                    ? throw new ArgumentOutOfRangeException( nameof( logicalChannelNumber ), logicalChannelNumber, "Channel list does not include this channel." )
                                    : channelList[logicalChannelNumber];
        }

        /// <summary> Selects a channel by name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channelList"> The channel list. </param>
        /// <param name="channelName"> Name of the channel. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public static OpenLayers.Base.ChannelListEntry SelectNamedChannel( this OpenLayers.Base.ChannelList channelList, string channelName )
        {
            OpenLayers.Base.ChannelListEntry result = null;
            if ( channelList is not object )
            {
                throw new ArgumentNullException( nameof( channelList ), "Channel list is not defined" );
            }
            else if ( channelList.Count == 0 )
            {
                throw new ArgumentNullException( nameof( channelList ), "Channel list is empty" );
            }
            else if ( string.IsNullOrWhiteSpace( channelName ) )
            {
                throw new ArgumentNullException( nameof( channelName ), "Channel name must not be empty." );
            }
            else
            {
                foreach ( OpenLayers.Base.ChannelListEntry entry in channelList )
                {
                    if ( channelName.Equals( entry.Name, StringComparison.OrdinalIgnoreCase ) )
                    {
                        result = entry;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary> Selects the physical channel. </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="channelList">           The channel list. </param>
        /// <param name="physicalChannelNumber"> The physical channel number. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public static OpenLayers.Base.ChannelListEntry SelectPhysicalChannel( this OpenLayers.Base.ChannelList channelList, int physicalChannelNumber )
        {
            OpenLayers.Base.ChannelListEntry result = null;
            if ( channelList is not object )
            {
                throw new ArgumentOutOfRangeException( nameof( channelList ), "Channel list is not defined" );
            }
            else if ( channelList.Count == 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( channelList ), "Channel list is empty" );
            }
            else
            {
                for ( int i = 0, loopTo = channelList.Count - 1; i <= loopTo; i++ )
                {
                    if ( channelList[i].PhysicalChannelNumber == physicalChannelNumber )
                    {
                        return channelList[i];
                    }
                }

                foreach ( OpenLayers.Base.ChannelListEntry entry in channelList )
                {
                    if ( entry.PhysicalChannelNumber == physicalChannelNumber )
                    {
                        result = entry;
                        break;
                    }
                }
            }

            return result;
        }

        #endregion

        #region " DEVICE "

        /// <summary> Returns true if the device is engaged.  This permits detecting if the device failed
        /// to engage such as the case after waking up from system sleep. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> Reference to a valid <see cref="OpenLayers.Base.Device">device</see>. </param>
        /// <returns> <c>True</c> if the specified device is exists; otherwise, <c>False</c>. </returns>
        public static bool Engaged( this OpenLayers.Base.Device device )
        {
            if ( device is not object )
            {
                throw new ArgumentNullException( nameof( device ) );
            }

            var hardwareInfo = device.GetHardwareInfo();
            return !(hardwareInfo.VendorId.Equals( 0 ) | hardwareInfo.ProductId.Equals( 0 ));
        }

        /// <summary> Returns the <see cref="isr.IO.OL.DeviceFamily">device family</see>. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> isr.IO.OL.DeviceFamily. </returns>
        public static DeviceFamily DeviceFamily( this OpenLayers.Base.Device device )
        {
            if ( device is not object )
            {
                throw new ArgumentNullException( nameof( device ) );
            }
            // "DT9816-A" 
            return device.BoardModelName.Substring( 0, 4 ).Equals( "DT98", StringComparison.OrdinalIgnoreCase ) ? OL.DeviceFamily.DT9800 : OL.DeviceFamily.None;
        }

        #endregion

        #region " DEVICE MANAGER "

        /// <summary> Returns true if the device exists.  This permits detecting if the device failed to
        /// connect such as the case after waking up from system sleep. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the device name. </param>
        /// <returns> <c>True</c> if a device with the specified name exists; otherwise, <c>False</c>. </returns>
        public static bool Exists( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : string.IsNullOrWhiteSpace( deviceName )
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.HardwareAvailable() && manager.GetDeviceNames().Contains( deviceName, StringComparer.CurrentCultureIgnoreCase );
        }

        /// <summary> Fetches the device name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the device name. </param>
        /// <returns> OpenLayers.Base.HardwareInfo. </returns>
        public static OpenLayers.Base.HardwareInfo DeviceHardwareInfo( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : deviceName is not object || deviceName.Length == 0
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.HardwareAvailable() ? manager.SelectDevice( deviceName ).GetHardwareInfo() : new OpenLayers.Base.HardwareInfo();
        }

        /// <summary> Fetches the device driver version number for the specified board. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the board. </param>
        /// <returns> Version number for the board.<para>
        /// The returned string has the format "VM.XY.BB" described as follows:</para><para>
        /// Code    Meaning Possible Values</para><para>
        /// V   Release type    V - released software version </para><para>
        /// M   Major revision number   00 through 99 - major feature change</para><para>
        /// X   Minor revision number   00 through 99 - minor feature change</para><para>
        /// Y   Patch revision number   00 through 99 - minor bug fix</para><para>
        /// BB  Internal build number   00 through 99</para> </returns>
        public static string DeviceDriverVersion( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : deviceName is not object || deviceName.Length == 0
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.HardwareAvailable() ? manager.SelectDevice( deviceName ).DriverVersion : string.Empty;
        }

        /// <summary> Fetches the unique id of the specified board. </summary>
        /// <remarks> This field contains the year (1 or 2 digits), week (1 or 2 digits), test station (1
        /// digit), and sequence number (3 digits) of the device when it was tested in Manufacturing. For
        /// example, if BoardId contains the value 5469419, this device was tested in 2005, week 46, on
        /// test station 9, and is unit number 419. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the device name. </param>
        /// <returns> Int64. </returns>
        public static long DeviceUniqueId( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : deviceName is not object || deviceName.Length == 0
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.HardwareAvailable() ? Convert.ToInt64( manager.SelectDevice( deviceName ).GetHardwareInfo().BoardId ) : 0L;
        }

        /// <summary> Selects and returns an open layers device.  If the system has multiple board, this
        /// would allow the operator to select a board from a list. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the device name. </param>
        /// <returns> OpenLayers.Base.Device. </returns>
        public static OpenLayers.Base.Device SelectDevice( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : string.IsNullOrWhiteSpace( deviceName )
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.Exists( deviceName ) ? manager.GetDevice( deviceName ) : null;
        }

        #endregion

        #region " RANGE "

        /// <summary> Compares two ranges. </summary>
        /// <param name="left">  Specifies the range to compare to. </param>
        /// <param name="right"> Specifies the range to compare. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( this OpenLayers.Base.Range left, OpenLayers.Base.Range right )
        {
            return left is not object ? right is not object : right is not null && left.Low.Equals( right.Low ) && left.High.Equals( right.High );
        }

        /// <summary> Compares two ranges. </summary>
        /// <remarks> The two ranges are the same if their two end points are within the tolerance. </remarks>
        /// <param name="left">      Specifies the range to compare to. </param>
        /// <param name="right">     Specifies the range to compare. </param>
        /// <param name="tolerance"> Specifies the tolerance for comparing the two ranges. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( this OpenLayers.Base.Range left, OpenLayers.Base.Range right, double tolerance )
        {
            return left is not object
                ? right is not object
                : right is not null && Math.Abs( left.Low - right.Low ) <= tolerance && Math.Abs( left.High - right.High ) <= tolerance;
        }

        #endregion

        #region " SUBSYSTEM BASE "

        /// <summary> Returns the subsystem maximum reading. </summary>
        /// <param name="subsystem"> A reference to an open
        /// <see cref="OpenLayers.Base.SubsystemBase">subsystem</see> </param>
        /// <returns> System.Int32. </returns>
        public static int MaxReading( this OpenLayers.Base.SubsystemBase subsystem )
        {
            return subsystem is not object
                ? throw new ArgumentNullException( nameof( subsystem ) )
                : subsystem.Encoding == OpenLayers.Base.Encoding.TwosComplement ? Convert.ToInt32( Math.Pow( 2d, subsystem.Resolution - 1 ) ) - 1 : Convert.ToInt32( Math.Pow( 2d, subsystem.Resolution ) ) - 1;
        }

        /// <summary> Returns the subsystem minimum reading. </summary>
        /// <param name="subsystem"> A reference to an open
        /// <see cref="OpenLayers.Base.SubsystemBase">analog I/O subsystem</see> </param>
        /// <returns> System.Int32. </returns>
        public static int MinReading( this OpenLayers.Base.SubsystemBase subsystem )
        {
            return subsystem is not object
                ? throw new ArgumentNullException( nameof( subsystem ) )
                : subsystem.Encoding == OpenLayers.Base.Encoding.TwosComplement ? -Convert.ToInt32( Math.Pow( 2d, subsystem.Resolution - 1 ) ) : 0;
        }

        #endregion

        #region " SIGNAL LIST "

        /// <summary> Checks if a signal exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signalList"> The signal list. </param>
        /// <param name="signalName"> Name of the signal. </param>
        /// <returns> <c>True</c> if exists, <c>False</c> otherwise. </returns>
        public static bool Contains( this OpenLayers.Signals.MemorySignalList signalList, string signalName )
        {
            bool result = false;
            if ( signalList is not object )
            {
                return false;
            }
            else if ( signalList.Count == 0 )
            {
                return false;
            }
            else if ( string.IsNullOrWhiteSpace( signalName ) )
            {
                throw new ArgumentNullException( nameof( signalName ), "Signal name must not be empty." );
            }
            else
            {
                foreach ( OpenLayers.Signals.MemorySignal signal in signalList )
                {
                    result = signalName.Equals( signal.Name, StringComparison.OrdinalIgnoreCase );
                    if ( result )
                        break;
                }
            }

            return result;
        }

        /// <summary> Selects a signal by name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signalList"> The signal list. </param>
        /// <param name="signalName"> Name of the signal. </param>
        /// <returns> OpenLayers.Signals.MemorySignal. </returns>
        public static OpenLayers.Signals.MemorySignal SelectNamedSignal( this OpenLayers.Signals.MemorySignalList signalList, string signalName )
        {
            OpenLayers.Signals.MemorySignal result = null;
            if ( signalList is not object )
            {
                throw new ArgumentNullException( nameof( signalList ), "Signal list is not defined" );
            }
            else if ( signalList.Count == 0 )
            {
                throw new ArgumentNullException( nameof( signalList ), "Signal list is empty" );
            }
            else if ( string.IsNullOrWhiteSpace( signalName ) )
            {
                throw new ArgumentNullException( nameof( signalName ), "Signal name must not be empty." );
            }
            else
            {
                foreach ( OpenLayers.Signals.MemorySignal signal in signalList )
                {
                    if ( signalName.Equals( signal.Name, StringComparison.OrdinalIgnoreCase ) )
                    {
                        result = signal;
                        break;
                    }
                }
            }

            return result;
        }

        #endregion

    }
}
