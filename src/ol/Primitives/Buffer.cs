using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.IO.OL
{
    /// <summary>
    /// Provides added functionality to the Data Translation Open Layers
    /// <see cref="OpenLayers.Base.OlBuffer">buffer</see>.</summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public class Buffer : OpenLayers.Base.OlBuffer
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>Constructs this class.</summary>
        public Buffer( int sampleCount, OpenLayers.Base.AnalogSubsystem subsystem ) : base( sampleCount, subsystem )
        {
        }

        /// <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        /// <param name="disposing">True if this method releases both managed and unmanaged 
        ///   resources; <c>False</c> if this method releases only unmanaged resources.</param>
        /// <remarks>Executes in two distinct scenarios as determined by
        ///   its disposing parameter.  If True, the method has been called directly or 
        ///   indirectly by a user's code--managed and unmanaged resources can be disposed.
        ///   If disposing equals False, the method has been called by the 
        ///   runtime from inside the finalizer and you should not reference 
        ///   other objects--only unmanaged resources can be disposed.</remarks>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SHARED "

        /// <summary> Returns an Integer array of arrays. </summary>
        /// <param name="channelCount">      Number of channels. </param>
        /// <param name="samplesPerChannel"> The samples per channel. </param>
        /// <returns> . </returns>
        public static int[][] AllocateInteger( int channelCount, int samplesPerChannel )
        {
            // allocate array for readings data.
            var readings = new int[channelCount][];
            for ( int i = 0, loopTo = channelCount - 1; i <= loopTo; i++ )
                readings[i] = new int[samplesPerChannel];
            return readings;
        }

        /// <summary> Returns an empty Double array of arrays. </summary>
        /// <param name="channelCount">      . </param>
        /// <param name="samplesPerChannel"> . </param>
        /// <returns> . </returns>
        public static double[][] AllocateDouble( int channelCount, int samplesPerChannel )
        {
            // allocate array for readings data.
            var voltages = new double[channelCount][];
            for ( int i = 0, loopTo = channelCount - 1; i <= loopTo; i++ )
                voltages[i] = new double[samplesPerChannel];
            return voltages;
        }

        /// <summary>Retrieves double-precision data from the buffer to an array of arrays.</summary>
        /// <param name="buffer">Specifies the buffer.</param>
        /// <param name="channelList">Specifies the channel list.</param>
        /// <returns>Number of valid samples.</returns>
        public static double[][] Voltages( OpenLayers.Base.OlBuffer buffer, OpenLayers.Base.ChannelList channelList )
        {
            if ( buffer is not object )
                throw new ArgumentNullException( nameof( buffer ) );
            if ( channelList is not object )
                throw new ArgumentNullException( nameof( channelList ) );
            var data = new double[channelList.Count][];
            for ( int i = 0, loopTo = channelList.Count - 1; i <= loopTo; i++ )
                data[i] = buffer.GetDataAsVolts( channelList[i] );
            // For Each channel As OpenLayers.Base.ChannelListEntry In channelList
            // LogicalChannelNumber not defined data(channel.LogicalChannelNumber) = buffer.GetDataAsVolts(channel)
            // Next

            return data;
        }

        /// <summary> Fill buffers. </summary>
        /// <param name="readings"> The readings. </param>
        /// <param name="buffers">  The buffers. </param>
        [CLSCompliant( false )]
        public static void FillBuffers( uint[][] readings, OpenLayers.Base.OlBuffer[] buffers )
        {
            var samples = new uint[readings.GetUpperBound( 0 ) + 1];
            OpenLayers.Base.OlBuffer buffer = null;
            int channelCount = readings.GetUpperBound( 0 ) + 1;
            int samplesPerChannel = ( int ) Math.Round( buffer.BufferSizeInSamples / ( double ) channelCount );
            int startingIndex = 0;
            for ( int iBuffer = 1, loopTo = buffers.Count(); iBuffer <= loopTo; iBuffer++ )
            {
                buffer = buffers[iBuffer - 1];
                // now we must place the data in the buffer.
                for ( int channelIndex = 0, loopTo1 = readings.GetUpperBound( 0 ); channelIndex <= loopTo1; channelIndex++ )
                {
                    for ( int sampleIndex = startingIndex, loopTo2 = startingIndex + samplesPerChannel - 1; sampleIndex <= loopTo2; sampleIndex++ )
                        samples[sampleIndex] = readings[channelIndex][sampleIndex];
                }

                startingIndex += samplesPerChannel;
                buffer.PutDataAsRaw( samples );
            }
        }

        /// <summary> Allocates a set of buffers for data collection. </summary>
        /// <param name="bufferCount">  The buffer count. </param>
        /// <param name="bufferLength"> Length of the buffer. </param>
        public static IEnumerable<Buffer> AllocateBuffers( int bufferCount, int bufferLength, OpenLayers.Base.AnalogSubsystem subsystem )
        {
            // allocate the buffers array.  This is needed to buffer can be queued when restarting.
            var buffers = new List<Buffer>();
            while ( buffers.Count < bufferCount )
                buffers.Add( new Buffer( bufferLength, subsystem ) );
            return buffers;
        }

        #endregion

        #region " METHODS "

        /// <summary>Retrieves voltages from the buffer to an array of arrays.</summary>
        /// <param name="channelList">Specifies the channel list.</param>
        /// <returns>Number of valid samples.</returns>
        public double[][] Voltages( OpenLayers.Base.ChannelList channelList )
        {
            if ( channelList is not object )
                throw new ArgumentNullException( nameof( channelList ) );
            var data = new double[channelList.Count][];
            for ( int i = 0, loopTo = channelList.Count - 1; i <= loopTo; i++ )
                data[i] = this.GetDataAsVolts( channelList[i] );
            return data;
        }

        /// <summary>Retrieves readings from the buffer to an array of arrays.</summary>
        /// <param name="channelList">Specifies the channel list.</param>
        /// <returns>Number of valid samples.</returns>
        [CLSCompliant( false )]
        public uint[][] RawReadings( OpenLayers.Base.ChannelList channelList )
        {
            if ( channelList is not object )
                throw new ArgumentNullException( nameof( channelList ) );
            var data = new uint[channelList.Count][];
            for ( int i = 0, loopTo = channelList.Count - 1; i <= loopTo; i++ )
                data[i] = this.GetDataAsRawUInt32( channelList[i] );
            return data;
        }

        /// <summary>
        /// Retrieves readings from the buffer to an array of arrays. If any unsigned integer value is
        /// bigger than the maximum integer value, it is simply wrapped around to the corresponding
        /// negative integer value. 
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channelList"> Specifies the channel list. </param>
        /// <returns> Number of valid samples. </returns>
        public int[][] Readings( OpenLayers.Base.ChannelList channelList )
        {
            if ( channelList is not object )
                throw new ArgumentNullException( nameof( channelList ) );
            var data = new int[channelList.Count][];
            for ( int i = 0, loopTo = channelList.Count - 1; i <= loopTo; i++ )
                // https://www.c-sharpcorner.com/uploadfile/b942f9/how-to-convert-unsigned-integer-arrays-to-signed-arrays-and-vice-versa/
                data[i] = ( int[] ) ( object ) this.GetDataAsRawUInt32( channelList[i] );
            return data;
        }

        /// <summary> Unwrapped readings. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channelList"> Specifies the channel list. </param>
        /// <returns> An Integer()() </returns>
        public long[][] UnwrappedReadings( OpenLayers.Base.ChannelList channelList )
        {
            if ( channelList is not object )
                throw new ArgumentNullException( nameof( channelList ) );
            var data = new long[channelList.Count][];
            for ( int i = 0, loopTo = channelList.Count - 1; i <= loopTo; i++ )
                data[i] = ( long[] ) ( object ) this.GetDataAsRawUInt32( channelList[i] );
            return data;
        }

        #endregion

    }
}
