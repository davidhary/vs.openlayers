﻿using System.ComponentModel;

namespace isr.IO.OL
{

    /// <summary> Enumerates the device families.  This was created to address device features not
    /// covered in the API such as the individual gain settings for the channel list. </summary>
    public enum DeviceFamily
    {
        [Description( "None" )]
        None,
        [Description( "DT9800 Series" )]
        DT9800 = 9800
    }

    /// <summary> Enumerates the chart modes. </summary>
    public enum ChartModeId
    {
        None,
        Scope,
        StripChart
    }
}