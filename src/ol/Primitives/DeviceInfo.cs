
namespace isr.IO.OL
{
    /// <summary>
    /// reads device info.
    /// </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public class DeviceInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceInfo" /> class.
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        public DeviceInfo( string deviceName ) : base()
        {
            this.DeviceName = deviceName;
            this._SerialNumber = -1;
            this._ProductId = -1;
            _ = this.FetchBoardInfoInternal();
        }

        #endregion

        #region " DEVICE INFORMATION "

        /// <summary>
        /// Gets or sets the name of the device.
        /// </summary>
        /// <value>The name of the device.</value>
        public string DeviceName { get; set; }

        private int _SerialNumber;
        /// <summary>Returns the board serial number.
        /// Contains the year (1 or 2 digits), week (1 or 2 digits), test station (1 digit), 
        /// and sequence number (3 digits) of the device when it was tested in Manufacturing. 
        /// For example, if BoardId contains the value 5469419, this device was tested in 2005, 
        /// week 46, on test station 9, and is unit number 419. 
        /// </summary>
        public int SerialNumber
        {
            get {
                if ( this._SerialNumber < 0 )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._SerialNumber;
            }
        }

        private int _DeviceId;
        /// <summary>
        /// Returns the version number of the product. 
        /// If only one version of the product exists, this number is 1. 
        /// If two versions of the product exist, this number could be 1 or 2. 
        /// </summary>
        public int DeviceId
        {
            get {
                if ( this._DeviceId < 0 )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._DeviceId;
            }
        }

        private string _DriverVersion;
        /// <summary>
        /// Gets the board driver version.
        /// </summary>
        /// <value>The driver version.</value>
        public string DriverVersion
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._DriverVersion ) )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._DriverVersion;
            }
        }

        private int _InstanceNumber;
        /// <summary>
        /// Gets the instance number of the board.
        /// </summary>
        /// <value>The instance number.</value>
        /// <remarks>
        /// Instance number not provided with the .NET SDK. 
        /// This number is set to 1.
        /// </remarks>
        public int InstanceNumber
        {
            get {
                if ( this._InstanceNumber <= 0 )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._InstanceNumber;
            }
        }

        private int _ProductId;
        /// <summary>
        /// Gets the product identification number, such as DT9832.
        /// </summary>
        /// <value>The board product id.</value>
        public int ProductId
        {
            get {
                if ( this._ProductId < 0 )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._ProductId;
            }
        }

        private string _SoftwareVersion;
        /// <summary>Gets the board software SDK version.</summary>
        public string SoftwareVersion
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._SoftwareVersion ) )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._SoftwareVersion;
            }
        }

        private int _VendorId;
        /// <summary>
        /// Gets the identification number of the vendor.
        /// For most devices, this is 0x087 hexadecimal
        /// </summary>
        /// <value>The vendor id.</value>
        public int VendorId
        {
            get {
                if ( this._VendorId < 0 )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._VendorId;
            }
        }

        private string _DriverName = string.Empty;
        /// <summary>
        /// Gets the open layers driver name.
        /// e.g., DT9816
        /// </summary>
        /// <value>The name of the driver.</value>
        public string DriverName
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._DriverName ) )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._DriverName;
            }
        }

        private string _ModuleName = string.Empty;
        /// <summary>
        /// Gets the Module name.
        /// e.g., DT9816-A
        /// </summary>
        /// <value>The name of the module.</value>
        public string ModuleName
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._ModuleName ) )
                {
                    if ( this.FetchBoardInfo() )
                    {
                    }
                }

                return this._ModuleName;
            }
        }

        #endregion

        #region " DEVICE MANAGER "

        /// <summary>
        /// Fetches the board information from the device.
        /// </summary>
        /// <returns><c>True</c> if success, <c>False</c> otherwise</returns>
        private bool FetchBoardInfoInternal()
        {
            if ( string.IsNullOrWhiteSpace( this.DeviceName ) )
            {
                return false;
            }

            if ( !HardwareExists )
            {
                return false;
            }

            if ( !this.Exists() )
            {
                return false;
            }

            // get the device.
            using ( var device = new OpenLayers.Base.Device( this.DeviceName ) )
            {
                this._DriverVersion = device.DriverVersion; // "V6.7.3.0"
                My.MyLibrary.DoEvents();

                // This method is not provided in the new Open Layers
                // Me._boardSoftwareVersion = isr.Drivers.OL.Helper.SoftwareVersion()
                this._SoftwareVersion = OpenLayers.Base.DeviceMgr.Get().DeviceDriverVersion( this.DeviceName );
                this._DriverName = device.DriverName; // "DT9816"
                this._ModuleName = device.BoardModelName; // "DT9816-A"

                // update the board hardware information
                var hardwareInfo = device.GetHardwareInfo();
                this._InstanceNumber = 1;
                this._SerialNumber = hardwareInfo.BoardId; // 22639
                this._ProductId = hardwareInfo.ProductId; // 38934
                this._DeviceId = hardwareInfo.DeviceId; // 1
                this._VendorId = hardwareInfo.VendorId; // 2151
            }

            return this.SerialNumber > 0;
        }

        /// <summary>
        /// Fetches the board information from the device.
        /// </summary>
        /// <returns><c>True</c> if success, <c>False</c> otherwise</returns>
        public bool FetchBoardInfo()
        {
            return this.FetchBoardInfoInternal();
        }

        /// <summary>
        /// Gets a value indicating whether hardware exists.
        /// </summary>
        /// <value><c>True</c> if hardware exists; otherwise, <c>False</c>.</value>
        public static bool HardwareExists
        {
            get {
                bool affirmative = OpenLayers.Base.DeviceMgr.Get().HardwareAvailable();
                My.MyLibrary.DoEvents();
                return affirmative;
            }
        }

        /// <summary>
        /// Determines whether the device exists.
        /// </summary>
        /// <returns><c>True</c> if this device exists; otherwise, <c>False</c>.</returns>
        public bool Exists()
        {
            bool affirmative = OpenLayers.Base.DeviceMgr.Get().Exists( this.DeviceName );
            My.MyLibrary.DoEvents();
            return affirmative;
        }

        #endregion

    }
}
