using System;

namespace isr.IO.OL
{
    /// <summary>
    /// Adds functionality to the Data Translation Open Layers
    /// <see cref="OpenLayers.Base.AnalogOutputSubsystem">Analog Output subsystem</see>.
    /// </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public class AnalogOutputSubsystem : OpenLayers.Base.AnalogOutputSubsystem
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>Constructs this class.</summary>
        /// <param name="device">A reference to 
        ///     <see cref="OpenLayers.Base.Device">an open layers device</see>.</param>
        /// <param name="elementNumber">Specifies the subsystem logical element number.</param>
        public AnalogOutputSubsystem( OpenLayers.Base.Device device, int elementNumber ) : base( device, elementNumber )
        {
            this.LastSingleReadingsChannel = -1;
            this.LastSingleVoltagesChannel = -1;
            this._LastSingleVoltages = new System.Collections.Generic.Dictionary<int, double>();
            this._LastSingleReadings = new System.Collections.Generic.Dictionary<int, int>();
        }

        /// <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        /// <param name="disposing">True if this method releases both managed and unmanaged 
        ///   resources; <c>False</c> if this method releases only unmanaged resources.</param>
        /// <remarks>Executes in two distinct scenarios as determined by
        ///   its disposing parameter.  If True, the method has been called directly or 
        ///   indirectly by a user's code--managed and unmanaged resources can be disposed.
        ///   If disposing equals False, the method has been called by the 
        ///   runtime from inside the finalizer and you should not reference 
        ///   other objects--only unmanaged resources can be disposed.</remarks>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this.HandlesBufferDoneEvents = false;
                    }

                    // Free shared unmanaged resources
                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Aborts the subsystem operations.
        /// </summary>
        public override void Abort()
        {
            base.Abort();
            if ( this.BufferQueue is object )
            {
                this.BufferQueue.FreeAllQueuedBuffers();
            }
        }

        /// <summary>
        /// Starts the subsystem operations.
        /// </summary>
        public override void Start()
        {

            // get started.
            base.Start();
        }

        /// <summary>
        /// Stops the subsystem operations.
        /// </summary>
        public override void Stop()
        {
            base.Stop();
        }

        #endregion

        #region " PROPERTIES "

        private bool _HandlesBufferDoneEvents;
        /// <summary>
        /// Gets or Sets the condition as True have the sub system process buffer done events.
        /// </summary>
        public bool HandlesBufferDoneEvents
        {
            get => this._HandlesBufferDoneEvents;

            set {
                if ( this._HandlesBufferDoneEvents != value )
                {
                    if ( value )
                    {
                        BufferDoneEvent += this.BufferDoneHandler;
                    }
                    else
                    {
                        BufferDoneEvent -= this.BufferDoneHandler;
                    }

                    this._HandlesBufferDoneEvents = value;
                }
            }
        }

        /// <summary>
        /// Returns true if the subsystem has channels defined.
        /// </summary>
        public bool HasChannels => this.ChannelList.Count > 0;

        /// <summary> Gets or sets the last single voltages channel. </summary>
        /// <value> The last single voltages channel. </value>
        public int LastSingleVoltagesChannel { get; private set; }

        private readonly System.Collections.Generic.Dictionary<int, double> _LastSingleVoltages;

        /// <summary> Last single voltage setter. </summary>
        /// <remarks> David, 2022-01-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Double. </returns>
        public double LastSingleVoltageSetter( double value )
        {
            if ( this._LastSingleVoltages.ContainsKey( this.LastSingleVoltagesChannel ) )
            {
                _ = this._LastSingleVoltages.Remove( this.LastSingleVoltagesChannel );
            }

            this._LastSingleVoltages.Add( this.LastSingleVoltagesChannel, value );
            return value;
        }

        /// <summary> Last single Voltage. </summary>
        /// <returns>The last single Voltage.</returns>>
        public double LastSingleVoltage()
        {
            return this._LastSingleVoltages[this.LastSingleVoltagesChannel];
        }

        /// <summary> Last single Voltage. </summary>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns>The last single Voltage.</returns>>
        public double LastSingleVoltage( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : this._LastSingleVoltages[channel.PhysicalChannelNumber];
        }

        /// <summary> Last single Voltage. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns>The last single Voltage.</returns>>
        public double LastSingleVoltage( int physicalChannelNumber )
        {
            return this._LastSingleVoltages[physicalChannelNumber];
        }

        /// <summary> Gets or sets the last single readings channel. </summary>
        /// <value> The last single readings channel. </value>
        public int LastSingleReadingsChannel { get; private set; }

        private readonly System.Collections.Generic.Dictionary<int, int> _LastSingleReadings;

        /// <summary>
        /// Returns the last output reading set using <see cref="OutputSingleVoltage" />.
        /// </summary>
        /// <returns>The last single reading.</returns>>
        public int LastSingleReadingSetter( int value )
        {
            if ( this._LastSingleReadings.ContainsKey( this.LastSingleReadingsChannel ) )
            {
                _ = this._LastSingleReadings.Remove( this.LastSingleReadingsChannel );
            }

            this._LastSingleVoltages.Add( this.LastSingleReadingsChannel, value );
            return value;
        }

        /// <summary> Last single reading. </summary>
        /// <returns>The last single reading.</returns>>
        public int LastSingleReading()
        {
            return this._LastSingleReadings[this.LastSingleReadingsChannel];
        }

        /// <summary> Last single reading. </summary>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns>The last single reading.</returns>>
        public int LastSingleReading( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : this._LastSingleReadings[channel.PhysicalChannelNumber];
        }

        /// <summary> Last single reading. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns>The last single reading.</returns>>
        public int LastSingleReading( int physicalChannelNumber )
        {
            return this._LastSingleReadings[physicalChannelNumber];
        }

        /// <summary>
        /// True if has bipolar range.
        /// </summary>
        /// <value><c>True</c> if [bipolar range]; otherwise, <c>False</c>.</value>
        public bool BipolarRange => this.VoltageRange.Low < 0d;
        /// <summary>
        /// Gets or sets the length of each buffer allocated.
        /// </summary>
        /// <value>The length of the buffer.</value>
        public int BufferLength { get; set; }

        /// <summary>
        /// Returns the subsystem maximum voltage.
        /// </summary>
        /// <returns>The max voltage.</returns>>
        public double MaxVoltage()
        {
            return this.MaxVoltage( this.SelectedChannel );
        }

        /// <summary> Returns the subsystem maximum voltage. </summary>
        /// <exception cref="System.ArgumentNullException"> channel. </exception>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> The max voltage. </returns>
        public double MaxVoltage( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : channel.Gain == 0d ? 0d : this.VoltageRange.High / channel.Gain;
        }

        /// <summary> Returns the subsystem minimum voltage. </summary>
        /// <returns> The min voltage. </returns>
        public double MinVoltage()
        {
            return this.MinVoltage( this.SelectedChannel );
        }

        /// <summary> Returns the subsystem minimum voltage. </summary>
        /// <exception cref="System.ArgumentNullException"> channel. </exception>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> The min voltage. </returns>
        public double MinVoltage( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : channel.Gain == 0d ? 0d : this.VoltageRange.Low / channel.Gain;
        }

        /// <summary> Outputs a single reading value to the sub system. </summary>
        /// <remarks> David, 2022-01-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Integer. </returns>
        public int OutputSingleReading( int value )
        {
            return this.OutputSingleReading( this.SelectedChannel, value );
        }

        /// <summary>
        /// Outputs a single reading value to the sub system.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">channel</exception>
        /// <param name="channel">Reference to the channel.</param>
        public int OutputSingleReading( OpenLayers.Base.ChannelListEntry channel, int value )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : this.OutputSingleReading( channel.PhysicalChannelNumber, value );
        }

        /// <summary>
        /// Outputs a single reading value to the sub system.
        /// </summary>
        /// <param name="physicalChannelNumber">Specifies the physical channel number.</param>
        public int OutputSingleReading( int physicalChannelNumber, int value )
        {
            this.LastSingleReadingsChannel = physicalChannelNumber;
            _ = this.LastSingleReadingSetter( value );
            this.SetSingleValueAsRaw( physicalChannelNumber, value );
            return this.LastSingleReading();
        }

        /// <summary> Outputs as single voltage value. </summary>
        /// <remarks> David, 2022-01-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The output voltage. </returns>
        public double OutputSingleVoltage( double value )
        {
            return this.OutputSingleVoltage( this.SelectedChannel, value );
        }

        /// <summary> Outputs as single voltage value. </summary>
        /// <remarks> David, 2022-01-11. </remarks>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <param name="value">   The value. </param>
        /// <returns> The output voltage. </returns>
        public double OutputSingleVoltage( OpenLayers.Base.ChannelListEntry channel, double value )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : this.OutputSingleVoltage( channel.PhysicalChannelNumber, value );
        }

        /// <summary> Outputs as single voltage value. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <param name="value">                 The value. </param>
        /// <returns> The output voltage. </returns>
        public double OutputSingleVoltage( int physicalChannelNumber, double value )
        {
            this.LastSingleVoltagesChannel = physicalChannelNumber;
            _ = this.LastSingleVoltageSetter( value );
            this.SetSingleValueAsVolts( physicalChannelNumber, value );
            return this.LastSingleVoltage();
        }

        /// <summary>Gets or sets the status message.</summary>
        /// <value>A <see cref="System.String">String</see>.</value>
        /// <remarks>Use this property to get the status message generated by the object.</remarks>
        public string StatusMessage { get; private set; } = string.Empty;

        /// <summary>
        /// Returns the analog input voltage resolution.
        /// </summary>
        /// <returns>The voltage resolution.</returns>>
        public double VoltageResolution()
        {
            return this.VoltageResolution( this.SelectedChannel );
        }

        /// <summary>
        /// Returns the analog input voltage resolution.
        /// </summary>
        /// <returns>The voltage resolution.</returns>>
        /// <exception cref="System.ArgumentNullException">channel</exception>
        /// <param name="channel">Specifies a reference to a <see cref="OpenLayers.Base.ChannelListEntry">channel</see>.</param>
        public double VoltageResolution( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : (this.MaxVoltage( channel ) - this.MinVoltage( channel )) / this.Resolution;
        }

        #endregion

        #region " BUFFERS "

        /// <summary>
        /// Allocates a buffer for the subsystem.
        /// </summary>
        /// <param name="bufferLength">Length of the buffer.</param>
        /// <returns>OpenLayers.Base.OlBuffer.</returns>
        public OpenLayers.Base.OlBuffer AllocateBuffer( int bufferLength )
        {
            this.BufferLength = bufferLength;
            return new OpenLayers.Base.OlBuffer( bufferLength, this );
        }

        /// <summary> The buffers. </summary>
        private OpenLayers.Base.OlBuffer[] _Buffers;

        /// <summary> Select buffer. </summary>
        /// <param name="bufferIndex"> Zero-based index of the buffer. </param>
        /// <returns> An OpenLayers.Base.OlBuffer. </returns>
        public OpenLayers.Base.OlBuffer SelectBuffer( int bufferIndex )
        {
            return this._Buffers[bufferIndex];
        }

        /// <summary> Allocates a set of buffers for data collection. </summary>
        /// <param name="bufferCount">  The buffer count. </param>
        /// <param name="bufferLength"> Length of the buffer. </param>
        public void AllocateBuffers( int bufferCount, int bufferLength )
        {
            // save the buffer length
            this.BufferLength = bufferLength;

            // allocate the buffers array.  This is needed to buffer can be queued when restarting.
            this._Buffers = new OpenLayers.Base.OlBuffer[bufferCount];

            // allocate queue for data collection
            var i = default( int );
            while ( i < bufferCount )
            {
                this._Buffers[i] = new Buffer( this.BufferLength, this );
                i += 1;
            }
        }

        /// <summary> Gets the number of buffers that were allocated. </summary>
        /// <value> The buffers count. </value>
        public int BuffersCount => this._Buffers is not object ? 0 : this._Buffers.Length;

        /// <summary> Queue all allocated buffers. </summary>
        /// <returns> An OpenLayers.Base.BufferQueue. </returns>
        public OpenLayers.Base.BufferQueue QueueAllBuffers()
        {
            this.BufferQueue.FreeAllQueuedBuffers();
            return this.QueueAllFreeBuffers();
        }

        /// <summary> Queue all allocated buffers which states are Idle or Completed. </summary>
        public OpenLayers.Base.BufferQueue QueueAllFreeBuffers()
        {
            foreach ( OpenLayers.Base.OlBuffer buffer in this._Buffers )
            {
                if ( buffer.State == OpenLayers.Base.OlBuffer.BufferState.Idle || buffer.State == OpenLayers.Base.OlBuffer.BufferState.Completed )
                {
                    this.BufferQueue.QueueBuffer( buffer );
                }
            }

            return this.BufferQueue;
        }

        private readonly double[][] _Voltages;
        /// <summary>
        /// Returns a read only reference to the voltage array.
        /// </summary>
        /// <returns> An array of <see cref="T:System.Double"/> .</returns>
        public double[][] Voltages()
        {
            return this._Voltages;
        }

        private readonly uint[][] _Readings;
        /// <summary>Returns a read only reference to the readings array.</summary>
        [CLSCompliant( false )]
        public uint[][] Readings()
        {
            return this._Readings;
        }

        #endregion

        #region " CHANNELS "

        /// <summary>
        /// Add a set of channels.
        /// </summary>
        /// <param name="firstPhysicalChannel">The first physical channel.</param>
        /// <param name="lastPhysicalChannel">The last physical channel.</param>
        /// <returns>OpenLayers.Base.ChannelList.</returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                _ = this.ChannelList.Add( phyicalChannelNumber );
            return this.ChannelList;
        }

        /// <summary>
        /// Add a set of channels.
        /// </summary>
        /// <param name="firstPhysicalChannel">The first physical channel.</param>
        /// <param name="lastPhysicalChannel">The last physical channel.</param>
        /// <param name="gain">The gain.</param>
        /// <returns>OpenLayers.Base.ChannelList.</returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel, double gain )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                this.ChannelList.Add( phyicalChannelNumber ).Gain = gain;
            return this.ChannelList;
        }

        /// <summary>
        /// Gets or sets reference to the selected channel for this subsystem.
        /// </summary>
        /// <value>The selected channel.</value>
        public OpenLayers.Base.ChannelListEntry SelectedChannel { get; private set; }

        /// <summary>
        /// Gets the logical number (index) of the selected channel.
        /// </summary>
        /// <value>The selected channel logical number.</value>
        public int SelectedChannelLogicalNumber { get; private set; }

        /// <summary>
        /// Selects a channel.
        /// </summary>
        /// <param name="logicalChannelNumber">The logical channel number (channel index in the <see cref="OpenLayers.Base.ChannelList">channel list</see>.)</param>
        /// <returns>OpenLayers.Base.ChannelListEntry.</returns>
        public OpenLayers.Base.ChannelListEntry SelectLogicalChannel( int logicalChannelNumber )
        {
            this.SelectedChannel = this.ChannelList.SelectLogicalChannel( logicalChannelNumber );
            return this.SelectedChannel;
        }

        /// <summary>
        /// Selects a channel from the subsystem channel list by its physical channel number.
        /// </summary>
        /// <param name="physicalChannelNumber">Specifies the physical channel number.</param>
        /// <returns>OpenLayers.Base.ChannelListEntry.</returns>
        public OpenLayers.Base.ChannelListEntry SelectPhysicalChannel( int physicalChannelNumber )
        {
            if ( this.PhysicalChannelExists( physicalChannelNumber ) )
            {
                this.SelectedChannelLogicalNumber = this.ChannelList.LogicalChannelNumber( physicalChannelNumber );
                this.SelectedChannel = this.ChannelList[this.SelectedChannelLogicalNumber];
            }

            return this.SelectedChannel;
        }

        /// <summary> Queries if a given physical channel exists. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns> <c>True</c> if it succeeds, false if it fails. </returns>
        public bool PhysicalChannelExists( int physicalChannelNumber )
        {
            return this.ChannelList.Contains( physicalChannelNumber );
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary>
        /// Handles buffer done events.
        /// </summary>
        /// <param name="sender">Specifies reference to the 
        /// <see cref="OpenLayers.Base.SubsystemBase">subsystem</see></param>
        /// <param name="e">Specifies the 
        /// <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>.</param>
        private void BufferDoneHandler( object sender, OpenLayers.Base.BufferDoneEventArgs e )
        {
            this.OnBufferDone( e );
        }

        /// <summary>
        /// Handle buffer done events.
        /// </summary>
        /// <param name="e">The <see cref="OpenLayers.Base.BufferDoneEventArgs" /> instance containing the event data.</param>
        protected virtual void OnBufferDone( OpenLayers.Base.BufferDoneEventArgs e )
        {
        }

        #endregion

    }
}
