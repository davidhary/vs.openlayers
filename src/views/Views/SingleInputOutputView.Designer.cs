using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO.OL.WinViews
{
    [DesignerGenerated()]
    public partial class SingleInputOutputView
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
            var resources = new ComponentResourceManager(typeof(SingleInputOutputView));
            _ToolTip = new ToolTip(components);
            _InputFourVoltageTextBox = new TextBox();
            _InputThreeRangeCombo = new ComboBox();
            _InputFourRangeCombo = new ComboBox();
            _InputTwoRangeCombo = new ComboBox();
            _InputOneRangeCombo = new ComboBox();
            _InputFourChannelCombo = new ComboBox();
            _InputThreeChannelCombo = new ComboBox();
            _InputTwoChannelCombo = new ComboBox();
            _InputOneChannelCombo = new ComboBox();
            _InputOneVoltageTextBox = new TextBox();
            _InputTwoVoltageTextBox = new TextBox();
            _InputThreeVoltageTextBox = new TextBox();
            _AutoUpdateCheckBox = new CheckBox();
            _OpenDeviceCheckBox = new CheckBox();
            _UpdateIOButton = new Button();
            _AnalogOutputZeroVoltageNumeric = new NumericUpDown();
            _RangeLabel = new Label();
            _AnalogInputGroupBox = new GroupBox();
            _AnalogInputLabelA = new Label();
            _ChannelLabel = new Label();
            _UpdateIOTimer = new Timer(components);
            _DigitalIoGroupBox = new GroupBox();
            _DigitalOutputNumericUpDown = new Core.Controls.NumericUpDown();
            _DigitalInputNumericUpDown = new Core.Controls.NumericUpDown();
            _DigitalInputCheckBox = new CheckBox();
            _DigitalOutputCheckBox = new CheckBox();
            _AnalogOutputGroupBox = new GroupBox();
            _AnalogOutputOneVoltageNumeric = new NumericUpDown();
            _AnalogOutputZeroLabel = new Label();
            _AnalogOutputVoltageLabel = new Label();
            _AnalogOutputOneLabel = new Label();
            _AnalogOutputChannelLabel = new Label();
            _CounterGroupBox = new GroupBox();
            _CountTextBox = new TextBox();
            _CountCaptionLabel = new Label();
            _ResetCountCheckBox = new CheckBox();
            _BoardMessagesComboBoxLabel = new Label();
            _ApplicationMessagesComboBoxLabel = new Label();
            _BoardMessagesComboBox = new ComboBox();
            _ApplicationMessagesComboBox = new ComboBox();
            ((ISupportInitialize)_AnalogOutputZeroVoltageNumeric).BeginInit();
            _AnalogInputGroupBox.SuspendLayout();
            _DigitalIoGroupBox.SuspendLayout();
            ((ISupportInitialize)_DigitalOutputNumericUpDown).BeginInit();
            ((ISupportInitialize)_DigitalInputNumericUpDown).BeginInit();
            _AnalogOutputGroupBox.SuspendLayout();
            ((ISupportInitialize)_AnalogOutputOneVoltageNumeric).BeginInit();
            _CounterGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _InputFourVoltageTextBox
            // 
            _InputFourVoltageTextBox.AcceptsReturn = true;
            _InputFourVoltageTextBox.BackColor = SystemColors.Window;
            _InputFourVoltageTextBox.Cursor = Cursors.IBeam;
            _InputFourVoltageTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputFourVoltageTextBox.ForeColor = SystemColors.WindowText;
            _InputFourVoltageTextBox.Location = new Point(164, 131);
            _InputFourVoltageTextBox.MaxLength = 0;
            _InputFourVoltageTextBox.Name = "_InputFourVoltageTextBox";
            _InputFourVoltageTextBox.ReadOnly = true;
            _InputFourVoltageTextBox.RightToLeft = RightToLeft.No;
            _InputFourVoltageTextBox.Size = new Size(65, 25);
            _InputFourVoltageTextBox.TabIndex = 14;
            _ToolTip.SetToolTip(_InputFourVoltageTextBox, "Input voltage");
            // 
            // _InputThreeRangeCombo
            // 
            _InputThreeRangeCombo.BackColor = SystemColors.Window;
            _InputThreeRangeCombo.Cursor = Cursors.Default;
            _InputThreeRangeCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputThreeRangeCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputThreeRangeCombo.ForeColor = SystemColors.WindowText;
            _InputThreeRangeCombo.Items.AddRange(new object[] { "1", "2", "4", "5", "8", "10", "16", "20" });
            _InputThreeRangeCombo.Location = new Point(94, 100);
            _InputThreeRangeCombo.Name = "_InputThreeRangeCombo";
            _InputThreeRangeCombo.RightToLeft = RightToLeft.No;
            _InputThreeRangeCombo.Size = new Size(55, 25);
            _InputThreeRangeCombo.TabIndex = 10;
            _ToolTip.SetToolTip(_InputThreeRangeCombo, "Select input range");
            // 
            // _InputFourRangeCombo
            // 
            _InputFourRangeCombo.BackColor = SystemColors.Window;
            _InputFourRangeCombo.Cursor = Cursors.Default;
            _InputFourRangeCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputFourRangeCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputFourRangeCombo.ForeColor = SystemColors.WindowText;
            _InputFourRangeCombo.Items.AddRange(new object[] { "1", "2", "4", "5", "8", "10", "16", "20" });
            _InputFourRangeCombo.Location = new Point(94, 129);
            _InputFourRangeCombo.Name = "_InputFourRangeCombo";
            _InputFourRangeCombo.RightToLeft = RightToLeft.No;
            _InputFourRangeCombo.Size = new Size(55, 25);
            _InputFourRangeCombo.TabIndex = 13;
            _ToolTip.SetToolTip(_InputFourRangeCombo, "Select input range");
            // 
            // _InputTwoRangeCombo
            // 
            _InputTwoRangeCombo.BackColor = SystemColors.Window;
            _InputTwoRangeCombo.Cursor = Cursors.Default;
            _InputTwoRangeCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputTwoRangeCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputTwoRangeCombo.ForeColor = SystemColors.WindowText;
            _InputTwoRangeCombo.Items.AddRange(new object[] { "1", "2", "4", "5", "8", "10", "16", "20" });
            _InputTwoRangeCombo.Location = new Point(94, 71);
            _InputTwoRangeCombo.Name = "_InputTwoRangeCombo";
            _InputTwoRangeCombo.RightToLeft = RightToLeft.No;
            _InputTwoRangeCombo.Size = new Size(55, 25);
            _InputTwoRangeCombo.TabIndex = 7;
            _ToolTip.SetToolTip(_InputTwoRangeCombo, "Select input range");
            // 
            // _InputOneRangeCombo
            // 
            _InputOneRangeCombo.BackColor = SystemColors.Window;
            _InputOneRangeCombo.Cursor = Cursors.Default;
            _InputOneRangeCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputOneRangeCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputOneRangeCombo.ForeColor = SystemColors.WindowText;
            _InputOneRangeCombo.Items.AddRange(new object[] { "±20.00V", "±10.0.V", "  ±5.00V", "  ±4.00V", "  ±2.50V", "  ±2.00V", "  ±1.25V", "  ±1.00V" });
            _InputOneRangeCombo.Location = new Point(94, 42);
            _InputOneRangeCombo.Name = "_InputOneRangeCombo";
            _InputOneRangeCombo.RightToLeft = RightToLeft.No;
            _InputOneRangeCombo.Size = new Size(55, 25);
            _InputOneRangeCombo.TabIndex = 4;
            _ToolTip.SetToolTip(_InputOneRangeCombo, "Select input range");
            // 
            // _InputFourChannelCombo
            // 
            _InputFourChannelCombo.BackColor = SystemColors.Window;
            _InputFourChannelCombo.Cursor = Cursors.Default;
            _InputFourChannelCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputFourChannelCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputFourChannelCombo.ForeColor = SystemColors.WindowText;
            _InputFourChannelCombo.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _InputFourChannelCombo.Location = new Point(20, 129);
            _InputFourChannelCombo.Name = "_InputFourChannelCombo";
            _InputFourChannelCombo.RightToLeft = RightToLeft.No;
            _InputFourChannelCombo.Size = new Size(58, 25);
            _InputFourChannelCombo.TabIndex = 12;
            _ToolTip.SetToolTip(_InputFourChannelCombo, "Select input channel number");
            _InputFourChannelCombo.KeyPress += new KeyPressEventHandler( ChannelSelect_KeyPress );
            // 
            // _InputThreeChannelCombo
            // 
            _InputThreeChannelCombo.BackColor = SystemColors.Window;
            _InputThreeChannelCombo.Cursor = Cursors.Default;
            _InputThreeChannelCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputThreeChannelCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputThreeChannelCombo.ForeColor = SystemColors.WindowText;
            _InputThreeChannelCombo.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _InputThreeChannelCombo.Location = new Point(20, 100);
            _InputThreeChannelCombo.Name = "_InputThreeChannelCombo";
            _InputThreeChannelCombo.RightToLeft = RightToLeft.No;
            _InputThreeChannelCombo.Size = new Size(58, 25);
            _InputThreeChannelCombo.TabIndex = 9;
            _ToolTip.SetToolTip(_InputThreeChannelCombo, "Select input channel number");
            _InputThreeChannelCombo.KeyPress += new KeyPressEventHandler( ChannelSelect_KeyPress );
            // 
            // _InputTwoChannelCombo
            // 
            _InputTwoChannelCombo.BackColor = SystemColors.Window;
            _InputTwoChannelCombo.Cursor = Cursors.Default;
            _InputTwoChannelCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputTwoChannelCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputTwoChannelCombo.ForeColor = SystemColors.WindowText;
            _InputTwoChannelCombo.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _InputTwoChannelCombo.Location = new Point(20, 71);
            _InputTwoChannelCombo.Name = "_InputTwoChannelCombo";
            _InputTwoChannelCombo.RightToLeft = RightToLeft.No;
            _InputTwoChannelCombo.Size = new Size(58, 25);
            _InputTwoChannelCombo.TabIndex = 6;
            _ToolTip.SetToolTip(_InputTwoChannelCombo, "Select input channel number");
            _InputTwoChannelCombo.KeyPress += new KeyPressEventHandler( ChannelSelect_KeyPress );
            // 
            // _InputOneChannelCombo
            // 
            _InputOneChannelCombo.BackColor = SystemColors.Window;
            _InputOneChannelCombo.Cursor = Cursors.Default;
            _InputOneChannelCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _InputOneChannelCombo.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputOneChannelCombo.ForeColor = SystemColors.WindowText;
            _InputOneChannelCombo.Items.AddRange(new object[] { "0", "1", "2", "3" });
            _InputOneChannelCombo.Location = new Point(20, 42);
            _InputOneChannelCombo.Name = "_InputOneChannelCombo";
            _InputOneChannelCombo.RightToLeft = RightToLeft.No;
            _InputOneChannelCombo.Size = new Size(58, 25);
            _InputOneChannelCombo.TabIndex = 3;
            _ToolTip.SetToolTip(_InputOneChannelCombo, "Select input channel number");
            _InputOneChannelCombo.KeyPress += new KeyPressEventHandler( ChannelSelect_KeyPress );
            // 
            // _InputOneVoltageTextBox
            // 
            _InputOneVoltageTextBox.AcceptsReturn = true;
            _InputOneVoltageTextBox.BackColor = SystemColors.Window;
            _InputOneVoltageTextBox.Cursor = Cursors.IBeam;
            _InputOneVoltageTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputOneVoltageTextBox.ForeColor = SystemColors.WindowText;
            _InputOneVoltageTextBox.Location = new Point(164, 44);
            _InputOneVoltageTextBox.MaxLength = 0;
            _InputOneVoltageTextBox.Name = "_InputOneVoltageTextBox";
            _InputOneVoltageTextBox.ReadOnly = true;
            _InputOneVoltageTextBox.RightToLeft = RightToLeft.No;
            _InputOneVoltageTextBox.Size = new Size(65, 25);
            _InputOneVoltageTextBox.TabIndex = 5;
            _ToolTip.SetToolTip(_InputOneVoltageTextBox, "Input voltage");
            // 
            // _InputTwoVoltageTextBox
            // 
            _InputTwoVoltageTextBox.AcceptsReturn = true;
            _InputTwoVoltageTextBox.BackColor = SystemColors.Window;
            _InputTwoVoltageTextBox.Cursor = Cursors.IBeam;
            _InputTwoVoltageTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputTwoVoltageTextBox.ForeColor = SystemColors.WindowText;
            _InputTwoVoltageTextBox.Location = new Point(164, 73);
            _InputTwoVoltageTextBox.MaxLength = 0;
            _InputTwoVoltageTextBox.Name = "_InputTwoVoltageTextBox";
            _InputTwoVoltageTextBox.ReadOnly = true;
            _InputTwoVoltageTextBox.RightToLeft = RightToLeft.No;
            _InputTwoVoltageTextBox.Size = new Size(65, 25);
            _InputTwoVoltageTextBox.TabIndex = 8;
            _ToolTip.SetToolTip(_InputTwoVoltageTextBox, "Input voltage");
            // 
            // _InputThreeVoltageTextBox
            // 
            _InputThreeVoltageTextBox.AcceptsReturn = true;
            _InputThreeVoltageTextBox.BackColor = SystemColors.Window;
            _InputThreeVoltageTextBox.Cursor = Cursors.IBeam;
            _InputThreeVoltageTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _InputThreeVoltageTextBox.ForeColor = SystemColors.WindowText;
            _InputThreeVoltageTextBox.Location = new Point(164, 102);
            _InputThreeVoltageTextBox.MaxLength = 0;
            _InputThreeVoltageTextBox.Name = "_InputThreeVoltageTextBox";
            _InputThreeVoltageTextBox.ReadOnly = true;
            _InputThreeVoltageTextBox.RightToLeft = RightToLeft.No;
            _InputThreeVoltageTextBox.Size = new Size(65, 25);
            _InputThreeVoltageTextBox.TabIndex = 11;
            _ToolTip.SetToolTip(_InputThreeVoltageTextBox, "Input voltage");
            // 
            // _AutoUpdateCheckBox
            // 
            _AutoUpdateCheckBox.AutoSize = true;
            _AutoUpdateCheckBox.BackColor = SystemColors.Control;
            _AutoUpdateCheckBox.Cursor = Cursors.Default;
            _AutoUpdateCheckBox.Enabled = false;
            _AutoUpdateCheckBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _AutoUpdateCheckBox.ForeColor = SystemColors.ControlText;
            _AutoUpdateCheckBox.Location = new Point(26, 18);
            _AutoUpdateCheckBox.Name = "_AutoUpdateCheckBox";
            _AutoUpdateCheckBox.RightToLeft = RightToLeft.No;
            _AutoUpdateCheckBox.Size = new Size(105, 21);
            _AutoUpdateCheckBox.TabIndex = 2;
            _AutoUpdateCheckBox.Text = "Auto Update";
            _ToolTip.SetToolTip(_AutoUpdateCheckBox, "Check to update all inputs and outputs on a timer event");
            _AutoUpdateCheckBox.UseVisualStyleBackColor = false;
            _AutoUpdateCheckBox.CheckedChanged += new EventHandler( AutoUpdateCheckBox_CheckedChanged );
            // 
            // _OpenDeviceCheckBox
            // 
            _OpenDeviceCheckBox.Appearance = Appearance.Button;
            _OpenDeviceCheckBox.Enabled = false;
            _OpenDeviceCheckBox.FlatStyle = FlatStyle.System;
            _OpenDeviceCheckBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _OpenDeviceCheckBox.Location = new Point(436, 12);
            _OpenDeviceCheckBox.Name = "_OpenDeviceCheckBox";
            _OpenDeviceCheckBox.Size = new Size(120, 32);
            _OpenDeviceCheckBox.TabIndex = 0;
            _OpenDeviceCheckBox.Text = "&Open Device";
            _OpenDeviceCheckBox.TextAlign = ContentAlignment.MiddleCenter;
            _ToolTip.SetToolTip(_OpenDeviceCheckBox, "Click to open the device");
            _OpenDeviceCheckBox.CheckedChanged += new EventHandler( OpenDeviceCheckBox_CheckedChanged );
            // 
            // _UpdateIOButton
            // 
            _UpdateIOButton.Enabled = false;
            _UpdateIOButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _UpdateIOButton.Location = new Point(137, 12);
            _UpdateIOButton.Name = "_UpdateIOButton";
            _UpdateIOButton.Size = new Size(120, 32);
            _UpdateIOButton.TabIndex = 1;
            _UpdateIOButton.Text = "&Update I/O";
            _ToolTip.SetToolTip(_UpdateIOButton, "Updates the I/O");
            _UpdateIOButton.UseVisualStyleBackColor = true;
            _UpdateIOButton.Click += new EventHandler( UpdateIOButton_Click );
            // 
            // _AnalogOutputZeroVoltageNumeric
            // 
            _AnalogOutputZeroVoltageNumeric.DecimalPlaces = 4;
            _AnalogOutputZeroVoltageNumeric.Enabled = false;
            _AnalogOutputZeroVoltageNumeric.Location = new Point(46, 44);
            _AnalogOutputZeroVoltageNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _AnalogOutputZeroVoltageNumeric.Minimum = new decimal(new int[] { 10, 0, 0, (int)-2147483648L });
            _AnalogOutputZeroVoltageNumeric.Name = "_AnalogOutputZeroVoltageNumeric";
            _AnalogOutputZeroVoltageNumeric.Size = new Size(69, 25);
            _AnalogOutputZeroVoltageNumeric.TabIndex = 5;
            _ToolTip.SetToolTip(_AnalogOutputZeroVoltageNumeric, "Output voltage");
            // 
            // _RangeLabel
            // 
            _RangeLabel.AutoSize = true;
            _RangeLabel.BackColor = SystemColors.Control;
            _RangeLabel.Cursor = Cursors.Default;
            _RangeLabel.ForeColor = SystemColors.ControlText;
            _RangeLabel.Location = new Point(89, 23);
            _RangeLabel.Name = "_RangeLabel";
            _RangeLabel.RightToLeft = RightToLeft.No;
            _RangeLabel.Size = new Size(65, 17);
            _RangeLabel.TabIndex = 1;
            _RangeLabel.Text = "Range [V]";
            // 
            // _AnalogInputGroupBox
            // 
            _AnalogInputGroupBox.Controls.Add(_InputFourVoltageTextBox);
            _AnalogInputGroupBox.Controls.Add(_InputThreeRangeCombo);
            _AnalogInputGroupBox.Controls.Add(_AnalogInputLabelA);
            _AnalogInputGroupBox.Controls.Add(_RangeLabel);
            _AnalogInputGroupBox.Controls.Add(_ChannelLabel);
            _AnalogInputGroupBox.Controls.Add(_InputFourRangeCombo);
            _AnalogInputGroupBox.Controls.Add(_InputTwoRangeCombo);
            _AnalogInputGroupBox.Controls.Add(_InputOneRangeCombo);
            _AnalogInputGroupBox.Controls.Add(_InputFourChannelCombo);
            _AnalogInputGroupBox.Controls.Add(_InputThreeChannelCombo);
            _AnalogInputGroupBox.Controls.Add(_InputTwoChannelCombo);
            _AnalogInputGroupBox.Controls.Add(_InputOneChannelCombo);
            _AnalogInputGroupBox.Controls.Add(_InputOneVoltageTextBox);
            _AnalogInputGroupBox.Controls.Add(_InputTwoVoltageTextBox);
            _AnalogInputGroupBox.Controls.Add(_InputThreeVoltageTextBox);
            _AnalogInputGroupBox.Enabled = false;
            _AnalogInputGroupBox.Location = new Point(312, 54);
            _AnalogInputGroupBox.Name = "_AnalogInputGroupBox";
            _AnalogInputGroupBox.Size = new Size(249, 170);
            _AnalogInputGroupBox.TabIndex = 5;
            _AnalogInputGroupBox.TabStop = false;
            _AnalogInputGroupBox.Text = "Analog Inputs";
            // 
            // _AnalogInputLabelA
            // 
            _AnalogInputLabelA.AutoSize = true;
            _AnalogInputLabelA.BackColor = SystemColors.Control;
            _AnalogInputLabelA.Cursor = Cursors.Default;
            _AnalogInputLabelA.ForeColor = SystemColors.ControlText;
            _AnalogInputLabelA.Location = new Point(170, 25);
            _AnalogInputLabelA.Name = "_AnalogInputLabelA";
            _AnalogInputLabelA.RightToLeft = RightToLeft.No;
            _AnalogInputLabelA.Size = new Size(52, 17);
            _AnalogInputLabelA.TabIndex = 2;
            _AnalogInputLabelA.Text = "Voltage";
            // 
            // _ChannelLabel
            // 
            _ChannelLabel.AutoSize = true;
            _ChannelLabel.BackColor = SystemColors.Control;
            _ChannelLabel.Cursor = Cursors.Default;
            _ChannelLabel.ForeColor = SystemColors.ControlText;
            _ChannelLabel.Location = new Point(22, 23);
            _ChannelLabel.Name = "_ChannelLabel";
            _ChannelLabel.RightToLeft = RightToLeft.No;
            _ChannelLabel.Size = new Size(54, 17);
            _ChannelLabel.TabIndex = 0;
            _ChannelLabel.Text = "Channel";
            // 
            // _UpdateIOTimer
            // 
            _UpdateIOTimer.Interval = 1000;
            _UpdateIOTimer.Tick += new EventHandler( UpdateIOTimer_Tick );
            // 
            // _DigitalIoGroupBox
            // 
            _DigitalIoGroupBox.Controls.Add(_DigitalOutputNumericUpDown);
            _DigitalIoGroupBox.Controls.Add(_DigitalInputNumericUpDown);
            _DigitalIoGroupBox.Controls.Add(_DigitalInputCheckBox);
            _DigitalIoGroupBox.Controls.Add(_DigitalOutputCheckBox);
            _DigitalIoGroupBox.Enabled = false;
            _DigitalIoGroupBox.Location = new Point(12, 54);
            _DigitalIoGroupBox.Name = "_DigitalIoGroupBox";
            _DigitalIoGroupBox.Size = new Size(142, 111);
            _DigitalIoGroupBox.TabIndex = 3;
            _DigitalIoGroupBox.TabStop = false;
            _DigitalIoGroupBox.Text = "Digital I/O";
            // 
            // _DigitalOutputNumericUpDown
            // 
            _DigitalOutputNumericUpDown.Hexadecimal = true;
            _DigitalOutputNumericUpDown.Location = new Point(80, 73);
            _DigitalOutputNumericUpDown.Maximum = new decimal(new int[] { 255, 0, 0, 0 });
            _DigitalOutputNumericUpDown.Name = "_DigitalOutputNumericUpDown";
            _DigitalOutputNumericUpDown.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _DigitalOutputNumericUpDown.ReadOnlyBackColor = SystemColors.Control;
            _DigitalOutputNumericUpDown.ReadOnlyForeColor = SystemColors.WindowText;
            _DigitalOutputNumericUpDown.ReadWriteBackColor = SystemColors.Window;
            _DigitalOutputNumericUpDown.ReadWriteForeColor = SystemColors.ControlText;
            _DigitalOutputNumericUpDown.Size = new Size(48, 25);
            _DigitalOutputNumericUpDown.TabIndex = 3;
            _DigitalOutputNumericUpDown.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _DigitalInputNumericUpDown
            // 
            _DigitalInputNumericUpDown.BackColor = SystemColors.Control;
            _DigitalInputNumericUpDown.CausesValidation = false;
            _DigitalInputNumericUpDown.ForeColor = SystemColors.WindowText;
            _DigitalInputNumericUpDown.Hexadecimal = true;
            _DigitalInputNumericUpDown.Location = new Point(80, 44);
            _DigitalInputNumericUpDown.Maximum = new decimal(new int[] { 255, 0, 0, 0 });
            _DigitalInputNumericUpDown.Name = "_DigitalInputNumericUpDown";
            _DigitalInputNumericUpDown.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _DigitalInputNumericUpDown.ReadOnly = true;
            _DigitalInputNumericUpDown.ReadOnlyBackColor = SystemColors.Control;
            _DigitalInputNumericUpDown.ReadOnlyForeColor = SystemColors.WindowText;
            _DigitalInputNumericUpDown.ReadWriteBackColor = SystemColors.Window;
            _DigitalInputNumericUpDown.ReadWriteForeColor = SystemColors.ControlText;
            _DigitalInputNumericUpDown.Size = new Size(48, 25);
            _DigitalInputNumericUpDown.TabIndex = 1;
            _DigitalInputNumericUpDown.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _DigitalInputCheckBox
            // 
            _DigitalInputCheckBox.AutoSize = true;
            _DigitalInputCheckBox.BackColor = SystemColors.Control;
            _DigitalInputCheckBox.Cursor = Cursors.Default;
            _DigitalInputCheckBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _DigitalInputCheckBox.ForeColor = SystemColors.ControlText;
            _DigitalInputCheckBox.Location = new Point(8, 46);
            _DigitalInputCheckBox.Name = "_DigitalInputCheckBox";
            _DigitalInputCheckBox.RightToLeft = RightToLeft.No;
            _DigitalInputCheckBox.Size = new Size(60, 21);
            _DigitalInputCheckBox.TabIndex = 0;
            _DigitalInputCheckBox.Text = "Input";
            _DigitalInputCheckBox.UseVisualStyleBackColor = false;
            // 
            // _DigitalOutputCheckBox
            // 
            _DigitalOutputCheckBox.AutoSize = true;
            _DigitalOutputCheckBox.BackColor = SystemColors.Control;
            _DigitalOutputCheckBox.Cursor = Cursors.Default;
            _DigitalOutputCheckBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _DigitalOutputCheckBox.ForeColor = SystemColors.ControlText;
            _DigitalOutputCheckBox.Location = new Point(7, 75);
            _DigitalOutputCheckBox.Name = "_DigitalOutputCheckBox";
            _DigitalOutputCheckBox.RightToLeft = RightToLeft.No;
            _DigitalOutputCheckBox.Size = new Size(71, 21);
            _DigitalOutputCheckBox.TabIndex = 2;
            _DigitalOutputCheckBox.Text = "Output";
            _DigitalOutputCheckBox.UseVisualStyleBackColor = false;
            // 
            // _AnalogOutputGroupBox
            // 
            _AnalogOutputGroupBox.Controls.Add(_AnalogOutputOneVoltageNumeric);
            _AnalogOutputGroupBox.Controls.Add(_AnalogOutputZeroVoltageNumeric);
            _AnalogOutputGroupBox.Controls.Add(_AnalogOutputZeroLabel);
            _AnalogOutputGroupBox.Controls.Add(_AnalogOutputVoltageLabel);
            _AnalogOutputGroupBox.Controls.Add(_AnalogOutputOneLabel);
            _AnalogOutputGroupBox.Controls.Add(_AnalogOutputChannelLabel);
            _AnalogOutputGroupBox.Enabled = false;
            _AnalogOutputGroupBox.Location = new Point(172, 54);
            _AnalogOutputGroupBox.Name = "_AnalogOutputGroupBox";
            _AnalogOutputGroupBox.Size = new Size(129, 111);
            _AnalogOutputGroupBox.TabIndex = 4;
            _AnalogOutputGroupBox.TabStop = false;
            _AnalogOutputGroupBox.Text = "Analog Outputs";
            // 
            // _AnalogOutputOneVoltageNumeric
            // 
            _AnalogOutputOneVoltageNumeric.DecimalPlaces = 4;
            _AnalogOutputOneVoltageNumeric.Enabled = false;
            _AnalogOutputOneVoltageNumeric.Location = new Point(46, 71);
            _AnalogOutputOneVoltageNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _AnalogOutputOneVoltageNumeric.Minimum = new decimal(new int[] { 10, 0, 0, (int)-2147483648L });
            _AnalogOutputOneVoltageNumeric.Name = "_AnalogOutputOneVoltageNumeric";
            _AnalogOutputOneVoltageNumeric.Size = new Size(69, 25);
            _AnalogOutputOneVoltageNumeric.TabIndex = 5;
            // 
            // _AnalogOutputZeroLabel
            // 
            _AnalogOutputZeroLabel.AutoSize = true;
            _AnalogOutputZeroLabel.BackColor = SystemColors.Control;
            _AnalogOutputZeroLabel.Cursor = Cursors.Default;
            _AnalogOutputZeroLabel.ForeColor = SystemColors.ControlText;
            _AnalogOutputZeroLabel.Location = new Point(26, 48);
            _AnalogOutputZeroLabel.Name = "_AnalogOutputZeroLabel";
            _AnalogOutputZeroLabel.RightToLeft = RightToLeft.No;
            _AnalogOutputZeroLabel.Size = new Size(18, 17);
            _AnalogOutputZeroLabel.TabIndex = 2;
            _AnalogOutputZeroLabel.Text = "0:";
            _AnalogOutputZeroLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _AnalogOutputVoltageLabel
            // 
            _AnalogOutputVoltageLabel.AutoSize = true;
            _AnalogOutputVoltageLabel.BackColor = SystemColors.Control;
            _AnalogOutputVoltageLabel.Cursor = Cursors.Default;
            _AnalogOutputVoltageLabel.ForeColor = SystemColors.ControlText;
            _AnalogOutputVoltageLabel.Location = new Point(58, 25);
            _AnalogOutputVoltageLabel.Name = "_AnalogOutputVoltageLabel";
            _AnalogOutputVoltageLabel.RightToLeft = RightToLeft.No;
            _AnalogOutputVoltageLabel.Size = new Size(52, 17);
            _AnalogOutputVoltageLabel.TabIndex = 1;
            _AnalogOutputVoltageLabel.Text = "Voltage";
            // 
            // _AnalogOutputOneLabel
            // 
            _AnalogOutputOneLabel.AutoSize = true;
            _AnalogOutputOneLabel.BackColor = SystemColors.Control;
            _AnalogOutputOneLabel.Cursor = Cursors.Default;
            _AnalogOutputOneLabel.ForeColor = SystemColors.ControlText;
            _AnalogOutputOneLabel.Location = new Point(26, 75);
            _AnalogOutputOneLabel.Name = "_AnalogOutputOneLabel";
            _AnalogOutputOneLabel.RightToLeft = RightToLeft.No;
            _AnalogOutputOneLabel.Size = new Size(18, 17);
            _AnalogOutputOneLabel.TabIndex = 4;
            _AnalogOutputOneLabel.Text = "1:";
            _AnalogOutputOneLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _AnalogOutputChannelLabel
            // 
            _AnalogOutputChannelLabel.AutoSize = true;
            _AnalogOutputChannelLabel.BackColor = SystemColors.Control;
            _AnalogOutputChannelLabel.Cursor = Cursors.Default;
            _AnalogOutputChannelLabel.ForeColor = SystemColors.ControlText;
            _AnalogOutputChannelLabel.Location = new Point(4, 25);
            _AnalogOutputChannelLabel.Name = "_AnalogOutputChannelLabel";
            _AnalogOutputChannelLabel.RightToLeft = RightToLeft.No;
            _AnalogOutputChannelLabel.Size = new Size(54, 17);
            _AnalogOutputChannelLabel.TabIndex = 0;
            _AnalogOutputChannelLabel.Text = "Channel";
            // 
            // _CounterGroupBox
            // 
            _CounterGroupBox.Controls.Add(_CountTextBox);
            _CounterGroupBox.Controls.Add(_CountCaptionLabel);
            _CounterGroupBox.Controls.Add(_ResetCountCheckBox);
            _CounterGroupBox.Enabled = false;
            _CounterGroupBox.Location = new Point(12, 172);
            _CounterGroupBox.Name = "_CounterGroupBox";
            _CounterGroupBox.Size = new Size(289, 53);
            _CounterGroupBox.TabIndex = 6;
            _CounterGroupBox.TabStop = false;
            _CounterGroupBox.Text = "Counter";
            // 
            // _CountTextBox
            // 
            _CountTextBox.AcceptsReturn = true;
            _CountTextBox.BackColor = SystemColors.Window;
            _CountTextBox.Cursor = Cursors.IBeam;
            _CountTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _CountTextBox.ForeColor = SystemColors.WindowText;
            _CountTextBox.Location = new Point(56, 18);
            _CountTextBox.MaxLength = 0;
            _CountTextBox.Name = "_CountTextBox";
            _CountTextBox.RightToLeft = RightToLeft.No;
            _CountTextBox.Size = new Size(80, 25);
            _CountTextBox.TabIndex = 1;
            // 
            // _CountCaptionLabel
            // 
            _CountCaptionLabel.AutoSize = true;
            _CountCaptionLabel.BackColor = SystemColors.Control;
            _CountCaptionLabel.Cursor = Cursors.Default;
            _CountCaptionLabel.ForeColor = SystemColors.ControlText;
            _CountCaptionLabel.Location = new Point(5, 23);
            _CountCaptionLabel.Name = "_CountCaptionLabel";
            _CountCaptionLabel.RightToLeft = RightToLeft.No;
            _CountCaptionLabel.Size = new Size(49, 17);
            _CountCaptionLabel.TabIndex = 0;
            _CountCaptionLabel.Text = "Count: ";
            _CountCaptionLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _ResetCountCheckBox
            // 
            _ResetCountCheckBox.AutoSize = true;
            _ResetCountCheckBox.BackColor = SystemColors.Control;
            _ResetCountCheckBox.Cursor = Cursors.Default;
            _ResetCountCheckBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _ResetCountCheckBox.ForeColor = SystemColors.ControlText;
            _ResetCountCheckBox.Location = new Point(164, 19);
            _ResetCountCheckBox.Name = "_ResetCountCheckBox";
            _ResetCountCheckBox.RightToLeft = RightToLeft.No;
            _ResetCountCheckBox.Size = new Size(113, 21);
            _ResetCountCheckBox.TabIndex = 2;
            _ResetCountCheckBox.Text = "Reset Counter";
            _ResetCountCheckBox.UseVisualStyleBackColor = false;
            // 
            // _BoardMessagesComboBoxLabel
            // 
            _BoardMessagesComboBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _BoardMessagesComboBoxLabel.AutoSize = true;
            _BoardMessagesComboBoxLabel.BackColor = SystemColors.Control;
            _BoardMessagesComboBoxLabel.Cursor = Cursors.Default;
            _BoardMessagesComboBoxLabel.ForeColor = SystemColors.ControlText;
            _BoardMessagesComboBoxLabel.Location = new Point(9, 228);
            _BoardMessagesComboBoxLabel.Name = "_BoardMessagesComboBoxLabel";
            _BoardMessagesComboBoxLabel.RightToLeft = RightToLeft.No;
            _BoardMessagesComboBoxLabel.Size = new Size(106, 17);
            _BoardMessagesComboBoxLabel.TabIndex = 7;
            _BoardMessagesComboBoxLabel.Text = "Board Messages";
            // 
            // _ApplicationMessagesComboBoxLabel
            // 
            _ApplicationMessagesComboBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _ApplicationMessagesComboBoxLabel.AutoSize = true;
            _ApplicationMessagesComboBoxLabel.BackColor = SystemColors.Control;
            _ApplicationMessagesComboBoxLabel.Cursor = Cursors.Default;
            _ApplicationMessagesComboBoxLabel.ForeColor = SystemColors.ControlText;
            _ApplicationMessagesComboBoxLabel.Location = new Point(9, 279);
            _ApplicationMessagesComboBoxLabel.Name = "_ApplicationMessagesComboBoxLabel";
            _ApplicationMessagesComboBoxLabel.RightToLeft = RightToLeft.No;
            _ApplicationMessagesComboBoxLabel.Size = new Size(130, 17);
            _ApplicationMessagesComboBoxLabel.TabIndex = 9;
            _ApplicationMessagesComboBoxLabel.Text = "Application Message";
            // 
            // _BoardMessagesComboBox
            // 
            _BoardMessagesComboBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _BoardMessagesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _BoardMessagesComboBox.FormattingEnabled = true;
            _BoardMessagesComboBox.Location = new Point(9, 247);
            _BoardMessagesComboBox.Name = "_BoardMessagesComboBox";
            _BoardMessagesComboBox.Size = new Size(552, 25);
            _BoardMessagesComboBox.TabIndex = 8;
            // 
            // _ApplicationMessagesComboBox
            // 
            _ApplicationMessagesComboBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _ApplicationMessagesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _ApplicationMessagesComboBox.FormattingEnabled = true;
            _ApplicationMessagesComboBox.Location = new Point(9, 298);
            _ApplicationMessagesComboBox.Name = "_ApplicationMessagesComboBox";
            _ApplicationMessagesComboBox.Size = new Size(552, 25);
            _ApplicationMessagesComboBox.TabIndex = 10;
            // 
            // SingleIO
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            ClientSize = new Size(571, 335);
            Controls.Add(_ApplicationMessagesComboBox);
            Controls.Add(_BoardMessagesComboBox);
            Controls.Add(_AnalogInputGroupBox);
            Controls.Add(_DigitalIoGroupBox);
            Controls.Add(_AnalogOutputGroupBox);
            Controls.Add(_CounterGroupBox);
            Controls.Add(_BoardMessagesComboBoxLabel);
            Controls.Add(_AutoUpdateCheckBox);
            Controls.Add(_OpenDeviceCheckBox);
            Controls.Add(_UpdateIOButton);
            Controls.Add(_ApplicationMessagesComboBoxLabel);
            Name = "SingleInputOutputView";
            Text = "Single Input Output";
            ((ISupportInitialize)_AnalogOutputZeroVoltageNumeric).EndInit();
            _AnalogInputGroupBox.ResumeLayout(false);
            _AnalogInputGroupBox.PerformLayout();
            _DigitalIoGroupBox.ResumeLayout(false);
            _DigitalIoGroupBox.PerformLayout();
            ((ISupportInitialize)_DigitalOutputNumericUpDown).EndInit();
            ((ISupportInitialize)_DigitalInputNumericUpDown).EndInit();
            _AnalogOutputGroupBox.ResumeLayout(false);
            _AnalogOutputGroupBox.PerformLayout();
            ((ISupportInitialize)_AnalogOutputOneVoltageNumeric).EndInit();
            _CounterGroupBox.ResumeLayout(false);
            _CounterGroupBox.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolTip _ToolTip;
        private TextBox _InputFourVoltageTextBox;
        private ComboBox _InputThreeRangeCombo;
        private ComboBox _InputFourRangeCombo;
        private ComboBox _InputTwoRangeCombo;
        private ComboBox _InputOneRangeCombo;
        private ComboBox _InputFourChannelCombo;
        private ComboBox _InputThreeChannelCombo;
        private ComboBox _InputTwoChannelCombo;
        private ComboBox _InputOneChannelCombo;
        private TextBox _InputOneVoltageTextBox;
        private TextBox _InputTwoVoltageTextBox;
        private TextBox _InputThreeVoltageTextBox;
        private CheckBox _AutoUpdateCheckBox;
        private CheckBox _OpenDeviceCheckBox;
        private Button _UpdateIOButton;
        private Label _RangeLabel;
        private GroupBox _AnalogInputGroupBox;
        private Label _AnalogInputLabelA;
        private Label _ChannelLabel;
        private Timer _UpdateIOTimer;
        private GroupBox _DigitalIoGroupBox;
        private Core.Controls.NumericUpDown _DigitalOutputNumericUpDown;
        private Core.Controls.NumericUpDown _DigitalInputNumericUpDown;
        private CheckBox _DigitalInputCheckBox;
        private CheckBox _DigitalOutputCheckBox;
        private GroupBox _AnalogOutputGroupBox;
        private Label _AnalogOutputZeroLabel;
        private Label _AnalogOutputOneLabel;
        private GroupBox _CounterGroupBox;
        private TextBox _CountTextBox;
        private Label _CountCaptionLabel;
        private CheckBox _ResetCountCheckBox;
        private Label _BoardMessagesComboBoxLabel;
        private Label _ApplicationMessagesComboBoxLabel;
        private ComboBox _BoardMessagesComboBox;
        private ComboBox _ApplicationMessagesComboBox;
        private Label _AnalogOutputVoltageLabel;
        private Label _AnalogOutputChannelLabel;
        private NumericUpDown _AnalogOutputOneVoltageNumeric;
        private NumericUpDown _AnalogOutputZeroVoltageNumeric;
    }
}
