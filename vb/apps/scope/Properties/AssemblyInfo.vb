Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Open Layers Library Tester")> 
<Assembly: AssemblyDescription("Tester for the Open Layers Library")> 
<Assembly: AssemblyProduct("IO.Open.Layers.Library.Scope.Tester")> 
<Assembly: CLSCompliant(False)> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 
