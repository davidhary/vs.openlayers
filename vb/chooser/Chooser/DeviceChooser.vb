''' <summary>
''' Displays a form for selecting a device.
''' </summary>
''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
'''   instance.(c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Class DeviceChooser
    Inherits isr.Core.Forma.FormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="DeviceChooser" /> class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As DeviceChooser

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As DeviceChooser
        If DeviceChooser._Instance Is Nothing OrElse DeviceChooser._Instance.IsDisposed Then
            SyncLock DeviceChooser.SyncLocker
                DeviceChooser._Instance = New DeviceChooser
            End SyncLock
        End If
        Return DeviceChooser._Instance
    End Function

#End Region

#Region " SHARD "

    ''' <summary>
    ''' Selects and returns an open layers device name.  If the board name is not
    ''' found, allow the user to select from the list of existing boards..
    ''' </summary>
    ''' <param name="deviceName">Name of the device.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function SelectDeviceName(ByVal deviceName As String) As String

        If OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            Dim boards As String() = OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()
            If OpenLayers.Base.DeviceMgr.Get.GetDeviceNames().Length = 1 Then
                Return OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()(0)
            Else
                Dim index As Integer = Array.IndexOf(Of String)(boards, deviceName)
                Return If(index >= 0,
                    boards(index),
                    If(DeviceChooser.[Get].ShowDialog() = Windows.Forms.DialogResult.OK, DeviceChooser.[Get].DeviceName, String.Empty))
            End If
        Else
            Return String.Empty
        End If

    End Function

    ''' <summary>
    ''' Selects and returns an open layers device name.  If the system has multiple
    ''' boards, this would allow the operator to select a board from a list.
    ''' </summary>
    ''' <returns>System.String.</returns>
    Public Shared Function SelectDeviceName() As String

        Return If(OpenLayers.Base.DeviceMgr.Get.HardwareAvailable,
            If(OpenLayers.Base.DeviceMgr.Get.GetDeviceNames().Length = 1,
                OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()(0),
                If(DeviceChooser.[Get].ShowDialog() = Windows.Forms.DialogResult.OK, DeviceChooser.[Get].DeviceName, String.Empty)),
            String.Empty)

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Returns the selected device name.
    ''' </summary>
    ''' <value>The name of the device.</value>
    Public ReadOnly Property DeviceName() As String
        Get
            Return Me._DeviceNameChooser.DeviceName
        End Get
    End Property

#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Accepts changes and exits this form.
    ''' </summary>
    Private Sub OkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AcceptButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Cancels changes and exits this form.
    ''' </summary>
    Private Sub CancelButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        MyBase.OnShown(e)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Handles the Load event of the form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Handles the Shown event of the form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub Form_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Initialize and set the user interface
            Me._DeviceNameChooser.RefreshDeviceNamesList()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary>
    ''' Handles the DeviceSelected event of the DeviceNameChooser1 control.
    ''' Enables acceptance exist.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub DeviceNameChooser_DeviceSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DeviceNameChooser.DeviceSelected
        Me._AcceptButton.Enabled = Me._DeviceNameChooser.DeviceName.Length > 0
    End Sub

#End Region

End Class
