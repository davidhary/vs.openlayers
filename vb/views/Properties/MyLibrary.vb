﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = isr.IO.OL.My.ProjectTraceEventId.OpenLayersForms

        Public Const AssemblyTitle As String = "Open Layers Views Library"
        Public Const AssemblyDescription As String = "Open Layers Views Library"
        Public Const AssemblyProduct As String = "Open.Layers.Views.Library"

    End Class

End Namespace

