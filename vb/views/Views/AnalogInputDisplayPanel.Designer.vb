<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AnalogInputDisplayPanel

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._AnalogInputDisplay = New isr.IO.OL.Display.AnalogInputDisplay()
        Me.SuspendLayout()
        '
        '_AnalogInputDisplay
        '
        Me._AnalogInputDisplay.CalculatePeriodCount = 0
        Me._AnalogInputDisplay.ChartMode = isr.IO.OL.ChartModeId.Scope
        Me._AnalogInputDisplay.ConfigurationPanelVisible = True
        Me._AnalogInputDisplay.Dock = System.Windows.Forms.DockStyle.Fill
        Me._AnalogInputDisplay.Location = New System.Drawing.Point(3, 3)
        Me._AnalogInputDisplay.MaximumMovingAverageLength = 1000
        Me._AnalogInputDisplay.MaximumRefreshRate = New Decimal(New Integer() {100, 0, 0, 0})
        Me._AnalogInputDisplay.MaximumSampleSize = 1000000
        Me._AnalogInputDisplay.MaximumSamplingRate = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me._AnalogInputDisplay.MaximumSignalBufferSize = 1000000
        Me._AnalogInputDisplay.MinimumMovingAverageLength = 2
        Me._AnalogInputDisplay.MinimumRefreshRate = New Decimal(New Integer() {1, 0, 0, 0})
        Me._AnalogInputDisplay.MinimumSamplingRate = New Decimal(New Integer() {0, 0, 0, 0})
        Me._AnalogInputDisplay.MinimumSignalMemoryLength = 0
        Me._AnalogInputDisplay.Name = "_AnalogInputDisplay"
        Me._AnalogInputDisplay.SampleSize = 1000
        Me._AnalogInputDisplay.SamplingPeriod = 0.0R
        Me._AnalogInputDisplay.SignalBufferSize = 1000
        Me._AnalogInputDisplay.SignalMemoryLength = 10000
        Me._AnalogInputDisplay.Size = New System.Drawing.Size(871, 510)
        Me._AnalogInputDisplay.TabIndex = 0
        Me._AnalogInputDisplay.UpdatePeriodCount = 10
        '
        'AnalogInputDisplayPanel
        '
        Me.ClientSize = New System.Drawing.Size(885, 542)
        Me.Controls.Add(Me._AnalogInputDisplay)
        Me.Name = "AnalogInputDisplayView"
        Me.Text = "Analog Input Display View"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _AnalogInputDisplay As isr.IO.OL.Display.AnalogInputDisplay
End Class
