Imports isr.Core.WinForms.ComboBoxExtensions
Imports isr.IO.OL.WinViews.ExceptionExtensions
''' <summary> A control panel for single I/O. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Class SingleInputOutputView
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then

                ' disable the timer
                Me._UpdateIOTimer.Enabled = False
                Windows.Forms.Application.DoEvents()

                If Me.DigitalInInternal IsNot Nothing Then Me.DigitalInInternal.Dispose() : Me.DigitalInInternal = Nothing
                If Me.DigitalOutInternal IsNot Nothing Then Me.DigitalOutInternal.Dispose() : Me.DigitalOutInternal = Nothing
                If Me.AnalogInputInternal IsNot Nothing Then Me.AnalogInputInternal.Dispose() : Me.AnalogInputInternal = Nothing
                If Me.AnalogOutputInternal IsNot Nothing Then Me.AnalogOutputInternal.Dispose() : Me.AnalogOutputInternal = Nothing

                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Gets or sets a value indicating whether this instance is open. </summary>
    ''' <value> <c>True</c> if this instance is open; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsOpen As Boolean
        Get
            Return Me.Device IsNot Nothing
        End Get
    End Property

    ''' <summary> Gets or sets the default device. </summary>
    ''' <value> The device. </value>
    Private Property Device As OpenLayers.Base.Device

    Private _BoardName As String
    ''' <summary> Gets or sets the default board name. </summary>
    ''' <value> The name of the board. </value>
    Public Property BoardName As String
        Get
            Return Me._BoardName
        End Get
        Set(value As String)
            Me._BoardName = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    Public Property Caption As String
        Get
            Return Me.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Caption) Then
                Me.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> opens access to the open layers device. </summary>
    ''' <remarks> Use this method to open the driver in real or demo modes. </remarks>
    ''' <param name="deviceName"> Name of the device. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenIO(ByVal deviceName As String)

        ' indicate that the device is not open.
        If Me.Device IsNot Nothing Then
            Me.Device.Dispose()
            Me.Device = Nothing
        End If

        If String.IsNullOrWhiteSpace(deviceName) Then
            Me.BoardMessage = "Board not connected."
            Return
        End If

        ' clear the board name.
        Me.BoardName = String.Empty

        Try

            ' get a board 
            Me.ApplicationMessage = "Opening I/O Board " & deviceName & "..."
            Me.BoardMessage = "Opening I/O Board " & deviceName & "..."
            Me.Device = OpenLayers.Base.DeviceMgr.Get.SelectDevice(deviceName)
            Me._UpdateIOButton.Enabled = Me.Device IsNot Nothing
            Me._AutoUpdateCheckBox.Enabled = Me.Device IsNot Nothing

            If Me.Device IsNot Nothing Then

                Me.BoardName = Me.Device.DeviceName
                Me.ApplicationMessage = "Opened I/O Board " & Me.BoardName
                Me.BoardMessage = "Opened I/O Board " & Me.BoardName

                Dim assy As New isr.Core.MyAssemblyInfo(My.Application.Info)
                Me.Text = assy.BuildDefaultCaption($": {Me.BoardName}: SINGLE I/O ")

                ' open the sub systems
                Me._DigitalInputCheckBox.Enabled = False
                If Me.DigitalInInternal IsNot Nothing Then
                    Me.DigitalInInternal.Dispose()
                End If
                If Me.Device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.DigitalInput) > 0 Then
                    Me.ApplicationMessage = "Opening digital input..."
                    Me.DigitalInInternal = New isr.IO.OL.DigitalInputSubsystem(Me.Device, 0)
                    Me.ApplicationMessage = "Digital input opened."

                    If Me.DigitalInInternal IsNot Nothing Then
                        Me.ApplicationMessage = "Configuring digital input..."
                        Me.DigitalInInternal.Configure(OpenLayers.Base.DataFlow.SingleValue)
                        Me._DigitalIoGroupBox.Enabled = True
                        Me._DigitalInputCheckBox.Enabled = True
                        Me.ApplicationMessage = "Digital input configured."
                    Else
                        Me._DigitalIoGroupBox.Enabled = False
                        Me.ApplicationMessage = "Failed configuring digital input."
                    End If
                End If

                Me._DigitalOutputCheckBox.Enabled = False
                If Me.DigitalOutInternal IsNot Nothing Then
                    Me.DigitalOutInternal.Dispose()
                End If

                If Me.Device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.DigitalOutput) > 0 Then
                    Me.ApplicationMessage = "Opening digital output..."
                    Me.DigitalOutInternal = New isr.IO.OL.DigitalOutputSubsystem(Me.Device, 0)
                    Me.ApplicationMessage = "Digital output opened."
                    If Me.DigitalOutInternal IsNot Nothing Then
                        Me.ApplicationMessage = "Configuring digital output..."
                        Me.DigitalOutInternal.Configure(OpenLayers.Base.DataFlow.SingleValue)
                        Me._DigitalIoGroupBox.Enabled = True
                        Me._DigitalOutputCheckBox.Enabled = True
                        Me.ApplicationMessage = "Digital output configured."
                    Else
                        Me.ApplicationMessage = "Failed configuring digital output."
                    End If
                End If

                Me._AnalogInputGroupBox.Enabled = False
                If Me.AnalogInputInternal IsNot Nothing Then
                    Me.AnalogInputInternal.Dispose()
                End If

                If Me.Device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.AnalogInput) > 0 Then
                    ' open analog to digital sub systems
                    Me.ApplicationMessage = "Opening analog input..."
                    Me.AnalogInputInternal = New isr.IO.OL.AnalogInputSubsystem(Me.Device, 0)
                    Me.ApplicationMessage = "analog input opened."
                    If Me.AnalogInputInternal IsNot Nothing Then
                        Me.ApplicationMessage = "Configuring analog input..."
                        Me._AnalogInputGroupBox.Enabled = True
                        Me.AnalogInputInternal.DataFlow = OpenLayers.Base.DataFlow.SingleValue
                        Me.AnalogInputInternal.Config()
                        Me._InputOneChannelCombo.Enabled = True
                        Me._InputOneChannelCombo.DataSource = Me.AnalogInputInternal.AvailableChannels("{0}")
                        Me._InputOneChannelCombo.SelectedIndex = Math.Min(0, Me._InputOneChannelCombo.Items.Count - 1)
                        Me._InputOneRangeCombo.Enabled = True
                        Me._InputOneRangeCombo.DataSource = Me.AnalogInputInternal.AvailableRanges("{0}/{1}")
                        Me._InputOneRangeCombo.SelectedIndex = 0

                        Me._InputTwoChannelCombo.Enabled = True
                        Me._InputTwoChannelCombo.DataSource = Me.AnalogInputInternal.AvailableChannels("{0}")
                        Me._InputTwoChannelCombo.SelectedIndex = Math.Min(1, Me._InputOneChannelCombo.Items.Count - 1)
                        Me._InputTwoRangeCombo.Enabled = True
                        Me._InputTwoRangeCombo.DataSource = Me.AnalogInputInternal.AvailableRanges("{0}/{1}")
                        Me._InputTwoRangeCombo.SelectedIndex = 0

                        Me._InputThreeChannelCombo.Enabled = True
                        Me._InputThreeChannelCombo.DataSource = Me.AnalogInputInternal.AvailableChannels("{0}")
                        Me._InputThreeChannelCombo.SelectedIndex = Math.Min(2, Me._InputOneChannelCombo.Items.Count - 1)
                        Me._InputThreeRangeCombo.Enabled = True
                        Me._InputThreeRangeCombo.DataSource = Me.AnalogInputInternal.AvailableRanges("{0}/{1}")
                        Me._InputThreeRangeCombo.SelectedIndex = 0

                        Me._InputFourChannelCombo.Enabled = True
                        Me._InputFourChannelCombo.DataSource = Me.AnalogInputInternal.AvailableChannels("{0}")
                        Me._InputFourChannelCombo.SelectedIndex = Math.Min(3, Me._InputOneChannelCombo.Items.Count - 1)
                        Me._InputFourRangeCombo.Enabled = True
                        Me._InputFourRangeCombo.DataSource = Me.AnalogInputInternal.AvailableRanges("{0}/{1}")
                        Me._InputFourRangeCombo.SelectedIndex = 0
                        Me.ApplicationMessage = "Analog input configured."
                    Else
                        Me.ApplicationMessage = "Failed configuring analog input."
                        Me._InputOneChannelCombo.Enabled = False
                        Me._InputOneRangeCombo.Enabled = False
                        Me._InputTwoChannelCombo.Enabled = False
                        Me._InputTwoRangeCombo.Enabled = False
                        Me._InputTwoChannelCombo.Enabled = False
                        Me._InputTwoRangeCombo.Enabled = False
                        Me._InputFourChannelCombo.Enabled = False
                        Me._InputFourRangeCombo.Enabled = False
                    End If
                End If

                ' open digital to analog sub systems
                Me._AnalogOutputGroupBox.Enabled = False
                If Me.AnalogOutputInternal IsNot Nothing Then
                    Me.AnalogOutputInternal.Dispose()
                End If

                ' check if device has analog output capabilities
                If Me.Device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.AnalogOutput) > 0 Then
                    Me.ApplicationMessage = "Opening analog output..."
                    Me.AnalogOutputInternal = New isr.IO.OL.AnalogOutputSubsystem(Me.Device, 1)
                    Me.ApplicationMessage = "analog output opened."
                    If Me.AnalogOutputInternal IsNot Nothing Then
                        Me.ApplicationMessage = "Configuring analog output..."
                        Me._AnalogOutputGroupBox.Enabled = True
                        Me.AnalogOutputInternal.DataFlow = OpenLayers.Base.DataFlow.SingleValue
                        Me.AnalogOutputInternal.Config()
                        Me.ApplicationMessage = "Analog output configured."
                    Else
                        Me.ApplicationMessage = "Failed configuring analog output."
                    End If
                End If

                ' open counter subsystems
                Me._CounterGroupBox.Enabled = False

                ' enable the timer
                Me._UpdateIOTimer.Enabled = True

                Me.ApplicationMessage = Me.BoardName & " configured."
                Me.BoardMessage = Me.BoardName & " opened."

            Else

                Me.Device = Nothing

                Me.BoardMessage = "board not found."

                ' disable the timer
                Me._UpdateIOTimer.Enabled = False

                Dim assy As New isr.Core.MyAssemblyInfo(My.Application.Info)
                Me.Text = assy.BuildDefaultCaption(". NO DEVICE: SINGLE I/O ")

            End If

            Me.NotifyPropertyChanged(NameOf(Me.IsOpen))


            ' enable controls
        Catch ex As System.Exception

            If String.IsNullOrWhiteSpace(deviceName) Then
                deviceName = "unknown"
            End If
            ex.Data.Add("@isr", "Exception opening IO.")
            ex.Data.Add("@isr.DeviceName", deviceName)
            Me.ShowException(ex)
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyLibrary.TraceEventId, ex.ToString)
            isr.Core.WindowsForms.ShowDialog(ex)

        End Try

    End Sub

    ''' <summary> Closes and releases the data acquisition sub systems. </summary>
    ''' <remarks> Use this method to close and release the driver. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CloseIO()

        Try

            ' disable the timer
            Me._UpdateIOTimer.Enabled = False
            Windows.Forms.Application.DoEvents()

            Try
                If Me.DigitalInInternal Is Nothing Then
                    Me.ApplicationMessage = "Warning. Digital input not defined."
                Else
                    Me.DigitalInInternal.Dispose()
                End If
            Catch ex As Exception
                Me.ShowException(ex)
            End Try

            Try
                If Me.DigitalOutInternal Is Nothing Then
                    Me.ApplicationMessage = "Warning. Digital output not defined."
                Else
                    Me.DigitalOutInternal.Dispose()
                End If
            Catch ex As Exception
                Me.ShowException(ex)
            End Try

            Try
                If Me.AnalogInputInternal Is Nothing Then
                    Me.ApplicationMessage = "Warning. Analog input not defined."
                Else
                    Me.AnalogInputInternal.Dispose()
                End If
            Catch ex As Exception
                Me.ShowException(ex)
            End Try

            Try
                If Me.AnalogOutputInternal IsNot Nothing Then
                    Me.AnalogOutputInternal.Dispose()
                End If
            Catch ex As Exception
                Me.ShowException(ex)
            End Try

            If Me.Device IsNot Nothing Then
                Me.Device.Dispose()
                Me.Device = Nothing
                Me.ApplicationMessage = Me.BoardName & " closed."
                Me.BoardMessage = Me.BoardName & " close."
            End If

        Catch ex As Exception
            Me.ShowException(ex)
        Finally

            ' disable all group boxes
            Me._AnalogInputGroupBox.Enabled = False
            Me._AnalogOutputGroupBox.Enabled = False
            Me._CounterGroupBox.Enabled = False
            Me._DigitalIoGroupBox.Enabled = False

        End Try

    End Sub

    ''' <summary> Terminates and disposes of class-level objects. </summary>
    ''' <remarks> Called from the form Closing method. </remarks>
    Private Sub TerminateObjects()

        ' disable all group boxes
        Me._AnalogInputGroupBox.Enabled = False
        Me._AnalogOutputGroupBox.Enabled = False
        Me._CounterGroupBox.Enabled = False
        Me._DigitalIoGroupBox.Enabled = False

        ' disable the timer
        Me._UpdateIOTimer.Enabled = False
        Windows.Forms.Application.DoEvents()

    End Sub

    ''' <summary> Updates all outputs and inputs. </summary>
    Private Sub UpdateIO()


        If Me.AnalogInputInternal IsNot Nothing Then

            Dim channelNumber As Int16
            Dim channelGain As Double
            Dim voltage As Double

            If Me.AnalogOutputInternal IsNot Nothing Then

                ' output analog voltages first so that we can use the analog output.
                Me.AnalogOutputInternal.OutputSingleVoltage(1, Me._AnalogOutputZeroVoltageNumeric.Value)
                Me.AnalogOutputInternal.OutputSingleVoltage(0, Me._AnalogOutputOneVoltageNumeric.Value)

            End If

            If Me._DigitalOutputCheckBox.Checked AndAlso Me.DigitalOutInternal IsNot Nothing Then
                Me.DigitalOutInternal.SingleReading = Convert.ToInt32(Me._DigitalOutputNumericUpDown.Value, Globalization.CultureInfo.CurrentCulture)
            End If

            If Me.AnalogInputInternal IsNot Nothing Then

                If Int16.TryParse(Me._InputOneChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelNumber = Int16.Parse(Me._InputOneChannelCombo.Text, Globalization.CultureInfo.CurrentCulture)
                    channelGain = Me.AnalogInputInternal.SupportedGains()(Convert.ToInt16(Me._InputOneRangeCombo.SelectedIndex))
                    voltage = Me.AnalogInputInternal.SingleVoltage(channelNumber, channelGain)
                    Me._InputOneVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

                If Int16.TryParse(Me._InputTwoChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelGain = Me.AnalogInputInternal.SupportedGains()(Convert.ToInt16(Me._InputTwoRangeCombo.SelectedIndex))
                    voltage = Me.AnalogInputInternal.SingleVoltage(channelNumber, channelGain)
                    Me._InputTwoVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

                If Int16.TryParse(Me._InputThreeChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelGain = Me.AnalogInputInternal.SupportedGains()(Convert.ToInt16(Me._InputThreeRangeCombo.SelectedIndex))
                    voltage = Me.AnalogInputInternal.SingleVoltage(channelNumber, channelGain)
                    Me._InputThreeVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

                If Int16.TryParse(Me._InputFourChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelGain = Me.AnalogInputInternal.SupportedGains()(Convert.ToInt16(Me._InputFourRangeCombo.SelectedIndex))
                    voltage = Me.AnalogInputInternal.SingleVoltage(channelNumber, channelGain)
                    Me._InputFourVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

            End If

        End If

        If Me._DigitalInputCheckBox.Checked AndAlso Me.DigitalInInternal IsNot Nothing Then
            Me._DigitalInputNumericUpDown.Value = Convert.ToDecimal(Me.DigitalInInternal.SingleReading, Globalization.CultureInfo.CurrentCulture)
        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets reference to the digital input subsystem.</summary>
    Private WithEvents DigitalInInternal As isr.IO.OL.DigitalInputSubsystem

    ''' <summary>Gets or sets reference to the digital output subsystem.</summary>
    Private WithEvents DigitalOutInternal As isr.IO.OL.DigitalOutputSubsystem

    ''' <summary>Gets or sets reference to the analog input subsystem.</summary>
    Private WithEvents AnalogInputInternal As isr.IO.OL.AnalogInputSubsystem

    ''' <summary>Gets or sets reference to the analog output subsystem.</summary>
    Private WithEvents AnalogOutputInternal As isr.IO.OL.AnalogOutputSubsystem

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> The status message. </value>
    Private Property ApplicationMessage As String
        Get
            Return Me._ApplicationMessagesComboBox.Text
        End Get
        Set(value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If Me._ApplicationMessagesComboBox.Items.Count > 50 Then
                    Do Until Me._ApplicationMessagesComboBox.Items.Count < 25
                        Me._ApplicationMessagesComboBox.Items.RemoveAt(Me._ApplicationMessagesComboBox.Items.Count - 1)
                    Loop
                End If
                Me._ApplicationMessagesComboBox.Items.Insert(0, value)
                If Me._ApplicationMessagesComboBox.SelectedIndex <> 0 Then
                    Me._ApplicationMessagesComboBox.SelectedIndex = 0
                End If
                Me._ApplicationMessagesComboBox.Invalidate()
                Me._ToolTip.SetToolTip(Me._ApplicationMessagesComboBox, value)
            End If
        End Set
    End Property

    ''' <summary> Shows the exception. </summary>
    ''' <param name="ex"> The ex. </param>
    Private Sub ShowException(ByVal ex As Exception)
        Me.ApplicationMessage = ex.Message
        Me.BoardMessage = ex.ToFullBlownString
        Me._ToolTip.SetToolTip(Me._BoardMessagesComboBox, ex.ToString)

    End Sub

    ''' <summary> Gets or sets the board error message. </summary>
    ''' <value> The board error message. </value>
    Private Property BoardMessage As String
        Get
            Return Me._BoardMessagesComboBox.Text
        End Get
        Set(value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If Me._BoardMessagesComboBox.Items.Count > 50 Then
                    Do Until Me._BoardMessagesComboBox.Items.Count < 25
                        Me._BoardMessagesComboBox.Items.RemoveAt(Me._BoardMessagesComboBox.Items.Count - 1)
                    Loop
                End If
                Me._BoardMessagesComboBox.Items.Insert(0, value)
                Me._ToolTip.SetToolTip(Me._BoardMessagesComboBox, value)
                If Me._BoardMessagesComboBox.SelectedIndex <> 0 Then
                    Me._BoardMessagesComboBox.SelectedIndex = 0
                End If
                Me._BoardMessagesComboBox.Invalidate()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sync locker.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As SingleInputOutputView

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SingleInputOutputView
        If Not SingleInputOutputView.Instantiated Then
            SyncLock SingleInputOutputView.SyncLocker
                SingleInputOutputView._Instance = New SingleInputOutputView
            End SyncLock
        End If
        Return SingleInputOutputView._Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock SingleInputOutputView.SyncLocker
                Return Not (SingleInputOutputView._Instance Is Nothing OrElse SingleInputOutputView._Instance.IsDisposed)
            End SyncLock
        End Get
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        Me._OpenDeviceCheckBox.Enabled = True
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by channelSelect for key press events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Key press event information. </param>
    Private Sub ChannelSelect_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _InputOneChannelCombo.KeyPress,
                                                                                    _InputTwoChannelCombo.KeyPress,
                                                                                    _InputThreeChannelCombo.KeyPress,
                                                                                    _InputFourChannelCombo.KeyPress
        CType(sender, ComboBox).SearchAndSelect(e)
    End Sub

    ''' <summary> Handles the CheckedChanged event of the openDeviceCheckBox control. Opens or closes
    ''' the device. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs"/> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenDeviceCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenDeviceCheckBox.CheckedChanged

        If Not Me._OpenDeviceCheckBox.Enabled Then Return
        Dim isOpening As Boolean = Me._OpenDeviceCheckBox.Checked
        Try
            Me._OpenDeviceCheckBox.Enabled = False
            If isOpening Then
                ' open the board
                Me.OpenIO(isr.IO.OL.DeviceChooser.DeviceChooser.SelectDeviceName())
            Else
                ' close the board
                Me.CloseIO()
            End If
            Me._OpenDeviceCheckBox.Checked = Me.IsOpen

            If Me._OpenDeviceCheckBox.Checked Then
                ' set the caption to close
                Me._OpenDeviceCheckBox.Text = "Cl&ose Device"
            Else
                ' set the caption to open
                Me._OpenDeviceCheckBox.Text = "&Open Device"
            End If
        Catch ex As Exception

            If String.IsNullOrWhiteSpace(Me.BoardName) Then
                Try
                    Me.BoardName = isr.IO.OL.DeviceChooser.DeviceChooser.SelectDeviceName()
                Catch
                End Try
            End If
            If String.IsNullOrWhiteSpace(Me.BoardName) Then
                Me.BoardName = "N/A"
            End If
            Me.ApplicationMessage = If(isOpening, "Failed opening " & Me.BoardName & " board", "Failed closing " & Me.BoardName & " board")
            ex.Data.Add("@isr", Me.ApplicationMessage)
            ex.Data.Add("@isr.BoardName", Me.BoardName)
            Me.ShowException(ex)
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyLibrary.TraceEventId, ex.ToString)
            isr.Core.WindowsForms.ShowDialog(ex)
        Finally
            Me._OpenDeviceCheckBox.Enabled = True
        End Try
    End Sub

    ''' <summary> Occurs upon timer events. </summary>
    ''' <remarks> Use this method to execute all timer actions. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub UpdateIOTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UpdateIOTimer.Tick

        If Me._AutoUpdateCheckBox.Checked Then

            Try

                Me._UpdateIOTimer.Enabled = False

                Me.UpdateIO()

                Me._UpdateIOTimer.Enabled = True


            Catch ex As System.Exception

                Me.ApplicationMessage = "Failed monitoring. Timer is stopped. Close and reopen the board."
                Me.ShowException(ex)

            End Try

        End If

    End Sub

    ''' <summary> Handles the CheckedChanged event of the autoUpdateCheckBox control. Toggles the
    ''' visibility of the manual update box. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs"/> instance containing the event data. </param>
    Private Sub AutoUpdateCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AutoUpdateCheckBox.CheckedChanged
        Me._UpdateIOButton.Visible = Not Me._AutoUpdateCheckBox.Checked
    End Sub

    ''' <summary> Event handler. Called by UpdateIOButton for click events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub UpdateIOButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UpdateIOButton.Click

        Try

            Me.updateIO()

        Catch ex As System.Exception

            Me.showException(ex)

        End Try

    End Sub

#End Region

End Class
