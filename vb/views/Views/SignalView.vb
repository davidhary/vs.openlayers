''' <summary> Displays acquired signals. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 6/8/2019 </para></remarks>
Public Class SignalView
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
        ' Add any initialization after the InitializeComponent() call
        Me.OnInstantiate()
        ' update the interval timer.
        Me._CreateDataTimer.Interval = 200
        Me.InitializingComponents = False
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Initializes additional components. </summary>
    ''' <remarks> Call this method from the class constructor if you would like to add other controls
    ''' or change anything set by the InitializeComponent method. </remarks>
    Private Sub OnInstantiate()

        Me._Display.ChartBeginInit()
        Me._Display.XDataCurrentRangeMin = _XYMin
        Me._Display.XDataCurrentRangeMax = _XYMax
        Me._Display.XDataRangeMin = _XYMin
        Me._Display.XDataRangeMax = _XYMax
        Me._Display.XDataName = "Time"
        Me._Display.XDataUnit = "sec"
        Me._Display.SignalBufferLength = _DefaultBufferLength
        Me._XMinTrackBar.Value = CType(Me._Display.XDataCurrentRangeMin, Integer)
        Me._XMaxTrackBar.Value = CType(Me._Display.XDataCurrentRangeMax, Integer)
        Me._SineWaveSignal = New OpenLayers.Signals.MemorySignal With {
            .Name = "SineWave",
            .RangeMax = _XYMax,
            .RangeMin = _XYMin,
            .CurrentRangeMax = _XYMax,
            .CurrentRangeMin = _XYMin,
            .Unit = "SineWave Unit"
        }
        Me._RampSignal = New OpenLayers.Signals.MemorySignal With {
            .Name = "Ramp",
            .RangeMax = _XYMax,
            .RangeMin = _XYMin,
            .CurrentRangeMax = _XYMax,
            .CurrentRangeMin = _XYMin,
            .Unit = "Ramp Unit"
        }
        Me._SquareWaveSignal = New OpenLayers.Signals.MemorySignal With {
            .Name = "SquareWave",
            .RangeMax = _XYMax,
            .RangeMin = _XYMin,
            .CurrentRangeMax = _XYMax,
            .CurrentRangeMin = _XYMin,
            .Unit = "SquareWave Unit"
        }
        Me._Display.ChartEndInit()

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The default buffer length. </summary>
    Private Const _DefaultBufferLength As Long = 100000

    ''' <summary> The x/y minimum. </summary>
    Private Const _XYMin As Long = 0

    ''' <summary> The x/y maximum. </summary>
    Private Const _XYMax As Long = 1000

    ''' <summary> The sine wave signal. </summary>
    Private _SineWaveSignal As OpenLayers.Signals.MemorySignal

    ''' <summary> The ramp signal. </summary>
    Private _RampSignal As OpenLayers.Signals.MemorySignal

    ''' <summary> The square wave signal. </summary>
    Private _SquareWaveSignal As OpenLayers.Signals.MemorySignal

#End Region

#Region " METHODS "

    ''' <summary> Creates signal data. </summary>
    ''' <param name="memorySignal"> The memory signal. </param>
    Private Sub CreateSignalData(ByVal memorySignal As OpenLayers.Signals.MemorySignal)
        Dim rnd As New Random(DateTimeOffset.Now.Millisecond)
        Dim xOffset As Integer = rnd.Next(-20, 20)
        Select Case memorySignal.Name
            Case "SineWave"
                Dim i As Integer = 0
                While i < Me._Display.SignalBufferLength
                    memorySignal.Data(i) = ((Math.Sin((i + xOffset) * (Math.PI / 180) * 0.01) * 380) + 400) + rnd.Next(20)
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
            Case "Ramp"
                Dim i As Integer = 0
                While i < Me._Display.SignalBufferLength
                    memorySignal.Data(i) = (((i + xOffset) Mod 100) * 2) + rnd.Next(20)
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
            Case "SquareWave"
                Dim offset As Integer = rnd.Next(20)
                Dim i As Integer = 0
                While i < Me._Display.SignalBufferLength
                    memorySignal.Data(i) = If(((i + xOffset) Mod 100) > ((30 + 3 * 10) + offset), 300 + rnd.Next(10), 10 + rnd.Next(10))
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
            Case Else
                Dim i As Integer = 0
                While i < Me._Display.SignalBufferLength
                    memorySignal.Data(i) = i
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
        End Select
    End Sub

    ''' <summary> Event handler. Called by _SingleRadioButton for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SingleRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SingleRadioButton.CheckedChanged
        Me._Display.DisableRendering()
        Me._Display.BandMode = If(Me._SingleRadioButton.Checked, OpenLayers.Controls.BandMode.SingleBand, OpenLayers.Controls.BandMode.MultiBand)
        Me._Display.EnableRendering()
        Me._Display.SignalUpdate()
    End Sub

    ''' <summary> Sets track bar positions dependent on current range. </summary>
    Private Sub SetTrackbarPositionsDependentOnCurrentRange()
        If Me._Display.Signals.Count > 0 Then
            If Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMax > CType(Me._YMaxTrackBar.Maximum, Double) Then
                Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMax = CType(Me._YMaxTrackBar.Maximum, Double)
            End If
            If Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMin < CType(Me._YMinTrackBar.Minimum, Double) Then
                Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMin = CType(Me._YMinTrackBar.Minimum, Double)
            End If
            Me._YMaxTrackBar.Value = If(Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMax > Me._YMaxTrackBar.Maximum,
                Me._YMaxTrackBar.Maximum,
                If(Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMax < Me._YMaxTrackBar.Minimum,
                    Me._YMaxTrackBar.Minimum,
                    CType(Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMax, Integer)))
            Me._YMinTrackBar.Value = If(Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMin < Me._YMaxTrackBar.Minimum,
                Me._YMinTrackBar.Minimum,
                If(Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMin > Me._YMaxTrackBar.Maximum,
                    Me._YMinTrackBar.Maximum,
                    CType(Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMin, Integer)))
        End If
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Event handler. Called by _createDataTimer for tick events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CreateDataTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CreateDataTimer.Tick
        Me._Display.DisableRendering()
        Dim indexSignal As Integer = 0
        While indexSignal < Me._Display.Signals.Count
            Me.CreateSignalData(Me._Display.Signals(indexSignal))
            System.Math.Min(System.Threading.Interlocked.Increment(indexSignal), indexSignal - 1)
        End While
        Dim sw As Stopwatch = Stopwatch.StartNew
        Me._Display.EnableRendering()
        Me._Display.SignalUpdate()
        Me._Display.Refresh()
        sw.Stop()
        Me._TimeLabel.Text = $"{sw.Elapsed.TotalMilliseconds} [ms]"
    End Sub

    ''' <summary> Event handler. Called by _XMinTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub XMinTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XMinTrackBar.Scroll
        If Me._Display.Signals.Count > 0 Then
            If Me._XMinTrackBar.Value >= Me._XMaxTrackBar.Value Then
                Me._XMinTrackBar.Value = Me._XMaxTrackBar.Value - 10
            End If
            Me._Display.XDataCurrentRangeMin = CType(Me._XMinTrackBar.Value, Double)
            Me._Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _XMaxTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub XMaxTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XMaxTrackBar.Scroll
        If Me._Display.Signals.Count > 0 Then
            If Me._XMinTrackBar.Value >= Me._XMaxTrackBar.Value Then
                Me._XMaxTrackBar.Value = Me._XMinTrackBar.Value + 10
            End If
            Me._Display.XDataCurrentRangeMax = CType(Me._XMaxTrackBar.Value, Double)
            Me._Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _YMaxTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub YMaxTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _YMaxTrackBar.Scroll
        If Me._Display.Signals.Count > 0 Then
            If Me._YMinTrackBar.Value >= Me._YMaxTrackBar.Value Then
                Me._YMaxTrackBar.Value = Me._YMinTrackBar.Value + 10
            End If
            Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMax = CType(Me._YMaxTrackBar.Value, Double)
            Me._Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _YMinTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub YMinTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _YMinTrackBar.Scroll
        If Me._Display.Signals.Count > 0 Then
            If Me._YMinTrackBar.Value >= Me._YMaxTrackBar.Value Then
                Me._YMinTrackBar.Value = Me._YMaxTrackBar.Value - 10
            End If
            Me._Display.Signals(Me._SignalListComboBox.SelectedIndex).CurrentRangeMin = CType(Me._YMinTrackBar.Value, Double)
            Me._Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _SignalListComboBox for selected value changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SignalListComboBox_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SignalListComboBox.SelectedValueChanged
        Me.SetTrackbarPositionsDependentOnCurrentRange()
    End Sub

    ''' <summary> Event handler. Called by _ColorDataButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ColorDataButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ColorDataButton.Click
        If Me._Display.Signals.Count > 0 Then
            Me._ColorDialog.Color = Me._Display.GetCurveColor(Me._SignalListComboBox.SelectedIndex)
            Me._ColorDialog.ShowDialog()
            Me._Display.SetCurveColor(Me._SignalListComboBox.SelectedIndex, Me._ColorDialog.Color)
        End If
    End Sub

    ''' <summary> Event handler. Called by _ColorAxesButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ColorAxesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ColorAxesButton.Click
        Me._ColorDialog.Color = Me._Display.AxesColor
        Me._ColorDialog.ShowDialog()
        Me._Display.AxesColor = Me._ColorDialog.Color

    End Sub

    ''' <summary> Event handler. Called by _ColorGridsButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ColorGridsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ColorGridsButton.Click
        Me._ColorDialog.Color = Me._Display.GridColor
        Me._ColorDialog.ShowDialog()
        Me._Display.GridColor = Me._ColorDialog.Color

    End Sub

    ''' <summary> Event handler. Called by _AutoScaleButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoScaleButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AutoScaleButton.Click
        Me._Display.DisableRendering()
        Me._Display.AutoScale = True
        Me.SetTrackbarPositionsDependentOnCurrentRange()
        Me._Display.EnableRendering()
        Me._Display.SignalUpdate()
    End Sub

    ''' <summary> Event handler. Called by _PrintButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PrintButton.Click
        Me._Display.Print()
    End Sub

    ''' <summary> Event handler. Called by )_SineWaveCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SineWaveCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SineWaveCheckBox.CheckedChanged
        Dim checkBox1 As CheckBox
        checkBox1 = CType(sender, CheckBox)

        If checkBox1.Checked Then
            Me._CreateDataTimer.Enabled = False
            Me._Display.DisableRendering()
            Me._Display.Signals.Add(Me._SineWaveSignal)
            Dim indexSignal As Integer = Me._SignalListComboBox.Items.Add(Me._SineWaveSignal.Name)
            Me._SignalListComboBox.SelectedIndex = indexSignal
            Me._Display.EnableRendering()
            Me._CreateDataTimer.Enabled = True
        Else
            If Me._Display.Signals.Contains(Me._SineWaveSignal) Then
                Me._Display.DisableRendering()
                Dim signalIndex As Integer = Me._Display.Signals.IndexOf(Me._SineWaveSignal)
                Me._Display.Signals.Remove(Me._SineWaveSignal)
                Me._SignalListComboBox.Items.RemoveAt(signalIndex)
                If Not (Me._SignalListComboBox.Items.Count = 0) Then
                    Me._SignalListComboBox.SelectedIndex = 0
                End If
                Me._Display.EnableRendering()
                Me._Display.SignalUpdate()
            End If
        End If
    End Sub

    ''' <summary> Event handler. Called by _RampCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RampCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RampCheckBox.CheckedChanged
        Dim checkBox1 As CheckBox
        checkBox1 = CType(sender, CheckBox)

        If checkBox1.Checked Then
            Me._CreateDataTimer.Enabled = False
            Me._Display.DisableRendering()
            Me._Display.Signals.Add(Me._RampSignal)
            Dim indexSignal As Integer = Me._SignalListComboBox.Items.Add(Me._RampSignal.Name)
            Me._SignalListComboBox.SelectedIndex = indexSignal
            Me._Display.EnableRendering()
            Me._CreateDataTimer.Enabled = True
        Else
            If Me._Display.Signals.Contains(Me._RampSignal) Then
                Me._Display.DisableRendering()
                Dim signalIndex As Integer = Me._Display.Signals.IndexOf(Me._RampSignal)
                Me._Display.Signals.Remove(Me._RampSignal)
                Me._SignalListComboBox.Items.RemoveAt(signalIndex)
                If Not (Me._SignalListComboBox.Items.Count = 0) Then
                    Me._SignalListComboBox.SelectedIndex = 0
                End If
                Me._Display.EnableRendering()
                Me._Display.SignalUpdate()
            End If
        End If
    End Sub

    ''' <summary> Event handler. Called by _SquareWaveCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SquareWaveCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SquareWaveCheckBox.CheckedChanged

        Dim checkBox1 As CheckBox
        checkBox1 = CType(sender, CheckBox)

        If checkBox1.Checked Then
            Me._CreateDataTimer.Enabled = False
            Me._Display.DisableRendering()
            Me._Display.Signals.Add(Me._SquareWaveSignal)
            Dim indexSignal As Integer = Me._SignalListComboBox.Items.Add(Me._SquareWaveSignal.Name)
            Me._SignalListComboBox.SelectedIndex = indexSignal
            Me._Display.EnableRendering()
            Me._CreateDataTimer.Enabled = True
        Else
            If Me._Display.Signals.Contains(Me._SquareWaveSignal) Then
                Me._Display.DisableRendering()
                Dim signalIndex As Integer = Me._Display.Signals.IndexOf(Me._SquareWaveSignal)
                Me._Display.Signals.Remove(Me._SquareWaveSignal)
                Me._SignalListComboBox.Items.RemoveAt(signalIndex)
                If Not (Me._SignalListComboBox.Items.Count = 0) Then
                    Me._SignalListComboBox.SelectedIndex = 0
                End If
                Me._Display.EnableRendering()
                Me._Display.SignalUpdate()
            End If
        End If
    End Sub

#End Region

End Class
