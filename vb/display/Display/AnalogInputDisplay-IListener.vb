Imports isr.Core
Partial Public Class AnalogInputDisplay
    Implements ITraceMessageListener

    ''' <summary> Gets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Protected Overloads ReadOnly Property IsDisposed As Boolean Implements ITraceMessageListener.IsDisposed
        Get
            Return MyBase.IsDisposed
        End Get
    End Property

    ''' <summary> Writes. </summary>
    ''' <param name="message"> The message to add. </param>
    Public Sub Write(message As String) Implements IMessageListener.Write
        Me.TraceEvent(New TraceMessage(TraceEventType.Information, My.MyLibrary.TraceEventId, message))
    End Sub

    ''' <summary> Writes a line. </summary>
    ''' <param name="message"> The message to add. </param>
    Public Sub WriteLine(message As String) Implements IMessageListener.WriteLine
        Me.TraceEvent(New TraceMessage(TraceEventType.Information, My.MyLibrary.TraceEventId, message))
    End Sub

    ''' <summary> Registers this event. </summary>
    ''' <param name="level"> The level. </param>
    Public Overridable Sub Register(level As TraceEventType) Implements IMessageListener.Register
    End Sub

    ''' <summary> Gets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Overridable Property TraceLevel As TraceEventType Implements IMessageListener.TraceLevel

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
    Public Function ShouldTrace(ByVal value As TraceEventType) As Boolean Implements IMessageListener.ShouldTrace
        Return value <= Me.TraceLevel
    End Function

    ''' <summary> Gets the sentinel indicating this as thread safe. </summary>
    ''' <value> True if thread safe. </value>
    Public Overridable ReadOnly Property IsThreadSafe As Boolean Implements IMessageListener.IsThreadSafe
        Get
            Return True
        End Get
    End Property

    ''' <summary> Gets or sets a unique identifier. </summary>
    ''' <value> The identifier of the unique. </value>
    Protected ReadOnly Property UniqueId As Guid = Guid.NewGuid Implements IMessageListener.UniqueId

    ''' <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
    ''' <param name="other"> The i trace message listener to compare to this object. </param>
    ''' <returns>
    ''' <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
    ''' </returns>
    Public Overloads Function Equals(other As IMessageListener) As Boolean Implements IEquatable(Of IMessageListener).Equals
        Return other IsNot Nothing AndAlso Guid.Equals(other.UniqueId, Me.UniqueId)
    End Function

    Private Sub ApplyTraceLevelThis(ByVal value As TraceEventType)
        Me._TraceLevel = value
    End Sub

    ''' <summary> Applies the trace level. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Public Overloads Sub ApplyTraceLevel(ByVal value As TraceEventType) Implements IMessageListener.ApplyTraceLevel
        Me.ApplyTraceLevelThis(value)
    End Sub

    ''' <summary> Gets the type of the listener. </summary>
    ''' <value> The type of the listener. </value>
    Public ReadOnly Property ListenerType As ListenerType Implements IMessageListener.ListenerType
        Get
            Return ListenerType.Display
        End Get
    End Property

    ''' <summary> Trace event overriding the trace level. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> A String. </returns>
    Public Function TraceEventOverride(value As TraceMessage) As String Implements ITraceMessageListener.TraceEventOverride
        If value Is Nothing Then
            Return String.Empty
        Else
            Me.TraceEvent(value)
            isr.IO.OL.My.MyLibrary.DoEvents()
            Return value.Details
        End If
    End Function

    ''' <summary> Writes a trace event to the trace listener overriding the trace level. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The message format. </param>
    ''' <param name="args">      Specified the message arguments. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEventOverride(eventType As TraceEventType, id As Integer, format As String, ParamArray args() As Object) As String Implements ITraceMessageListener.TraceEventOverride
        Return Me.TraceEventOverride(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The message format. </param>
    ''' <param name="args">      Specified the message arguments. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(eventType As TraceEventType, id As Integer, format As String, ParamArray args() As Object) As String Implements ITraceMessageListener.TraceEvent
        Return Me.TraceEvent(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(value As TraceMessage) As String Implements ITraceMessageListener.TraceEvent
        If value Is Nothing Then
            Return String.Empty
        Else
            If Me.ShouldTrace(value.EventType) Then
                Me._ChartToolStripStatusLabel.ToolTipText = value.Details
                Me._ChartToolStripStatusLabel.Text = value.ExtractSynopsis
            End If
            isr.IO.OL.My.MyLibrary.DoEvents()
            Return value.Details
        End If
    End Function

End Class
