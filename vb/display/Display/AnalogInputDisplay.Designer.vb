<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AnalogInputDisplay

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AnalogInputDisplay))
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._BoardTabPage = New System.Windows.Forms.TabPage()
        Me._BoardTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._BoardPanel = New System.Windows.Forms.Panel()
        Me._BoardGroupBox = New System.Windows.Forms.GroupBox()
        Me._BoardInputRangesComboBox = New System.Windows.Forms.ComboBox()
        Me._BoardInputRangesComboBoxLabel = New System.Windows.Forms.Label()
        Me._OpenCloseCheckBox = New System.Windows.Forms.CheckBox()
        Me._DeviceNameChooser = New isr.IO.OL.DeviceChooser.DeviceNameChooser()
        Me._DeviceNameLabel = New System.Windows.Forms.Label()
        Me._GlobalGroupBox = New System.Windows.Forms.GroupBox()
        Me._ConfigureButton = New System.Windows.Forms.Button()
        Me._ResetButton = New System.Windows.Forms.Button()
        Me._RestoreDefaultsButton = New System.Windows.Forms.Button()
        Me._ChartTabPage = New System.Windows.Forms.TabPage()
        Me._ChartTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ChartTypeTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._BandModeGroupBox = New System.Windows.Forms.GroupBox()
        Me._MultipleRadioButton = New System.Windows.Forms.RadioButton()
        Me._SingleRadioButton = New System.Windows.Forms.RadioButton()
        Me._ChartTypeGroupBox = New System.Windows.Forms.GroupBox()
        Me._StripChartRadioButton = New System.Windows.Forms.RadioButton()
        Me._ScopeRadioButton = New System.Windows.Forms.RadioButton()
        Me._ChartColorsTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ChartColorsGroupBox = New System.Windows.Forms.GroupBox()
        Me._ColorGridsButton = New System.Windows.Forms.Button()
        Me._ColorAxesButton = New System.Windows.Forms.Button()
        Me._ChartTitlesTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ChartTitlesGroupBox = New System.Windows.Forms.GroupBox()
        Me._ChartFooterTextBox = New System.Windows.Forms.TextBox()
        Me._ChartFooterTextBoxLabel = New System.Windows.Forms.Label()
        Me._ChartTitleTextBox = New System.Windows.Forms.TextBox()
        Me._ChartTitleTextBoxLabel = New System.Windows.Forms.Label()
        Me._ChartEnableTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ChartEnableGroupBox = New System.Windows.Forms.GroupBox()
        Me._RefreshRateNumeric = New System.Windows.Forms.NumericUpDown()
        Me._RefreshRateNumericLabel = New System.Windows.Forms.Label()
        Me._DisplayAverageCheckBox = New System.Windows.Forms.CheckBox()
        Me._DisplayVoltageCheckBox = New System.Windows.Forms.CheckBox()
        Me._EnableChartingCheckBox = New System.Windows.Forms.CheckBox()
        Me._channelsTabPage = New System.Windows.Forms.TabPage()
        Me._ChannelsTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ChannelsGroupBox = New System.Windows.Forms.GroupBox()
        Me._ChannelNameTextBox = New System.Windows.Forms.TextBox()
        Me._ChannelNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._AddChannelButton = New System.Windows.Forms.Button()
        Me._ChannelRangesComboBox = New System.Windows.Forms.ComboBox()
        Me._ChannelRangeComboBoxLabel = New System.Windows.Forms.Label()
        Me._ChannelComboBox = New System.Windows.Forms.ComboBox()
        Me._ChannelComboBoxLabel = New System.Windows.Forms.Label()
        Me._RemoveChannelButton = New System.Windows.Forms.Button()
        Me._ChannelsListView = New System.Windows.Forms.ListView()
        Me._ChannelChannelColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._ChannelRangeColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._SamplingGroupBox = New System.Windows.Forms.GroupBox()
        Me._SampleSizeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._BuffersCountNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SamplingRateNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SmartBufferingCheckBox = New System.Windows.Forms.CheckBox()
        Me._ContinuousBufferingCheckBox = New System.Windows.Forms.CheckBox()
        Me._SamplingRateNumericLabel = New System.Windows.Forms.Label()
        Me._SampleSizeNumericLabel = New System.Windows.Forms.Label()
        Me._BuffersCountNumericLabel = New System.Windows.Forms.Label()
        Me._ProcessingGroupBox = New System.Windows.Forms.GroupBox()
        Me._MovingAverageLengthNumeric = New System.Windows.Forms.NumericUpDown()
        Me._MovingAverageLengthNumericLabel = New System.Windows.Forms.Label()
        Me._CalculateAverageCheckBox = New System.Windows.Forms.CheckBox()
        Me._SignalsTabPage = New System.Windows.Forms.TabPage()
        Me._SignalsTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._SignalChannelGroupBox = New System.Windows.Forms.GroupBox()
        Me._AddSignalButton = New System.Windows.Forms.Button()
        Me._SignalChannelComboBox = New System.Windows.Forms.ComboBox()
        Me._SignalChannelComboBoxLabel = New System.Windows.Forms.Label()
        Me._SignalSamplingGroupBox = New System.Windows.Forms.GroupBox()
        Me._SignalMemoryLengthNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SignalBufferSizeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SignalMemoryLengthNumericLabel = New System.Windows.Forms.Label()
        Me._SignalBufferSizeNumericLabel = New System.Windows.Forms.Label()
        Me._SignalsGroupBox = New System.Windows.Forms.GroupBox()
        Me._UpdateSignalButton = New System.Windows.Forms.Button()
        Me._SignalMinNumericUpDownLabel = New System.Windows.Forms.Label()
        Me._SignalMaxNumericUpDownLabel = New System.Windows.Forms.Label()
        Me._SignalMinNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me._SignalMaxNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me._ColorSignalButton = New System.Windows.Forms.Button()
        Me._RemoveSignalButton = New System.Windows.Forms.Button()
        Me._SignalsListView = New System.Windows.Forms.ListView()
        Me._SignalChannelColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._SignalRangeColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._Chart = New OpenLayers.Controls.Display()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._MainTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._DisplayTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._ChartToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._CurrentVoltageToolStripLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._AverageVoltageToolStripLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._ActionsToolStripDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._StartStopToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConfigureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InfoProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._Tabs.SuspendLayout()
        Me._BoardTabPage.SuspendLayout()
        Me._BoardTableLayoutPanel.SuspendLayout()
        Me._BoardPanel.SuspendLayout()
        Me._BoardGroupBox.SuspendLayout()
        Me._GlobalGroupBox.SuspendLayout()
        Me._ChartTabPage.SuspendLayout()
        Me._ChartTableLayoutPanel.SuspendLayout()
        Me._ChartTypeTableLayoutPanel.SuspendLayout()
        Me._BandModeGroupBox.SuspendLayout()
        Me._ChartTypeGroupBox.SuspendLayout()
        Me._ChartColorsTableLayoutPanel.SuspendLayout()
        Me._ChartColorsGroupBox.SuspendLayout()
        Me._ChartTitlesTableLayoutPanel.SuspendLayout()
        Me._ChartTitlesGroupBox.SuspendLayout()
        Me._ChartEnableTableLayoutPanel.SuspendLayout()
        Me._ChartEnableGroupBox.SuspendLayout()
        CType(Me._RefreshRateNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._channelsTabPage.SuspendLayout()
        Me._ChannelsTableLayoutPanel.SuspendLayout()
        Me._ChannelsGroupBox.SuspendLayout()
        Me._SamplingGroupBox.SuspendLayout()
        CType(Me._SampleSizeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._BuffersCountNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SamplingRateNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ProcessingGroupBox.SuspendLayout()
        CType(Me._MovingAverageLengthNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._SignalsTabPage.SuspendLayout()
        Me._SignalsTableLayoutPanel.SuspendLayout()
        Me._SignalChannelGroupBox.SuspendLayout()
        Me._SignalSamplingGroupBox.SuspendLayout()
        CType(Me._SignalMemoryLengthNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SignalBufferSizeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._SignalsGroupBox.SuspendLayout()
        CType(Me._SignalMinNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SignalMaxNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Chart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._errorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._MainTableLayoutPanel.SuspendLayout()
        Me._DisplayTableLayoutPanel.SuspendLayout()
        Me._StatusStrip.SuspendLayout()
        CType(Me._InfoProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._BoardTabPage)
        Me._Tabs.Controls.Add(Me._ChartTabPage)
        Me._Tabs.Controls.Add(Me._channelsTabPage)
        Me._Tabs.Controls.Add(Me._SignalsTabPage)
        Me._Tabs.Location = New System.Drawing.Point(3, 3)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.Padding = New System.Drawing.Point(5, 3)
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(224, 550)
        Me._Tabs.TabIndex = 0
        '
        '_BoardTabPage
        '
        Me._BoardTabPage.Controls.Add(Me._BoardTableLayoutPanel)
        Me._BoardTabPage.Location = New System.Drawing.Point(4, 26)
        Me._BoardTabPage.Name = "_BoardTabPage"
        Me._BoardTabPage.Size = New System.Drawing.Size(216, 520)
        Me._BoardTabPage.TabIndex = 0
        Me._BoardTabPage.Text = "Board"
        Me._BoardTabPage.UseVisualStyleBackColor = True
        '
        '_BoardTableLayoutPanel
        '
        Me._BoardTableLayoutPanel.ColumnCount = 3
        Me._BoardTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._BoardTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._BoardTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._BoardTableLayoutPanel.Controls.Add(Me._BoardPanel, 1, 1)
        Me._BoardTableLayoutPanel.Controls.Add(Me._GlobalGroupBox, 1, 3)
        Me._BoardTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._BoardTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._BoardTableLayoutPanel.Name = "_BoardTableLayoutPanel"
        Me._BoardTableLayoutPanel.RowCount = 5
        Me._BoardTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._BoardTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._BoardTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me._BoardTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._BoardTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666!))
        Me._BoardTableLayoutPanel.Size = New System.Drawing.Size(216, 520)
        Me._BoardTableLayoutPanel.TabIndex = 0
        '
        '_BoardPanel
        '
        Me._BoardPanel.Controls.Add(Me._BoardGroupBox)
        Me._BoardPanel.Controls.Add(Me._OpenCloseCheckBox)
        Me._BoardPanel.Controls.Add(Me._DeviceNameChooser)
        Me._BoardPanel.Controls.Add(Me._DeviceNameLabel)
        Me._BoardPanel.Location = New System.Drawing.Point(8, 74)
        Me._BoardPanel.Name = "_BoardPanel"
        Me._BoardPanel.Size = New System.Drawing.Size(200, 161)
        Me._BoardPanel.TabIndex = 1
        '
        '_BoardGroupBox
        '
        Me._BoardGroupBox.Controls.Add(Me._BoardInputRangesComboBox)
        Me._BoardGroupBox.Controls.Add(Me._BoardInputRangesComboBoxLabel)
        Me._BoardGroupBox.Location = New System.Drawing.Point(10, 97)
        Me._BoardGroupBox.Name = "_BoardGroupBox"
        Me._BoardGroupBox.Size = New System.Drawing.Size(181, 50)
        Me._BoardGroupBox.TabIndex = 3
        Me._BoardGroupBox.TabStop = False
        Me._BoardGroupBox.Text = "Board"
        '
        '_BoardInputRangesComboBox
        '
        Me._BoardInputRangesComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._BoardInputRangesComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._BoardInputRangesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._BoardInputRangesComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._BoardInputRangesComboBox.Location = New System.Drawing.Point(89, 16)
        Me._BoardInputRangesComboBox.Name = "_BoardInputRangesComboBox"
        Me._BoardInputRangesComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._BoardInputRangesComboBox.Size = New System.Drawing.Size(83, 25)
        Me._BoardInputRangesComboBox.TabIndex = 1
        '
        '_BoardInputRangesComboBoxLabel
        '
        Me._BoardInputRangesComboBoxLabel.AutoSize = True
        Me._BoardInputRangesComboBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._BoardInputRangesComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._BoardInputRangesComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._BoardInputRangesComboBoxLabel.Location = New System.Drawing.Point(6, 20)
        Me._BoardInputRangesComboBoxLabel.Name = "_BoardInputRangesComboBoxLabel"
        Me._BoardInputRangesComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._BoardInputRangesComboBoxLabel.Size = New System.Drawing.Size(81, 17)
        Me._BoardInputRangesComboBoxLabel.TabIndex = 0
        Me._BoardInputRangesComboBoxLabel.Text = "Input Range:"
        Me._BoardInputRangesComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_OpenCloseCheckBox
        '
        Me._OpenCloseCheckBox.Appearance = System.Windows.Forms.Appearance.Button
        Me._OpenCloseCheckBox.Enabled = False
        Me._OpenCloseCheckBox.Location = New System.Drawing.Point(11, 64)
        Me._OpenCloseCheckBox.Name = "_OpenCloseCheckBox"
        Me._OpenCloseCheckBox.Size = New System.Drawing.Size(180, 28)
        Me._OpenCloseCheckBox.TabIndex = 2
        Me._OpenCloseCheckBox.Text = "&Open"
        Me._OpenCloseCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_DeviceNameChooser
        '
        Me._DeviceNameChooser.Location = New System.Drawing.Point(11, 27)
        Me._DeviceNameChooser.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._DeviceNameChooser.Name = "_DeviceNameChooser"
        Me._DeviceNameChooser.Size = New System.Drawing.Size(180, 28)
        Me._DeviceNameChooser.TabIndex = 1
        '
        '_DeviceNameLabel
        '
        Me._DeviceNameLabel.AutoSize = True
        Me._DeviceNameLabel.Location = New System.Drawing.Point(11, 9)
        Me._DeviceNameLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._DeviceNameLabel.Name = "_DeviceNameLabel"
        Me._DeviceNameLabel.Size = New System.Drawing.Size(88, 17)
        Me._DeviceNameLabel.TabIndex = 0
        Me._DeviceNameLabel.Text = "Device Name:"
        Me._DeviceNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_GlobalGroupBox
        '
        Me._GlobalGroupBox.Controls.Add(Me._ConfigureButton)
        Me._GlobalGroupBox.Controls.Add(Me._ResetButton)
        Me._GlobalGroupBox.Controls.Add(Me._RestoreDefaultsButton)
        Me._GlobalGroupBox.Location = New System.Drawing.Point(8, 250)
        Me._GlobalGroupBox.Name = "_GlobalGroupBox"
        Me._GlobalGroupBox.Size = New System.Drawing.Size(200, 124)
        Me._GlobalGroupBox.TabIndex = 0
        Me._GlobalGroupBox.TabStop = False
        Me._GlobalGroupBox.Text = "Global"
        '
        '_ConfigureButton
        '
        Me._ConfigureButton.Location = New System.Drawing.Point(10, 85)
        Me._ConfigureButton.Name = "_ConfigureButton"
        Me._ConfigureButton.Size = New System.Drawing.Size(180, 28)
        Me._ConfigureButton.TabIndex = 2
        Me._ConfigureButton.Text = "&Configure"
        Me._ToolTip.SetToolTip(Me._ConfigureButton, "Configure using current settings.")
        Me._ConfigureButton.UseVisualStyleBackColor = True
        '
        '_ResetButton
        '
        Me._ResetButton.Location = New System.Drawing.Point(11, 52)
        Me._ResetButton.Name = "_ResetButton"
        Me._ResetButton.Size = New System.Drawing.Size(180, 28)
        Me._ResetButton.TabIndex = 1
        Me._ResetButton.Text = "Re&set"
        Me._ToolTip.SetToolTip(Me._ResetButton, "Reset to Opened state conditions.")
        Me._ResetButton.UseVisualStyleBackColor = True
        '
        '_RestoreDefaultsButton
        '
        Me._RestoreDefaultsButton.Location = New System.Drawing.Point(11, 19)
        Me._RestoreDefaultsButton.Name = "_RestoreDefaultsButton"
        Me._RestoreDefaultsButton.Size = New System.Drawing.Size(180, 28)
        Me._RestoreDefaultsButton.TabIndex = 0
        Me._RestoreDefaultsButton.Text = "&Restore Defaults"
        Me._ToolTip.SetToolTip(Me._RestoreDefaultsButton, "Restore  to default settings")
        Me._RestoreDefaultsButton.UseVisualStyleBackColor = True
        '
        '_ChartTabPage
        '
        Me._ChartTabPage.Controls.Add(Me._ChartTableLayoutPanel)
        Me._ChartTabPage.Location = New System.Drawing.Point(4, 26)
        Me._ChartTabPage.Name = "_ChartTabPage"
        Me._ChartTabPage.Size = New System.Drawing.Size(216, 520)
        Me._ChartTabPage.TabIndex = 1
        Me._ChartTabPage.Text = "Chart"
        Me._ChartTabPage.UseVisualStyleBackColor = True
        '
        '_ChartTableLayoutPanel
        '
        Me._ChartTableLayoutPanel.ColumnCount = 3
        Me._ChartTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1.0!))
        Me._ChartTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ChartTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1.0!))
        Me._ChartTableLayoutPanel.Controls.Add(Me._ChartTypeTableLayoutPanel, 1, 2)
        Me._ChartTableLayoutPanel.Controls.Add(Me._ChartColorsTableLayoutPanel, 1, 3)
        Me._ChartTableLayoutPanel.Controls.Add(Me._ChartTitlesTableLayoutPanel, 1, 4)
        Me._ChartTableLayoutPanel.Controls.Add(Me._ChartEnableTableLayoutPanel, 1, 1)
        Me._ChartTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChartTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._ChartTableLayoutPanel.Name = "_ChartTableLayoutPanel"
        Me._ChartTableLayoutPanel.RowCount = 6
        Me._ChartTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTableLayoutPanel.Size = New System.Drawing.Size(216, 520)
        Me._ChartTableLayoutPanel.TabIndex = 0
        '
        '_ChartTypeTableLayoutPanel
        '
        Me._ChartTypeTableLayoutPanel.ColumnCount = 6
        Me._ChartTypeTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._ChartTypeTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._ChartTypeTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ChartTypeTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._ChartTypeTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ChartTypeTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._ChartTypeTableLayoutPanel.Controls.Add(Me._BandModeGroupBox, 4, 1)
        Me._ChartTypeTableLayoutPanel.Controls.Add(Me._ChartTypeGroupBox, 2, 1)
        Me._ChartTypeTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChartTypeTableLayoutPanel.Location = New System.Drawing.Point(4, 159)
        Me._ChartTypeTableLayoutPanel.Name = "_ChartTypeTableLayoutPanel"
        Me._ChartTypeTableLayoutPanel.RowCount = 3
        Me._ChartTypeTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTypeTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartTypeTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTypeTableLayoutPanel.Size = New System.Drawing.Size(208, 99)
        Me._ChartTypeTableLayoutPanel.TabIndex = 1
        '
        '_BandModeGroupBox
        '
        Me._BandModeGroupBox.Controls.Add(Me._MultipleRadioButton)
        Me._BandModeGroupBox.Controls.Add(Me._SingleRadioButton)
        Me._BandModeGroupBox.Location = New System.Drawing.Point(109, 10)
        Me._BandModeGroupBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._BandModeGroupBox.Name = "_BandModeGroupBox"
        Me._BandModeGroupBox.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._BandModeGroupBox.Size = New System.Drawing.Size(90, 79)
        Me._BandModeGroupBox.TabIndex = 1
        Me._BandModeGroupBox.TabStop = False
        Me._BandModeGroupBox.Text = "Band mode"
        '
        '_MultipleRadioButton
        '
        Me._MultipleRadioButton.Location = New System.Drawing.Point(8, 47)
        Me._MultipleRadioButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._MultipleRadioButton.Name = "_MultipleRadioButton"
        Me._MultipleRadioButton.Size = New System.Drawing.Size(76, 24)
        Me._MultipleRadioButton.TabIndex = 1
        Me._MultipleRadioButton.Text = "Multiple"
        '
        '_SingleRadioButton
        '
        Me._SingleRadioButton.Checked = True
        Me._SingleRadioButton.Location = New System.Drawing.Point(8, 19)
        Me._SingleRadioButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._SingleRadioButton.Name = "_SingleRadioButton"
        Me._SingleRadioButton.Size = New System.Drawing.Size(66, 24)
        Me._SingleRadioButton.TabIndex = 0
        Me._SingleRadioButton.TabStop = True
        Me._SingleRadioButton.Text = "Single"
        '
        '_ChartTypeGroupBox
        '
        Me._ChartTypeGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ChartTypeGroupBox.Controls.Add(Me._StripChartRadioButton)
        Me._ChartTypeGroupBox.Controls.Add(Me._ScopeRadioButton)
        Me._ChartTypeGroupBox.Location = New System.Drawing.Point(8, 10)
        Me._ChartTypeGroupBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartTypeGroupBox.Name = "_ChartTypeGroupBox"
        Me._ChartTypeGroupBox.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartTypeGroupBox.Size = New System.Drawing.Size(91, 79)
        Me._ChartTypeGroupBox.TabIndex = 0
        Me._ChartTypeGroupBox.TabStop = False
        Me._ChartTypeGroupBox.Text = "Chart Type"
        '
        '_StripChartRadioButton
        '
        Me._StripChartRadioButton.Location = New System.Drawing.Point(8, 47)
        Me._StripChartRadioButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._StripChartRadioButton.Name = "_StripChartRadioButton"
        Me._StripChartRadioButton.Size = New System.Drawing.Size(77, 24)
        Me._StripChartRadioButton.TabIndex = 1
        Me._StripChartRadioButton.Text = "Strip Chart"
        '
        '_ScopeRadioButton
        '
        Me._ScopeRadioButton.Checked = True
        Me._ScopeRadioButton.Location = New System.Drawing.Point(8, 19)
        Me._ScopeRadioButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ScopeRadioButton.Name = "_ScopeRadioButton"
        Me._ScopeRadioButton.Size = New System.Drawing.Size(67, 24)
        Me._ScopeRadioButton.TabIndex = 0
        Me._ScopeRadioButton.TabStop = True
        Me._ScopeRadioButton.Text = "Scope"
        '
        '_ChartColorsTableLayoutPanel
        '
        Me._ChartColorsTableLayoutPanel.ColumnCount = 3
        Me._ChartColorsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartColorsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ChartColorsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartColorsTableLayoutPanel.Controls.Add(Me._ChartColorsGroupBox, 1, 1)
        Me._ChartColorsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChartColorsTableLayoutPanel.Location = New System.Drawing.Point(4, 264)
        Me._ChartColorsTableLayoutPanel.Name = "_ChartColorsTableLayoutPanel"
        Me._ChartColorsTableLayoutPanel.RowCount = 3
        Me._ChartColorsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartColorsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartColorsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartColorsTableLayoutPanel.Size = New System.Drawing.Size(208, 115)
        Me._ChartColorsTableLayoutPanel.TabIndex = 2
        '
        '_ChartColorsGroupBox
        '
        Me._ChartColorsGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ChartColorsGroupBox.Controls.Add(Me._ColorGridsButton)
        Me._ChartColorsGroupBox.Controls.Add(Me._ColorAxesButton)
        Me._ChartColorsGroupBox.Location = New System.Drawing.Point(41, 11)
        Me._ChartColorsGroupBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartColorsGroupBox.Name = "_ChartColorsGroupBox"
        Me._ChartColorsGroupBox.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartColorsGroupBox.Size = New System.Drawing.Size(125, 92)
        Me._ChartColorsGroupBox.TabIndex = 0
        Me._ChartColorsGroupBox.TabStop = False
        Me._ChartColorsGroupBox.Text = "Chart Colors"
        '
        '_ColorGridsButton
        '
        Me._ColorGridsButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ColorGridsButton.Location = New System.Drawing.Point(16, 57)
        Me._ColorGridsButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ColorGridsButton.Name = "_ColorGridsButton"
        Me._ColorGridsButton.Size = New System.Drawing.Size(95, 23)
        Me._ColorGridsButton.TabIndex = 1
        Me._ColorGridsButton.Text = "Grids color"
        '
        '_ColorAxesButton
        '
        Me._ColorAxesButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ColorAxesButton.Location = New System.Drawing.Point(16, 25)
        Me._ColorAxesButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ColorAxesButton.Name = "_ColorAxesButton"
        Me._ColorAxesButton.Size = New System.Drawing.Size(95, 23)
        Me._ColorAxesButton.TabIndex = 0
        Me._ColorAxesButton.Text = "Axes color"
        '
        '_ChartTitlesTableLayoutPanel
        '
        Me._ChartTitlesTableLayoutPanel.ColumnCount = 3
        Me._ChartTitlesTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTitlesTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ChartTitlesTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTitlesTableLayoutPanel.Controls.Add(Me._ChartTitlesGroupBox, 1, 1)
        Me._ChartTitlesTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChartTitlesTableLayoutPanel.Location = New System.Drawing.Point(4, 385)
        Me._ChartTitlesTableLayoutPanel.Name = "_ChartTitlesTableLayoutPanel"
        Me._ChartTitlesTableLayoutPanel.RowCount = 3
        Me._ChartTitlesTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTitlesTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartTitlesTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartTitlesTableLayoutPanel.Size = New System.Drawing.Size(208, 121)
        Me._ChartTitlesTableLayoutPanel.TabIndex = 3
        '
        '_ChartTitlesGroupBox
        '
        Me._ChartTitlesGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ChartTitlesGroupBox.Controls.Add(Me._ChartFooterTextBox)
        Me._ChartTitlesGroupBox.Controls.Add(Me._ChartFooterTextBoxLabel)
        Me._ChartTitlesGroupBox.Controls.Add(Me._ChartTitleTextBox)
        Me._ChartTitlesGroupBox.Controls.Add(Me._ChartTitleTextBoxLabel)
        Me._ChartTitlesGroupBox.Location = New System.Drawing.Point(15, 5)
        Me._ChartTitlesGroupBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartTitlesGroupBox.Name = "_ChartTitlesGroupBox"
        Me._ChartTitlesGroupBox.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartTitlesGroupBox.Size = New System.Drawing.Size(177, 110)
        Me._ChartTitlesGroupBox.TabIndex = 0
        Me._ChartTitlesGroupBox.TabStop = False
        Me._ChartTitlesGroupBox.Text = "Titles"
        '
        '_ChartFooterTextBox
        '
        Me._ChartFooterTextBox.Location = New System.Drawing.Point(8, 80)
        Me._ChartFooterTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartFooterTextBox.Name = "_ChartFooterTextBox"
        Me._ChartFooterTextBox.Size = New System.Drawing.Size(163, 25)
        Me._ChartFooterTextBox.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._ChartFooterTextBox, "Chart Footer")
        '
        '_ChartFooterTextBoxLabel
        '
        Me._ChartFooterTextBoxLabel.AutoSize = True
        Me._ChartFooterTextBoxLabel.Location = New System.Drawing.Point(5, 61)
        Me._ChartFooterTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._ChartFooterTextBoxLabel.Name = "_ChartFooterTextBoxLabel"
        Me._ChartFooterTextBoxLabel.Size = New System.Drawing.Size(53, 17)
        Me._ChartFooterTextBoxLabel.TabIndex = 2
        Me._ChartFooterTextBoxLabel.Text = "Footer: "
        Me._ChartFooterTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_ChartTitleTextBox
        '
        Me._ChartTitleTextBox.Location = New System.Drawing.Point(8, 36)
        Me._ChartTitleTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartTitleTextBox.Name = "_ChartTitleTextBox"
        Me._ChartTitleTextBox.Size = New System.Drawing.Size(163, 25)
        Me._ChartTitleTextBox.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._ChartTitleTextBox, "Chart Title")
        '
        '_ChartTitleTextBoxLabel
        '
        Me._ChartTitleTextBoxLabel.AutoSize = True
        Me._ChartTitleTextBoxLabel.Location = New System.Drawing.Point(5, 17)
        Me._ChartTitleTextBoxLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._ChartTitleTextBoxLabel.Name = "_ChartTitleTextBoxLabel"
        Me._ChartTitleTextBoxLabel.Size = New System.Drawing.Size(39, 17)
        Me._ChartTitleTextBoxLabel.TabIndex = 0
        Me._ChartTitleTextBoxLabel.Text = "Title: "
        Me._ChartTitleTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_ChartEnableTableLayoutPanel
        '
        Me._ChartEnableTableLayoutPanel.ColumnCount = 3
        Me._ChartEnableTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartEnableTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ChartEnableTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartEnableTableLayoutPanel.Controls.Add(Me._ChartEnableGroupBox, 1, 1)
        Me._ChartEnableTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChartEnableTableLayoutPanel.Location = New System.Drawing.Point(4, 14)
        Me._ChartEnableTableLayoutPanel.Name = "_ChartEnableTableLayoutPanel"
        Me._ChartEnableTableLayoutPanel.RowCount = 3
        Me._ChartEnableTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartEnableTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChartEnableTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChartEnableTableLayoutPanel.Size = New System.Drawing.Size(208, 139)
        Me._ChartEnableTableLayoutPanel.TabIndex = 0
        '
        '_ChartEnableGroupBox
        '
        Me._ChartEnableGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ChartEnableGroupBox.Controls.Add(Me._RefreshRateNumeric)
        Me._ChartEnableGroupBox.Controls.Add(Me._RefreshRateNumericLabel)
        Me._ChartEnableGroupBox.Controls.Add(Me._DisplayAverageCheckBox)
        Me._ChartEnableGroupBox.Controls.Add(Me._DisplayVoltageCheckBox)
        Me._ChartEnableGroupBox.Controls.Add(Me._EnableChartingCheckBox)
        Me._ChartEnableGroupBox.Location = New System.Drawing.Point(23, 11)
        Me._ChartEnableGroupBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartEnableGroupBox.Name = "_ChartEnableGroupBox"
        Me._ChartEnableGroupBox.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChartEnableGroupBox.Size = New System.Drawing.Size(162, 117)
        Me._ChartEnableGroupBox.TabIndex = 0
        Me._ChartEnableGroupBox.TabStop = False
        Me._ChartEnableGroupBox.Text = "Chart"
        '
        '_RefreshRateNumeric
        '
        Me._RefreshRateNumeric.Location = New System.Drawing.Point(101, 87)
        Me._RefreshRateNumeric.Name = "_RefreshRateNumeric"
        Me._RefreshRateNumeric.Size = New System.Drawing.Size(51, 25)
        Me._RefreshRateNumeric.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._RefreshRateNumeric, "Display Refresh Rate Per Second")
        Me._RefreshRateNumeric.Value = New Decimal(New Integer() {24, 0, 0, 0})
        '
        '_RefreshRateNumericLabel
        '
        Me._RefreshRateNumericLabel.AutoSize = True
        Me._RefreshRateNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._RefreshRateNumericLabel.Location = New System.Drawing.Point(14, 91)
        Me._RefreshRateNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._RefreshRateNumericLabel.Name = "_RefreshRateNumericLabel"
        Me._RefreshRateNumericLabel.Size = New System.Drawing.Size(85, 17)
        Me._RefreshRateNumericLabel.TabIndex = 3
        Me._RefreshRateNumericLabel.Text = "Refresh Rate:"
        Me._RefreshRateNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_DisplayAverageCheckBox
        '
        Me._DisplayAverageCheckBox.AutoSize = True
        Me._DisplayAverageCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._DisplayAverageCheckBox.Location = New System.Drawing.Point(13, 64)
        Me._DisplayAverageCheckBox.Name = "_DisplayAverageCheckBox"
        Me._DisplayAverageCheckBox.Size = New System.Drawing.Size(121, 21)
        Me._DisplayAverageCheckBox.TabIndex = 2
        Me._DisplayAverageCheckBox.Text = "Display Average"
        Me._ToolTip.SetToolTip(Me._DisplayAverageCheckBox, "Check to display average voltage")
        Me._DisplayAverageCheckBox.UseVisualStyleBackColor = True
        '
        '_DisplayVoltageCheckBox
        '
        Me._DisplayVoltageCheckBox.AutoSize = True
        Me._DisplayVoltageCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._DisplayVoltageCheckBox.Location = New System.Drawing.Point(17, 41)
        Me._DisplayVoltageCheckBox.Name = "_DisplayVoltageCheckBox"
        Me._DisplayVoltageCheckBox.Size = New System.Drawing.Size(118, 21)
        Me._DisplayVoltageCheckBox.TabIndex = 1
        Me._DisplayVoltageCheckBox.Text = "Display Voltage"
        Me._ToolTip.SetToolTip(Me._DisplayVoltageCheckBox, "Check to display current voltage")
        Me._DisplayVoltageCheckBox.UseVisualStyleBackColor = True
        '
        '_EnableChartingCheckBox
        '
        Me._EnableChartingCheckBox.AutoSize = True
        Me._EnableChartingCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._EnableChartingCheckBox.Checked = True
        Me._EnableChartingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._EnableChartingCheckBox.Location = New System.Drawing.Point(15, 18)
        Me._EnableChartingCheckBox.Name = "_EnableChartingCheckBox"
        Me._EnableChartingCheckBox.Size = New System.Drawing.Size(119, 21)
        Me._EnableChartingCheckBox.TabIndex = 0
        Me._EnableChartingCheckBox.Text = "Enable Charting"
        Me._ToolTip.SetToolTip(Me._EnableChartingCheckBox, "Check to enable charting")
        Me._EnableChartingCheckBox.UseVisualStyleBackColor = True
        '
        '_channelsTabPage
        '
        Me._channelsTabPage.Controls.Add(Me._ChannelsTableLayoutPanel)
        Me._channelsTabPage.Location = New System.Drawing.Point(4, 26)
        Me._channelsTabPage.Name = "_channelsTabPage"
        Me._channelsTabPage.Size = New System.Drawing.Size(216, 520)
        Me._channelsTabPage.TabIndex = 3
        Me._channelsTabPage.Text = "Channels"
        Me._channelsTabPage.UseVisualStyleBackColor = True
        '
        '_ChannelsTableLayoutPanel
        '
        Me._ChannelsTableLayoutPanel.ColumnCount = 3
        Me._ChannelsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChannelsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ChannelsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChannelsTableLayoutPanel.Controls.Add(Me._ChannelsGroupBox, 1, 1)
        Me._ChannelsTableLayoutPanel.Controls.Add(Me._SamplingGroupBox, 1, 3)
        Me._ChannelsTableLayoutPanel.Controls.Add(Me._ProcessingGroupBox, 1, 5)
        Me._ChannelsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChannelsTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._ChannelsTableLayoutPanel.Name = "_ChannelsTableLayoutPanel"
        Me._ChannelsTableLayoutPanel.RowCount = 7
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ChannelsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ChannelsTableLayoutPanel.Size = New System.Drawing.Size(216, 520)
        Me._ChannelsTableLayoutPanel.TabIndex = 0
        '
        '_ChannelsGroupBox
        '
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelNameTextBox)
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelNameTextBoxLabel)
        Me._ChannelsGroupBox.Controls.Add(Me._AddChannelButton)
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelRangesComboBox)
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelRangeComboBoxLabel)
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelComboBox)
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelComboBoxLabel)
        Me._ChannelsGroupBox.Controls.Add(Me._RemoveChannelButton)
        Me._ChannelsGroupBox.Controls.Add(Me._ChannelsListView)
        Me._ChannelsGroupBox.Location = New System.Drawing.Point(8, 12)
        Me._ChannelsGroupBox.Name = "_ChannelsGroupBox"
        Me._ChannelsGroupBox.Size = New System.Drawing.Size(200, 229)
        Me._ChannelsGroupBox.TabIndex = 0
        Me._ChannelsGroupBox.TabStop = False
        Me._ChannelsGroupBox.Text = "Acquired Channels"
        Me._ToolTip.SetToolTip(Me._ChannelsGroupBox, "List of acquired channels.")
        '
        '_ChannelNameTextBox
        '
        Me._ChannelNameTextBox.Location = New System.Drawing.Point(63, 75)
        Me._ChannelNameTextBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ChannelNameTextBox.Name = "_ChannelNameTextBox"
        Me._ChannelNameTextBox.Size = New System.Drawing.Size(124, 25)
        Me._ChannelNameTextBox.TabIndex = 5
        Me._ToolTip.SetToolTip(Me._ChannelNameTextBox, "Channel Name")
        '
        '_ChannelNameTextBoxLabel
        '
        Me._ChannelNameTextBoxLabel.AutoSize = True
        Me._ChannelNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ChannelNameTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ChannelNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ChannelNameTextBoxLabel.Location = New System.Drawing.Point(15, 79)
        Me._ChannelNameTextBoxLabel.Name = "_ChannelNameTextBoxLabel"
        Me._ChannelNameTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelNameTextBoxLabel.Size = New System.Drawing.Size(46, 17)
        Me._ChannelNameTextBoxLabel.TabIndex = 4
        Me._ChannelNameTextBoxLabel.Text = "Name:"
        Me._ChannelNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_AddChannelButton
        '
        Me._AddChannelButton.BackColor = System.Drawing.SystemColors.Control
        Me._AddChannelButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._AddChannelButton.Enabled = False
        Me._AddChannelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AddChannelButton.Location = New System.Drawing.Point(14, 104)
        Me._AddChannelButton.Name = "_AddChannelButton"
        Me._AddChannelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AddChannelButton.Size = New System.Drawing.Size(70, 23)
        Me._AddChannelButton.TabIndex = 6
        Me._AddChannelButton.Text = "Add"
        Me._ToolTip.SetToolTip(Me._AddChannelButton, "Add channel as a signal")
        Me._AddChannelButton.UseVisualStyleBackColor = False
        '
        '_ChannelRangesComboBox
        '
        Me._ChannelRangesComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._ChannelRangesComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ChannelRangesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ChannelRangesComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ChannelRangesComboBox.Location = New System.Drawing.Point(63, 47)
        Me._ChannelRangesComboBox.Name = "_ChannelRangesComboBox"
        Me._ChannelRangesComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelRangesComboBox.Size = New System.Drawing.Size(87, 25)
        Me._ChannelRangesComboBox.TabIndex = 3
        '
        '_ChannelRangeComboBoxLabel
        '
        Me._ChannelRangeComboBoxLabel.AutoSize = True
        Me._ChannelRangeComboBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ChannelRangeComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ChannelRangeComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ChannelRangeComboBoxLabel.Location = New System.Drawing.Point(13, 51)
        Me._ChannelRangeComboBoxLabel.Name = "_ChannelRangeComboBoxLabel"
        Me._ChannelRangeComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelRangeComboBoxLabel.Size = New System.Drawing.Size(48, 17)
        Me._ChannelRangeComboBoxLabel.TabIndex = 2
        Me._ChannelRangeComboBoxLabel.Text = "Range:"
        Me._ChannelRangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ChannelComboBox
        '
        Me._ChannelComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._ChannelComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ChannelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ChannelComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ChannelComboBox.Location = New System.Drawing.Point(63, 19)
        Me._ChannelComboBox.Name = "_ChannelComboBox"
        Me._ChannelComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelComboBox.Size = New System.Drawing.Size(65, 25)
        Me._ChannelComboBox.TabIndex = 1
        '
        '_ChannelComboBoxLabel
        '
        Me._ChannelComboBoxLabel.AutoSize = True
        Me._ChannelComboBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ChannelComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ChannelComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ChannelComboBoxLabel.Location = New System.Drawing.Point(0, 23)
        Me._ChannelComboBoxLabel.Name = "_ChannelComboBoxLabel"
        Me._ChannelComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelComboBoxLabel.Size = New System.Drawing.Size(61, 17)
        Me._ChannelComboBoxLabel.TabIndex = 0
        Me._ChannelComboBoxLabel.Text = "Channel: "
        Me._ChannelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_RemoveChannelButton
        '
        Me._RemoveChannelButton.BackColor = System.Drawing.SystemColors.Control
        Me._RemoveChannelButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RemoveChannelButton.Enabled = False
        Me._RemoveChannelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RemoveChannelButton.Location = New System.Drawing.Point(116, 104)
        Me._RemoveChannelButton.Name = "_RemoveChannelButton"
        Me._RemoveChannelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RemoveChannelButton.Size = New System.Drawing.Size(70, 23)
        Me._RemoveChannelButton.TabIndex = 7
        Me._RemoveChannelButton.Text = "Remove"
        Me._ToolTip.SetToolTip(Me._RemoveChannelButton, "Remove this channel")
        Me._RemoveChannelButton.UseVisualStyleBackColor = False
        '
        '_ChannelsListView
        '
        Me._ChannelsListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me._ChannelChannelColumnHeader, Me._ChannelRangeColumnHeader})
        Me._ChannelsListView.GridLines = True
        Me._ChannelsListView.Location = New System.Drawing.Point(13, 130)
        Me._ChannelsListView.MultiSelect = False
        Me._ChannelsListView.Name = "_ChannelsListView"
        Me._ChannelsListView.Size = New System.Drawing.Size(174, 89)
        Me._ChannelsListView.TabIndex = 8
        Me._ChannelsListView.UseCompatibleStateImageBehavior = False
        '
        '_ChannelChannelColumnHeader
        '
        Me._ChannelChannelColumnHeader.Text = "Channel"
        '
        '_ChannelRangeColumnHeader
        '
        Me._ChannelRangeColumnHeader.Text = "Range"
        '
        '_SamplingGroupBox
        '
        Me._SamplingGroupBox.Controls.Add(Me._SampleSizeNumeric)
        Me._SamplingGroupBox.Controls.Add(Me._BuffersCountNumeric)
        Me._SamplingGroupBox.Controls.Add(Me._SamplingRateNumeric)
        Me._SamplingGroupBox.Controls.Add(Me._SmartBufferingCheckBox)
        Me._SamplingGroupBox.Controls.Add(Me._ContinuousBufferingCheckBox)
        Me._SamplingGroupBox.Controls.Add(Me._SamplingRateNumericLabel)
        Me._SamplingGroupBox.Controls.Add(Me._SampleSizeNumericLabel)
        Me._SamplingGroupBox.Controls.Add(Me._BuffersCountNumericLabel)
        Me._SamplingGroupBox.Location = New System.Drawing.Point(8, 255)
        Me._SamplingGroupBox.Name = "_SamplingGroupBox"
        Me._SamplingGroupBox.Size = New System.Drawing.Size(200, 157)
        Me._SamplingGroupBox.TabIndex = 1
        Me._SamplingGroupBox.TabStop = False
        Me._SamplingGroupBox.Text = "Sampling"
        '
        '_SampleSizeNumeric
        '
        Me._SampleSizeNumeric.Location = New System.Drawing.Point(130, 71)
        Me._SampleSizeNumeric.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me._SampleSizeNumeric.Name = "_SampleSizeNumeric"
        Me._SampleSizeNumeric.Size = New System.Drawing.Size(59, 25)
        Me._SampleSizeNumeric.TabIndex = 5
        Me._ToolTip.SetToolTip(Me._SampleSizeNumeric, "The number of samples per buffer. This is the number of samples processed each ti" & _
        "me a channel buffer is collected.")
        Me._SampleSizeNumeric.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        '_BuffersCountNumeric
        '
        Me._BuffersCountNumeric.Location = New System.Drawing.Point(130, 43)
        Me._BuffersCountNumeric.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._BuffersCountNumeric.Name = "_BuffersCountNumeric"
        Me._BuffersCountNumeric.Size = New System.Drawing.Size(59, 25)
        Me._BuffersCountNumeric.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._BuffersCountNumeric, "The number of signal buffers to queue")
        Me._BuffersCountNumeric.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        '_SamplingRateNumeric
        '
        Me._SamplingRateNumeric.Location = New System.Drawing.Point(130, 15)
        Me._SamplingRateNumeric.Maximum = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me._SamplingRateNumeric.Name = "_SamplingRateNumeric"
        Me._SamplingRateNumeric.Size = New System.Drawing.Size(59, 25)
        Me._SamplingRateNumeric.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._SamplingRateNumeric, "The number of samples to collect per second.")
        Me._SamplingRateNumeric.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        '_SmartBufferingCheckBox
        '
        Me._SmartBufferingCheckBox.AutoSize = True
        Me._SmartBufferingCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._SmartBufferingCheckBox.Checked = True
        Me._SmartBufferingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._SmartBufferingCheckBox.Location = New System.Drawing.Point(31, 129)
        Me._SmartBufferingCheckBox.Name = "_SmartBufferingCheckBox"
        Me._SmartBufferingCheckBox.Size = New System.Drawing.Size(124, 21)
        Me._SmartBufferingCheckBox.TabIndex = 7
        Me._SmartBufferingCheckBox.Text = "Smart Buffering: "
        Me._ToolTip.SetToolTip(Me._SmartBufferingCheckBox, "Allocates at least 256 samples for each channel buffer in Strip Chart or slow sam" & _
        "pling modes.")
        Me._SmartBufferingCheckBox.UseVisualStyleBackColor = True
        '
        '_ContinuousBufferingCheckBox
        '
        Me._ContinuousBufferingCheckBox.AutoSize = True
        Me._ContinuousBufferingCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContinuousBufferingCheckBox.Checked = True
        Me._ContinuousBufferingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ContinuousBufferingCheckBox.Location = New System.Drawing.Point(1, 103)
        Me._ContinuousBufferingCheckBox.Name = "_ContinuousBufferingCheckBox"
        Me._ContinuousBufferingCheckBox.Size = New System.Drawing.Size(155, 21)
        Me._ContinuousBufferingCheckBox.TabIndex = 6
        Me._ContinuousBufferingCheckBox.Text = "Continuous Buffering: "
        Me._ToolTip.SetToolTip(Me._ContinuousBufferingCheckBox, "Check to collect repeated buffering")
        Me._ContinuousBufferingCheckBox.UseVisualStyleBackColor = True
        '
        '_SamplingRateNumericLabel
        '
        Me._SamplingRateNumericLabel.AutoSize = True
        Me._SamplingRateNumericLabel.Location = New System.Drawing.Point(20, 19)
        Me._SamplingRateNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SamplingRateNumericLabel.Name = "_SamplingRateNumericLabel"
        Me._SamplingRateNumericLabel.Size = New System.Drawing.Size(108, 17)
        Me._SamplingRateNumericLabel.TabIndex = 0
        Me._SamplingRateNumericLabel.Text = "Samples/Second:"
        Me._SamplingRateNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_SampleSizeNumericLabel
        '
        Me._SampleSizeNumericLabel.AutoSize = True
        Me._SampleSizeNumericLabel.Location = New System.Drawing.Point(47, 75)
        Me._SampleSizeNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SampleSizeNumericLabel.Name = "_SampleSizeNumericLabel"
        Me._SampleSizeNumericLabel.Size = New System.Drawing.Size(81, 17)
        Me._SampleSizeNumericLabel.TabIndex = 4
        Me._SampleSizeNumericLabel.Text = "Sample Size:"
        Me._SampleSizeNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_BuffersCountNumericLabel
        '
        Me._BuffersCountNumericLabel.AutoSize = True
        Me._BuffersCountNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._BuffersCountNumericLabel.Location = New System.Drawing.Point(9, 47)
        Me._BuffersCountNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._BuffersCountNumericLabel.Name = "_BuffersCountNumericLabel"
        Me._BuffersCountNumericLabel.Size = New System.Drawing.Size(119, 17)
        Me._BuffersCountNumericLabel.TabIndex = 2
        Me._BuffersCountNumericLabel.Text = "Number of Buffers:"
        Me._BuffersCountNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ProcessingGroupBox
        '
        Me._ProcessingGroupBox.Controls.Add(Me._MovingAverageLengthNumeric)
        Me._ProcessingGroupBox.Controls.Add(Me._MovingAverageLengthNumericLabel)
        Me._ProcessingGroupBox.Controls.Add(Me._CalculateAverageCheckBox)
        Me._ProcessingGroupBox.Location = New System.Drawing.Point(8, 426)
        Me._ProcessingGroupBox.Name = "_ProcessingGroupBox"
        Me._ProcessingGroupBox.Size = New System.Drawing.Size(200, 82)
        Me._ProcessingGroupBox.TabIndex = 2
        Me._ProcessingGroupBox.TabStop = False
        Me._ProcessingGroupBox.Text = "Processing"
        '
        '_MovingAverageLengthNumeric
        '
        Me._MovingAverageLengthNumeric.Location = New System.Drawing.Point(137, 48)
        Me._MovingAverageLengthNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._MovingAverageLengthNumeric.Name = "_MovingAverageLengthNumeric"
        Me._MovingAverageLengthNumeric.Size = New System.Drawing.Size(48, 25)
        Me._MovingAverageLengthNumeric.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._MovingAverageLengthNumeric, "Number of samples in the moving average.")
        Me._MovingAverageLengthNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_MovingAverageLengthNumericLabel
        '
        Me._MovingAverageLengthNumericLabel.AutoSize = True
        Me._MovingAverageLengthNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._MovingAverageLengthNumericLabel.Location = New System.Drawing.Point(5, 52)
        Me._MovingAverageLengthNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._MovingAverageLengthNumericLabel.Name = "_MovingAverageLengthNumericLabel"
        Me._MovingAverageLengthNumericLabel.Size = New System.Drawing.Size(127, 17)
        Me._MovingAverageLengthNumericLabel.TabIndex = 1
        Me._MovingAverageLengthNumericLabel.Text = "Moving Avg. Length:"
        Me._MovingAverageLengthNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._ToolTip.SetToolTip(Me._MovingAverageLengthNumericLabel, "Number of samples in the moving average.")
        '
        '_CalculateAverageCheckBox
        '
        Me._CalculateAverageCheckBox.AutoSize = True
        Me._CalculateAverageCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._CalculateAverageCheckBox.Location = New System.Drawing.Point(16, 21)
        Me._CalculateAverageCheckBox.Name = "_CalculateAverageCheckBox"
        Me._CalculateAverageCheckBox.Size = New System.Drawing.Size(134, 21)
        Me._CalculateAverageCheckBox.TabIndex = 0
        Me._CalculateAverageCheckBox.Text = "Calculate Average:"
        Me._ToolTip.SetToolTip(Me._CalculateAverageCheckBox, "Check to calculate average voltage")
        Me._CalculateAverageCheckBox.UseVisualStyleBackColor = True
        '
        '_SignalsTabPage
        '
        Me._SignalsTabPage.Controls.Add(Me._SignalsTableLayoutPanel)
        Me._SignalsTabPage.Location = New System.Drawing.Point(4, 26)
        Me._SignalsTabPage.Name = "_SignalsTabPage"
        Me._SignalsTabPage.Size = New System.Drawing.Size(216, 520)
        Me._SignalsTabPage.TabIndex = 2
        Me._SignalsTabPage.Text = "Signals"
        Me._SignalsTabPage.UseVisualStyleBackColor = True
        '
        '_SignalsTableLayoutPanel
        '
        Me._SignalsTableLayoutPanel.ColumnCount = 3
        Me._SignalsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SignalsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._SignalsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SignalsTableLayoutPanel.Controls.Add(Me._SignalChannelGroupBox, 1, 1)
        Me._SignalsTableLayoutPanel.Controls.Add(Me._SignalSamplingGroupBox, 1, 5)
        Me._SignalsTableLayoutPanel.Controls.Add(Me._SignalsGroupBox, 1, 3)
        Me._SignalsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SignalsTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._SignalsTableLayoutPanel.Name = "_SignalsTableLayoutPanel"
        Me._SignalsTableLayoutPanel.RowCount = 7
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._SignalsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SignalsTableLayoutPanel.Size = New System.Drawing.Size(216, 520)
        Me._SignalsTableLayoutPanel.TabIndex = 0
        '
        '_SignalChannelGroupBox
        '
        Me._SignalChannelGroupBox.Controls.Add(Me._AddSignalButton)
        Me._SignalChannelGroupBox.Controls.Add(Me._SignalChannelComboBox)
        Me._SignalChannelGroupBox.Controls.Add(Me._SignalChannelComboBoxLabel)
        Me._SignalChannelGroupBox.Location = New System.Drawing.Point(8, 46)
        Me._SignalChannelGroupBox.Name = "_SignalChannelGroupBox"
        Me._SignalChannelGroupBox.Size = New System.Drawing.Size(199, 83)
        Me._SignalChannelGroupBox.TabIndex = 0
        Me._SignalChannelGroupBox.TabStop = False
        Me._SignalChannelGroupBox.Text = "Signal: "
        Me._ToolTip.SetToolTip(Me._SignalChannelGroupBox, "Select a channel to add as a signal")
        '
        '_AddSignalButton
        '
        Me._AddSignalButton.BackColor = System.Drawing.SystemColors.Control
        Me._AddSignalButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._AddSignalButton.Enabled = False
        Me._AddSignalButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AddSignalButton.Location = New System.Drawing.Point(110, 19)
        Me._AddSignalButton.Name = "_AddSignalButton"
        Me._AddSignalButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AddSignalButton.Size = New System.Drawing.Size(68, 23)
        Me._AddSignalButton.TabIndex = 0
        Me._AddSignalButton.Text = "Add"
        Me._ToolTip.SetToolTip(Me._AddSignalButton, "Add channel as a signal")
        Me._AddSignalButton.UseVisualStyleBackColor = False
        '
        '_SignalChannelComboBox
        '
        Me._SignalChannelComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalChannelComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalChannelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SignalChannelComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._SignalChannelComboBox.Location = New System.Drawing.Point(20, 47)
        Me._SignalChannelComboBox.Name = "_SignalChannelComboBox"
        Me._SignalChannelComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalChannelComboBox.Size = New System.Drawing.Size(156, 25)
        Me._SignalChannelComboBox.TabIndex = 2
        '
        '_SignalChannelComboBoxLabel
        '
        Me._SignalChannelComboBoxLabel.AutoSize = True
        Me._SignalChannelComboBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._SignalChannelComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalChannelComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalChannelComboBoxLabel.Location = New System.Drawing.Point(20, 28)
        Me._SignalChannelComboBoxLabel.Name = "_SignalChannelComboBoxLabel"
        Me._SignalChannelComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalChannelComboBoxLabel.Size = New System.Drawing.Size(61, 17)
        Me._SignalChannelComboBoxLabel.TabIndex = 1
        Me._SignalChannelComboBoxLabel.Text = "Channel: "
        Me._SignalChannelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_SignalSamplingGroupBox
        '
        Me._SignalSamplingGroupBox.Controls.Add(Me._SignalMemoryLengthNumeric)
        Me._SignalSamplingGroupBox.Controls.Add(Me._SignalBufferSizeNumeric)
        Me._SignalSamplingGroupBox.Controls.Add(Me._SignalMemoryLengthNumericLabel)
        Me._SignalSamplingGroupBox.Controls.Add(Me._SignalBufferSizeNumericLabel)
        Me._SignalSamplingGroupBox.Location = New System.Drawing.Point(8, 395)
        Me._SignalSamplingGroupBox.Name = "_SignalSamplingGroupBox"
        Me._SignalSamplingGroupBox.Size = New System.Drawing.Size(200, 78)
        Me._SignalSamplingGroupBox.TabIndex = 2
        Me._SignalSamplingGroupBox.TabStop = False
        Me._SignalSamplingGroupBox.Text = "Sampling"
        '
        '_SignalMemoryLengthNumeric
        '
        Me._SignalMemoryLengthNumeric.Location = New System.Drawing.Point(114, 48)
        Me._SignalMemoryLengthNumeric.Maximum = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me._SignalMemoryLengthNumeric.Name = "_SignalMemoryLengthNumeric"
        Me._SignalMemoryLengthNumeric.Size = New System.Drawing.Size(69, 25)
        Me._SignalMemoryLengthNumeric.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._SignalMemoryLengthNumeric, "Number of elements to store for the entire strip chart.")
        '
        '_SignalBufferSizeNumeric
        '
        Me._SignalBufferSizeNumeric.Location = New System.Drawing.Point(114, 16)
        Me._SignalBufferSizeNumeric.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me._SignalBufferSizeNumeric.Name = "_SignalBufferSizeNumeric"
        Me._SignalBufferSizeNumeric.Size = New System.Drawing.Size(69, 25)
        Me._SignalBufferSizeNumeric.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._SignalBufferSizeNumeric, "Number of elements to display for each signal.")
        '
        '_SignalMemoryLengthNumericLabel
        '
        Me._SignalMemoryLengthNumericLabel.AutoSize = True
        Me._SignalMemoryLengthNumericLabel.Location = New System.Drawing.Point(9, 52)
        Me._SignalMemoryLengthNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SignalMemoryLengthNumericLabel.Name = "_SignalMemoryLengthNumericLabel"
        Me._SignalMemoryLengthNumericLabel.Size = New System.Drawing.Size(103, 17)
        Me._SignalMemoryLengthNumericLabel.TabIndex = 2
        Me._SignalMemoryLengthNumericLabel.Text = "Memory Length:"
        Me._SignalMemoryLengthNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._ToolTip.SetToolTip(Me._SignalMemoryLengthNumericLabel, "The signal memory length")
        '
        '_SignalBufferSizeNumericLabel
        '
        Me._SignalBufferSizeNumericLabel.AutoSize = True
        Me._SignalBufferSizeNumericLabel.Location = New System.Drawing.Point(40, 20)
        Me._SignalBufferSizeNumericLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me._SignalBufferSizeNumericLabel.Name = "_SignalBufferSizeNumericLabel"
        Me._SignalBufferSizeNumericLabel.Size = New System.Drawing.Size(72, 17)
        Me._SignalBufferSizeNumericLabel.TabIndex = 0
        Me._SignalBufferSizeNumericLabel.Text = "Buffer Size:"
        Me._SignalBufferSizeNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._ToolTip.SetToolTip(Me._SignalBufferSizeNumericLabel, "The signal buffer size.")
        '
        '_SignalsGroupBox
        '
        Me._SignalsGroupBox.Controls.Add(Me._UpdateSignalButton)
        Me._SignalsGroupBox.Controls.Add(Me._SignalMinNumericUpDownLabel)
        Me._SignalsGroupBox.Controls.Add(Me._SignalMaxNumericUpDownLabel)
        Me._SignalsGroupBox.Controls.Add(Me._SignalMinNumericUpDown)
        Me._SignalsGroupBox.Controls.Add(Me._SignalMaxNumericUpDown)
        Me._SignalsGroupBox.Controls.Add(Me._ColorSignalButton)
        Me._SignalsGroupBox.Controls.Add(Me._RemoveSignalButton)
        Me._SignalsGroupBox.Controls.Add(Me._SignalsListView)
        Me._SignalsGroupBox.Location = New System.Drawing.Point(8, 143)
        Me._SignalsGroupBox.Name = "_SignalsGroupBox"
        Me._SignalsGroupBox.Size = New System.Drawing.Size(200, 238)
        Me._SignalsGroupBox.TabIndex = 1
        Me._SignalsGroupBox.TabStop = False
        Me._SignalsGroupBox.Text = "Charted Signals"
        '
        '_UpdateSignalButton
        '
        Me._UpdateSignalButton.BackColor = System.Drawing.SystemColors.Control
        Me._UpdateSignalButton.Location = New System.Drawing.Point(31, 198)
        Me._UpdateSignalButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._UpdateSignalButton.Name = "_UpdateSignalButton"
        Me._UpdateSignalButton.Size = New System.Drawing.Size(138, 27)
        Me._UpdateSignalButton.TabIndex = 7
        Me._UpdateSignalButton.Text = "Update"
        Me._ToolTip.SetToolTip(Me._UpdateSignalButton, "Update the Signal Range")
        Me._UpdateSignalButton.UseVisualStyleBackColor = False
        '
        '_SignalMinNumericUpDownLabel
        '
        Me._SignalMinNumericUpDownLabel.AutoSize = True
        Me._SignalMinNumericUpDownLabel.BackColor = System.Drawing.Color.Transparent
        Me._SignalMinNumericUpDownLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalMinNumericUpDownLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalMinNumericUpDownLabel.Location = New System.Drawing.Point(31, 152)
        Me._SignalMinNumericUpDownLabel.Name = "_SignalMinNumericUpDownLabel"
        Me._SignalMinNumericUpDownLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalMinNumericUpDownLabel.Size = New System.Drawing.Size(69, 17)
        Me._SignalMinNumericUpDownLabel.TabIndex = 3
        Me._SignalMinNumericUpDownLabel.Text = "Minimum: "
        Me._SignalMinNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_SignalMaxNumericUpDownLabel
        '
        Me._SignalMaxNumericUpDownLabel.AutoSize = True
        Me._SignalMaxNumericUpDownLabel.BackColor = System.Drawing.Color.Transparent
        Me._SignalMaxNumericUpDownLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalMaxNumericUpDownLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalMaxNumericUpDownLabel.Location = New System.Drawing.Point(106, 152)
        Me._SignalMaxNumericUpDownLabel.Name = "_SignalMaxNumericUpDownLabel"
        Me._SignalMaxNumericUpDownLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalMaxNumericUpDownLabel.Size = New System.Drawing.Size(72, 17)
        Me._SignalMaxNumericUpDownLabel.TabIndex = 4
        Me._SignalMaxNumericUpDownLabel.Text = "Maximum: "
        Me._SignalMaxNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_SignalMinNumericUpDown
        '
        Me._SignalMinNumericUpDown.DecimalPlaces = 2
        Me._SignalMinNumericUpDown.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._SignalMinNumericUpDown.Location = New System.Drawing.Point(31, 170)
        Me._SignalMinNumericUpDown.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._SignalMinNumericUpDown.Minimum = New Decimal(New Integer() {10, 0, 0, -2147483648})
        Me._SignalMinNumericUpDown.Name = "_SignalMinNumericUpDown"
        Me._SignalMinNumericUpDown.Size = New System.Drawing.Size(63, 25)
        Me._SignalMinNumericUpDown.TabIndex = 5
        Me._SignalMinNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_SignalMaxNumericUpDown
        '
        Me._SignalMaxNumericUpDown.DecimalPlaces = 2
        Me._SignalMaxNumericUpDown.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._SignalMaxNumericUpDown.Location = New System.Drawing.Point(107, 170)
        Me._SignalMaxNumericUpDown.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._SignalMaxNumericUpDown.Minimum = New Decimal(New Integer() {10, 0, 0, -2147483648})
        Me._SignalMaxNumericUpDown.Name = "_SignalMaxNumericUpDown"
        Me._SignalMaxNumericUpDown.Size = New System.Drawing.Size(63, 25)
        Me._SignalMaxNumericUpDown.TabIndex = 6
        Me._SignalMaxNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_ColorSignalButton
        '
        Me._ColorSignalButton.BackColor = System.Drawing.SystemColors.Control
        Me._ColorSignalButton.Location = New System.Drawing.Point(107, 126)
        Me._ColorSignalButton.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me._ColorSignalButton.Name = "_ColorSignalButton"
        Me._ColorSignalButton.Size = New System.Drawing.Size(63, 23)
        Me._ColorSignalButton.TabIndex = 2
        Me._ColorSignalButton.Text = "Color"
        Me._ToolTip.SetToolTip(Me._ColorSignalButton, "Select a new color for this signal")
        Me._ColorSignalButton.UseVisualStyleBackColor = False
        '
        '_RemoveSignalButton
        '
        Me._RemoveSignalButton.BackColor = System.Drawing.SystemColors.Control
        Me._RemoveSignalButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RemoveSignalButton.Enabled = False
        Me._RemoveSignalButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RemoveSignalButton.Location = New System.Drawing.Point(31, 126)
        Me._RemoveSignalButton.Name = "_RemoveSignalButton"
        Me._RemoveSignalButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RemoveSignalButton.Size = New System.Drawing.Size(63, 23)
        Me._RemoveSignalButton.TabIndex = 1
        Me._RemoveSignalButton.Text = "Remove"
        Me._ToolTip.SetToolTip(Me._RemoveSignalButton, "Remove this signal")
        Me._RemoveSignalButton.UseVisualStyleBackColor = False
        '
        '_SignalsListView
        '
        Me._SignalsListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me._SignalChannelColumnHeader, Me._SignalRangeColumnHeader})
        Me._SignalsListView.GridLines = True
        Me._SignalsListView.Location = New System.Drawing.Point(19, 19)
        Me._SignalsListView.MultiSelect = False
        Me._SignalsListView.Name = "_SignalsListView"
        Me._SignalsListView.Size = New System.Drawing.Size(162, 103)
        Me._SignalsListView.TabIndex = 0
        Me._SignalsListView.UseCompatibleStateImageBehavior = False
        '
        '_SignalChannelColumnHeader
        '
        Me._SignalChannelColumnHeader.Text = "Channel"
        '
        '_SignalRangeColumnHeader
        '
        Me._SignalRangeColumnHeader.Text = "Range"
        '
        '_Chart
        '
        Me._Chart.AutoScale = False
        Me._Chart.AxesColor = System.Drawing.Color.Blue
        Me._Chart.BackGradientAngle = 0.0!
        Me._Chart.BackGradientColor = System.Drawing.SystemColors.Control
        Me._Chart.BackgroundStyle = OpenLayers.Chart.Layers.BackgroundStyle.Color
        Me._Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand
        Me._Chart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._Chart.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Chart.Footer = "Footer"
        Me._Chart.FooterFont = New System.Drawing.Font(Me.Font.FontFamily, 8.0!)
        Me._Chart.ForeColor = System.Drawing.Color.Blue
        Me._Chart.GridColor = System.Drawing.Color.Blue
        Me._Chart.Location = New System.Drawing.Point(3, 3)
        Me._Chart.Name = "_Chart"
        Me._Chart.SignalBufferLength = CType(0, Long)
        Me._Chart.Size = New System.Drawing.Size(641, 520)
        Me._Chart.TabIndex = 0
        Me._Chart.Title = "Analog Input Voltage Time Series"
        Me._Chart.TitleFont = New System.Drawing.Font(Me.Font.FontFamily, 8.25!, System.Drawing.FontStyle.Bold)
        Me._Chart.XDataCurrentRangeMax = 1000.0R
        Me._Chart.XDataCurrentRangeMin = 0.0R
        Me._Chart.XDataName = "Time"
        Me._Chart.XDataRangeMax = 1000.0R
        Me._Chart.XDataRangeMin = 0.0R
        Me._Chart.XDataUnit = "sec"
        '
        '_errorProvider
        '
        Me._errorProvider.ContainerControl = Me
        '
        '_MainTableLayoutPanel
        '
        Me._MainTableLayoutPanel.ColumnCount = 2
        Me._MainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._MainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._MainTableLayoutPanel.Controls.Add(Me._Tabs, 0, 0)
        Me._MainTableLayoutPanel.Controls.Add(Me._DisplayTableLayoutPanel, 1, 0)
        Me._MainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MainTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._MainTableLayoutPanel.Name = "_MainTableLayoutPanel"
        Me._MainTableLayoutPanel.RowCount = 1
        Me._MainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._MainTableLayoutPanel.Size = New System.Drawing.Size(887, 556)
        Me._MainTableLayoutPanel.TabIndex = 1
        '
        '_DisplayTableLayoutPanel
        '
        Me._DisplayTableLayoutPanel.ColumnCount = 1
        Me._DisplayTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._DisplayTableLayoutPanel.Controls.Add(Me._Chart, 0, 0)
        Me._DisplayTableLayoutPanel.Controls.Add(Me._StatusStrip, 0, 1)
        Me._DisplayTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._DisplayTableLayoutPanel.Location = New System.Drawing.Point(233, 3)
        Me._DisplayTableLayoutPanel.Name = "_DisplayTableLayoutPanel"
        Me._DisplayTableLayoutPanel.RowCount = 2
        Me._DisplayTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.46756!))
        Me._DisplayTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._DisplayTableLayoutPanel.Size = New System.Drawing.Size(651, 550)
        Me._DisplayTableLayoutPanel.TabIndex = 3
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ChartToolStripStatusLabel, Me._CurrentVoltageToolStripLabel, Me._AverageVoltageToolStripLabel, Me._ActionsToolStripDropDownButton})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 526)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.ShowItemToolTips = True
        Me._StatusStrip.Size = New System.Drawing.Size(647, 24)
        Me._StatusStrip.TabIndex = 3
        Me._StatusStrip.Text = "<ready>"
        '
        '_ChartToolStripStatusLabel
        '
        Me._ChartToolStripStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._ChartToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me._ChartToolStripStatusLabel.Name = "_ChartToolStripStatusLabel"
        Me._ChartToolStripStatusLabel.Size = New System.Drawing.Size(456, 19)
        Me._ChartToolStripStatusLabel.Spring = True
        Me._ChartToolStripStatusLabel.Text = "Status: N/A"
        Me._ChartToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me._ChartToolStripStatusLabel.ToolTipText = "Status"
        '
        '_CurrentVoltageToolStripLabel
        '
        Me._CurrentVoltageToolStripLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._CurrentVoltageToolStripLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me._CurrentVoltageToolStripLabel.Name = "_CurrentVoltageToolStripLabel"
        Me._CurrentVoltageToolStripLabel.Size = New System.Drawing.Size(51, 19)
        Me._CurrentVoltageToolStripLabel.Text = "Voltage"
        Me._CurrentVoltageToolStripLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._CurrentVoltageToolStripLabel.ToolTipText = " Voltage"
        '
        '_AverageVoltageToolStripLabel
        '
        Me._AverageVoltageToolStripLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._AverageVoltageToolStripLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me._AverageVoltageToolStripLabel.DoubleClickEnabled = True
        Me._AverageVoltageToolStripLabel.Name = "_AverageVoltageToolStripLabel"
        Me._AverageVoltageToolStripLabel.Size = New System.Drawing.Size(54, 19)
        Me._AverageVoltageToolStripLabel.Text = "Average"
        Me._AverageVoltageToolStripLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._AverageVoltageToolStripLabel.ToolTipText = "Mean"
        '
        '_ActionsToolStripDropDownButton
        '
        Me._ActionsToolStripDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StartStopToolStripMenuItem, Me._AbortToolStripMenuItem, Me._ConfigureToolStripMenuItem})
        Me._ActionsToolStripDropDownButton.Image = CType(resources.GetObject("_ActionsToolStripDropDownButton.Image"), System.Drawing.Image)
        Me._ActionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ActionsToolStripDropDownButton.Name = "_ActionsToolStripDropDownButton"
        Me._ActionsToolStripDropDownButton.Size = New System.Drawing.Size(71, 22)
        Me._ActionsToolStripDropDownButton.Text = "Action"
        Me._ActionsToolStripDropDownButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_StartStopToolStripMenuItem
        '
        Me._StartStopToolStripMenuItem.Name = "_StartStopToolStripMenuItem"
        Me._StartStopToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._StartStopToolStripMenuItem.Text = "START"
        Me._StartStopToolStripMenuItem.ToolTipText = "Starts data collection and chart (if enabled)"
        '
        '_AbortToolStripMenuItem
        '
        Me._AbortToolStripMenuItem.Name = "_AbortToolStripMenuItem"
        Me._AbortToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._AbortToolStripMenuItem.Text = "Abort"
        '
        '_ConfigureToolStripMenuItem
        '
        Me._ConfigureToolStripMenuItem.Checked = True
        Me._ConfigureToolStripMenuItem.CheckOnClick = True
        Me._ConfigureToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ConfigureToolStripMenuItem.Name = "_ConfigureToolStripMenuItem"
        Me._ConfigureToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ConfigureToolStripMenuItem.Text = "Configure"
        '
        '_InfoProvider
        '
        Me._InfoProvider.ContainerControl = Me
        Me._InfoProvider.Icon = CType(resources.GetObject("_InfoProvider.Icon"), System.Drawing.Icon)
        '
        'AnalogInputDisplay
        '
        Me.Controls.Add(Me._MainTableLayoutPanel)
        Me.Name = "AnalogInputDisplay"
        Me.Size = New System.Drawing.Size(887, 556)
        Me._Tabs.ResumeLayout(False)
        Me._BoardTabPage.ResumeLayout(False)
        Me._BoardTableLayoutPanel.ResumeLayout(False)
        Me._BoardPanel.ResumeLayout(False)
        Me._BoardPanel.PerformLayout()
        Me._BoardGroupBox.ResumeLayout(False)
        Me._BoardGroupBox.PerformLayout()
        Me._GlobalGroupBox.ResumeLayout(False)
        Me._ChartTabPage.ResumeLayout(False)
        Me._ChartTableLayoutPanel.ResumeLayout(False)
        Me._ChartTypeTableLayoutPanel.ResumeLayout(False)
        Me._BandModeGroupBox.ResumeLayout(False)
        Me._ChartTypeGroupBox.ResumeLayout(False)
        Me._ChartColorsTableLayoutPanel.ResumeLayout(False)
        Me._ChartColorsGroupBox.ResumeLayout(False)
        Me._ChartTitlesTableLayoutPanel.ResumeLayout(False)
        Me._ChartTitlesGroupBox.ResumeLayout(False)
        Me._ChartTitlesGroupBox.PerformLayout()
        Me._ChartEnableTableLayoutPanel.ResumeLayout(False)
        Me._ChartEnableGroupBox.ResumeLayout(False)
        Me._ChartEnableGroupBox.PerformLayout()
        CType(Me._RefreshRateNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._channelsTabPage.ResumeLayout(False)
        Me._ChannelsTableLayoutPanel.ResumeLayout(False)
        Me._ChannelsGroupBox.ResumeLayout(False)
        Me._ChannelsGroupBox.PerformLayout()
        Me._SamplingGroupBox.ResumeLayout(False)
        Me._SamplingGroupBox.PerformLayout()
        CType(Me._SampleSizeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._BuffersCountNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SamplingRateNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ProcessingGroupBox.ResumeLayout(False)
        Me._ProcessingGroupBox.PerformLayout()
        CType(Me._MovingAverageLengthNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._SignalsTabPage.ResumeLayout(False)
        Me._SignalsTableLayoutPanel.ResumeLayout(False)
        Me._SignalChannelGroupBox.ResumeLayout(False)
        Me._SignalChannelGroupBox.PerformLayout()
        Me._SignalSamplingGroupBox.ResumeLayout(False)
        Me._SignalSamplingGroupBox.PerformLayout()
        CType(Me._SignalMemoryLengthNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SignalBufferSizeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._SignalsGroupBox.ResumeLayout(False)
        Me._SignalsGroupBox.PerformLayout()
        CType(Me._SignalMinNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SignalMaxNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Chart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._errorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._MainTableLayoutPanel.ResumeLayout(False)
        Me._DisplayTableLayoutPanel.ResumeLayout(False)
        Me._DisplayTableLayoutPanel.PerformLayout()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        CType(Me._InfoProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Chart As OpenLayers.Controls.Display
    Private WithEvents _Tabs As System.Windows.Forms.TabControl
    Private WithEvents _BoardTabPage As System.Windows.Forms.TabPage
    Private WithEvents _ChartTabPage As System.Windows.Forms.TabPage
    Private WithEvents _SignalsTabPage As System.Windows.Forms.TabPage
    Private WithEvents _BoardTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _DeviceNameChooser As isr.IO.OL.DeviceChooser.DeviceNameChooser
    Private WithEvents _OpenCloseCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ChartTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _DeviceNameLabel As System.Windows.Forms.Label
    Private WithEvents _SignalsTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _SignalChannelGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _SignalChannelComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _SignalChannelComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SignalsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _AddSignalButton As System.Windows.Forms.Button
    Private WithEvents _RemoveSignalButton As System.Windows.Forms.Button
    Private WithEvents _SignalsListView As System.Windows.Forms.ListView
    Private WithEvents _SignalChannelColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents _SignalRangeColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents _ColorSignalButton As System.Windows.Forms.Button
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _SignalSamplingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _SignalBufferSizeNumericLabel As System.Windows.Forms.Label
    Private WithEvents _GlobalGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _RestoreDefaultsButton As System.Windows.Forms.Button
    Private WithEvents _BandModeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _MultipleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _SingleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _ChartTypeTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ChartTypeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _StripChartRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _ScopeRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _ChartColorsTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ChartColorsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _ColorGridsButton As System.Windows.Forms.Button
    Private WithEvents _ColorAxesButton As System.Windows.Forms.Button
    Private WithEvents _SignalMaxNumericUpDownLabel As System.Windows.Forms.Label
    Private WithEvents _SignalMaxNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents _SignalMinNumericUpDownLabel As System.Windows.Forms.Label
    Private WithEvents _SignalMinNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ChannelsTabPage As System.Windows.Forms.TabPage
    Private WithEvents _ChannelsTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _SamplingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _SamplingRateNumericLabel As System.Windows.Forms.Label
    Private WithEvents _SampleSizeNumericLabel As System.Windows.Forms.Label
    Private WithEvents _BuffersCountNumericLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _RemoveChannelButton As System.Windows.Forms.Button
    Private WithEvents _ChannelsListView As System.Windows.Forms.ListView
    Private WithEvents _ChannelChannelColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents _ChannelRangeColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents _MainTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _DisplayTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _ChartToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _CurrentVoltageToolStripLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _AverageVoltageToolStripLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ActionsToolStripDropDownButton As System.Windows.Forms.ToolStripDropDownButton
    Private WithEvents _StartStopToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _AbortToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ConfigureToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _BoardPanel As System.Windows.Forms.Panel
    Private WithEvents _ChartTitlesTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ChartTitlesGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _ChartTitleTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChartTitleTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChartFooterTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChartFooterTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _UpdateSignalButton As System.Windows.Forms.Button
    Private WithEvents _SignalMemoryLengthNumericLabel As System.Windows.Forms.Label
    Private WithEvents _ChartEnableTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ChartEnableGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _EnableChartingCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _DisplayAverageCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _DisplayVoltageCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _BoardGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _BoardInputRangesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _BoardInputRangesComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChannelNameTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _AddChannelButton As System.Windows.Forms.Button
    Private WithEvents _ChannelRangesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ChannelRangeComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ChannelComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ProcessingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _MovingAverageLengthNumericLabel As System.Windows.Forms.Label
    Private WithEvents _CalculateAverageCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ConfigureButton As System.Windows.Forms.Button
    Private WithEvents _ResetButton As System.Windows.Forms.Button
    Private WithEvents _ContinuousBufferingCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SmartBufferingCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _InfoProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _RefreshRateNumericLabel As System.Windows.Forms.Label
    Private WithEvents _SamplingRateNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _SampleSizeNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _BuffersCountNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _MovingAverageLengthNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _SignalMemoryLengthNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _SignalBufferSizeNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _RefreshRateNumeric As System.Windows.Forms.NumericUpDown

End Class

