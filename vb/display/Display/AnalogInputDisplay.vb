Imports System.ComponentModel
Imports System.Drawing
Imports isr.Core
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.IO.OL.Display.ExceptionExtensions
Imports isr.Core.WinForms.CheckBoxExtensions
Imports isr.Core.WinForms.RadioButtonExtensions
Imports isr.Core.WinForms.ToolStripExtensions
Imports isr.IO.OL
''' <summary> The new chart control based on the Open Layers
''' <see cref="OpenLayers.Controls.Display">display</see>
''' class. </summary>
''' <remarks> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
<Description("Analog Input Display Control"), ToolboxBitmap(GetType(isr.IO.OL.Display.AnalogInputDisplay), "AnalogInputDisplay.bmp")>
Public Class AnalogInputDisplay
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="AnalogInputDisplay" /> class. </summary>
    Public Sub New()

        MyBase.New()

        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._StopTimeout = TimeSpan.FromMilliseconds(1000)
        Me.ConfigureSuspensionQueue = New Generic.Queue(Of Boolean)
        Me._BroadcastLevel = TraceEventType.Warning
        Me._RefreshRateNumeric.MaximumSetter(100)
        Me._RefreshRateNumeric.MinimumSetter(1)
        Me._BuffersCountNumeric.MaximumSetter(1000)
        Me._BuffersCountNumeric.MinimumSetter(2)
        Me._MovingAverageLengthNumeric.MinimumSetter(2)
        Me._SampleSizeNumeric.MinimumSetter(2)
        Me._SampleSizeNumeric.ValueSetter(1000)
        Me._SignalBufferSizeNumeric.MinimumSetter(2)
        Me.UsbBufferSize = 256
        Me._VoltsReadingsStringFormat = "{0:0.000 V}"
        Me._VoltsReadingFormat = "0.000"
        Me._CalculatePeriodCount = 10
        Me._UpdatePeriodCount = 10
        Me._RefreshStopwatch = New Stopwatch

		' allow single selections
		Me._SignalsListView.MultiSelect = False

		' Set the view to show details.
		Me._SignalsListView.View = View.Details

		' Do not Allow the user to edit item text.
		Me._SignalsListView.LabelEdit = False

		' Do not Allow the user to rearrange columns.
		Me._SignalsListView.AllowColumnReorder = False

		' Do Not Display check boxes.
		Me._SignalsListView.CheckBoxes = False

		' Select the item and sub items when selection is made.
		Me._SignalsListView.FullRowSelect = True

		' Display grid lines.
		Me._SignalsListView.GridLines = True

		' Do not Sort the items in the list in ascending order.
		Me._SignalsListView.Sorting = SortOrder.None

		' Create columns for the items and sub items.
		Me._SignalsListView.Columns.Clear()
		Me._SignalsListView.Columns.Add("#", -2, HorizontalAlignment.Left)
		Me._SignalsListView.Columns.Add("Name", -2, HorizontalAlignment.Left)
		Me._SignalsListView.Columns.Add("Range", -2, HorizontalAlignment.Left)
		Me._SignalsListView.Invalidate()

		' allow single selections
		Me._ChannelsListView.MultiSelect = False

		' Set the view to show details.
		Me._ChannelsListView.View = View.Details

		' Do not Allow the user to edit item text.
		Me._ChannelsListView.LabelEdit = False

		' Do not Allow the user to rearrange columns.
		Me._ChannelsListView.AllowColumnReorder = False

		' Do Not Display check boxes.
		Me._ChannelsListView.CheckBoxes = False

		' Select the item and sub items when selection is made.
		Me._ChannelsListView.FullRowSelect = True

		' Display grid lines.
		Me._ChannelsListView.GridLines = True

		' Do not Sort the items in the list in ascending order.
		Me._ChannelsListView.Sorting = SortOrder.None

		' Create columns for the items and sub items.
		Me._ChannelsListView.Columns.Clear()
		Me._ChannelsListView.Columns.Add("#", -2, HorizontalAlignment.Left)
		Me._ChannelsListView.Columns.Add("Name", -2, HorizontalAlignment.Left)
		Me._ChannelsListView.Columns.Add("Range", -2, HorizontalAlignment.Left)
		Me._ChannelsListView.Invalidate()

        ' enable selecting a device.
        Me._DeviceNameChooser.Enabled = Not Me.IsOpen

        '    Me.BuffersCount = Me.BuffersCount

        ' disable controls.
        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

    End Sub

    ''' <summary> Creates a new AnalogInputDisplay. </summary>
    ''' <returns> An AnalogInputDisplay. </returns>
    Public Shared Function Create() As AnalogInputDisplay
        Dim result As AnalogInputDisplay = Nothing
        Try
            result = New AnalogInputDisplay
        Catch
            If result IsNot Nothing Then result.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Cleans up managed components. </summary>
    ''' <remarks> Use this method to reclaim managed resources used by this class. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnDisposeManagedResources()

        If Me.AcquisitionStartedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.AcquisitionStartedEvent.GetInvocationList
                RemoveHandler Me.AcquisitionStarted, CType(d, Global.System.EventHandler(Of TraceMessageEventArgs))
            Next
        End If

        If Me.AcquisitionStoppedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.AcquisitionStoppedEvent.GetInvocationList
                RemoveHandler Me.AcquisitionStopped, CType(d, Global.System.EventHandler(Of TraceMessageEventArgs))
            Next
        End If

        If Me.BoardsLocatedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.BoardsLocatedEvent.GetInvocationList
                RemoveHandler Me.BoardsLocated, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.BufferDoneEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.BufferDoneEvent.GetInvocationList
                RemoveHandler Me.BufferDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.DisconnectedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.DisconnectedEvent.GetInvocationList
                RemoveHandler Me.Disconnected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.UpdatePeriodDoneEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.UpdatePeriodDoneEvent.GetInvocationList
                RemoveHandler Me.UpdatePeriodDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.DriverErrorEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.DriverErrorEvent.GetInvocationList
                RemoveHandler Me.DriverError, CType(d, Global.System.EventHandler(Of TraceMessageEventArgs))
            Next
        End If

        ' stop data acquisition and close the device.
        Try
            If Me.AnalogInput IsNot Nothing Then
                Me.TryReleaseAnalogInput()
                Me._AnalogInput.Dispose()
                Me._AnalogInput = Nothing
            End If
        Catch
        End Try

        Try
            If Me._BufferQueue IsNot Nothing Then
                Me._BufferQueue.Clear()
                Me._BufferQueue = Nothing
            End If
        Catch
        End Try

        Try
            If Me._Device IsNot Nothing Then
                Me._Device.Dispose()
                Me._Device = Nothing
            End If
        Catch
        End Try

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Agnostic.PropertyNotifyControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me.components IsNot Nothing Then Me.components.Dispose()
                    Me.OnDisposeManagedResources()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        If String.IsNullOrWhiteSpace(Me.ChartTitle) Then
            Me.ChartTitle = "Analog Input Time Series"
        Else
            ' set default values.  For some reason the text values do not get set upon starting.
            Me.ChartTitle = Me.ChartTitle
        End If
        Me.ChartFooter = Me.ChartFooter
    End Sub

#End Region

#Region " STATE TRANSITION CONTROL  "

    ''' <summary> Gets the stop or abort timeout. </summary>
    ''' <value> The timeout. </value>
    Public Property StopTimeout As TimeSpan

    ''' <summary> Enumerates the state transition commands. </summary>
    Private Enum TransitionCommand
        ''' <summary> An Enum constant representing the none option. </summary>
        None
        ''' <summary> An Enum constant representing the open] option. </summary>
        [Open]
        ''' <summary> An Enum constant representing the configure option. </summary>
        Configure
        ''' <summary> An Enum constant representing the start] option. </summary>
        [Start]
        ''' <summary> An Enum constant representing the stop] option. </summary>
        [Stop]
        ''' <summary> An Enum constant representing the abort option. </summary>
        Abort
        ''' <summary> An Enum constant representing the close option. </summary>
        Close
    End Enum

    ''' <summary> Returns the analog input subsystem state. Returns the
    ''' <see cref="Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue">state </see>
    ''' as a dummy output to indicate that the analog input is not defined. </summary>
    ''' <value> The analog input state. </value>
    Public ReadOnly Property AnalogInputState() As Global.OpenLayers.Base.SubsystemBase.States
        Get
            Return If(Me.AnalogInput Is Nothing, Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue, Me.AnalogInput.State)
        End Get
    End Property

    ''' <summary> Report an illegal transition. </summary>
    ''' <param name="transitionCommand"> The transition command. </param>
    ''' <param name="state">             The state. </param>
    Private Sub OnIllegalTransition(ByVal transitionCommand As TransitionCommand, ByVal state As Global.OpenLayers.Base.SubsystemBase.States)
        Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                   "Illegal transition command {0} issued when in the {1} state--Ignored;. ",
                                   transitionCommand, state)
    End Sub

    ''' <summary> Report an illegal state. </summary>
    ''' <param name="state"> The state. </param>
    Private Sub OnIllegalState(ByVal state As Global.OpenLayers.Base.SubsystemBase.States)

        Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Illegal state {0};. ", state)

    End Sub

    ''' <summary> Report an unhandled transition request. </summary>
    ''' <param name="transitionCommand"> The transition command. </param>
    ''' <param name="state">             The state. </param>
    Private Sub OnUnhandledTransition(ByVal transitionCommand As TransitionCommand, ByVal state As Global.OpenLayers.Base.SubsystemBase.States)
        Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Unhandled transition command {0} issued when in the {1} state--ignored;. ",
                                   transitionCommand, state)
    End Sub

    ''' <summary> Report an Unnecessary transition. </summary>
    ''' <param name="transitionCommand"> The transition command. </param>
    ''' <param name="state">             The state. </param>
    Private Sub OnUnnecessaryTransition(ByVal transitionCommand As TransitionCommand, ByVal state As Global.OpenLayers.Base.SubsystemBase.States)
        Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Unnecessary transition command {0};. issued when in the {1} state--ignored.",
                                   transitionCommand, state)

    End Sub

    ''' <summary> Returns true if the analog input is in continuous operation running state. </summary>
    ''' <value> The continuous operation running. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContinuousOperationRunning() As Boolean
        Get
            Return Me.IsOpen AndAlso Me.AnalogInput.ContinuousOperationRunning
        End Get
    End Property

    ''' <summary> Returns true if the analog input is in continuous operation. </summary>
    ''' <value> The continuous operation active. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContinuousOperationActive() As Boolean
        Get
            Return Me.IsOpen AndAlso Me.AnalogInput.ContinuousOperationActive
        End Get
    End Property

    ''' <summary> Stops a continuous operation on the subsystem immediately without waiting for the
    ''' current buffer to be filled. </summary>
    Public Sub Abort()

        Dim command As TransitionCommand = TransitionCommand.Abort
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState

        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state.
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state
                Me.OnIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' the board was not stated
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                Me.TryStopContinuousInput(Me.StopTimeout, False)

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' already aborting.
                Me.OnUnnecessaryTransition(command, state)

            Case Else

                Me.OnUnhandledTransition(command, state)

        End Select

    End Sub

    ''' <summary> Sends a signal to close the analog input and release the selected board. </summary>
    Public Sub [Close]()

        Dim command As TransitionCommand = TransitionCommand.Close
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' this will await for aborting and then close.
                Me.TryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state.
                Me.TryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state.
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' the board is connected but not yet configured.
                Me.TryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 

                ' stop running.
                Me.Stop()

                ' close
                Me.TryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' this will await for stopping and then close.
                Me.TryCloseAnalogInput()

            Case Else

                Me.OnUnhandledTransition(command, state)

        End Select

    End Sub

    ''' <summary> Configures the board for data collection. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function TryConfigure(ByRef details As String) As Boolean

        Dim command As TransitionCommand = TransitionCommand.Configure
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' must wait for aborting to complete.
                Me.OnIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' the analog input can be reconfigured.
                Me.StartStopEnabled = Me.TryConfigureThis(details)
                Return Me.StartStopEnabled

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state. need to open first.
                Me.OnIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' this is the first configuration after opening the board.
                Me.StartStopEnabled = Me.TryConfigureThis(details)
                Return Me.StartStopEnabled

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                Me.OnIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' must wait for stopping to complete.
                Me.OnIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Else

                Me.OnUnhandledTransition(command, state)
                details = "Unhandled Transition"
                Return False

        End Select

    End Function

    ''' <summary> Sends a signal to select a board and open the analog input. </summary>
    ''' <param name="deviceName"> Name of the device. </param>
    Public Sub [Open](ByVal deviceName As String)

        Me._CandidateDeviceName = deviceName
        Dim command As TransitionCommand = TransitionCommand.Open
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' Already open
                Me.OnIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state. Already open
                Me.OnIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state
                Me.TryOpenAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' board is already open.
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                ' Already open
                Me.OnIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' Already open
                Me.OnIllegalTransition(command, state)

            Case Else

                Me.OnUnhandledTransition(command, state)

        End Select

    End Sub

    ''' <summary> Starts data collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is
    ''' invalid. </exception>
    Public Sub [Start]()

        If Me.ConfigurationRequired Then
            Throw New Global.System.InvalidOperationException("Configuration required")
        Else
            Dim command As TransitionCommand = TransitionCommand.Start
            Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
            Select Case state

                Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                    Me.TryStartContinuousInput()

                Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                    ' this is the stopped state.
                    Me.TryStartContinuousInput()

                Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                    ' this is the closed state. need to open first.
                    Me.OnIllegalTransition(command, state)

                Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                    ' illegal -- board not configured yet.
                    Me.OnIllegalTransition(command, state)

                Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                     Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                     Global.OpenLayers.Base.SubsystemBase.States.Running

                    ' The final analog output sample has been written from the FIFO on the device. 
                    ' The subsystem was pre-started for a continuous operation. 
                    Me.OnUnnecessaryTransition(command, state)

                Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                    Me.TryStartContinuousInput()

                Case Else

                    Me.OnUnhandledTransition(command, state)

            End Select

        End If

    End Sub

    ''' <summary> Gets or sets the started status.  This is reflected in the status of the Start/Stop
    ''' control. </summary>
    ''' <value> The started. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Started() As Boolean
        Get
            Return Me._StartStopToolStripMenuItem.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.StartStopEnabled = False
            Me._StartStopToolStripMenuItem.Checked = value
            Me._StartStopToolStripMenuItem.Invalidate()
            ' enable or disable all chart controls as necessary
            Me.EnableChartControls()
        End Set
    End Property

    ''' <summary> Gets or sets the start stop enable state. </summary>
    ''' <value> The start stop enabled. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StartStopEnabled() As Boolean

        Get
            Return Me._StartStopToolStripMenuItem.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._StartStopToolStripMenuItem.Enabled = value
        End Set
    End Property

    ''' <summary> Stops Running. </summary>
    Public Sub [Stop]()

        Dim command As TransitionCommand = TransitionCommand.Stop
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' already stopping.
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state. already stopped.
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state. Not open.
                Me.OnIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' the board was not started.
                Me.OnUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                Me.TryStopContinuousInput(Me.StopTimeout, False)

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' already stopping.
                Me.OnUnnecessaryTransition(command, state)

            Case Else

                Me.OnUnhandledTransition(command, state)

        End Select

    End Sub

#End Region

#Region " CONFIGURATION/OPERATION COMMANDS "

    ''' <summary> Restores default settings. </summary>
    Public Sub RestoreDefaults()
        Me.SuspendConfig()
        If Me.ChartMode = ChartModeId.Scope Then
            Me.BuffersCount = 5
            Me.SampleSize = 1000
            Me.SamplingRate = 1000
        ElseIf Me.ChartMode = ChartModeId.StripChart Then
            Me.BuffersCount = 20
            Me.SampleSize = 2
            Me.SamplingRate = 240
            Me.SignalBufferSize = 240
        End If
        Me.CalculateEnabled = False
        Me.CalculatePeriodCount = 10
        Me.ChartingEnabled = True
        Me.DisplayAverageEnabled = False
        Me.DisplayVoltageEnabled = False
        Me.FiniteBuffering = False
        Me.MovingAverageLength = 10
        Me.RefreshRate = 24
        Me.UpdateEventEnabled = False
        Me.UpdatePeriodCount = 10
        Me.UsingSmartBuffering = False
        Me.ResumeConfig()
    End Sub

    ''' <summary> Reset to Opened and not running conditions. </summary>
    ''' <exception cref="TimeoutException"> Abort timeout--system still running after
    ''' abort. </exception>
    Public Sub Reset()

        Try

            Me.Cursor = Cursors.WaitCursor

            ' abort if running.
            Me.TryStopContinuousInput(Me.StopTimeout, True)

            ' clear the configuration suspension Queue.
            Me.ConfigureSuspensionQueue = New Generic.Queue(Of Boolean)

            If Me.AnalogInput Is Nothing Then
                ' clear the display.
                Me.ClearChannelsList()
            End If

        Catch

            Throw

        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " FUNCTIONAL PROPERTIES "

    ''' <summary> Gets the
    ''' <see cref="System.Diagnostics.TraceEventType">broadcast level</see>. </summary>
    ''' <value> The broadcast level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property BroadcastLevel() As System.Diagnostics.TraceEventType

#End Region

#Region " DEVICE "

    ''' <summary> Releases the analog input event handlers. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub TryReleaseAnalogInput()

        If Me.AnalogInput IsNot Nothing Then

            ' add event handlers for the events we're interested in
            Try
                RemoveHandler Me.AnalogInput.GeneralFailureEvent, AddressOf Me.HandleGeneralFailure
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.BufferDoneEvent, AddressOf Me.HandleBufferDone
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.QueueDoneEvent, AddressOf Me.HandleQueueDone
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.QueueStoppedEvent, AddressOf Me.HandleQueueStopped
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.DeviceRemovedEvent, AddressOf Me.HandleDeviceRemoved
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.DriverRunTimeErrorEvent, AddressOf Me.HandleDriverRuntimeError
            Catch
            End Try

        End If

    End Sub

    ''' <summary> Stops acquisition, if necessary, and closes the analog input board. </summary>
    ''' <remarks> Use this method to close the instance.  The method returns true if success or false
    ''' if it failed closing the instance. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Operation timed out occurred attempting to close
    ''' the analog input
    ''' <innerException cref="System.TimeoutException">Abort timeout--system still running after
    ''' abort</innerException>
    ''' or Open layers exception occurred attempting to close the analog input
    ''' <innerException cref="Global.OpenLayers.Base.OlException">open layers
    ''' exception.</innerException> </exception>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CloseAnalogInput()

        Try

            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Closing...;. ")

            ' clear the channel list before closing the channel
            Me.ClearChannelsList()

            ' stop if still running
            Me.TryStopContinuousInput(Me.StopTimeout, False)

            ' removes event handlers for the events we're interested in
            Me.TryReleaseAnalogInput()

            If Me.AnalogInput IsNot Nothing Then
                Me.AnalogInput.Dispose()
                Me._AnalogInput = Nothing
            End If

            ' clear the buffer queue
            If Me._BufferQueue IsNot Nothing Then
                Me._BufferQueue.Clear()
                Me._BufferQueue = Nothing
            End If

            If Me._Device IsNot Nothing Then
                Me._Device.Dispose()
                Me._Device = Nothing
            End If

            ' clear the board name.
            Me._CandidateDeviceName = String.Empty

        Catch ex As TimeoutException

            Throw New isr.Core.OperationFailedException("Operation timed out attempting to close the analog input", ex)

        Catch ex As Global.OpenLayers.Base.OlException

            Throw New isr.Core.OperationFailedException("Open layers exception occurred attempting to close the analog input", ex)

        Finally

            Try
                If Me.AnalogInput IsNot Nothing Then
                    Me.AnalogInput.Dispose()
                    Me._AnalogInput = Nothing
                End If
            Catch
            End Try

            Try
                If Me._BufferQueue IsNot Nothing Then
                    Me._BufferQueue.Clear()
                    Me._BufferQueue = Nothing
                End If
            Catch
            End Try

            Try
                If Me._Device IsNot Nothing Then
                    Me._Device.Dispose()
                    Me._Device = Nothing
                End If
            Catch
            End Try


            ' force turning off of the start button
            Me.Started = False

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = Not Me.IsOpen

            ' disable user interface
            Me.EnableChartControls()
            Me.EnableSignalsControls()
            Me.EnableChannelsControls()

            If Me.IsOpen Then

                ' if still open, display message and leave in the closing state. 
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed closing;. ")

            Else

                ' set the closed prompt
                Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Closed;. ")

            End If

        End Try

    End Sub

    ''' <summary> Tries to close the analog input. </summary>
    Private Sub TryCloseAnalogInput()

        Try

            Me.CloseAnalogInput()

        Catch ex As OperationFailedException

            If TypeOf ex.InnerException Is Global.OpenLayers.Base.OlException Then

                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Open Layers Driver exception occurred closing;. {0}", ex.ToFullBlownString)

            ElseIf TypeOf ex.InnerException Is System.TimeoutException Then

                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Timeout exception occurred closing;. {0}", ex.ToFullBlownString)

            Else

                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Unexpected exception occurred closing;. {0}", ex.ToFullBlownString)

            End If

        Catch

            Throw

        End Try

    End Sub

    ''' <summary> Disconnected an already disconnected board. </summary>
    ''' <remarks> Use this method to close the instance.  The method returns true if success or false
    ''' if it failed closing the instance. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Operation timed out occurred attempting to close
    ''' the analog input
    ''' <innerException cref="System.TimeoutException">Abort timeout--system still running after
    ''' abort</innerException>
    ''' or Open layers exception occurred attempting to close the analog input
    ''' <innerException cref="Global.OpenLayers.Base.OlException">open layers
    ''' exception.</innerException> </exception>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DisconnectAnalogInput()

        Try

            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disconnecting...;. ")

            ' removes event handlers for the events we're interested in
            Me.TryReleaseAnalogInput()

            ' clear the board name.
            Me._CandidateDeviceName = String.Empty

        Catch ex As TimeoutException

            Throw New isr.Core.OperationFailedException("Operation timed out attempting to close the analog input", ex)

        Catch ex As Global.OpenLayers.Base.OlException

            Throw New isr.Core.OperationFailedException("Open layers exception occurred attempting to close the analog input", ex)

        Finally

            Try
                If Me.AnalogInput IsNot Nothing Then
                    ' Me.AnalogInput.Dispose()
                    Me._AnalogInput = Nothing
                End If
            Catch
            End Try

            Try
                If Me._BufferQueue IsNot Nothing Then
                    'Me._bufferQueue.Clear()
                    Me._BufferQueue = Nothing
                End If
            Catch
            End Try

            Try
                If Me._Device IsNot Nothing Then
                    'Me._Device.Dispose()
                    Me._Device = Nothing
                End If
            Catch
            End Try

            Try
                ' clear the channel list -- this clears the displays.
                Me.ClearChannelsList()
            Catch
            End Try

            ' force turning off of the start button
            Me.Started = False

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = True
            Me._OpenCloseCheckBox.Enabled = False

            Try
                ' clear the list of devices.
                Me._DeviceNameChooser.RefreshDeviceNamesList()
            Catch
            End Try

            ' disable user interface
            Me.EnableChartControls()
            Me.EnableSignalsControls()
            Me.EnableChannelsControls()

            If Me.IsOpen Then

                ' if still open, display message and leave in the closing state. 
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed disconnecting;. ")

            Else

                ' set the closed prompt
                Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disconnected;. ")

            End If

        End Try

    End Sub

    ''' <summary> Name of the candidate device. </summary>
    Private _CandidateDeviceName As String

    ''' <summary> Gets the board name. </summary>
    ''' <value> The name of the device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DeviceName() As String
        Get
            Return If(Me.Device Is Nothing, "n/a", Me.Device.DeviceName)
        End Get
    End Property

    ''' <summary> Gets the default device. </summary>
    ''' <value> The device. </value>
    Private Property Device As Global.OpenLayers.Base.Device

    ''' <summary> Determines whether the specified range equals the analog input voltage range. Used as
    ''' a predicate in the array search for an index. </summary>
    ''' <param name="range"> The range. </param>
    ''' <returns> <c>True</c> if [is equal range] [the specified range]; otherwise, <c>False</c>. </returns>
    Private Function IsEqualRange(ByVal range As Global.OpenLayers.Base.Range) As Boolean
        Return range IsNot Nothing AndAlso isr.IO.OL.Equals(range, Me.AnalogInput.VoltageRange, 0.0001)
    End Function

    ''' <summary> <c>True</c> if board disconnected. </summary>
    Private _BoardDisconnected As Boolean

    ''' <summary> Gets or sets a value indicating whether the board was disconnected. </summary>
    ''' <remarks> Allows to immediately address the disconnection of the device. </remarks>
    ''' <value> <c>True</c> if disconnected; otherwise, <c>False</c>. </value>
    Public ReadOnly Property BoardDisconnected As Boolean
        Get
            Return Me._BoardDisconnected
        End Get
    End Property

    ''' <summary> Gets the Open status flag. </summary>
    ''' <remarks> Use this property to get the open status of this instance, which is True if the
    ''' instance was opened. </remarks>
    ''' <value> <c>IsOpen</c> is a Boolean property that can be read from (read only). </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsOpen() As Boolean
        Get
            Return Not (Me._BoardDisconnected OrElse Me.AnalogInput Is Nothing)
        End Get
    End Property

    ''' <summary> Opens the analog input board and instantiates the analog input subsystem. Requires
    ''' setting of the candidate device name. </summary>
    ''' <remarks> Use this method to open the instance.  The method returns true if success or false if
    ''' it failed opening the instance. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    Private Sub OpenAnalogInput()

        If Not Global.OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            ' Me.onHardwareDisconnected(New Global.OpenLayers.Base.GeneralEventArgs)
            Dim eventHandler As EventHandler(Of EventArgs) = Me.DisconnectedEvent
            If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, New EventArgs)
            Throw New InvalidOperationException("Hardware not available.")
        End If

        If String.IsNullOrWhiteSpace(Me._CandidateDeviceName) Then
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Attempted opening board with an empty board name;. ")
            Throw New System.InvalidOperationException("Board name not specified.")
        End If

        Try

            ' reset to open state conditions.
            Me.Reset()

            ' Opens the subsystem
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Opening board '{0}';. ", Me._CandidateDeviceName)

            ' get a board 
            Me._Device = Global.OpenLayers.Base.DeviceMgr.Get.SelectDevice(Me._CandidateDeviceName)

            If Me.Device Is Nothing Then

                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed opening board '{0}';. ", Me._CandidateDeviceName)
                Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, "Failed opening board '{0}'", Me._CandidateDeviceName))

            Else

                Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Opened Device {0} Board #{1};. ",
                                  Me.Device.DeviceName, Me.Device.GetHardwareInfo.BoardId)

                ' update the board name.
                Me._CandidateDeviceName = Me.Device.DeviceName

                ' instantiate the open layer analog input sub system
                Me._AnalogInput = New isr.IO.OL.AnalogInputSubsystem(Me.Device, 0)

                ' clear the selected channel and signal
                Me.SelectChannel(-1)
                Me.SelectSignal(-1)

                ' set the channel type
                Me.AnalogInput.ChannelType = If(Me.AnalogInput.ChannelType = Global.OpenLayers.Base.ChannelType.SingleEnded,
                    If(Me.AnalogInput.SupportsSingleEndedInput,
                        Global.OpenLayers.Base.ChannelType.SingleEnded,
                        Global.OpenLayers.Base.ChannelType.Differential),
                    If(Me.AnalogInput.SupportsDifferentialInput,
                        Global.OpenLayers.Base.ChannelType.Differential,
                        Global.OpenLayers.Base.ChannelType.SingleEnded))

                ' set the board voltage range selection list
                Me._BoardInputRangesComboBox.DataSource = Me.AnalogInput.AvailableBoardRanges("({0},{1}) V")
                Dim i As Integer = Array.FindIndex(Me.AnalogInput.SupportedVoltageRanges,
                                                   New Predicate(Of Global.OpenLayers.Base.Range)(AddressOf Me.IsEqualRange))
                Me._BoardInputRangesComboBox.SelectedIndex = i

                ' set the channel selection list.
                Me._ChannelComboBox.DataSource = Me.AnalogInput.AvailableChannels("{0}")

                ' set the ranges combo
                Me._ChannelRangesComboBox.DataSource = Me.AnalogInput.AvailableRanges("({0},{1}) V")

                ' set some properties
                Me.AnalogInput.DataFlow = Global.OpenLayers.Base.DataFlow.Continuous
                Dim toolTipText As String = "Channel samples per second between {0} and {1}"
                toolTipText = String.Format(Globalization.CultureInfo.CurrentCulture,
                                            toolTipText, Me.AnalogInput.Clock.MinFrequency, Me.AnalogInput.Clock.MaxFrequency)
                Me._ToolTip.SetToolTip(Me._SamplingRateNumeric, toolTipText)

                ' add event handlers for the events we're interested in
                AddHandler Me.AnalogInput.GeneralFailureEvent, AddressOf Me.HandleGeneralFailure
                AddHandler Me.AnalogInput.BufferDoneEvent, AddressOf Me.HandleBufferDone
                AddHandler Me.AnalogInput.QueueDoneEvent, AddressOf Me.HandleQueueDone
                AddHandler Me.AnalogInput.QueueStoppedEvent, AddressOf Me.HandleQueueStopped
                AddHandler Me.AnalogInput.DeviceRemovedEvent, AddressOf Me.HandleDeviceRemoved
                AddHandler Me.AnalogInput.DriverRunTimeErrorEvent, AddressOf Me.HandleDriverRuntimeError

                Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Initialized analog input on '{0}';. ", Me.DeviceName)

                ' reset if board was disconnected.
                If Me.BoardDisconnected Then
                    Me.Reset()
                End If

                Me._BoardDisconnected = False

            End If

        Catch

            ' close to meet strong guarantees
            Try
                Me.Close()
            Finally
            End Try

            ' throw an exception
            Throw

        Finally

            ' force turning off of the start button
            Me.Started = False

            ' enable user interface
            Me.EnableChartControls()
            Me.EnableSignalsControls()
            Me.EnableChannelsControls()

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = Not Me.IsOpen

            Me._AnalogInputConfigurationRequired = True
            Me._ChartConfigurationRequired = True

        End Try

    End Sub

    ''' <summary> Tries to open the analog input. </summary>
    Private Sub TryOpenAnalogInput()

        Try

            Me.OpenAnalogInput()

        Catch ex As InvalidOperationException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Invalid operation exception occurred opening;. {0}", ex.ToFullBlownString)

        Catch ex As OperationFailedException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Operation failed exception occurred opening;. {0}", ex.ToFullBlownString)

        Catch

            Throw

        End Try

    End Sub

#End Region

#Region " ANALOG INPUT CONFIGURATION "

    ''' <summary> The analog input. </summary>
    Private _AnalogInput As AnalogInputSubsystem

    ''' <summary> Gets reference to the analog input. </summary>
    ''' <value> The analog input. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property AnalogInput() As AnalogInputSubsystem
        Get
            Return Me._AnalogInput
        End Get
    End Property

#Region " CHANNELS "

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <param name="gain">          . </param>
    ''' <param name="name">          The name. </param>
    Public Sub AddChannel(ByVal channelNumber As Integer, ByVal gain As Double, ByVal name As String)

        ' add the new channel to the channel list.
        Me.AnalogInput.AddChannel(channelNumber, gain, name)
        Me.SelectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' update the display
        Me.UpdateChannelList()

        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

        ' request configuration of analog input.
        Me.AnalogInputConfigurationRequired = True

    End Sub

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <param name="gainIndex">     . </param>
    ''' <param name="name">          The name. </param>
    Public Sub AddChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer, ByVal name As String)

        ' add the new channel to the channel list.
        Me.AnalogInput.AddChannel(channelNumber, gainIndex, name)
        Me.SelectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' update the display
        Me.UpdateChannelList()

        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

        ' request configuration of analog input.
        Me.AnalogInputConfigurationRequired = True

    End Sub

    ''' <summary> Clears the channel and signal lists. </summary>
    Public Sub ClearChannelsList()

        ' Clear the selected signal and list
        Me.ClearSignalsList()

        ' Clear the selected channel and list
        Me.SelectChannel(-1)

        ' the list must have at least one item.
        If Me.AnalogInput IsNot Nothing AndAlso Not Me.ContinuousOperationActive Then
            Me.AnalogInput.ChannelList.Clear()
        ElseIf Me.AnalogInput IsNot Nothing AndAlso Me.ContinuousOperationActive Then
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Attempt to clear the channel list was ignored because the subsystem is running;. ")
        End If

        If Me.AnalogInput Is Nothing OrElse Me.AnalogInput.ChannelsCount = 0 Then

            ' remove all items from the list view
            Me._ChannelsListView.Items.Clear()
            Me._ChannelsListView.Invalidate()

            ' clear the channel list.
            Me._SignalChannelComboBox.Items.Clear()
            Me._SignalChannelComboBox.Invalidate()

        End If

        ' update user interface to reflect the removal of the channels.
        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

    End Sub

    ''' <summary> Returns a channel list view item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> The channel. </param>
    ''' <returns> The new channel list view item. </returns>
    Private Function CreateChannelListViewItem(ByVal channel As Global.OpenLayers.Base.ChannelListEntry) As ListViewItem
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        ' Create the channel item
        Dim channelItem As New Windows.Forms.ListViewItem(channel.PhysicalChannelNumber.ToString(Globalization.CultureInfo.CurrentCulture))
        channelItem.SubItems.Add(channel.Name)
        Dim formatValue As String = "({0},{1}) V"
        channelItem.SubItems.Add(String.Format(Globalization.CultureInfo.CurrentCulture, formatValue,
                                               Me.AnalogInput.MinVoltage, Me.AnalogInput.MaxVoltage))
        Return channelItem
    End Function

    ''' <summary> Returns a default channel name. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <returns> The default channel name. </returns>
    Private Shared Function DefaultChannelName(ByVal channelNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "Ch. {0}", channelNumber)
    End Function

    ''' <summary> Removes the specified channel from the list of channels. </summary>
    ''' <param name="channelNumber"> . </param>
    Public Sub RemoveChannel(ByVal channelNumber As Integer)

        Dim physicalChannelNumber As Integer = channelNumber
        If Me.AnalogInput.PhysicalChannelExists(physicalChannelNumber) Then
            Dim channel As Global.OpenLayers.Base.ChannelListEntry = Me.AnalogInput.SelectPhysicalChannel(physicalChannelNumber)

            Me.RemoveSignal(channel)

            Me.AnalogInput.ChannelList.Remove(channel)

            Me.UpdateChannelList()

            If Me.SelectedChannel Is Nothing OrElse Not Me.AnalogInput.ChannelList.Contains(Me.SelectedChannel) Then
                If Me.AnalogInput.HasChannels Then
                    Me.SelectChannel(0)
                End If
            End If

            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True

        End If

    End Sub

    ''' <summary> Removes the specified channel from the list of channels. </summary>
    ''' <param name="channel"> The channel. </param>
    Public Sub RemoveChannel(ByVal channel As Global.OpenLayers.Base.ChannelListEntry)

        If channel IsNot Nothing AndAlso Me.AnalogInput.ChannelList.Contains(channel) Then
            Me.RemoveSignal(channel)
            Me.AnalogInput.ChannelList.Remove(channel)
            Me.UpdateChannelList()

            If Me.SelectedChannel Is Nothing OrElse Not Me.AnalogInput.ChannelList.Contains(Me.SelectedChannel) Then
                If Me.AnalogInput.HasChannels Then
                    Me.SelectChannel(0)
                End If
            End If

            If Me.SelectedSignal Is Nothing OrElse Not Me.Chart.Signals.Contains(Me.SelectedSignal) Then
                If Me.Chart.Signals.Count > 0 Then
                    Me.SelectSignal(0)
                End If
            End If

        End If

    End Sub

    ''' <summary> Selects a channel. </summary>
    ''' <param name="channelIndex"> The channel logical number = channel index. </param>
    ''' <returns> The selected channel. </returns>
    Private Function SelectChannel(ByVal channelIndex As Integer) As Global.OpenLayers.Base.ChannelListEntry

        Me.SelectedChannelIndex = channelIndex
        If Me.AnalogInput IsNot Nothing AndAlso channelIndex >= 0 AndAlso
            channelIndex < Me.AnalogInput.ChannelList.Count Then

            Me.SelectedChannel = Me.AnalogInput.ChannelList(Me.SelectedChannelIndex)

        Else

            Me.SelectedChannelIndex = -1
            Me.SelectedChannel = Nothing

        End If

        Return Me.SelectedChannel

    End Function

    ''' <summary> Gets or sets the index of the selected channel in the channel list. This is required
    ''' because some Open Layer methods requires the index rather than the channel. </summary>
    ''' <value> The selected channel index. </value>
    Private Property SelectedChannelIndex As Integer

    ''' <summary> Gets or sets the selected channel. </summary>
    ''' <value> The selected channel. </value>
    Private Property SelectedChannel() As Global.OpenLayers.Base.ChannelListEntry

    ''' <summary> Updates a channel in the channel list. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <param name="gainIndex">     . </param>
    ''' <param name="name">          The name. </param>
    Public Sub UpdateChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer, ByVal name As String)

        If String.IsNullOrWhiteSpace(name) Then
            name = String.Empty
        End If
        Me.AnalogInput.UpdateChannel(channelNumber, gainIndex, name)

        ' add the new channel to the channel list.
        Me.SelectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' update the display
        Me.UpdateChannelList()

        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

        ' request configuration of analog input.
        Me.AnalogInputConfigurationRequired = True

    End Sub

    ''' <summary> Updates the channel list display. </summary>
    Private Sub UpdateChannelList()

        Dim selectedIndex As Integer = -1
        If Me._ChannelsListView.SelectedIndices.Count > 0 Then
            selectedIndex = Me._ChannelsListView.SelectedIndices.Item(0)
        End If

        Dim selectedSignalChannelIndex As Integer = -1
        If Me._SignalChannelComboBox.Items.Count > 0 Then
            selectedSignalChannelIndex = Me._SignalChannelComboBox.SelectedIndex
        End If

        ' update the list of available channels for signals.
        Me._SignalChannelComboBox.Items.Clear()
        Me._ChannelsListView.Items.Clear()
        For Each channel As Global.OpenLayers.Base.ChannelListEntry In Me.AnalogInput.ChannelList

            ' get a channel item,
            Dim channelItem As ListViewItem = Me.CreateChannelListViewItem(channel)

            Me._ChannelsListView.Items.Add(channelItem)

            ' add the channel to the list of signal channels
            Me._SignalChannelComboBox.Items.Add(channel.Name)

        Next
        Me._ChannelsListView.Invalidate()

        If Me._ChannelsListView.Items.Count > 0 Then
            selectedIndex = Math.Min(Math.Max(selectedIndex, 0), Me._ChannelsListView.Items.Count - 1)
            Me._ChannelsListView.Items(selectedIndex).Selected = True
        End If

        If Me._SignalChannelComboBox.Items.Count > 0 Then
            selectedSignalChannelIndex = Math.Min(Math.Max(selectedSignalChannelIndex, 0),
                                                  Me._SignalChannelComboBox.Items.Count - 1)
            Me._SignalChannelComboBox.SelectedIndex = selectedSignalChannelIndex
        End If

    End Sub

#End Region

#Region " SIGNALS "

    ''' <summary> Adds a signal to the display. </summary>
    ''' <param name="channelIndex"> The channel index. </param>
    Public Sub AddSignal(ByVal channelIndex As Integer)

        Me.AddSignal(Me.AnalogInput.ChannelList(channelIndex),
                     CDbl(Me._SignalMinNumericUpDown.Value), CDbl(Me._SignalMaxNumericUpDown.Value),
                     Me._DefaultSignalColors(Me.Chart.Signals.Count))

    End Sub

    ''' <summary> Adds a signal to the display. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel">    The channel. </param>
    ''' <param name="minVoltage"> The minimum voltage. </param>
    ''' <param name="maxVoltage"> The maximum voltage. </param>
    ''' <param name="color">      The color. </param>
    Public Sub AddSignal(ByVal channel As Global.OpenLayers.Base.ChannelListEntry,
                              ByVal minVoltage As Double, ByVal maxVoltage As Double, ByVal color As System.Drawing.Color)

        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If

        ' update the selected channel
        Me.SelectChannel(Me.AnalogInput.ChannelList.LogicalChannelNumber(channel))

        ' select the specified channel
        Me.AnalogInput.SelectPhysicalChannel(channel.PhysicalChannelNumber)

        ' add signal 
        Dim signal As New OpenLayers.Signals.MemorySignal With {.Name = channel.Name, .RangeMax = Me.AnalogInput.MaxVoltage,
                                                                .RangeMin = Me.AnalogInput.MinVoltage, .CurrentRangeMax = maxVoltage,
                                                                .CurrentRangeMin = minVoltage, .Unit = "Volts"}

        Me.Chart.DisableRendering()

        ' add signal to the chart
        Me.Chart.Signals.Add(signal)

        ' set color.
        Me.Chart.SetCurveColor(Me.Chart.Signals.Count - 1, color)

        ' configure the chart
        Me.Chart.SignalListUpdate()

        Me.Chart.EnableRendering()

        ' update the list of signals.
        Me.UpdateSignalList()

        ' select this signal
        Me.SelectSignal(Me.Chart.Signals.Count - 1)

        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True

    End Sub

    ''' <summary> Clears the channel and signal lists. </summary>
    Public Sub ClearSignalsList()

        ' Clear the selected signal and list
        Me.SelectSignal(-1)
        Me.Chart.Signals.Clear()

        ' remove all items from the list view
        Me._SignalsListView.Items.Clear()
        Me._SignalsListView.Invalidate()

        ' update user interface to reflect the removal of the channels.
        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()

        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True

    End Sub

    ''' <summary> Returns a channel list view item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> The channel. </param>
    ''' <param name="signal">  . </param>
    ''' <returns> The new signal list view item. </returns>
    Private Shared Function CreateSignalListViewItem(ByVal channel As Global.OpenLayers.Base.ChannelListEntry,
                                                     ByVal signal As OpenLayers.Signals.MemorySignal) As ListViewItem
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        If signal Is Nothing Then
            Throw New ArgumentNullException(NameOf(signal))
        End If
        ' Create the signal item
        Dim signalItem As New Windows.Forms.ListViewItem(channel.PhysicalChannelNumber.ToString(Globalization.CultureInfo.CurrentCulture))
        signalItem.SubItems.Add(signal.Name)
        Dim formatValue As String = "({0},{1}) V"
        signalItem.SubItems.Add(String.Format(Globalization.CultureInfo.CurrentCulture, formatValue,
                                              signal.CurrentRangeMin, signal.CurrentRangeMax))
        Return signalItem
    End Function

    ''' <summary> Removes the signal associated with the specified channel. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> The channel. </param>
    Public Sub RemoveSignal(ByVal channel As Global.OpenLayers.Base.ChannelListEntry)
        If channel Is Nothing Then Throw New ArgumentNullException(NameOf(channel))

        If Me.Chart.Signals.Contains(channel.Name) Then
            Me.RemoveSignal(Me.Chart.Signals.SelectNamedSignal(channel.Name))
        End If

    End Sub

    ''' <summary> Removes the signal associated with the specified channel. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="signal"> . </param>
    Public Sub RemoveSignal(ByVal signal As OpenLayers.Signals.MemorySignal)
        If signal Is Nothing Then
            Throw New ArgumentNullException(NameOf(signal))
        End If
        If Me.Chart.Signals.Contains(signal) Then

            Me.Chart.DisableRendering()

            ' remove the signal
            Me.Chart.Signals.Remove(signal)

            ' update the chart
            Me.Chart.SignalListUpdate()

            Me.Chart.EnableRendering()

            ' update the list of signals.
            Me.UpdateSignalList()

            If Me.SelectedSignal Is Nothing OrElse Not Me.Chart.Signals.Contains(Me.SelectedSignal) Then
                If Me.Chart.Signals.Count > 0 Then
                    Me.SelectSignal(0)
                End If
            End If
        End If

    End Sub

    ''' <summary> Selects a signal. </summary>
    ''' <param name="signalIndex"> . </param>
    ''' <returns> The selected signal. </returns>
    Private Function SelectSignal(ByVal signalIndex As Integer) As OpenLayers.Signals.MemorySignal

        Me.SelectedSignalIndex = signalIndex
        If signalIndex >= 0 AndAlso signalIndex < Me.Chart.Signals.Count Then

            Me.SelectedSignal = Me.Chart.Signals(Me.SelectedSignalIndex)

            ' update tool tips for displays
            Me.DisplayAverageEnabled = Me.DisplayAverageEnabled
            Me.DisplayVoltageEnabled = Me.DisplayVoltageEnabled

            ' update selected signal display
            If Me.SelectedSignal.CurrentRangeMin < Me._SignalMinNumericUpDown.Maximum AndAlso
               Me.SelectedSignal.CurrentRangeMin > Me._SignalMinNumericUpDown.Minimum Then
                Me._SignalMinNumericUpDown.Value = CDec(Me.SelectedSignal.CurrentRangeMin)
            End If
            If Me.SelectedSignal.CurrentRangeMax < Me._SignalMaxNumericUpDown.Maximum AndAlso
               Me.SelectedSignal.CurrentRangeMax > Me._SignalMaxNumericUpDown.Minimum Then
                Me._SignalMaxNumericUpDown.Value = CDec(Me.SelectedSignal.CurrentRangeMax)
            End If

            If Me.SelectedSignalIndex >= 0 AndAlso Me.SelectedSignalIndex < Me._SignalsListView.Items.Count Then
                If Me._SignalsListView.SelectedIndices.Count >= 1 AndAlso Me._SignalsListView.SelectedIndices.Item(0) <> signalIndex Then
                    Dim wasEnabled As Boolean = Me._SignalsListView.Enabled
                    Me._SignalsListView.Enabled = False
                    Me._SignalsListView.SelectedIndices.Clear()
                    Me._SignalsListView.SelectedIndices.Add(Me.SelectedSignalIndex)
                    Me._SignalsListView.Enabled = wasEnabled
                End If
            End If

        Else

            Me.SelectedSignalIndex = -1
            Me.SelectedSignal = Nothing

        End If

        Return Me.SelectedSignal

    End Function

    ''' <summary> Gets or sets the index of the selected signal in the signal list. This is required
    ''' because some Open Layer methods requires the index rather than the signal. </summary>
    ''' <value> The selected signal index. </value>
    Private Property SelectedSignalIndex As Integer

    ''' <summary> Gets or sets the selected signal. </summary>
    ''' <value> The selected signal. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private Property SelectedSignal() As OpenLayers.Signals.MemorySignal

    ''' <summary>
    ''' Gets or sets the logical channel numbers for each signal.
    ''' </summary>
    Private _SignalLogicalChannelNumbers() As Integer

    ''' <summary> Updates the list of signals. </summary>
    Private Sub UpdateSignalList()

        Dim selectedIndex As Integer = -1
        If Me._SignalsListView.SelectedIndices.Count > 0 Then
            selectedIndex = Me._SignalsListView.SelectedIndices.Item(0)
        End If

        If Me.Chart.Signals.Count > 0 Then
            ReDim Me._SignalLogicalChannelNumbers(Me.Chart.Signals.Count - 1)
            Array.Clear(Me._SignalLogicalChannelNumbers, 0, Me._SignalLogicalChannelNumbers.Length)
        End If

        Dim unused As New OpenLayers.Signals.MemorySignal
        Me._SignalsListView.Items.Clear()

        Dim signal As OpenLayers.Signals.MemorySignal

        For Each signal In Me.Chart.Signals

            ' get the channel index (logical number)
            Dim channelIndex As Integer = Me.AnalogInput.ChannelList.LogicalChannelNumber(signal.Name)

            ' save the channel index for selecting the buffer to display for the signal.
            Me._SignalLogicalChannelNumbers(Me._SignalsListView.Items.Count) = channelIndex

            ' get a signal item,
            Dim signalItem As ListViewItem = CreateSignalListViewItem(Me.AnalogInput.ChannelList.Item(channelIndex), signal)

            ' get the item color from the signal color.
            signalItem.ForeColor = Me.Chart.GetCurveColor(Me._SignalsListView.Items.Count)

            ' add the entry.
            Me._SignalsListView.Items.Add(signalItem)

        Next
        Me._SignalsListView.Invalidate()

        ' update the signals control box.
        Me.EnableSignalsControls()

        If Me._SignalsListView.Items.Count > 0 Then
        End If

        If Me._SignalsListView.Items.Count > selectedIndex + 1 Then
            If selectedIndex > 0 Then
                Me._SignalsListView.Items(selectedIndex).Selected = True
            End If
        End If

    End Sub


#End Region

#Region " BUFFERS COUNT "

    ''' <summary> Gets or sets the maximum Buffers Count. </summary>
    ''' <value> The max Buffers Count. </value>
    <Description("The display maximum Buffers Count."), Category("Data"), DefaultValue(1000)>
    Public Property MaximumBuffersCount As Integer
        Get
            Return CInt(Me._BuffersCountNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._BuffersCountNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Buffers Count. </summary>
    ''' <value> The Min Buffers Count. </value>
    <Description("The display minimum Buffers Count."), Category("Data"), DefaultValue(2)>
    Public Property MinimumBuffersCount As Integer
        Get
            Return CInt(Me._BuffersCountNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._BuffersCountNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of data collection buffers. </summary>
    ''' <value> The number of buffers. </value>
    <Description("Gets or sets the number of data collection buffers."), Category("Data"), DefaultValue(5)>
    Public Property BuffersCount() As Integer

        Get
            Return CInt(Me._BuffersCountNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.BuffersCount Then
                Me._BuffersCountNumeric.ValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " CHANNEL BUFFER SIZE "

    ''' <summary> Size of the channel buffer. </summary>
    Private _ChannelBufferSize As Integer

    ''' <summary> Gets or sets the number of samples per channel. In scope mode this also equals the
    ''' signal size. In strip chart mode, the board collects small buffers of 2 or more points. The
    ''' signal is updated to display these new data when the buffers are filled. With
    ''' <see cref="UsingSmartBuffering">smart buffering</see> this is larger than the
    ''' <see cref="UsbBufferSize">USB buffer size</see>. </summary>
    ''' <value> The size of the channel buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ChannelBufferSize() As Integer
        Get
            Return Me._ChannelBufferSize
        End Get
    End Property

    ''' <summary> Size of the sample buffer. </summary>
    Private _SampleBufferSize As Integer

    ''' <summary> Gets or sets total number of samples in the analog input buffer.  This is read only
    ''' because the control exposes the buffer size per channel and configure this value based on the
    ''' number of channels. Equals the channel buffer size times the number of channels. </summary>
    ''' <value> The size of the sample buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SampleBufferSize() As Integer
        Get
            Return Me._SampleBufferSize
        End Get
    End Property

    ''' <summary> Gets or sets the maximum Sample Size. </summary>
    ''' <value> The max Sample Size. </value>
    <Description("The maximum Sample Size."), Category("Data"), DefaultValue(1000)>
    Public Property MaximumSampleSize As Integer
        Get
            Return CInt(Me._SampleSizeNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._SampleSizeNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Sample Size. </summary>
    ''' <value> The Min Sample Size. </value>
    <Description("The minimum Sample Size."), Category("Data"), DefaultValue(2)>
    Public Property MinimumSampleSize As Integer
        Get
            Return CInt(Me._SampleSizeNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._SampleSizeNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of samples acquired per channel buffer. In strip chart mode
    ''' smart buffering this could be smaller than the signal buffer size. ????which is allocated
    ''' based on the USB buffer size of 256 samples. </summary>
    ''' <value> The size of the sample. </value>
    <Description("Gets or sets the number of samples to collect per channel buffer."), Category("Data"), DefaultValue(1000.0)>
    Public Property SampleSize() As Integer

        Get
            Return CInt(Me._SampleSizeNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.SampleSize Then
                Me._SampleSizeNumeric.ValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " MOVING AVERAGE "

    ''' <summary> Gets or sets the maximum Moving Average Length. </summary>
    ''' <value> The max Moving Average Length. </value>
    <Description("The display maximum Moving Average Length."), Category("Data"), DefaultValue(100)>
    Public Property MaximumMovingAverageLength As Integer
        Get
            Return CInt(Me._MovingAverageLengthNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._MovingAverageLengthNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Moving Average Length. </summary>
    ''' <value> The Min Moving Average Length. </value>
    <Description("The display minimum Moving Average Length."), Category("Data"), DefaultValue(1)>
    Public Property MinimumMovingAverageLength As Integer
        Get
            Return CInt(Me._MovingAverageLengthNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._MovingAverageLengthNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the average length of the moving. </summary>
    ''' <value> The average length of the moving. </value>
    <Description("The number of point to use when calculating the moving average for the status bar."), Category("Data"), DefaultValue(10)>
    Public Property MovingAverageLength() As Integer

        Get
            Return CInt(Me._MovingAverageLengthNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.MovingAverageLength Then
                Me._MovingAverageLengthNumeric.ValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " REFRESH RATE "

    ''' <summary> Gets or sets the maximum refresh rate. </summary>
    ''' <value> The max refresh rate. </value>
    <Description("The display maximum refresh rate per second."), Category("Appearance"), DefaultValue(100)>
    Public Property MaximumRefreshRate As Decimal
        Get
            Return Me._RefreshRateNumeric.Maximum
        End Get
        Set(value As Decimal)
            Me._RefreshRateNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum refresh rate. </summary>
    ''' <value> The Min refresh rate. </value>
    <Description("The display minimum refresh rate per second."), Category("Appearance"), DefaultValue(1.0)>
    Public Property MinimumRefreshRate As Decimal
        Get
            Return Me._RefreshRateNumeric.Minimum
        End Get
        Set(value As Decimal)
            Me._RefreshRateNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> The next refresh time. </summary>
    Private ReadOnly _RefreshStopwatch As Stopwatch

    ''' <summary> The refresh time span. </summary>
    Private _RefreshTimespan As TimeSpan

    ''' <summary> Gets or sets the display refresh rate. </summary>
    ''' <value> The refresh rate. </value>
    <Description("The display refresh rate per second."), Category("Appearance"), DefaultValue(24.0)>
    Public Property RefreshRate() As Double
        Get
            Return Me._RefreshRateNumeric.Value
        End Get
        Set(ByVal value As Double)
            If value <> Me.RefreshRate Then
                Me._RefreshRateNumeric.ValueSetter(value)
                ' request configuration of chart.
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " SAMPLING RATE "

    ''' <summary> Gets or sets the sampling period. This is the inverse of the
    ''' <see cref="ClockRate">clock frequency</see>. </summary>
    ''' <value> The sampling period. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SamplingPeriod As Double

        Get
            Return If(Me.IsOpen, 1.0# / Me.AnalogInput.Clock.Frequency, 0)
        End Get
        Set(value As Double)
            If value > 0 Then
                Me.ClockRate = 1.0# / value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the actual analog input board rate.  
    ''' The clock differs from the <see cref="SamplingRate">sampling rate</see> in cases where a
    ''' sampling rate slower then the minimum clock rate is desired or in strip chart
    ''' <see cref="UsingSmartBuffering">smart buffering mode</see>. </summary>
    ''' <value> The clock rate. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ClockRate() As Double
        Get
            Return If(Me.IsOpen, Me.AnalogInput.Clock.Frequency, 0)
        End Get
        Set(value As Double)
            If Me.IsOpen Then
                Me.AnalogInput.Clock.Frequency = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the maximum Sampling rate. </summary>
    ''' <value> The max Sampling rate. </value>
    <Description("The display maximum Sampling rate per second."), Category("Appearance"), DefaultValue(100000)>
    Public Property MaximumSamplingRate As Decimal
        Get
            Return Me._SamplingRateNumeric.Maximum
        End Get
        Set(value As Decimal)
            Me._SamplingRateNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Sampling rate. </summary>
    ''' <value> The Min Sampling rate. </value>
    <Description("The display minimum Sampling rate per second."), Category("Appearance"), DefaultValue(60)>
    Public Property MinimumSamplingRate As Decimal
        Get
            Return Me._SamplingRateNumeric.Minimum
        End Get
        Set(value As Decimal)
            Me._SamplingRateNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the sample rate in samples per second. </summary>
    ''' <value> The sampling rate. </value>
    <Description("Gets or sets the sample rate in samples per second."), Category("Data"), DefaultValue(1000.0)>
    Public Property SamplingRate() As Double

        Get
            Return Me._SamplingRateNumeric.Value
        End Get
        Set(ByVal value As Double)
            If Math.Abs(Me.SamplingRate - value) > 0.001 Then
                Me._SamplingRateNumeric.ValueSetter(CDec(value))
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
            If Me.IsOpen Then
                Me._ToolTip.SetToolTip(Me._SamplingRateNumeric,
                                       String.Format(Globalization.CultureInfo.CurrentCulture,
                                                     "Number of samples per second to collect. Board Clock Rate is {0}Hz",
                                                     Me.ClockRate.ToString("{0:0.00}", Globalization.CultureInfo.CurrentCulture)))
            Else
                Me._ToolTip.SetToolTip(Me._SamplingRateNumeric, "Number of samples per second to collect.")
            End If
        End Set
    End Property

#End Region

#Region " SIGNAL BUFFER SIZE "

    ''' <summary> Gets or sets the maximum Signal Buffer Size. </summary>
    ''' <value> The max Signal Buffer Size. </value>
    <Description("The maximum Signal Buffer Size."), Category("Data"), DefaultValue(10000)>
    Public Property MaximumSignalBufferSize As Integer
        Get
            Return CInt(Me._SignalBufferSizeNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._SignalBufferSizeNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Signal Buffer Size. </summary>
    ''' <value> The Min Signal Buffer Size. </value>
    <Description("The minimum Signal Buffer Size."), Category("Data"), DefaultValue(2)>
    Public Property MinimumSignalBufferSize As Integer
        Get
            Return CInt(Me._SignalBufferSizeNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._SignalBufferSizeNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of samples per signal. This is the same as the maximum
    ''' visible samples on the chart. In scope mode, this is the same as the channel buffer size.  In
    ''' strip chart a signal includes the samples for many small sequential buffers that are
    ''' retrieved from the voltage buffer. </summary>
    ''' <value> The size of the signal buffer. </value>
    <Description("Gets or sets the number of samples per signal."), Category("Data"), DefaultValue(1000.0)>
    Public Property SignalBufferSize() As Integer
        Get
            Return CInt(Me._SignalBufferSizeNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.SignalBufferSize Then
                Me._SignalBufferSizeNumeric.ValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " SIGNAL MEMORY LENGTH "

    ''' <summary> Gets or sets the maximum Signal Memory Length. </summary>
    ''' <value> The max Signal Memory Length. </value>
    <Description("The maximum Signal Memory Length."), Category("Data"), DefaultValue(100000000)>
    Public Property MaximumSignalMemoryLength As Integer
        Get
            Return CInt(Me._SignalMemoryLengthNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._SignalMemoryLengthNumeric.MaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Signal Memory Length. </summary>
    ''' <value> The Min Signal Memory Length. </value>
    <Description("The minimum Signal Memory Length."), Category("Data"), DefaultValue(2)>
    Public Property MinimumSignalMemoryLength As Integer
        Get
            Return CInt(Me._SignalMemoryLengthNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._SignalMemoryLengthNumeric.MinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the signal memory length. </summary>
    ''' <value> The length of the signal memory. </value>
    <Description("The signal memory length"), Category("Data"), DefaultValue(10000.0)>
    Public Property SignalMemoryLength() As Integer
        Get
            Return CInt(Me._SignalMemoryLengthNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.SignalMemoryLength Then
                Me._SignalMemoryLengthNumeric.ValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " BUFFERING "

    ''' <summary> Gets or sets the finite buffering mode.  When true, the system collects the specified
    ''' <see cref="BuffersCount">number of buffers</see> and stops with a buffer done event. </summary>
    ''' <value> The finite buffering. </value>
    <Description("Toggles the continuous (infinite) buffering mode."), Category("Appearance"), DefaultValue(False)>
    Public Property FiniteBuffering() As Boolean
        Get
            Return Not Me._ContinuousBufferingCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.FiniteBuffering Then
                Me._ContinuousBufferingCheckBox.Checked = Not value
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

    ''' <summary> Gets the number of samples allocated per USB buffer. Data collection is accomplished
    ''' in USB buffer sizes.  Thus, the buffer done event is invoked only upon completion of the last
    ''' USB buffer required to collect the sample buffer size.  If the sample buffer size is smaller
    ''' than a USB buffer, the board collects as many sample buffers as would fit into the USB buffer
    ''' size and then issues as many buffer done events as buffers collected. </summary>
    ''' <value> The size of the USB buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property UsbBufferSize() As Integer

    ''' <summary> Gets or set the smart buffering option.  With smart buffering a buffer size of
    ''' multiples of the USB buffer size is allocated.  In strip chart smart buffering mode, a
    ''' channel buffer size of at least <see cref="UsbBufferSize">USB buffer size</see>
    ''' is allocated. </summary>
    ''' <value> The using smart buffering. </value>
    <Description("Gets or set the smart buffering option."), Category("Data"), DefaultValue(True)>
    Public Property UsingSmartBuffering() As Boolean
        Get
            Return Me._SmartBufferingCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.UsingSmartBuffering Then
                Me._SmartBufferingCheckBox.Checked = value
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#End Region

#Region " ANALOG INPUT ACQUISITION "

    ''' <summary> The sample counter. </summary>
    Private _SampleCounter As Long

    ''' <summary> Returns the total number of counts since the chart started. </summary>
    ''' <value> The sample counter. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SampleCounter() As Long
        Get
            Return Me._SampleCounter
        End Get
    End Property

#End Region

#Region " CALCULATION CONFIGURATION "

    ''' <summary> Gets or sets the option for calculating the average voltage. </summary>
    ''' <value> The calculate enabled. </value>
    <Description("Toggles the status for calculating averages."), Category("Data"), DefaultValue(False)>
    Public Property CalculateEnabled() As Boolean
        Get
            Return Me._CalculateAverageCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.CalculateEnabled Then
                Me._CalculateAverageCheckBox.Checked = value
            End If
        End Set
    End Property

#End Region

#Region " CALCULATION OPERATION "

    ''' <summary> Gets or sets the calculation period down counts. </summary>
    ''' <value> The calculate period down counts. </value>
    Private Property CalculatePeriodDownCounts As Long

    ''' <summary> Gets or sets the number of samples between calculate events. </summary>
    ''' <value> The number of calculate periods. </value>
    <Description("Gets or sets the number of samples between calculation updates."), Category("Data"), DefaultValue(10.0)>
    Public Property CalculatePeriodCount() As Integer

    ''' <summary> The last averages. </summary>
    Private _LastAverages() As Double

    ''' <summary> Returns the last average saved for each channel. </summary>
    ''' <returns> The last average saved for each channel. </returns>
    Public Function LastAverages() As Double()
        Return Me._LastAverages
    End Function

    ''' <summary> The last voltages. </summary>
    Private _LastVoltages() As Double

    ''' <summary> Returns the last voltages saved for each channel. </summary>
    ''' <returns> Te last voltages saved for each channel.  </returns>
    Public Function LastVoltages() As Double()
        Return Me._LastVoltages
    End Function

#End Region

#Region " CHART CONFIGURATION "

    ''' <summary> Gets or sets the chart axes color. </summary>
    ''' <value> The color of the axes. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property AxesColor() As System.Drawing.Color

        Get
            Return Me.Chart.AxesColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Me.Chart.DisableRendering()
            Me.Chart.AxesColor = value
            Me.Chart.EnableRendering()
            Me.Chart.SignalUpdate()
        End Set
    End Property

    ''' <summary> Gets or sets reference to the chart control. </summary>
    ''' <value> The chart. </value>
    Public ReadOnly Property Chart() As OpenLayers.Controls.Display
        Get
            Return Me._Chart
        End Get
    End Property

    ''' <summary> Gets or sets the <see cref="OpenLayers.Controls.BandMode">band mode</see>.  In a
    ''' single band all channels are displayed on a single graph. In multi bank, each channel is
    ''' allocated its own graph. </summary>
    ''' <value> The is multi-band mode. </value>
    <Description("Gets or sets the chart band mode.  Multi Band means separate chart for each signal"),
    Category("Appearance"), DefaultValue(False)>
    Public Property IsMultibandMode() As Boolean

        Get
            Return Me.Chart.BandMode = OpenLayers.Controls.BandMode.MultiBand
        End Get
        Set(ByVal value As Boolean)
            Me.ChartBandMode = If(value, OpenLayers.Controls.BandMode.MultiBand, OpenLayers.Controls.BandMode.SingleBand)
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="OpenLayers.Controls.BandMode">band mode</see>.  In a
    ''' single band all channels are displayed on a single graph. In multi bank, each channel is
    ''' allocated its own graph.  This property is not exposed directly so as not to require the
    ''' hosting assembly to directly reference the <see cref="OpenLayers.Controls">controls</see>
    ''' assembly. </summary>
    ''' <value> The chart band mode. </value>
    Private Property ChartBandMode() As OpenLayers.Controls.BandMode

        Get
            Return Me.Chart.BandMode
        End Get
        Set(ByVal value As OpenLayers.Controls.BandMode)
            Me.Chart.DisableRendering()
            Me.Chart.BandMode = value
            Me.Chart.EnableRendering()
            Me.Chart.SignalUpdate()
            If Me._SingleRadioButton.Checked Xor (Me.Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand) Then
                Me._SingleRadioButton.SilentCheckedSetter(Me.Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the chart footer. </summary>
    ''' <value> The chart footer. </value>
    <Description("Gets or sets the chart footer."), Category("Appearance"), DefaultValue("")>
    Public Property ChartFooter() As String

        Get
            Return Me._ChartFooterTextBox.Text
        End Get
        Set(ByVal value As String)
            If value <> Me.ChartFooter Then
                Me._ChartFooterTextBox.Text = value
            End If
            If Me.Chart IsNot Nothing Then
                Me.Chart.Footer = value
            End If
            If String.IsNullOrWhiteSpace(value) Then
                Me._ToolTip.SetToolTip(Me._ChartFooterTextBox, "Chart Footer: Empty")
            Else
                Me._ToolTip.SetToolTip(Me._ChartFooterTextBox, value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="ChartModeId">scope or strip chart modes</see>. </summary>
    ''' <value> The chart mode. </value>
    <Description("Gets or sets the chart mode"), Category("Appearance"), DefaultValue(ChartModeId.Scope)>
    Public Property ChartMode() As ChartModeId

        Get
            Return If(Me._ScopeRadioButton.Checked, ChartModeId.Scope, ChartModeId.StripChart)
        End Get
        Set(ByVal value As ChartModeId)
            If value <> Me.ChartMode Then
                Me._ScopeRadioButton.SilentCheckedSetter(value = ChartModeId.Scope)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
            Me.EnableChannelsControls()
            Me.EnableSignalsControls()
        End Set
    End Property

    ''' <summary> Gets or sets the chart title. </summary>
    ''' <value> The chart title. </value>
    <Description("Gets or sets the chart title."), Category("Appearance"), DefaultValue("Analog Input Time Series")>
    Public Property ChartTitle() As String

        Get
            Return Me._ChartTitleTextBox.Text
        End Get
        Set(ByVal value As String)
            If value <> Me.ChartTitle Then
                Me._ChartTitleTextBox.Text = value
            End If
            If Me.Chart IsNot Nothing Then
                Me.Chart.Title = value
            End If
            If String.IsNullOrWhiteSpace(value) Then
                Me._ToolTip.SetToolTip(Me._ChartTitleTextBox, "Chart Title: Empty")
            Else
                Me._ToolTip.SetToolTip(Me._ChartTitleTextBox, value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets or set the charting enabled status. </summary>
    ''' <value> The charting enabled. </value>
    <Description("Toggles the charting of sampled data."), Category("Appearance"), DefaultValue(True)>
    Public Property ChartingEnabled() As Boolean

        Get
            Return Me._EnableChartingCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.ChartingEnabled Then
                Me._EnableChartingCheckBox.Checked = value
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the available signal colors.</summary>
    Private ReadOnly _DefaultSignalColors As System.Drawing.Color() = {System.Drawing.Color.Red, System.Drawing.Color.Blue,
                                                                       System.Drawing.Color.Magenta, System.Drawing.Color.Lime,
                                                                       System.Drawing.Color.Green, System.Drawing.Color.Orange}

    ''' <summary> Gets or sets the default signal color. </summary>
    ''' <value> The default signal color. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DefaultSignalColor(ByVal index As Integer) As System.Drawing.Color
        Get
            index = CInt(Math.Max(0, Math.Min(index, Me._DefaultSignalColors.Length - 1)))
            Return Me._DefaultSignalColors(index)
        End Get
        Set(ByVal value As System.Drawing.Color)
            index = CInt(Math.Max(0, Math.Min(index, Me._DefaultSignalColors.Length - 1)))
            Me._DefaultSignalColors(index) = value
        End Set
    End Property

    ''' <summary> Gets or sets the option for displaying the average voltage reading. </summary>
    ''' <value> The display average enabled. </value>
    <Description("Toggles the average voltage status display."), Category("Appearance"), DefaultValue(False)>
    Public Property DisplayAverageEnabled() As Boolean
        Get
            Return Me._DisplayAverageCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.DisplayAverageEnabled Then
                Me._DisplayAverageCheckBox.Checked = value
            End If
            If value Then
                Me._AverageVoltageToolStripLabel.ToolTipText = If(Me.SelectedSignal Is Nothing,
                    "Average",
                    String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "{0} Average", Me.SelectedSignal.Name))
            Else
                Me._AverageVoltageToolStripLabel.Text = String.Empty
                Me._AverageVoltageToolStripLabel.ToolTipText = "Average: Off"
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the option for displaying the voltage reading. </summary>
    ''' <value> The display voltage enabled. </value>
    <Description("Toggles the voltage reading display."), Category("Appearance"), DefaultValue(False)>
    Public Property DisplayVoltageEnabled() As Boolean
        Get
            Return Me._DisplayVoltageCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._DisplayVoltageCheckBox.Checked Then
                Me._DisplayVoltageCheckBox.Checked = value
            End If
            If value Then
                Me._CurrentVoltageToolStripLabel.ToolTipText = If(Me.SelectedSignal Is Nothing,
                    "Current Voltage",
                    String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "{0} voltage", Me.SelectedSignal.Name))
            Else
                Me._CurrentVoltageToolStripLabel.Text = String.Empty
                Me._CurrentVoltageToolStripLabel.ToolTipText = "Current Voltage: Off"
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the chart grid color. </summary>
    ''' <value> The color of the grid. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property GridColor() As System.Drawing.Color

        Get
            Return Me.Chart.GridColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Me.Chart.DisableRendering()
            Me.Chart.GridColor = value
            Me.Chart.EnableRendering()
            Me.Chart.SignalUpdate()
        End Set
    End Property

    ''' <summary> Set the color of the selected signal. </summary>
    ''' <value> The color of the selected signal. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedSignalColor() As System.Drawing.Color

        Get
            Return If(Me.SelectedSignal IsNot Nothing, Me.Chart.GetCurveColor(Me.SelectedSignalIndex), System.Drawing.Color.Blue)
        End Get
        Set(ByVal value As System.Drawing.Color)
            If Me.SelectedSignalIndex >= 0 Then
                Me.Chart.DisableRendering()

                ' set the color
                Me.Chart.SetCurveColor(Me.SelectedSignalIndex, value)

                ' update the chart
                Me.Chart.SignalListUpdate()

                Me.Chart.EnableRendering()

                ' update the list of signals.
                Me.UpdateSignalList()
            End If
        End Set
    End Property

    ''' <summary> The update period down counter. </summary>
    Private _UpdatePeriodDownCounter As Long

    ''' <summary> Gets or sets the number of samples between update events. </summary>
    ''' <value> The number of update periods. </value>
    <Description("Gets or sets the number of samples between update events."), Category("Data"), DefaultValue(10.0)>
    Public Property UpdatePeriodCount() As Integer

    ''' <summary> Gets or sets the option for calculating the average voltage. </summary>
    ''' <value> The update event enabled. </value>
    <Description("Toggles the status for update events."), Category("Data"), DefaultValue(False)>
    Public Property UpdateEventEnabled() As Boolean

    ''' <summary> The volts readings string format. </summary>
    Private _VoltsReadingsStringFormat As String
    ''' <summary> The volts reading format. </summary>
    Private _VoltsReadingFormat As String

    ''' <summary> Gets or sets the format for displaying the voltage. </summary>
    ''' <value> The volts reading format. </value>
    <Description("Formats the voltage reading display."), Category("Appearance"), DefaultValue("0.000")>
    Public Property VoltsReadingFormat() As String
        Get
            Return Me._VoltsReadingFormat
        End Get
        Set(ByVal value As String)
            Me._VoltsReadingFormat = value
            Me._VoltsReadingsStringFormat = "{0:" & value & " v}"
        End Set
    End Property

#End Region

#Region " CHART DISPLAY "

    ''' <summary> Gets or sets the time axis period in seconds or milliseconds. </summary>
    ''' <value> The time axis period. </value>
    Private Property TimeAxisPeriod As Double

    ''' <summary> Gets or sets the time axis duration in seconds or milliseconds. </summary>
    ''' <value> The time axis duration. </value>
    Private Property TimeAxisDuration As Double

    ''' <summary> Scroll the time frame of the chart. </summary>
    Private Sub ScrollTimeFrame()
        Dim newScrollValue As UInt64 = CULng(Me.SampleCounter - Me.SignalBufferSize)
        If newScrollValue < 0 Then
            newScrollValue = 0
        End If
        Me.ScrollTimeFrame(newScrollValue)
    End Sub

    ''' <summary> Scroll the time frame of the chart. </summary>
    ''' <param name="newScrollValue"> . </param>
    Private Sub ScrollTimeFrame(ByVal newScrollValue As UInt64)

        Dim startTime As Double = newScrollValue * Me.TimeAxisPeriod
        Dim endTime As Double = startTime + Me.TimeAxisDuration

        ' setup range of x-axis
        Me.Chart.XDataCurrentRangeMin = startTime
        Me.Chart.XDataCurrentRangeMax = endTime
        Me.Chart.XDataRangeMin = startTime
        Me.Chart.XDataRangeMax = endTime

    End Sub

#End Region

#Region " VALIDATE AND CONFIGURE "

    ''' <summary> <c>True</c> if analog input configuration required. </summary>
    Private _AnalogInputConfigurationRequired As Boolean

    ''' <summary> Gets or sets a value indicating whether [analog input configuration required].
    ''' Disables start when set True. </summary>
    ''' <value> <c>True</c> if [analog input configuration required]; otherwise, <c>False</c>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property AnalogInputConfigurationRequired As Boolean

        Get
            Return Me._AnalogInputConfigurationRequired OrElse
                   OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous <> Me.AnalogInputState
        End Get
        Private Set(value As Boolean)
            Me._AnalogInputConfigurationRequired = value
            If value Then
                Me.StartStopEnabled = False
            End If
        End Set
    End Property

    ''' <summary> <c>True</c> if chart configuration required. </summary>
    Private _ChartConfigurationRequired As Boolean

    ''' <summary> Gets or sets a value indicating whether [chart configuration required]. Disables
    ''' start when set True. </summary>
    ''' <value> <c>True</c> if [chart configuration required]; otherwise, <c>False</c>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ChartConfigurationRequired As Boolean

        Get
            Return Me._ChartConfigurationRequired
        End Get
        Set(value As Boolean)
            Me._ChartConfigurationRequired = value
            If value Then
                Me.StartStopEnabled = False
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a value indicating whether [configuration required]. </summary>
    ''' <value> <c>True</c> if [configuration required]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property ConfigurationRequired As Boolean

        Get
            Return Me.AnalogInputConfigurationRequired OrElse Me.ChartConfigurationRequired
        End Get
    End Property

    ''' <summary> Validates the configuration. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' validation failed. </param>
    ''' <returns> <c>True</c> if validated; otherwise, <c>False</c>. </returns>
    Public Function ValidateConfiguration(ByRef details As String) As Boolean

        Dim outcomeBuilder As New System.Text.StringBuilder
        If Not Me.IsOpen Then
            outcomeBuilder.Append("Board not initialized (not 'open')")
        ElseIf Not Me.AnalogInput.HasChannels Then
            ' configure only if we have defined channels.
            outcomeBuilder.Append("The list of analog input channels is empty.")
        Else
            If Me.SamplingRate > Me.AnalogInput.Clock.MaxFrequency Then
                Me.Enunciate(Me._SamplingRateNumeric, "Requested sampling rate {0} is larger than the maximum {1}",
                             Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                outcomeBuilder.AppendLine($"Requested sampling rate {Me.SamplingRate} is larger than the maximum {Me.AnalogInput.Clock.MaxFrequency}")

            End If

            If Me.BuffersCount < Me.MinimumBuffersCount Then
                Me.Enunciate(Me._BuffersCountNumeric, "Requested number of channel buffers {0} is lower than the minimum {1}",
                                             Me.BuffersCount, Me.MinimumBuffersCount)
                outcomeBuilder.AppendLine($"Requested number of channel buffers {Me.BuffersCount} is lower than the minimum {Me.MinimumBuffersCount}")
            ElseIf Me.BuffersCount > Me.MaximumBuffersCount Then
                Me.Enunciate(Me._BuffersCountNumeric, "Requested number of channel buffers {0} is greater than the maximum {1}",
                                             Me.BuffersCount, Me.MaximumBuffersCount)
                outcomeBuilder.AppendLine($"Requested number of channel buffers {Me.BuffersCount} is greater than the maximum {Me.MaximumBuffersCount}")
            End If

            If Me.MovingAverageLength < Me.MinimumMovingAverageLength Then
                Me.Enunciate(Me._MovingAverageLengthNumeric, "Requested length of moving average {0} is lower than the minimum {1}",
                                             Me.MovingAverageLength, Me.MinimumMovingAverageLength)
                outcomeBuilder.AppendLine($"Requested length of moving average {Me.MovingAverageLength} is lower than the minimum {Me.MinimumMovingAverageLength}")
            ElseIf Me.SignalBufferSize > 0 AndAlso Me.MovingAverageLength > Me.SignalBufferSize Then
                Me.Enunciate(Me._MovingAverageLengthNumeric, "Requested length of moving average {0} is greater than the signal buffer length {1}",
                                             Me.MovingAverageLength, Me.SignalBufferSize)
                outcomeBuilder.AppendLine($"Requested length of moving average {Me.MovingAverageLength} is greater than the signal buffer length {Me.SignalBufferSize}")
            End If

            If Me.SampleSize < Me.MinimumSampleSize Then
                Me.Enunciate(Me._SampleSizeNumeric, "Requested size of sample buffer {0} is lower than the minimum {1}",
                           Me.SampleSize, Me.MinimumSampleSize)
                outcomeBuilder.AppendLine($"Requested size of sample buffer {Me.SampleSize} is lower than the minimum {Me.MinimumSampleSize}")
            End If

            If Me.SamplingRate > Me.AnalogInput.Clock.MaxFrequency Then
                Me.Enunciate(Me._SamplingRateNumeric, "Requested sampling rate {0} is larger than the minimum {1}",
                              Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                outcomeBuilder.AppendLine($"Requested sampling rate {Me.SamplingRate} is larger than the minimum {Me.AnalogInput.Clock.MaxFrequency}")
            End If

            If Me.SignalBufferSize < Me.MinimumSignalBufferSize Then
                Me.Enunciate(Me._SignalBufferSizeNumeric, "Signal buffer size {0} must be larger than {1}",
                              Me.SignalBufferSize, Me.MinimumSignalBufferSize)
                outcomeBuilder.AppendLine($"Signal buffer size {Me.SignalBufferSize} must be larger than {Me.MinimumSignalBufferSize}")
            End If

            If Me.SignalMemoryLength < Me.SignalBufferSize Then
                Me.Enunciate(Me._SignalMemoryLengthNumeric, "Signal memory length {0} must be larger than signal buffer length {1}",
                              Me.SignalMemoryLength, Me.SignalBufferSize)
                outcomeBuilder.AppendLine($"Signal memory length {Me.SignalMemoryLength} must be larger than signal buffer length {Me.SignalBufferSize}")
            End If

            If Me.RefreshRate > Me.MaximumRefreshRate Then
                Me.Enunciate(Me._RefreshRateNumeric, "Refresh rate {0} exceeds maximum of {1}", Me.RefreshRate, Me.MaximumRefreshRate)
            ElseIf Me.RefreshRate < Me.MinimumRefreshRate Then
                Me.Enunciate(Me._RefreshRateNumeric, "Refresh rate {0} is below the minimum of {1}", Me.RefreshRate, Me.MinimumRefreshRate)
            End If
        End If

        If outcomeBuilder.Length > 0 Then
            details = outcomeBuilder.ToString
            Me._ErrorProvider.SetError(Me._ConfigureButton, details)
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary> Configures analog inputs and signals. Disables start if all clear. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Private Function TryConfigureThis(ByRef details As String) As Boolean

        If Me.ConfigurationRequired Then
            If Not Me.ValidateConfiguration(details) Then
                Me._ErrorProvider.SetError(Me._ConfigureButton, details)
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Validation failed;. Details: {0}", details)
                Return False
            End If
        End If

        Me._ErrorProvider.Clear()

        If Me.AnalogInputConfigurationRequired Then
            If Not Me.TryConfigureAnalogInput(details) Then
                Me._ErrorProvider.SetError(Me._ConfigureButton, details)
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Analog input configuration failed;. Details: {0}", details)
                Return False
            End If
        End If

        If Me.ChartConfigurationRequired Then
            If Not Me.TryConfigureChart(details) Then
                Me._ErrorProvider.SetError(Me._ConfigureButton, details)
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Chart configuration failed;. Details: {0}", details)
                Return False
            End If
        End If
        Return True

    End Function

    ''' <summary> Configures the analog input. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryConfigureAnalogInput(ByRef details As String) As Boolean

        Dim outcomeBuilder As New System.Text.StringBuilder

        If Me.ConfigureSuspended Then
            outcomeBuilder.Append("Configuration suspended")
        ElseIf Not Me.IsOpen Then
            outcomeBuilder.Append("Board not initialized (not 'open')")
        ElseIf Not Me.AnalogInput.HasChannels Then
            ' configure only if we have defined channels.
            outcomeBuilder.Append("The list of analog input channels is empty.")
        End If

        ' turn off the configuration required sentinel.
        Me.AnalogInputConfigurationRequired = False


        ReDim Me._LastAverages(Me.AnalogInput.ChannelList.Count - 1)
        Array.Clear(Me._LastAverages, 0, Me._LastAverages.Length)

        ReDim Me._LastVoltages(Me.AnalogInput.ChannelList.Count - 1)
        Array.Clear(Me._LastAverages, 0, Me._LastAverages.Length)

        Try

            Me.SuspendConfig()

            Dim desiredClockFrequency As Double = Me.SamplingRate
            Me._DecimationRatio = 1

            If Me.SamplingRate > Me.AnalogInput.Clock.MaxFrequency Then
                Me.Enunciate(Me._SamplingRateNumeric, "Requested sampling rate {0} is larger than the maximum {1}.",
                          Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                outcomeBuilder.AppendLine($"Requested sampling rate {Me.SamplingRate} is larger than the maximum {Me.AnalogInput.Clock.MaxFrequency}")
            End If

            ' set channel buffer size to sample size unless we have some special conditions.
            Me._ChannelBufferSize = Me.SampleSize

            ' set the sample buffer size based on the number of channels
            Me._SampleBufferSize = Me._ChannelBufferSize * Me.AnalogInput.ChannelList.Count

            If Me.ChartMode = ChartModeId.StripChart Then

                If Me.UsingSmartBuffering Then

                    ' with smart buffering each sample buffer must be at least 256 points
                    If Me.SampleBufferSize < Me.UsbBufferSize Then

                        ' set channel buffer size to the use buffer at least minimum sampling rate.
                        Me._DecimationRatio = CInt(Math.Max(Math.Ceiling(Me.UsbBufferSize / Me.SampleBufferSize),
                                                            Math.Ceiling(Me.AnalogInput.Clock.MinFrequency / Me.SamplingRate)))

                        ' set the sampling rate to reflect the increase in channel buffer size
                        desiredClockFrequency = Me._DecimationRatio * Me.SamplingRate
                        Me._ChannelBufferSize = CInt(Me._DecimationRatio * Me.SampleSize)

                        If desiredClockFrequency > Me.AnalogInput.Clock.MaxFrequency Then
                            Me.Enunciate(Me._SampleSizeNumeric,
                                      "Decimated sampling rate {0} is larger than the maximum {1}. Either use simple buffering or increase the sample size.",
                                       Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                            outcomeBuilder.AppendLine($"Decimated sampling rate {Me.SamplingRate} is larger than the maximum {Me.AnalogInput.Clock.MaxFrequency}. Either use simple buffering or increase the sample size.")
                        End If
                    Else
                        If desiredClockFrequency < Me.AnalogInput.Clock.MinFrequency Then
                            ' if sampling rate lower than the minimum clock frequency then we need to decimate.
                            Me._DecimationRatio = CInt(Math.Ceiling((Me.AnalogInput.Clock.MinFrequency + 1) / Me.SamplingRate))
                            desiredClockFrequency = Me._DecimationRatio * Me.SamplingRate
                            Me._ChannelBufferSize = CInt(Me._DecimationRatio * Me.SampleSize)
                        End If
                    End If
                End If

                Me.SignalBufferSize = Math.Max(Me.SampleSize, Me.SignalBufferSize)

                ' the signal memory length must be longer than the buffer size.
                Me.SignalMemoryLength = Math.Max(Me.SignalMemoryLength, Me.SignalBufferSize)

            Else

                If Me.SamplingRate < Me.AnalogInput.Clock.MinFrequency Then

                    ' if sampling rate lower than the minimum clock frequency then we need to decimate.
                    Me._DecimationRatio = CInt(Math.Ceiling((Me.AnalogInput.Clock.MinFrequency + 1) / Me.SamplingRate))
                    desiredClockFrequency = Me._DecimationRatio * Me.SamplingRate
                    Me._ChannelBufferSize = CInt(Me._DecimationRatio * Me.SampleSize)

                End If

                ' for scope we force the signal buffer size to equal the sample size.
                Me.SignalBufferSize = Me.SampleSize

                ' the signal memory length is the same as the buffer length.
                Me.SignalMemoryLength = Me.SignalBufferSize

            End If

            ' turn off the configuration required sentinel.
            Me.AnalogInputConfigurationRequired = False

            ' update the sample buffer size based on the number of channels
            Me._SampleBufferSize = Me.ChannelBufferSize * Me.AnalogInput.ChannelList.Count

            ' reallocate other value based on these calculations
            Me.MovingAverageLength = Math.Min(Me.MovingAverageLength, Me.SignalBufferSize)

            ' set the clock frequency
            Me.ClockRate = desiredClockFrequency

            ' configure the analog input
            Me.AnalogInput.Config()

            ' adjust the actual sampling rate based on the A/D clock frequency.
            Me.SamplingRate = Me.ClockRate / Me._DecimationRatio

            ' free then allocate buffers
            Me.AnalogInput.AllocateBuffers(Me.BuffersCount, Me.SampleBufferSize)

            ' Me._bufferPeriod = Me.samplingPeriod * Me.ChannelBufferSize

            ' allocate buffers.
            Me.AnalogInput.AllocateBuffers(Me.BuffersCount, Me.SampleBufferSize)

            ' check if the configuration sentinel was turned on indicating the some values are inconsistent with the user required values
            If Me.AnalogInputConfigurationRequired Then
                outcomeBuilder.AppendLine("Analog input configuration altered user set value and needs to be repeated.")
            End If

            If outcomeBuilder.Length > 0 Then
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Analog Input configuration failed;. Details: {0}", outcomeBuilder.ToString)
            Else
                Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Analog Input Configured;. ")
            End If

        Catch ex As Exception

            outcomeBuilder.AppendLine($"Exception occurred;. {ex.ToFullBlownString}")

        Finally

            Me.ResumeConfig()


        End Try

        If outcomeBuilder.Length > 0 Then
            details = outcomeBuilder.ToString
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary> Configures the analog input. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryConfigureChart(ByRef details As String) As Boolean

        Dim outcomeBuilder As New System.Text.StringBuilder

        If Me.ConfigureSuspended Then

            outcomeBuilder.Append("Configuration suspended")

        ElseIf Me.Chart.Signals.Count = 0 Then

            ' configure only if we have defined channels.
            outcomeBuilder.Append("The chart has no signal defined.")

        Else
            ' turn off the sentinel
            Me._ChartConfigurationRequired = False


            Try

                Me._RefreshTimespan = TimeSpan.FromSeconds(1 / Me.RefreshRate)

                ' stop the scope display.
                Me.Chart.DisableRendering()

                ' suspend configuration
                Me.SuspendConfig()

                ' set the chart duration to display one whole signal.
                Dim chartDuration As Double = Me.SignalBufferSize / Me.SamplingRate

                ' set chart for seconds or milliseconds
                If (chartDuration - 1) < 0.1 Then
                    Me.TimeAxisDuration = chartDuration * 1000
                    Me.TimeAxisPeriod = Me.SamplingPeriod * 1000
                    Me.Chart.XDataUnit = "Milliseconds"
                Else
                    Me.TimeAxisDuration = chartDuration
                    Me.TimeAxisPeriod = Me.SamplingPeriod
                    Me.Chart.XDataUnit = "Seconds"
                End If

                ' allocate signal size for the chart.
                Me.Chart.SignalBufferLength = Me.SignalBufferSize

                ' setup time axis title
                Me.Chart.XDataName = "Time"

                ' setup range of x-axis
                Me.ScrollTimeFrame(0)

                ' check if the configuration sentinel was turned on indicating the some values are inconsistent with the user required values
                If Me.ChartConfigurationRequired Then
                    outcomeBuilder.AppendLine("Chart configuration altered user set value and needs to be repeated.")
                End If

                If outcomeBuilder.Length > 0 Then
                    Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Chart configuration failed;. Details: {0}", outcomeBuilder.ToString)
                Else
                    Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Chart Configured;. ")
                End If

            Catch ex As Exception

                outcomeBuilder.AppendLine($"Exception occurred;. {ex.ToFullBlownString}")

            Finally

                Me.ResumeConfig()

                ' resume the display.
                Me.Chart.EnableRendering()

                ' notify chart that data is available
                Me.Chart.SignalUpdate()

            End Try

        End If

        If outcomeBuilder.Length > 0 Then
            details = outcomeBuilder.ToString
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary> Shows or hides the configuration panel. </summary>
    ''' <value> The configuration panel visible. </value>
    <Description("Shows or hides the configuration panel"), Category("Appearance"), DefaultValue(True)>
    Public Property ConfigurationPanelVisible() As Boolean

        Get
            Return Me._Tabs.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._Tabs.Visible = value
            Me._ConfigureToolStripMenuItem.SilentCheckedSetter(value)
        End Set
    End Property

    ''' <summary> Gets or set the status of configuration suspension. </summary>
    ''' <value> A Queue of configure suspensions. </value>
    Private Property ConfigureSuspensionQueue As Generic.Queue(Of Boolean)

    ''' <summary> Returns true if configuration is suspended. </summary>
    ''' <value> The configure suspended. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ConfigureSuspended() As Boolean
        Get
            Return Me.ConfigureSuspensionQueue.Count > 0
        End Get
    End Property

    ''' <summary> Resumes configuration. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub ResumeConfig()
        If Me.ConfigureSuspended Then
            Me.ConfigureSuspensionQueue.Dequeue()
        Else
            Throw New System.InvalidOperationException("Attempted to resume configuration that was not suspended.")
        End If
    End Sub

    ''' <summary> Suspends configuration to allow setting a few properties without reconfiguring. </summary>
    Public Sub SuspendConfig()
        Me.ConfigureSuspensionQueue.Enqueue(True)
    End Sub

    ''' <summary> Resumes configuration without an exception. </summary>
    ''' <returns> <c>True</c> if resumed, <c>False</c> if not suspended. </returns>
    Public Function TryResumeConfig() As Boolean
        If Me.ConfigureSuspended Then
            Me.ResumeConfig()
            Return True
        Else
            Return False
        End If
    End Function

#End Region

#Region " GUI "

    ''' <summary> Shows or hides the action menu. </summary>
    ''' <value> The action menu visible. </value>
    <Description("Shows or hides the actions menu"), Category("Appearance"), DefaultValue(True)>
    Public Property ActionMenuVisible() As Boolean
        Get
            Return Me._ActionsToolStripDropDownButton.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._ActionsToolStripDropDownButton.Visible = value
        End Set
    End Property

    ''' <summary> Enunciates the specified control. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="format">  The format. </param>
    ''' <param name="args">    The arguments. </param>
    Private Sub Enunciate(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object)
        Me._ErrorProvider.SetError(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

#End Region

#Region " DATA METHODS AND PROPERTIES  "

    ''' <summary> Gets or sets the pointer to the most recent (current) data point acquired.
    ''' For a scope display this is the last data point of the signal.
    ''' For a strip chart this is the last data point that was added to the signal. </summary>
    Private _LoopPointer As Integer

    ''' <summary> Allocates storage for voltages. </summary>
    ''' <param name="channels">         Number of channels. </param>
    ''' <param name="pointsPerChannel"> Number of samples per channel. </param>
    ''' <returns> The Voltages </returns>
    Public Function AllocateVoltages(ByVal channels As Integer, ByVal pointsPerChannel As Integer) As Double()()

        Me._Voltages = New Double(channels - 1)() {}
        For i As Integer = 0 To channels - 1
            Me._Voltages(i) = New Double(pointsPerChannel - 1) {}
        Next i
        Return Me._Voltages

    End Function

    ''' <summary> The voltages. </summary>
    Private _Voltages()() As Double

    ''' <summary> Returns a read only reference to the voltage array. </summary>
    ''' <returns> The Voltages </returns>
    Public Function Voltages() As Double()()
        Return Me._Voltages
    End Function

    ''' <summary> Returns a read only reference to the voltage array. </summary>
    ''' <param name="channelIndex"> Specifies the channel index. </param>
    ''' <returns> The Voltages </returns>
    Public Function Voltages(ByVal channelIndex As Integer) As Double()
        Return Me._Voltages(channelIndex)
    End Function

    ''' <summary> Returns the current voltage of the specifies channel. </summary>
    ''' <param name="channelIndex"> Specifies the channel index. </param>
    ''' <returns> The current voltage of the specifies channel </returns>
    Public Function CurrentVoltage(ByVal channelIndex As Integer) As Double
        Return If(Me._LoopPointer >= 0, Me._Voltages(channelIndex)(Me._LoopPointer), 0)
    End Function

    ''' <summary> Return a voltages array from the specified voltages. </summary>
    ''' <param name="channelIndex"> . </param>
    ''' <param name="pointCount">   Specifies the number of data points to return. </param>
    ''' <returns> Voltages array from the specified voltages. </returns>
    Public Function DataEpoch(ByVal channelIndex As Integer, ByVal pointCount As Integer) As Double()
        Return AnalogInputSubsystem.DataEpoch(Me.Voltages(channelIndex), Me._LoopPointer, pointCount, Me.SampleCounter)
    End Function

    ''' <summary> Return a voltages array from the specified voltages. </summary>
    ''' <param name="dataArray">  . </param>
    ''' <param name="pointCount"> Specifies the number of data points to return. </param>
    ''' <returns> Voltages array from the specified voltages. </returns>
    Public Function DataEpoch(ByVal dataArray() As Double, ByVal pointCount As Integer) As Double()
        If pointCount <= 0 OrElse dataArray Is Nothing OrElse dataArray.Length = 0 Then
            Dim nullData As Double() = Array.Empty(Of Double)()
            Return nullData
        End If
        Return AnalogInputSubsystem.DataEpoch(dataArray, Me._LoopPointer, pointCount, Me.SampleCounter)
    End Function

#End Region

#Region " PROCESS BUFFER METHODS "

    ''' <summary>
    ''' Gets or sets a queue of <see cref="OpenLayers.Base.OlBuffer">Open Layers buffers</see>.
    ''' </summary>
    Private _BufferQueue As System.Collections.Generic.Queue(Of Global.OpenLayers.Base.OlBuffer)

    ''' <summary> Calculate and save the averages. </summary>
    Private Sub CalculateAverages()

        For channelIndex As Integer = 0 To Me.AnalogInput.ChannelsCount - 1

            ' calculate the average of the selected data epoch.
            Me._LastAverages(channelIndex) = AnalogInputSubsystem.Average(Me.DataEpoch(channelIndex, Me.MovingAverageLength))

        Next

    End Sub

    ''' <summary> The chart volts locker. </summary>
    Private ReadOnly _ChartVoltsLocker As New Object

    ''' <summary> Charts the saved voltages. </summary>
    Private Sub ChartVoltageTimeSeries()

        If Me.ChartingEnabled AndAlso Me.SampleCounter > 0 Then

            Try

                Me.Chart.DisableRendering()

                If Me.ChartMode = ChartModeId.Scope Then

                    Dim signalIndex As Integer = 0
                    For Each signal As OpenLayers.Signals.MemorySignal In Me.Chart.Signals
                        signal.Data = Me.Voltages(Me._SignalLogicalChannelNumbers(signalIndex))
                        signalIndex += 1
                    Next

                ElseIf Me.ChartMode = ChartModeId.StripChart Then

                    For signalIndex As Integer = 0 To Me.Chart.Signals.Count - 1
                        Me.Chart.Signals.Item(signalIndex).Data = AnalogInputSubsystem.DataEpoch(
                                                                            Me.Voltages(Me._SignalLogicalChannelNumbers(signalIndex)),
                                                                            Me._LoopPointer, Me.SignalBufferSize, Me.SampleCounter)
                    Next

                    ' This slows down operation big time.  So it is not done. 
                    ' update the time frame
                    ' Me.scrollTimeFrame()

                End If

            Catch

                Throw

            Finally

                Me.Chart.EnableRendering()
                Me.Chart.SignalUpdate()

                ' start count-down to next display time.
                Me._RefreshStopwatch.Restart()

            End Try

        End If

    End Sub

    ''' <summary> The decimation ratio. </summary>
    Private _DecimationRatio As Integer

    ''' <summary> Gets the relative number of samples collected per sample selected. </summary>
    ''' <value> The decimation ratio. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DecimationRatio() As Integer
        Get
            Return Me._DecimationRatio
        End Get
    End Property

    ''' <summary> Display values for the current channel. </summary>
    Private Sub DisplayVoltages()

        If Me.DisplayAverageEnabled AndAlso Me.CalculateEnabled Then
            Me._AverageVoltageToolStripLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                  Me._VoltsReadingsStringFormat, Me._LastAverages(Me.SelectedChannelIndex))
        End If

        If Me.DisplayVoltageEnabled Then
            Me._CurrentVoltageToolStripLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                  Me._VoltsReadingsStringFormat, Me._LastVoltages(Me.SelectedChannelIndex))
        End If

    End Sub

    ''' <summary> Processes the buffer queue. </summary>
    ''' <remarks> Test results for scope display: 5 KHz, 1000 Samples, 5 buffers, 10 points average.
    ''' Storage   CALC           Display        Chart         Total 0.1388ms  off: 0.0156ms  off:
    ''' 0.0134ms  on:  0.045ms  0.2128ms 0.1449ms  off: 0.0162ms  v:   0.8381ms; on:  0.0411ms
    ''' 1.0403ms 0.1435ms  on:  0.0185ms  v:   0.0316ms; on:  0.0463ms 0.2399ms 0.1519ms  on:
    ''' 0.0185ms  v,a: 1.5622ms; on:  0.0441ms 1.7767ms 0.1385ms  on:  0.0187ms  v,a: 1.4083ms; off:
    ''' 0.0159ms 1.5814ms Strip Chart 200 Hz, 10 samples. Storage   CALC           Display
    ''' Chart         Total 0.1209ms  off:0.0165ms   off: 0.0137ms  on:  0.0841ms 0.2352ms 0.0385ms
    ''' 0.0154ms        0.0131ms       5.7326ms 5.7996ms. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ProcessBufferQueue()

        If Me._BufferQueue.Count <= 0 Then
            ' this allows simple repeating without having to worry about 
            ' an empty queue
            Exit Sub
        End If

        Dim buffer As Global.OpenLayers.Base.OlBuffer = Me._BufferQueue.Dequeue

        Try

            If buffer.ValidSamples >= Me.SampleBufferSize Then

                ' save buffered data
                Me.CopyBuffer(buffer)

                If Me.CalculatePeriodDownCounts <= 0 Then
                    ' reset the count
                    Me.CalculatePeriodDownCounts = Me.CalculatePeriodCount
                    ' calculate averages as necessary
                    Me.CalculateAverages()
                End If

                If Me._UpdatePeriodDownCounter <= 0 Then
                    Me._UpdatePeriodDownCounter = Me.UpdatePeriodCount
                    Me.OnUpdatePeriodDone(New EventArgs)
                End If

            Else

                Debug.WriteLine("Missing points=" & buffer.ValidSamples)

            End If

        Catch ex As Global.OpenLayers.Base.OlException

            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed processing buffer",
                                    "Failure occurred on sample {0} loop pointer {1};. {2}",
                                    Me.SampleCounter, Me._LoopPointer, ex.ToFullBlownString)

        Catch ex As Exception

            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed processing buffer",
                                    "Failure occurred on sample {0} loop pointer {1};. {2}",
                                    Me.SampleCounter, Me._LoopPointer, ex.ToFullBlownString)

        Finally

            ' release the buffer to the queue
            If Not Me.FiniteBuffering Then
                ' if infinite buffering, re-queue the buffer so that it can be filled again.
                Me.AnalogInput.BufferQueue.QueueBuffer(buffer)
            End If

        End Try

        If Me._BufferQueue.Count <= 0 Then

            ' if done processing, check if time to update the display
            If Me._RefreshStopwatch.Elapsed > Me._RefreshTimespan Then

                SyncLock Me._ChartVoltsLocker

                    ' display 
                    Me.DisplayVoltages()

                    ' chart if required.
                    Me.ChartVoltageTimeSeries()

                End SyncLock

            End If
        Else
            ' repeat until the queue is processed.
            Me.ProcessBufferQueue()
        End If

    End Sub

    ''' <summary> Copy buffer data to the voltages array. </summary>
    ''' <param name="buffer"> . </param>
    Private Sub CopyBuffer(ByVal buffer As Global.OpenLayers.Base.OlBuffer)

        Dim channelIndex As Integer
        Dim loopPointer As Integer

        If Me.ChartMode = ChartModeId.Scope AndAlso Me.DecimationRatio = 1 Then

            ' if we are saving the entire buffer size, just go ahead.

            ' set the loop pointer to the last data point in the channel buffer.
            loopPointer = Me.ChannelBufferSize - 1

            ' if saving sub buffers, decimate starting with the last loop pointer or zero for scope.
            For channelIndex = 0 To Me.AnalogInput.ChannelsCount - 1

                ' update the voltage buffer.
                Me._Voltages(channelIndex) = buffer.GetDataAsVolts(Me.AnalogInput.ChannelList(channelIndex))

                ' save the last voltage
                Me._LastVoltages(channelIndex) = Me._Voltages(channelIndex)(loopPointer)

            Next

        Else

            Dim initialLoopPoint As Integer = -1
            If Me.ChartMode = ChartModeId.StripChart Then
                initialLoopPoint = Me._LoopPointer
            End If

            ' if saving sub buffers, decimate starting with the last loop pointer or zero for scope.
            For channelIndex = 0 To Me.AnalogInput.ChannelsCount - 1

                ' get the voltages for this channel
                Dim channelVolts() As Double = buffer.GetDataAsVolts(Me.AnalogInput.ChannelList(channelIndex))

                loopPointer = initialLoopPoint
                Dim voltage As Double
                For i As Integer = 0 To channelVolts.Length - 1 Step Me._DecimationRatio
                    voltage = channelVolts(i)
                    loopPointer += 1
                    If loopPointer >= Me.SignalMemoryLength Then
                        loopPointer = 0
                    End If
                    Me._Voltages(channelIndex)(loopPointer) = voltage
                Next

                ' save the last voltage
                Me._LastVoltages(channelIndex) = voltage

            Next

        End If

        ' update loop pointer
        ' Me._loopPointer += Me._channelBufferSize
        Me._LoopPointer = loopPointer

        ' update the sample counters
        Me._SampleCounter += Me.SampleSize

        If Me.CalculateEnabled Then
            Me.CalculatePeriodDownCounts -= Me.SampleSize
        End If
        If Me._UpdateEventEnabled Then
            Me._UpdatePeriodDownCounter -= Me.SampleSize
        End If

    End Sub

#End Region

#Region " PRIVATE GUI METHODS AND PROPERTIES "

    ''' <summary> Updates the enabled status of the add signal control. </summary>
    Private Sub EnableAddSignalControl()
        Me._AddSignalButton.Enabled = Me.IsOpen AndAlso Me._SignalChannelComboBox.SelectedIndex >= 0 AndAlso Not Me.ContinuousOperationActive
    End Sub

    ''' <summary> Updates the enabled status of the channels controls. </summary>
    Private Sub EnableChannelsControls()

        Me._AddChannelButton.Enabled = Me.IsOpen
        Me._ChannelsGroupBox.Enabled = Me.IsOpen
        Me._SamplingGroupBox.Enabled = Me.IsOpen
        Me._RemoveChannelButton.Enabled = Me.SelectedChannel IsNot Nothing AndAlso Not Me.ContinuousOperationActive
        Me._BoardGroupBox.Enabled = Me.IsOpen
        Me._ProcessingGroupBox.Enabled = True
        Me._SmartBufferingCheckBox.Enabled = Me.ChartMode = ChartModeId.StripChart

    End Sub

    ''' <summary> Updates the enabled status of the chart controls. </summary>
    Private Sub EnableChartControls()

        Me._ChartEnableGroupBox.Enabled = True
        Me._ChartTypeGroupBox.Enabled = Not Me.ContinuousOperationActive
        Me._BandModeGroupBox.Enabled = True
        Me._ChartColorsGroupBox.Enabled = True
        Me.StartStopEnabled = Me.IsOpen AndAlso Me.AnalogInput.HasChannels
        Me._ChartTitlesGroupBox.Enabled = True

    End Sub

    ''' <summary> Updates the enabled status of the signals controls. </summary>
    Private Sub EnableSignalsControls()
        Me.EnableAddSignalControl()
        Me._SignalChannelGroupBox.Enabled = Me.IsOpen AndAlso Me.AnalogInput.HasChannels AndAlso Not Me.ContinuousOperationActive
        Me._SignalSamplingGroupBox.Enabled = Not Me.ContinuousOperationActive
        Me._UpdateSignalButton.Enabled = Me.SelectedSignal IsNot Nothing
        Me._ColorSignalButton.Enabled = Me.SelectedSignal IsNot Nothing
        Me._SignalsListView.Enabled = True
        Me._RemoveSignalButton.Enabled = Me.SelectedSignal IsNot Nothing AndAlso Not Me.ContinuousOperationActive
        Me._SignalBufferSizeNumeric.Enabled = Me.ChartMode = ChartModeId.StripChart
        Me._SignalMemoryLengthNumeric.Enabled = Me.ChartMode = ChartModeId.StripChart

    End Sub

#End Region

#Region " EVENTS AND ON EVENTS "

#Region " ACQUISITION STATED "

    ''' <summary>Defines the event for announcing that acquisition started.</summary>
    Public Event AcquisitionStarted As EventHandler(Of TraceMessageEventArgs)

    ''' <summary> Raises the Acquisition Started event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnAcquisitionStarted(ByVal e As TraceMessageEventArgs)
        Dim eventHandler As EventHandler(Of TraceMessageEventArgs) = Me.AcquisitionStartedEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
    End Sub

    ''' <summary> Displays and raises the acquisition started message. </summary>
    ''' <param name="severity">      The severity. </param>
    ''' <param name="id">            The identity. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="detailsArgs">   The details args. </param>
    Private Sub OnDisplayAcquisitionStarted(ByVal severity As TraceEventType, ByVal id As Integer,
                                            ByVal detailsFormat As String, ByVal ParamArray detailsArgs() As Object)
        Dim traceMessage As New TraceMessage(severity, id, detailsFormat, detailsArgs)
        Me.TraceEvent(traceMessage)
        Me.OnAcquisitionStarted(New TraceMessageEventArgs(traceMessage))
    End Sub

    ''' <summary> Starts data acquisition and display. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Failed to start. </exception>
    Private Sub StartContinuousInput()

        If Not Global.OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            ' Me.onHardwareDisconnected(New Global.OpenLayers.Base.GeneralEventArgs)
            Dim eventHandler As EventHandler(Of EventArgs) = Me.DisconnectedEvent
            If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, New EventArgs)
            Throw New InvalidOperationException("Hardware not available.")
        End If

        If Me.ChartMode = ChartModeId.Scope Then
        ElseIf Me.ChartMode = ChartModeId.StripChart Then
            ' Set time of start
            Me.ScrollTimeFrame(0)
        Else
            Throw New InvalidOperationException("Strip chart mode not set.")
        End If

        ' instantiate the buffer queue
        Me._BufferQueue = New System.Collections.Generic.Queue(Of Global.OpenLayers.Base.OlBuffer)

        ' clear the voltage storage
        Me.AllocateVoltages(Me.AnalogInput.ChannelList.Count, Me.SignalMemoryLength)

        ' initialize sample count
        Me._SampleCounter = 0

        ' initialize the update period counter.
        Me._UpdatePeriodDownCounter = Me.UpdatePeriodCount

        ' initialize the calculate period counter.
        Me.CalculatePeriodDownCounts = Me.CalculatePeriodCount

        If Me.ChartMode = ChartModeId.Scope Then
            Me._LoopPointer = 0
        ElseIf Me.ChartMode = ChartModeId.StripChart Then
            Me._LoopPointer = -1
        End If

        ' Disable user interface until the chart is stopped.
        Me.EnableChartControls()
        Me.EnableSignalsControls()
        Me.EnableChannelsControls()
        Me._DeviceNameChooser.Enabled = Not Me.IsOpen

        ' enable selection of signals.
        Me._SignalsListView.Enabled = True

        ' select the channel
        If Me.SelectedSignal Is Nothing Then
            Me.SelectSignal(0)
        End If
        Me.AnalogInput.SelectNamedChannel(Me.SelectedSignal.Name)
        Me.SelectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' Set the status of the start/stop control making sure we can stop
        Me.Started = True

        ' clear the displays.
        Me._CurrentVoltageToolStripLabel.Text = String.Empty
        Me._AverageVoltageToolStripLabel.Text = String.Empty

        ' start data acquisition
        Me.AnalogInput.Start()

        If Me.ChartingEnabled Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Charting...;. ")
        Else
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Running...;. ")
        End If

        ' notify display that new data is available
        Me.Chart.SignalUpdate()

        ' start refresh count-down
        Me._RefreshStopwatch.Restart()

        ' raise the acquisition started event
        Me.OnDisplayAcquisitionStarted(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Acquisition started")

        If Not Me.Started Then
            Throw New isr.Core.OperationFailedException("Failed to start")
        End If

    End Sub

    ''' <summary> Tries to start continuous input. </summary>
    Private Sub TryStartContinuousInput()

        Try

            Me.StartContinuousInput()

        Catch ex As Global.OpenLayers.Base.OlException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Open Layers Driver exception occurred starting;. {0}", ex.ToFullBlownString)

        Catch ex As System.InvalidOperationException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Invalid operation occurred starting;. {0}", ex.ToFullBlownString)

        Catch ex As System.TimeoutException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Timeout exception occurred starting;. {0}", ex.ToFullBlownString)

        Catch

            Throw

        End Try

    End Sub

#End Region

#Region " ACQUISITION STOPPED "

    ''' <summary>Defines the event for announcing that acquisition stopped.</summary>
    Public Event AcquisitionStopped As EventHandler(Of TraceMessageEventArgs)

    ''' <summary> Raises the Acquisition Stopped event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnAcquisitionStopped(ByVal e As TraceMessageEventArgs)

        Dim eventHandler As EventHandler(Of TraceMessageEventArgs) = Me.AcquisitionStoppedEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)

        ' force abort
        Me.TryStopContinuousInput(Me.StopTimeout, True)

    End Sub

    ''' <summary> Displays and raises the acquisition Stopped message. </summary>
    ''' <param name="severity">      The severity. </param>
    ''' <param name="id">            The identity. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="detailsArgs">   The details arguments. </param>
    Private Sub OnDisplayAcquisitionStopped(ByVal severity As TraceEventType, ByVal id As Integer,
                                            ByVal detailsFormat As String, ByVal ParamArray detailsArgs() As Object)
        Dim traceMessage As New TraceMessage(severity, id, detailsFormat, detailsArgs)
        Me.Talker.Publish(traceMessage)
        Me.OnAcquisitionStopped(New TraceMessageEventArgs(traceMessage))
    End Sub

    ''' <summary> Stops or aborts continuous input. This also stops charting. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="abort">   if set to <c>True</c> [abort]. </param>
    ''' <exception cref="TimeoutException"> Abort timeout--system still running after abort. </exception>
    Private Sub StopContinuousInput(ByVal timeout As TimeSpan, ByVal abort As Boolean)

        Try

            If Me.ContinuousOperationActive Then

                ' stop data acquisition if running.
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Aborting data acquisition;. ")
                If abort Then
                    Me.AnalogInput.Abort(timeout)
                Else
                    Me.AnalogInput.Stop(timeout)
                End If

            End If

        Catch

            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed aborting data acquisition;. ")
            Throw

        Finally

            ' make sure we can start
            ' Set the status of the start/stop control making sure we can start
            Me.Started = Me.ContinuousOperationActive

            ' enable GUI
            Me.EnableChartControls()
            Me.EnableSignalsControls()
            Me.EnableChannelsControls()

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = Not Me.IsOpen

        End Try

    End Sub

    ''' <summary> Tries the stop continuous input. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="abort">   if set to <c>True</c> [abort]. </param>
    Private Sub TryStopContinuousInput(ByVal timeout As TimeSpan, ByVal abort As Boolean)

        Try

            Me.StopContinuousInput(timeout, abort)

        Catch ex As Global.OpenLayers.Base.OlException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Open Layers Driver exception occurred stopping;. {0}", ex.ToFullBlownString)

        Catch ex As System.TimeoutException

            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Timeout exception occurred stopping;. {0}", ex.ToFullBlownString)

        Catch

            Throw

        End Try

    End Sub

#End Region

#Region " BOARDS LOCATED "

    ''' <summary>Defines the event handler for locating the boards.</summary>
    Public Event BoardsLocated As EventHandler(Of EventArgs)

    ''' <summary> Raises the Boards Located event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnBoardsLocated(ByVal e As EventArgs)
        Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Data Acquisition board(s) located;. ")
        Dim eventHandler As EventHandler(Of EventArgs) = Me.BoardsLocatedEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
    End Sub

#End Region

#Region " BUFFER DONE "

    ''' <summary>Defines the buffer done event.</summary>
    Public Event BufferDone As EventHandler(Of EventArgs)

#End Region

#Region " DISCONNECTED "

    ''' <summary>Occurs when the subsystem detects that the hardware is no longer
    '''   connected.
    ''' </summary>
    Public Event Disconnected As EventHandler(Of EventArgs)

#End Region

#Region " UPDATE PERIOD DONE "

    ''' <summary>Notifies of new data.</summary>
    Public Event UpdatePeriodDone As EventHandler(Of EventArgs)

    ''' <summary> Raises the update period event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnUpdatePeriodDone(ByVal e As EventArgs)
        Dim eventHandler As EventHandler(Of EventArgs) = Me.UpdatePeriodDoneEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
    End Sub

#End Region

#Region " DRIVER ERROR "

    ''' <summary>Occurs when the driver had a general error.
    ''' </summary>
    Public Event DriverError As EventHandler(Of TraceMessageEventArgs)

    ''' <summary> Raises the driver general error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDriverError(ByVal e As TraceMessageEventArgs)

        ' force stop without raising exceptions.
        Me.TryStopContinuousInput(Me.StopTimeout, True)

        ' invoke the device error event
        Dim eventHandler As EventHandler(Of TraceMessageEventArgs) = Me.DriverErrorEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)

    End Sub

    ''' <summary> Displays and raises the driver error message. </summary>
    ''' <param name="severity">      The severity. </param>
    ''' <param name="id">            The identity. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="detailsArgs">   The details arguments. </param>
    Private Sub OnDisplayDriverError(ByVal severity As TraceEventType, ByVal id As Integer,
                                     ByVal detailsFormat As String, ByVal ParamArray detailsArgs() As Object)
        Dim traceMessage As New TraceMessage(severity, id, detailsFormat, detailsArgs)
        Me.TraceEvent(traceMessage)
        Me.OnDriverError(New TraceMessageEventArgs(traceMessage))
    End Sub

#End Region

#End Region

#Region " OPEN LAYERS HANDLERS "

    ''' <summary> Updates the acquired data and displays signals. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Buffer done event information. </param>
    Private Sub HandleBufferDone(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.BufferDoneEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.BufferDoneHandler(AddressOf Me.HandleBufferDone), New Object() {sender, e})
        Else
            Me.OnBufferDone(e)
        End If

    End Sub

    ''' <summary> The process buffer queue locker. </summary>
    Private ReadOnly _ProcessBufferQueueLocker As New Object

    ''' <summary> Handles the buffer done event from the board driver. </summary>
    ''' <remarks> Test results for scope display: 5 KHz, 1000 Samples, 5 buffers, 10 points average.
    ''' Storage   CALC           Display        Chart         Total 0.1388ms  off: 0.0156ms  off:
    ''' 0.0134ms  on:  0.045ms  0.2128ms 0.1449ms  off: 0.0162ms  v:   0.8381ms; on:  0.0411ms
    ''' 1.0403ms 0.1435ms  on:  0.0185ms  v:   0.0316ms; on:  0.0463ms 0.2399ms 0.1519ms  on:
    ''' 0.0185ms  v,a: 1.5622ms; on:  0.0441ms 1.7767ms 0.1385ms  on:  0.0187ms  v,a: 1.4083ms; off:
    ''' 0.0159ms 1.5814ms. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub OnBufferDone(ByVal e As Global.OpenLayers.Base.BufferDoneEventArgs)

        If e IsNot Nothing AndAlso Me._BufferQueue IsNot Nothing Then
            Try
                If e.OlBuffer Is Nothing Then
                    Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Unexpected null reference to Open Layer buffer occurred @{0};. ",
                                               Reflection.MethodInfo.GetCurrentMethod.Name)
                Else
                    ' Add the buffer to the queue separating the buffer that is queued from the
                    ' one that is processed.
                    Me._BufferQueue.Enqueue(e.OlBuffer)

                    SyncLock Me._ProcessBufferQueueLocker
                        ' process the buffer queue
                        Me.ProcessBufferQueue()
                    End SyncLock
                End If
            Catch ex As Exception
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred handling buffer done;. {0}", ex.ToFullBlownString)
            End Try
        End If

    End Sub

    ''' <summary> Handles the hardware disconnected event. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub HandleDeviceRemoved(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.DeviceRemovedHandler(AddressOf Me.HandleDeviceRemoved),
                      New Object() {sender, e})
        Else
            Me.OnHardwareDisconnected(e)
        End If

    End Sub

    ''' <summary> Close and raise the hardware disconnected event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub OnHardwareDisconnected(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        Dim activity As String = String.Empty
        Try
            Me._BoardDisconnected = True
            activity = $"driver disconnected on subsystem {e.Subsystem} at {e.DateTime:T}"
            Me.DisconnectAnalogInput()
            If e IsNot Nothing Then
                Me.PublishInfo($"{activity};. ")
            End If
        Catch ex As OperationFailedException
            ' ignore operation failure as this is expected at this point.
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Dim eventHandler As EventHandler(Of EventArgs) = Me.DisconnectedEvent
            If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
        End Try

    End Sub

    ''' <summary> Reports driver runtime errors. </summary>
    ''' <remarks> The Driver runtime error occurs when the device driver detects one of the following
    ''' error conditions during runtime: FifoOverflow, FifoUnderflow, DeviceOverClocked, TriggerError,
    ''' or DeviceError. See OpenLayerError for more information. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Driver run time error event information. </param>
    Private Sub HandleDriverRuntimeError(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.DriverRunTimeErrorEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.DriverRunTimeErrorEventHandler(AddressOf Me.HandleDriverRuntimeError),
                      New Object() {sender, e})
        Else
            Me.OnDriverRuntimeError(e)
        End If

    End Sub

    ''' <summary> Displays a message and raises the driver error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDriverRuntimeError(ByVal e As Global.OpenLayers.Base.DriverRunTimeErrorEventArgs)

        If e IsNot Nothing Then
            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Driver Runtime Error occurred",
                                    "Driver Runtime Failure #{0}: {1}. occurred on subsystem {2} at {3:T}",
                                    e.ErrorCode, e.Message, e.Subsystem, e.DateTime)
            Select Case e.ErrorCode
                Case Global.OpenLayers.Base.ErrorCode.AccessDenied, Global.OpenLayers.Base.ErrorCode.CannotOpenDriver,
                    Global.OpenLayers.Base.ErrorCode.DeviceError, Global.OpenLayers.Base.ErrorCode.GeneralFailure
                    Me.TryStopContinuousInput(Me.StopTimeout, True)
                Case Global.OpenLayers.Base.ErrorCode.FifoOverflow, Global.OpenLayers.Base.ErrorCode.FifoUnderflow
                    ' do nothing on bad data?!
                Case Else
                    ' do nothing
            End Select
        End If

    End Sub

    ''' <summary> Handles General Failure events.  Displays a message. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub HandleGeneralFailure(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.GeneralFailureHandler(AddressOf Me.HandleGeneralFailure),
                      New Object() {sender, e})
        Else
            Me.OnGeneralFailure(e)
        End If
    End Sub 'HandleOverrunError

    ''' <summary> Displays a message and raises the driver error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnGeneralFailure(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If e IsNot Nothing Then
            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "General Failure Error occurred",
                                    "General Failure occurred on subsystem {0} at {1:T}.",
                                    e.Subsystem, e.DateTime)
            Me.TryStopContinuousInput(Me.StopTimeout, True)
        End If

    End Sub

    ''' <summary> Handles the Queue Done events.  Displays a message. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub HandleQueueDone(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.QueueDoneHandler(AddressOf Me.HandleQueueDone), New Object() {sender, e})
        Else
            Me.OnQueueDone(e)
        End If
    End Sub 'HandleQueueDone

    ''' <summary> Handles the normal end of the queue. </summary>
    ''' <remarks> Rev. 2.0.2773. Finite buffering Queue tested successfully with suspend and recover. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnQueueDone(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)

        ' check if we have a finite or infinite buffering mode
        If Me.FiniteBuffering Then
            If e IsNot Nothing Then
                Me.OnDisplayAcquisitionStopped(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                              "Finished",
                                                              "Queue done on subsystem {0} at {1:T}",
                                                              e.Subsystem, e.DateTime)
            End If

        Else
            If e IsNot Nothing Then
                ' if this occurred on infinite buffering this represents a buffer overrun.
                Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Buffer overrun",
                                        "Buffer overrun Failure occurred on subsystem {0} at {1:T}. Queue count is {2}.",
                                        Me.AnalogInput, DateTimeOffset.Now(), Me.AnalogInput.BufferQueue.QueuedCount)
            End If

        End If

    End Sub

    ''' <summary> Handles the Queue Stopped events.  Displays a message. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub HandleQueueStopped(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.QueueStoppedHandler(AddressOf Me.HandleQueueStopped), New Object() {sender, e})
        Else
            Me.OnQueueStopped(e)
        End If
    End Sub

    ''' <summary> Handles a stopped queue. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnQueueStopped(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If e IsNot Nothing Then
            Me.OnDisplayAcquisitionStopped(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Queue stopped on subsystem {0} at {1:T};. ",
                                           e.Subsystem, e.DateTime)
        End If
    End Sub

#End Region

#Region " BOARD CONTROLS HANDLERS "

    ''' <summary> Enables the initialization button. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceNameChooser_DeviceSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DeviceNameChooser.DeviceSelected
        Me._OpenCloseCheckBox.Enabled = Me._DeviceNameChooser.DeviceName.Length > 0
    End Sub

    ''' <summary> Handles the CheckedChanged event of the _openCloseCheckBox control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub OpenCloseCheckBox_CheckedChanged(sender As Object, e As System.EventArgs) Handles _OpenCloseCheckBox.CheckedChanged
        Me._OpenCloseCheckBox.Text = If(Me._OpenCloseCheckBox.Checked, "Cl&ose", "&Open")
    End Sub

    ''' <summary> Opens of closes the strip chart. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenCloseCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenCloseCheckBox.Click

        If Me._OpenCloseCheckBox.Enabled Then
            If Me._OpenCloseCheckBox.Checked Then
                Try
                    Me.Open(Me._DeviceNameChooser.DeviceName)
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred opening;. {0}", ex.ToFullBlownString)
                End Try
            Else
                Try
                    Me.Close()
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred closing;. {0}", ex.ToFullBlownString)
                End Try
            End If
        End If

    End Sub

#End Region

#Region " CHART CONTROLS HANDLERS "

    ''' <summary> Event handler. Called by _AbortToolStripMenuItem for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AbortToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AbortToolStripMenuItem.Click
        If Me.IsOpen Then
            Try
                Me.Abort()
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred aborting;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Handles the Validating event of the ChartTitleTextBox control. Updates the chart
    ''' title. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.ComponentModel.CancelEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub ChartTitleTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _ChartTitleTextBox.Validating
        Try
            Me.ChartTitle = Me.ChartTitle
        Catch ex As Global.OpenLayers.Base.OlException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Handles the Validating event of the ChartFooterTextBox control. Updates the chart
    ''' footer. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.ComponentModel.CancelEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub ChartFooterTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _ChartFooterTextBox.Validating
        Try
            Me.ChartFooter = Me.ChartFooter
        Catch ex As Global.OpenLayers.Base.OlException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart footer;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Handles the Click event of the colorAxesButton control. Selects the color. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    Private Sub ColorAxesButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ColorAxesButton.Click
        Try
            Using dialog As New Windows.Forms.ColorDialog
                dialog.Color = Me.AxesColor
                If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Me.AxesColor = dialog.Color
                End If
            End Using
        Catch ex As Global.OpenLayers.Base.OlException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting axis color;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _colorGridsButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ColorGridsButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ColorGridsButton.Click
        Try
            Using dialog As New Windows.Forms.ColorDialog
                dialog.Color = Me.GridColor
                If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Me.GridColor = dialog.Color
                End If
            End Using
        Catch ex As Global.OpenLayers.Base.OlException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting grid color;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _DisplayVoltageCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DisplayVoltageCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DisplayVoltageCheckBox.CheckedChanged
        If Me._DisplayVoltageCheckBox.Enabled Then
            Me.DisplayVoltageEnabled = Me._DisplayVoltageCheckBox.Checked
        End If
    End Sub

    ''' <summary> Event handler. Called by _DisplayAverageCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DisplayAverageCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DisplayAverageCheckBox.CheckedChanged
        If Me._DisplayAverageCheckBox.Enabled Then
            Me.DisplayAverageEnabled = Me._DisplayAverageCheckBox.Checked
        End If
    End Sub

    ''' <summary> Event handler. Called by _EnableChartingCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EnableChartingCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnableChartingCheckBox.CheckedChanged
        If Me._EnableChartingCheckBox.Enabled Then
            Me.ChartingEnabled = Me._EnableChartingCheckBox.Checked
        End If
    End Sub

    ''' <summary> Event handler. Called by _RefreshRateNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RefreshRateNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _RefreshRateNumeric.ValueChanged
        ' request configuration of chart.
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Selects the chart mode as scope or strip chart. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ScopeRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ScopeRadioButton.CheckedChanged
        If Me._ChartTypeGroupBox.Enabled AndAlso Me._ScopeRadioButton.Enabled Then
            Try
                Me.ChartMode = If(Me._ScopeRadioButton.Checked, ChartModeId.Scope, ChartModeId.StripChart)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart mode;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Change BandMode. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SingleRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _SingleRadioButton.CheckedChanged
        If Me._SingleRadioButton.Enabled Then
            Try
                Me.Chart.DisableRendering()
                Me.ChartBandMode = If(Me._SingleRadioButton.Checked, OpenLayers.Controls.BandMode.SingleBand, OpenLayers.Controls.BandMode.MultiBand)
                Me.Chart.EnableRendering()
                Me.Chart.SignalUpdate()
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart band mode;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Handles the CheckedChanged event of the _StartStopToolStripMenuItem control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub StartStopToolStripMenuItem_CheckedChanged(sender As Object, e As System.EventArgs) Handles _StartStopToolStripMenuItem.CheckedChanged
        If Me._StartStopToolStripMenuItem.Checked Then
            Me._StartStopToolStripMenuItem.Text = "&STOP"
            Me._StartStopToolStripMenuItem.ToolTipText = "Stops data collection and chart (if enabled)"
        Else
            Me._StartStopToolStripMenuItem.Text = "&START"
            Me._StartStopToolStripMenuItem.ToolTipText = "Starts data collection and chart (if enabled)"
        End If
        Me._StartStopToolStripMenuItem.Invalidate()
    End Sub 'singleRadioButton_CheckedChanged

    ''' <summary> Starts or stops Running. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StartStopToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _StartStopToolStripMenuItem.Click
        If Me._StartStopToolStripMenuItem.Enabled Then
            If Me.Started Then
                Try
                    Me.Stop()
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred stopping;. {0}", ex.ToFullBlownString)
                End Try
            Else
                Try
                    Me.Start()
                Catch ex As InvalidOperationException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Invalid Operation Exception occurred starting;. {0}", ex.ToFullBlownString)
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Driver Exception occurred starting;. {0}", ex.ToFullBlownString)
                End Try
            End If
        End If
    End Sub

#End Region

#Region " GLOBAL CONTROLS HANDLERS "

    ''' <summary> Event handler. Called by _ConfigureButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConfigureButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConfigureButton.Click
        If Me._ConfigureButton.Enabled Then
            Try
                Dim details As String = String.Empty
                Me.StartStopEnabled = Me.TryConfigureThis(details)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred configuring analog input or chart;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Show or hide the configuration menu. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConfigureToolStripMenuItem1_Checkchanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConfigureToolStripMenuItem.CheckedChanged
        If Me._ConfigureToolStripMenuItem.Enabled Then
            If (Me.ConfigurationPanelVisible = Me._ConfigureToolStripMenuItem.Checked) Then
                Me._ConfigureToolStripMenuItem.Checked = Not Me._ConfigureToolStripMenuItem.Checked
            End If
        End If
        Me.ConfigurationPanelVisible = Me._ConfigureToolStripMenuItem.Checked
    End Sub

    ''' <summary> Event handler. Called by _ResetButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
        If Me._ResetButton.Enabled Then
            Try
                Me.Reset()
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred resetting to opened state values;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Restores default values. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RestoreDefaultsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RestoreDefaultsButton.Click
        Me.RestoreDefaults()
    End Sub

#End Region

#Region " CHANNELS CONTROLS HANDLERS "

    ''' <summary> Adds or updates a channel. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddChannelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AddChannelButton.Click
        If Me._AddChannelButton.Enabled AndAlso Me._ChannelsGroupBox.Enabled AndAlso Me._ChannelRangesComboBox.SelectedIndex >= 0 Then
            Dim physicalChannelNumber As Integer
            If Me.IsOpen AndAlso Integer.TryParse(Me._ChannelComboBox.Text, physicalChannelNumber) Then
                Try
                    Me.AddChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                               "Exception occurred adding channel {0};. {1}", physicalChannelNumber, ex.ToFullBlownString)
                Finally
                    If Me.AnalogInput.HasChannels Then
                        If Me.AnalogInput.PhysicalChannelExists(physicalChannelNumber) Then
                            Me.UpdateChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                        Else
                            Me.AddChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                        End If
                    Else
                        Me.AddChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                    End If
                End Try
            End If
        End If
    End Sub

    ''' <summary> Selects the board input range. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BoardInputRangesComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _BoardInputRangesComboBox.SelectedIndexChanged
        If Me.IsOpen AndAlso Me._BoardInputRangesComboBox.SelectedIndex >= 0 Then
            Try
                Me.AnalogInput.VoltageRange = Me.AnalogInput.SupportedVoltageRanges(Me._BoardInputRangesComboBox.SelectedIndex)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting voltage range;. {0}", ex.ToFullBlownString)
            End Try

            Dim max As Decimal
            Dim min As Decimal
            max = Math.Min(Me._SignalMinNumericUpDown.Maximum, CDec(Me.AnalogInput.VoltageRange.High))
            min = Math.Max(Me._SignalMinNumericUpDown.Minimum, CDec(Me.AnalogInput.VoltageRange.Low))
            Me._SignalMinNumericUpDown.Value = Math.Max(Math.Min(Me._SignalMinNumericUpDown.Value, max), min)
            Me._SignalMinNumericUpDown.Maximum = CDec(Me.AnalogInput.VoltageRange.High)
            Me._SignalMinNumericUpDown.Minimum = CDec(Me.AnalogInput.VoltageRange.Low)

            max = Math.Min(Me._SignalMaxNumericUpDown.Maximum, CDec(Me.AnalogInput.VoltageRange.High))
            min = Math.Max(Me._SignalMaxNumericUpDown.Minimum, CDec(Me.AnalogInput.VoltageRange.Low))
            Me._SignalMaxNumericUpDown.Value = Math.Max(Math.Min(Me._SignalMaxNumericUpDown.Value, max), min)
            Me._SignalMaxNumericUpDown.Maximum = CDec(Me.AnalogInput.VoltageRange.High)
            Me._SignalMaxNumericUpDown.Minimum = CDec(Me.AnalogInput.VoltageRange.Low)

            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

    ''' <summary> Event handler. Called by _BuffersCountNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BuffersCountNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _BuffersCountNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _CalculateAverageCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CalculateAverageCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CalculateAverageCheckBox.CheckedChanged
        If Me._CalculateAverageCheckBox.Enabled Then
            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

    ''' <summary> selects a new channel or an existing channel. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ChannelComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChannelComboBox.SelectedIndexChanged
        If Me._ChannelComboBox.SelectedIndex >= 0 Then
            Dim physicalChannelNumber As Short = Short.Parse(Me._ChannelComboBox.Text, Globalization.CultureInfo.CurrentCulture)
            If Me.IsOpen AndAlso Me.AnalogInput.HasChannels Then
                If Me.AnalogInput.PhysicalChannelExists(physicalChannelNumber) Then
                    Dim channel As Global.OpenLayers.Base.ChannelListEntry = Me.AnalogInput.ChannelList.SelectPhysicalChannel(physicalChannelNumber)
                    Me._ChannelNameTextBox.Text = channel.Name
                    Me._ChannelRangesComboBox.SelectedIndex = Me.AnalogInput.SupportedGainIndex(channel.Gain)
                    Me._AddChannelButton.Text = "Update"
                Else
                    Me._AddChannelButton.Text = "Add"
                    Me._ChannelNameTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                "A in {0}", physicalChannelNumber)
                End If
            Else
                Me._ChannelNameTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "A in {0}", physicalChannelNumber)
            End If
        End If
    End Sub

    ''' <summary> Event handler. Called by _ChannelsListView for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ChannelsListView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ChannelsListView.Click
        If Me.IsOpen AndAlso Me._ChannelsListView.SelectedIndices.Count > 0 Then
            Try
                Me.SelectChannel(Me._ChannelsListView.SelectedIndices.Item(0))
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred selecting channel;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _ContinuousBufferingCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ContinuousBufferingCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ContinuousBufferingCheckBox.CheckedChanged
        If Me._ContinuousBufferingCheckBox.Enabled Then
            Me.FiniteBuffering = Not Me._ContinuousBufferingCheckBox.Checked
            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

    ''' <summary> Event handler. Called by _MovingAverageLengthNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MovingAverageLengthNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _MovingAverageLengthNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Removes the selected channel and signal from the lists. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveChannelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RemoveChannelButton.Click
        If Me.SelectedChannel IsNot Nothing Then
            Try
                Me.RemoveChannel(Me.SelectedChannel)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred removing the selected channel;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _SampleSizeNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SampleSizeNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SampleSizeNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SamplingRateNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SamplingRateNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SamplingRateNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SmartBufferingCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SmartBufferingCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SmartBufferingCheckBox.CheckedChanged
        If Me._SmartBufferingCheckBox.Enabled Then
            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

#End Region

#Region " SIGNALS CONTROLS HANDLERS "

    ''' <summary> Adds a signals. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddSignalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AddSignalButton.Click
        If Me.AnalogInput IsNot Nothing AndAlso Me._AddSignalButton.Enabled AndAlso Me._SignalChannelComboBox.SelectedIndex >= 0 Then
            Try
                Me.AddSignal(Me.AnalogInput.ChannelList(Me._SignalChannelComboBox.SelectedIndex),
                             CDbl(Me._SignalMinNumericUpDown.Value), CDbl(Me._SignalMaxNumericUpDown.Value),
                             Me._DefaultSignalColors(Me.Chart.Signals.Count))
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred adding signal;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Update selected signal color. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ColorSignalButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ColorSignalButton.Click
        If Me.SelectedSignal IsNot Nothing Then
            Try
                Using dialog As New Windows.Forms.ColorDialog
                    dialog.Color = Me.SelectedSignalColor
                    If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Me.SelectedSignalColor = dialog.Color
                    End If
                End Using
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting signal color;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _RemoveSignalButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveSignalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RemoveSignalButton.Click
        Try
            Me.RemoveSignal(Me.SelectedSignal)
        Catch ex As Global.OpenLayers.Base.OlException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred removing signal;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _SignalBufferSizeNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SignalBufferSizeNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SignalBufferSizeNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SignalMemoryLengthNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SignalMemoryLengthNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SignalMemoryLengthNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SignalChannelComboBox for selected value changed
    ''' events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SignalChannelComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalChannelComboBox.SelectedValueChanged
        ' Update the signal ranges based on the channel
        Me.EnableAddSignalControl()
        If Me.IsOpen Then
            Try
                Me.AnalogInput.SelectLogicalChannel(Me._SignalChannelComboBox.SelectedIndex)
                Me._SignalMaxNumericUpDown.Value = Decimal.Parse(Me.AnalogInput.MaxVoltage.ToString(Globalization.CultureInfo.CurrentCulture),
                                                                 Globalization.CultureInfo.CurrentCulture)
                Me._SignalMinNumericUpDown.Value = Decimal.Parse(Me.AnalogInput.MinVoltage.ToString(Globalization.CultureInfo.CurrentCulture),
                                                                 Globalization.CultureInfo.CurrentCulture)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred selecting signal channel;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Removes a signal from the list. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SignalsListView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalsListView.Click
        If Me.Chart.Signals.Count > 0 AndAlso Me._SignalsListView.SelectedIndices.Count > 0 Then
            Try
                Me.SelectSignal(Me._SignalsListView.SelectedIndices.Item(0))
            Catch ex As Global.OpenLayers.Base.OlException
                Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred selecting signal;. {0}", ex.ToFullBlownString)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _SignalMaxNumericUpDown for validating events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub SignalMaxNumericUpDown_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _SignalMaxNumericUpDown.Validating
        e.Cancel = Me._SignalMaxNumericUpDown.Value <= Me._SignalMinNumericUpDown.Value
    End Sub

    ''' <summary> Event handler. Called by _SignalMinNumericUpDown for validating events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub SignalMinNumericUpDown_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _SignalMinNumericUpDown.Validating
        e.Cancel = Me._SignalMaxNumericUpDown.Value <= Me._SignalMinNumericUpDown.Value
    End Sub

    ''' <summary> Updates the selected signal. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UpdateSignalButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UpdateSignalButton.Click
        If Me.SelectedSignal IsNot Nothing Then
            If Me._SignalMinNumericUpDown.Value < Me._SignalMaxNumericUpDown.Value Then
                Try
                    Me.Chart.DisableRendering()
                    If Me.SelectedSignal.RangeMin < Me._SignalMinNumericUpDown.Value AndAlso
                        Me.SelectedSignal.RangeMax > Me._SignalMinNumericUpDown.Value Then
                        Me.SelectedSignal.CurrentRangeMin = CDbl(Me._SignalMinNumericUpDown.Value)
                    End If
                    If Me.SelectedSignal.RangeMin < Me._SignalMaxNumericUpDown.Value AndAlso
                        Me.SelectedSignal.RangeMax > Me._SignalMaxNumericUpDown.Value Then
                        Me.SelectedSignal.CurrentRangeMax = CDbl(Me._SignalMaxNumericUpDown.Value)
                    End If
                    Me.Chart.SignalListUpdate()
                    Me.Chart.EnableRendering()
                    Me.UpdateSignalList()
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred updating selected signal;. {0}", ex.ToFullBlownString)
                End Try
            End If
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies the talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

#End Region

End Class

