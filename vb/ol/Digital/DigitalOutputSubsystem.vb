''' <summary> Adds functionality to the Data Translation Open Layers
''' <see cref="OpenLayers.Base.DigitalInputSubsystem">digital Output subsystem</see>. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 8/7/2013 </para></remarks>
Public Class DigitalOutputSubsystem
    Inherits Global.OpenLayers.Base.DigitalOutputSubsystem

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <param name="device">        A reference to
    ''' <see cref="OpenLayers.Base.Device">an open layers device</see>. </param>
    ''' <param name="elementNumber"> Specifies the subsystem logical element number. </param>
    Public Sub New(ByVal device As OpenLayers.Base.Device, ByVal elementNumber As Integer)

        MyBase.new(device, elementNumber)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.disposed Then

                If disposing Then

                    ' remove handlers
                    Me.HandlesBufferDoneEvents = False

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " SUBSYSTEM "

    ''' <summary> Configures digital output sub-system. </summary>
    ''' <param name="dataFLow"> The data f low. </param>
    Public Sub Configure(ByVal dataFLow As OpenLayers.Base.DataFlow)
        MyBase.DataFlow = dataFLow
        MyBase.Config()
    End Sub

    ''' <summary> Gets or sets a single value on the sub system. </summary>
    ''' <value> The single reading. </value>
    Public Property SingleReading() As Integer
        Get
            Return Me._lastSingleReading
        End Get
        Set(value As Integer)
            Me._lastSingleReading = value
            MyBase.SetSingleValue(value)
        End Set
    End Property

#End Region

#Region " CHANNELS "

    ''' <summary> Add a set of channels. </summary>
    ''' <param name="firstPhysicalChannel"> . </param>
    ''' <param name="lastPhysicalChannel">  . </param>
    ''' <returns> A list of. </returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber)

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    ''' <summary> Add a set of channels. </summary>
    ''' <param name="firstPhysicalChannel"> . </param>
    ''' <param name="lastPhysicalChannel">  . </param>
    ''' <param name="gain">                 . </param>
    ''' <returns> A list of. </returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer,
                                ByVal gain As Double) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber).Gain = gain

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    ''' <summary> The selected channel. </summary>
    Private _SelectedChannel As OpenLayers.Base.ChannelListEntry

    ''' <summary> Gets reference to the selected channel for this subsystem. </summary>
    ''' <value> The selected channel. </value>
    Public ReadOnly Property SelectedChannel() As OpenLayers.Base.ChannelListEntry
        Get
            Return Me._selectedChannel
        End Get
    End Property

    ''' <summary> The selected channel logical number. </summary>
    Private _SelectedChannelLogicalNumber As Integer

    ''' <summary> Gets the logical number (index) of the selected channel. </summary>
    ''' <value> The selected channel logical number. </value>
    Public ReadOnly Property SelectedChannelLogicalNumber() As Integer
        Get
            Return Me._selectedChannelLogicalNumber
        End Get
    End Property

    ''' <summary> Selects a channel. </summary>
    ''' <param name="logicalChannelNumber"> The logical channel number (the channel index in the
    ''' <see cref="OpenLayers.Base.ChannelList">channel list</see>.) </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    Public Function SelectLogicalChannel(ByVal logicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        Me._selectedChannel = MyBase.ChannelList.SelectLogicalChannel(logicalChannelNumber)
        Return Me._selectedChannel
    End Function

    ''' <summary> Selects a channel from the subsystem channel list by its physical channel number. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns> A list of. </returns>
    Public Function SelectPhysicalChannel(ByVal physicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        If Me.PhysicalChannelExists(physicalChannelNumber) Then
            Me._selectedChannelLogicalNumber = MyBase.ChannelList.LogicalChannelNumber(physicalChannelNumber)
            Me._selectedChannel = MyBase.ChannelList.Item(Me._selectedChannelLogicalNumber)
        End If
        Return Me._selectedChannel
    End Function

    ''' <summary> Returns true if the specified channel exists. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns> A list of. </returns>
    Public Function PhysicalChannelExists(ByVal physicalChannelNumber As Integer) As Boolean
        Return MyBase.ChannelList.Contains(physicalChannelNumber)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Aborts the subsystem operations. </summary>
    Public Overrides Sub Abort()

        MyBase.Abort()

        If MyBase.BufferQueue IsNot Nothing Then
            MyBase.BufferQueue.FreeAllQueuedBuffers()
        End If

    End Sub

    ''' <summary> Configures the digital output for single channel output. </summary>
    Public Sub ConfigureSingleOutput()
        Me._selectedChannel = If(MyBase.ChannelList.Count = 0, MyBase.ChannelList.Add(0), MyBase.ChannelList.Item(0))
        Me._selectedChannel.Gain = 1
    End Sub

    ''' <summary> Starts the subsystem operations. </summary>
    Public Overrides Sub Start()
        MyBase.Start()
    End Sub

    ''' <summary> Stops the subsystem operations. </summary>
    Public Overrides Sub [Stop]()
        MyBase.Stop()
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> true to handles buffer done events. </summary>
    Private _HandlesBufferDoneEvents As Boolean

    ''' <summary> Gets or sets or Sets the condition as True have the sub system process buffer done
    ''' events. </summary>
    ''' <value> The handles buffer done events. </value>
    Public Property HandlesBufferDoneEvents() As Boolean
        Get
            Return Me._handlesBufferDoneEvents
        End Get
        Set(ByVal value As Boolean)
            If Me._handlesBufferDoneEvents <> Value Then
                If Value Then
                    AddHandler Me.BufferDoneEvent, AddressOf Me.BufferDoneHandler
                Else
                    RemoveHandler Me.BufferDoneEvent, AddressOf Me.BufferDoneHandler
                End If
                Me._handlesBufferDoneEvents = Value
            End If
        End Set
    End Property

    ''' <summary> Returns true if the subsystem has channels defined. </summary>
    ''' <value> The has channels. </value>
    Public ReadOnly Property HasChannels() As Boolean
        Get
            Return MyBase.ChannelList.Count > 0
        End Get
    End Property

    ''' <summary>Gets or sets the last input or output value set using <see cref="SingleReading"/>.</summary>
    Private _LastSingleReading As Integer

    ''' <summary> Returns the last output reading set using <see cref="SingleReading"/>. </summary>
    ''' <value> The last single reading. </value>
    Public ReadOnly Property LastSingleReading() As Integer
        Get
            Return Me._lastSingleReading
        End Get
    End Property

    ''' <summary> Gets or sets the length of each buffer allocated. </summary>
    ''' <value> The length of the buffer. </value>
    Public Property BufferLength() As Integer

    ''' <summary> true to retrieves readings. </summary>
    Private _RetrievesReadings As Boolean

    ''' <summary> Gets or sets or Sets the condition as True have the system retrieve readings. </summary>
    ''' <value> The retrieves readings. </value>
    Public Property RetrievesReadings() As Boolean
        Get
            Return Me._retrievesReadings
        End Get
        Set(ByVal value As Boolean)
            If Value Then
                Me._handlesBufferDoneEvents = True
            End If
            Me._retrievesReadings = Value
        End Set
    End Property

    ''' <summary> Gets the status message. </summary>
    ''' <remarks> Use this property to get the status message generated by the object. </remarks>
    ''' <value> A <see cref="System.String">String</see>. </value>
    Public ReadOnly Property StatusMessage() As String = String.Empty

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Handles buffer done events. </summary>
    ''' <param name="sender"> Specifies reference to the
    ''' <see cref="OpenLayers.Base.SubsystemBase">subsystem</see> </param>
    ''' <param name="e">      Specifies the
    ''' <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>. </param>
    Private Sub BufferDoneHandler(ByVal sender As Object, ByVal e As OpenLayers.Base.BufferDoneEventArgs)
    End Sub

    ''' <summary> Handle buffer done events. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnBufferDone(ByVal e As OpenLayers.Base.BufferDoneEventArgs)
    End Sub

#End Region

End Class
