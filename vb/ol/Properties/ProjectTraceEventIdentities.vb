﻿Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    Public Enum ProjectTraceEventId
        <Description("None")> None = 0
        <Description("Open Layers")> OpenLayers = isr.Core.ProjectTraceEventId.OpenLayers
        <Description("Open Layers Forms")> OpenLayersForms = ProjectTraceEventId.OpenLayers + &H1
        <Description("Open Layers Display")> OpenLayersDisplay = ProjectTraceEventId.OpenLayers + &H2
        <Description("Open Layers Single IO")> OpenLayersSingleIO = ProjectTraceEventId.OpenLayers + &H3
        <Description("Open Layers Scope")> OpenLayersScope = ProjectTraceEventId.OpenLayers + &H4
        <Description("Open Layers Tester")> OpenLayersTester = ProjectTraceEventId.OpenLayers + &H5
    End Enum

End Namespace

