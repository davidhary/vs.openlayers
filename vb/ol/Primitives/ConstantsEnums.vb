
''' <summary> Enumerates the device families.  This was created to address device features not
''' covered in the API such as the individual gain settings for the channel list. </summary>
Public Enum DeviceFamily
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("DT9800 Series")> DT9800 = 9800
End Enum

''' <summary> Enumerates the chart modes. </summary>
Public Enum ChartModeId
    None
    Scope
    StripChart
End Enum

