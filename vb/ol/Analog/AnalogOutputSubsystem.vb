''' <summary>
''' Adds functionality to the Data Translation Open Layers
''' <see cref="OpenLayers.Base.AnalogOutputSubsystem">Analog Output subsystem</see>.
''' </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Class AnalogOutputSubsystem

    Inherits Global.OpenLayers.Base.AnalogOutputSubsystem

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="device">A reference to 
    '''     <see cref="OpenLayers.Base.Device">an open layers device</see>.</param>
    ''' <param name="elementNumber">Specifies the subsystem logical element number.</param>
    Public Sub New(ByVal device As OpenLayers.Base.Device, ByVal elementNumber As Integer)

        MyBase.New(device, elementNumber)
        Me._LastSingleReadingsChannel = -1
        Me._LastSingleVoltagesChannel = -1
        Me._LastSingleVoltages = New Collections.Generic.Dictionary(Of Integer, Double)
        Me._LastSingleReadings = New Collections.Generic.Dictionary(Of Integer, Integer)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.disposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.HandlesBufferDoneEvents = False

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Aborts the subsystem operations.
    ''' </summary>
    Public Overrides Sub Abort()

        MyBase.Abort()

        If MyBase.BufferQueue IsNot Nothing Then
            MyBase.BufferQueue.FreeAllQueuedBuffers()
        End If

    End Sub

    ''' <summary>
    ''' Starts the subsystem operations.
    ''' </summary>
    Public Overrides Sub Start()

        ' get started.
        MyBase.Start()

    End Sub

    ''' <summary>
    ''' Stops the subsystem operations.
    ''' </summary>
    Public Overrides Sub [Stop]()

        MyBase.Stop()

    End Sub

#End Region

#Region " PROPERTIES "

    Private _HandlesBufferDoneEvents As Boolean
    ''' <summary>
    ''' Gets or Sets the condition as True have the sub system process buffer done events.
    ''' </summary>
    Public Property HandlesBufferDoneEvents() As Boolean
        Get
            Return Me._HandlesBufferDoneEvents
        End Get
        Set(ByVal value As Boolean)
            If Me._HandlesBufferDoneEvents <> Value Then
                If Value Then
                    AddHandler Me.BufferDoneEvent, AddressOf Me.BufferDoneHandler
                Else
                    RemoveHandler Me.BufferDoneEvent, AddressOf Me.BufferDoneHandler
                End If
                Me._HandlesBufferDoneEvents = Value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the subsystem has channels defined.
    ''' </summary>
    Public ReadOnly Property HasChannels() As Boolean
        Get
            Return MyBase.ChannelList.Count > 0
        End Get
    End Property

    ''' <summary> Gets or sets the last single voltages channel. </summary>
    ''' <value> The last single voltages channel. </value>
    Public ReadOnly Property LastSingleVoltagesChannel As Integer

    Private ReadOnly _LastSingleVoltages As Collections.Generic.Dictionary(Of Integer, Double)

    ''' <summary> Last single voltage setter. </summary>
    ''' <remarks> David, 2022-01-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Double. </returns>
    Public Function LastSingleVoltageSetter(ByVal value As Double) As Double
        If Me._LastSingleVoltages.ContainsKey(Me._LastSingleVoltagesChannel) Then
            Me._LastSingleVoltages.Remove(Me._LastSingleVoltagesChannel)
        End If
        Me._LastSingleVoltages.Add(Me._LastSingleVoltagesChannel, value)
        Return value
    End Function

    ''' <summary> Last single Voltage. </summary>
    ''' <returns>The last single Voltage.</returns>>
    Public Function LastSingleVoltage() As Double
        Return Me._LastSingleVoltages(Me._LastSingleVoltagesChannel)
    End Function

    ''' <summary> Last single Voltage. </summary>
    ''' <param name="channel"> Specifies a reference to a
    '''                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <returns>The last single Voltage.</returns>>
    Public Function LastSingleVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return Me._LastSingleVoltages(channel.PhysicalChannelNumber)
    End Function

    ''' <summary> Last single Voltage. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns>The last single Voltage.</returns>>
    Public Function LastSingleVoltage(ByVal physicalChannelNumber As Integer) As Double
        Return Me._LastSingleVoltages(physicalChannelNumber)
    End Function

    ''' <summary> Gets or sets the last single readings channel. </summary>
    ''' <value> The last single readings channel. </value>
    Public ReadOnly Property LastSingleReadingsChannel As Integer

    Private ReadOnly _LastSingleReadings As Collections.Generic.Dictionary(Of Integer, Integer)

    ''' <summary>
    ''' Returns the last output reading set using <see cref="OutputSingleVoltage" />.
    ''' </summary>
    ''' <returns>The last single reading.</returns>>
    Public Function LastSingleReadingSetter(ByVal value As Integer) As Integer
        If Me._LastSingleReadings.ContainsKey(Me._LastSingleReadingsChannel) Then
            Me._LastSingleReadings.Remove(Me._LastSingleReadingsChannel)
        End If
        Me._LastSingleVoltages.Add(Me._LastSingleReadingsChannel, value)
        Return value
    End Function

    ''' <summary> Last single reading. </summary>
    ''' <returns>The last single reading.</returns>>
    Public Function LastSingleReading() As Integer
        Return Me._LastSingleReadings(Me._LastSingleReadingsChannel)
    End Function

    ''' <summary> Last single reading. </summary>
    ''' <param name="channel"> Specifies a reference to a
    '''                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <returns>The last single reading.</returns>>
    Public Function LastSingleReading(ByVal channel As OpenLayers.Base.ChannelListEntry) As Integer
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return Me._LastSingleReadings(channel.PhysicalChannelNumber)
    End Function

    ''' <summary> Last single reading. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns>The last single reading.</returns>>
    Public Function LastSingleReading(ByVal physicalChannelNumber As Integer) As Integer
        Return Me._LastSingleReadings(physicalChannelNumber)
    End Function

    ''' <summary>
    ''' True if has bipolar range.
    ''' </summary>
    ''' <value><c>True</c> if [bipolar range]; otherwise, <c>False</c>.</value>
    Public ReadOnly Property BipolarRange() As Boolean
        Get
            Return MyBase.VoltageRange.Low < 0
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the length of each buffer allocated.
    ''' </summary>
    ''' <value>The length of the buffer.</value>
    Public Property BufferLength() As Integer

    ''' <summary>
    ''' Returns the subsystem maximum voltage.
    ''' </summary>
    ''' <returns>The max voltage.</returns>>
    Public Function MaxVoltage() As Double
        Return Me.MaxVoltage(Me.SelectedChannel)
    End Function

    ''' <summary> Returns the subsystem maximum voltage. </summary>
    ''' <exception cref="System.ArgumentNullException"> channel. </exception>
    ''' <param name="channel"> Specifies a reference to a
    '''                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <returns> The max voltage. </returns>
    Public Function MaxVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return If(channel.Gain = 0, 0, MyBase.VoltageRange.High / channel.Gain)
    End Function

    ''' <summary> Returns the subsystem minimum voltage. </summary>
    ''' <returns> The min voltage. </returns>
    Public Function MinVoltage() As Double
        Return Me.MinVoltage(Me.SelectedChannel)
    End Function

    ''' <summary> Returns the subsystem minimum voltage. </summary>
    ''' <exception cref="System.ArgumentNullException"> channel. </exception>
    ''' <param name="channel"> Specifies a reference to a
    '''                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <returns> The min voltage. </returns>
    Public Function MinVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return If(channel.Gain = 0, 0, MyBase.VoltageRange.Low / channel.Gain)
    End Function

    ''' <summary> Outputs a single reading value to the sub system. </summary>
    ''' <remarks> David, 2022-01-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Public Function OutputSingleReading(ByVal value As Integer) As Integer
        Return Me.OutputSingleReading(Me.SelectedChannel, value)
    End Function

    ''' <summary>
    ''' Outputs a single reading value to the sub system.
    ''' </summary>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Reference to the channel.</param>
    Public Function OutputSingleReading(ByVal channel As OpenLayers.Base.ChannelListEntry, ByVal value As Integer) As Integer
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return Me.OutputSingleReading(channel.PhysicalChannelNumber, value)
    End Function

    ''' <summary>
    ''' Outputs a single reading value to the sub system.
    ''' </summary>
    ''' <param name="physicalChannelNumber">Specifies the physical channel number.</param>
    Public Function OutputSingleReading(ByVal physicalChannelNumber As Integer, ByVal value As Integer) As Integer
        Me._LastSingleReadingsChannel = physicalChannelNumber
        Me.LastSingleReadingSetter(value)
        MyBase.SetSingleValueAsRaw(physicalChannelNumber, value)
        Return Me.LastSingleReading()
    End Function

    ''' <summary> Outputs as single voltage value. </summary>
    ''' <remarks> David, 2022-01-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The output voltage. </returns>
    Public Function OutputSingleVoltage(ByVal value As Double) As Double
        Return Me.OutputSingleVoltage(Me.SelectedChannel, value)
    End Function

    ''' <summary> Outputs as single voltage value. </summary>
    ''' <remarks> David, 2022-01-11. </remarks>
    ''' <param name="channel"> Specifies a reference to a
    '''                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
    ''' <param name="value">   The value. </param>
    ''' <returns> The output voltage. </returns>
    Public Function OutputSingleVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry, ByVal value As Double) As Double
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return Me.OutputSingleVoltage(channel.PhysicalChannelNumber, value)
    End Function

    ''' <summary> Outputs as single voltage value. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <param name="value">                 The value. </param>
    ''' <returns> The output voltage. </returns>
    Public Function OutputSingleVoltage(ByVal physicalChannelNumber As Integer, ByVal value As Double) As Double
        Me._LastSingleVoltagesChannel = physicalChannelNumber
        Me.LastSingleVoltageSetter(value)
        MyBase.SetSingleValueAsVolts(physicalChannelNumber, value)
        Return Me.LastSingleVoltage()
    End Function

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    Public ReadOnly Property StatusMessage() As String = String.Empty

    ''' <summary>
    ''' Returns the analog input voltage resolution.
    ''' </summary>
    ''' <returns>The voltage resolution.</returns>>
    Public Function VoltageResolution() As Double
        Return Me.VoltageResolution(Me.SelectedChannel)
    End Function

    ''' <summary>
    ''' Returns the analog input voltage resolution.
    ''' </summary>
    ''' <returns>The voltage resolution.</returns>>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Specifies a reference to a <see cref="OpenLayers.Base.ChannelListEntry">channel</see>.</param>
    Public Function VoltageResolution(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        If channel Is Nothing Then
            Throw New ArgumentNullException(NameOf(channel))
        End If
        Return (Me.MaxVoltage(channel) - Me.MinVoltage(channel)) / MyBase.Resolution
    End Function

#End Region

#Region " BUFFERS "

    ''' <summary>
    ''' Allocates a buffer for the subsystem.
    ''' </summary>
    ''' <param name="bufferLength">Length of the buffer.</param>
    ''' <returns>OpenLayers.Base.OlBuffer.</returns>
    Public Function AllocateBuffer(ByVal bufferLength As Integer) As OpenLayers.Base.OlBuffer
        Me.BufferLength = bufferLength
        Return New OpenLayers.Base.OlBuffer(bufferLength, Me)
    End Function

    ''' <summary> The buffers. </summary>
    Private _Buffers() As OpenLayers.Base.OlBuffer

    ''' <summary> Select buffer. </summary>
    ''' <param name="bufferIndex"> Zero-based index of the buffer. </param>
    ''' <returns> An OpenLayers.Base.OlBuffer. </returns>
    Public Function SelectBuffer(ByVal bufferIndex As Integer) As OpenLayers.Base.OlBuffer
        Return Me._Buffers(bufferIndex)
    End Function

    ''' <summary> Allocates a set of buffers for data collection. </summary>
    ''' <param name="bufferCount">  The buffer count. </param>
    ''' <param name="bufferLength"> Length of the buffer. </param>
    Public Sub AllocateBuffers(ByVal bufferCount As Integer, ByVal bufferLength As Integer)
        ' save the buffer length
        Me.BufferLength = bufferLength

        ' allocate the buffers array.  This is needed to buffer can be queued when restarting.
        Me._Buffers = New OpenLayers.Base.OlBuffer(bufferCount - 1) {}

        ' allocate queue for data collection
        Dim i As Integer
        Do While i < bufferCount
            Me._Buffers(i) = New isr.IO.OL.Buffer(Me.BufferLength, Me)
            i += 1
        Loop
    End Sub

    ''' <summary> Gets the number of buffers that were allocated. </summary>
    ''' <value> The buffers count. </value>
    Public ReadOnly Property BuffersCount() As Integer
        Get
            Return If(Me._Buffers Is Nothing, 0, Me._Buffers.Length)
        End Get
    End Property

    ''' <summary> Queue all allocated buffers. </summary>
    ''' <returns> An OpenLayers.Base.BufferQueue. </returns>
    Public Function QueueAllBuffers() As OpenLayers.Base.BufferQueue
        MyBase.BufferQueue.FreeAllQueuedBuffers()
        Return Me.QueueAllFreeBuffers()
    End Function

    ''' <summary> Queue all allocated buffers which states are Idle or Completed. </summary>
    Public Function QueueAllFreeBuffers() As OpenLayers.Base.BufferQueue
        For Each buffer As OpenLayers.Base.OlBuffer In Me._Buffers
            If buffer.State = OpenLayers.Base.OlBuffer.BufferState.Idle OrElse
                buffer.State = OpenLayers.Base.OlBuffer.BufferState.Completed Then
                MyBase.BufferQueue.QueueBuffer(buffer)
            End If
        Next
        Return MyBase.BufferQueue
    End Function

    Private ReadOnly _Voltages()() As Double
    ''' <summary>
    ''' Returns a read only reference to the voltage array.
    ''' </summary>
    ''' <returns> An array of <see cref="T:System.Double"/> .</returns>
    Public Function Voltages() As Double()()
        Return Me._Voltages
    End Function

    Private ReadOnly _Readings()() As UInteger
    ''' <summary>Returns a read only reference to the readings array.</summary>
    <CLSCompliant(False)>
    Public Function Readings() As UInteger()()
        Return Me._Readings
    End Function

#End Region

#Region " CHANNELS "

    ''' <summary>
    ''' Add a set of channels.
    ''' </summary>
    ''' <param name="firstPhysicalChannel">The first physical channel.</param>
    ''' <param name="lastPhysicalChannel">The last physical channel.</param>
    ''' <returns>OpenLayers.Base.ChannelList.</returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber)

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    ''' <summary>
    ''' Add a set of channels.
    ''' </summary>
    ''' <param name="firstPhysicalChannel">The first physical channel.</param>
    ''' <param name="lastPhysicalChannel">The last physical channel.</param>
    ''' <param name="gain">The gain.</param>
    ''' <returns>OpenLayers.Base.ChannelList.</returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer,
                                ByVal gain As Double) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber).Gain = gain

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    Private _SelectedChannel As OpenLayers.Base.ChannelListEntry
    ''' <summary>
    ''' Gets or sets reference to the selected channel for this subsystem.
    ''' </summary>
    ''' <value>The selected channel.</value>
    Public ReadOnly Property SelectedChannel() As OpenLayers.Base.ChannelListEntry
        Get
            Return Me._SelectedChannel
        End Get
    End Property

    Private _SelectedChannelLogicalNumber As Integer

    ''' <summary>
    ''' Gets the logical number (index) of the selected channel.
    ''' </summary>
    ''' <value>The selected channel logical number.</value>
    Public ReadOnly Property SelectedChannelLogicalNumber() As Integer
        Get
            Return Me._SelectedChannelLogicalNumber
        End Get
    End Property

    ''' <summary>
    ''' Selects a channel.
    ''' </summary>
    ''' <param name="logicalChannelNumber">The logical channel number (channel index in the <see cref="ChannelList">channel list</see>.)</param>
    ''' <returns>OpenLayers.Base.ChannelListEntry.</returns>
    Public Function SelectLogicalChannel(ByVal logicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        Me._SelectedChannel = MyBase.ChannelList.SelectLogicalChannel(logicalChannelNumber)
        Return Me._SelectedChannel
    End Function

    ''' <summary>
    ''' Selects a channel from the subsystem channel list by its physical channel number.
    ''' </summary>
    ''' <param name="physicalChannelNumber">Specifies the physical channel number.</param>
    ''' <returns>OpenLayers.Base.ChannelListEntry.</returns>
    Public Function SelectPhysicalChannel(ByVal physicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        If Me.PhysicalChannelExists(physicalChannelNumber) Then
            Me._SelectedChannelLogicalNumber = MyBase.ChannelList.LogicalChannelNumber(physicalChannelNumber)
            Me._SelectedChannel = MyBase.ChannelList.Item(Me._SelectedChannelLogicalNumber)
        End If
        Return Me._SelectedChannel
    End Function

    ''' <summary> Queries if a given physical channel exists. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns> <c>True</c> if it succeeds, false if it fails. </returns>
    Public Function PhysicalChannelExists(ByVal physicalChannelNumber As Integer) As Boolean
        Return MyBase.ChannelList.Contains(physicalChannelNumber)
    End Function

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Handles buffer done events.
    ''' </summary>
    ''' <param name="sender">Specifies reference to the 
    ''' <see cref="OpenLayers.Base.SubsystemBase">subsystem</see></param>
    ''' <param name="e">Specifies the 
    ''' <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>.</param>
    Private Sub BufferDoneHandler(ByVal sender As Object, ByVal e As OpenLayers.Base.BufferDoneEventArgs)
        Me.OnBufferDone(e)
    End Sub

    ''' <summary>
    ''' Handle buffer done events.
    ''' </summary>
    ''' <param name="e">The <see cref="OpenLayers.Base.BufferDoneEventArgs" /> instance containing the event data.</param>
    Protected Overridable Sub OnBufferDone(ByVal e As OpenLayers.Base.BufferDoneEventArgs)
    End Sub

#End Region

End Class

